package com.simple.util;

import java.util.ArrayList;
import java.util.List;

import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.OapiProcessinstanceCreateRequest;
import com.dingtalk.api.request.OapiUserGetRequest;
import com.dingtalk.api.response.OapiProcessinstanceCreateResponse;
import com.dingtalk.api.response.OapiUserGetResponse;
import com.simple.common.config.Constant;
import com.simple.ding.DingController;
import com.simple.ding.Env;
import com.simple.util.AccessTokenUtil;
import com.taobao.api.ApiException;

public class ShenPiUtil {
	/**
	 * 打印审批
	 */
	public static String printShenPi(String sqUserId, String tech_fileId,String nb_file_code, String file_name, String edition, String shenPiUserId,String fbFile_type,String cs_user_ids) 
			throws ApiException {

		String accceToken = AccessTokenUtil.getToken();
		DefaultDingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/topapi/processinstance/create");
		OapiProcessinstanceCreateRequest request = new OapiProcessinstanceCreateRequest();
		request.setAgentId(Constant.AGENT_ID);
		request.setProcessCode(Env.DJ_BACK_PROCESSCODE);
		//获取发起人姓名
		String accessToken=DingController.getAccessToken();
		DingTalkClient client_user = new DefaultDingTalkClient("https://oapi.dingtalk.com/user/get");
		OapiUserGetRequest request_user = new OapiUserGetRequest();
		request_user.setUserid(sqUserId);
		request_user.setHttpMethod("GET");
		OapiUserGetResponse  response_user = null;
		try {
			response_user = client_user.execute(request_user, accessToken);
		}catch(ApiException e) {
			e.printStackTrace();
		}
		// 审批流程参数
		List<OapiProcessinstanceCreateRequest.FormComponentValueVo> formComponentValues = new ArrayList<OapiProcessinstanceCreateRequest.FormComponentValueVo>();
		
		// 单行输入框  发起审批人id
		OapiProcessinstanceCreateRequest.FormComponentValueVo vo1 = new OapiProcessinstanceCreateRequest.FormComponentValueVo();
		vo1.setName("发起审批");
		vo1.setValue(response_user.getName());
		// 单行输入框  文件ID
		OapiProcessinstanceCreateRequest.FormComponentValueVo vo2 = new OapiProcessinstanceCreateRequest.FormComponentValueVo();
		vo2.setName("文件ID");
		vo2.setValue(tech_fileId);
		// 单行输入框 内部文件编码
		OapiProcessinstanceCreateRequest.FormComponentValueVo vo3 = new OapiProcessinstanceCreateRequest.FormComponentValueVo();
		vo3.setName("内部文件编码");
		vo3.setValue(nb_file_code);
		// 单行输入框 文件名称
		OapiProcessinstanceCreateRequest.FormComponentValueVo vo4 = new OapiProcessinstanceCreateRequest.FormComponentValueVo();
		vo4.setName("文件名称");
		vo4.setValue(file_name);
		// 单行输入框 版本号
		OapiProcessinstanceCreateRequest.FormComponentValueVo vo5 = new OapiProcessinstanceCreateRequest.FormComponentValueVo();
		vo5.setName("版本号");
		vo5.setValue(edition);
		// 单行输入框 发布文件类型
		OapiProcessinstanceCreateRequest.FormComponentValueVo vo6 = new OapiProcessinstanceCreateRequest.FormComponentValueVo();
		vo6.setName("发布文件类型");
		vo6.setValue(fbFile_type);

		// 添加单行输入框到表单
		formComponentValues.add(vo1);
		formComponentValues.add(vo2);
		formComponentValues.add(vo3);
		formComponentValues.add(vo4);
		formComponentValues.add(vo5);
		formComponentValues.add(vo6);

		request.setFormComponentValues(formComponentValues);
		request.setApprovers(shenPiUserId); // 审批人id
		request.setOriginatorUserId(sqUserId);// 申请人id
		request.setCcList(cs_user_ids);//抄送
		request.setCcPosition("FINISH");

		// 获取所在部门ID
		DingTalkClient client1 = new DefaultDingTalkClient("https://oapi.dingtalk.com/user/get");
		OapiUserGetRequest request1 = new OapiUserGetRequest();
		request1.setUserid(sqUserId);
		request1.setHttpMethod("GET");
		OapiUserGetResponse response1 = client1.execute(request1, accceToken);
		request.setDeptId(response1.getDepartment().get(0));

		OapiProcessinstanceCreateResponse response = client.execute(request, accceToken);

		return response.getProcessInstanceId();

	}
	/*
	 * 项目建号审批
	 */
	public static String proBuildShenPi(String fqUserId, String pro_id,String customerName, String FL_proNo,String shenPiUserId) throws ApiException {

		String accceToken = AccessTokenUtil.getToken();
		DefaultDingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/topapi/processinstance/create");
		OapiProcessinstanceCreateRequest request = new OapiProcessinstanceCreateRequest();
		request.setAgentId(Constant.AGENT_ID);
		request.setProcessCode(Env.proCreate_PROCESSCODE);
		//获取发起人姓名
		String accessToken=DingController.getAccessToken();
		DingTalkClient client_user = new DefaultDingTalkClient("https://oapi.dingtalk.com/user/get");
		OapiUserGetRequest request_user = new OapiUserGetRequest();
		request_user.setUserid(fqUserId);
		request_user.setHttpMethod("GET");
		OapiUserGetResponse  response_user = null;
		try {
			response_user = client_user.execute(request_user, accessToken);
		}catch(ApiException e) {
			e.printStackTrace();
		}
		// 审批流程参数
		List<OapiProcessinstanceCreateRequest.FormComponentValueVo> formComponentValues = new ArrayList<OapiProcessinstanceCreateRequest.FormComponentValueVo>();
		
		// 单行输入框  发起审批人id
		OapiProcessinstanceCreateRequest.FormComponentValueVo vo1 = new OapiProcessinstanceCreateRequest.FormComponentValueVo();
		vo1.setName("发起审批");
		vo1.setValue(response_user.getName());
		// 单行输入框  项目ID
		OapiProcessinstanceCreateRequest.FormComponentValueVo vo2 = new OapiProcessinstanceCreateRequest.FormComponentValueVo();
		vo2.setName("项目ID");
		vo2.setValue(pro_id);
		// 单行输入框 客户名称
		OapiProcessinstanceCreateRequest.FormComponentValueVo vo3 = new OapiProcessinstanceCreateRequest.FormComponentValueVo();
		vo3.setName("客户名称");
		vo3.setValue(customerName);
		// 单行输入框 FL项目号
		OapiProcessinstanceCreateRequest.FormComponentValueVo vo4 = new OapiProcessinstanceCreateRequest.FormComponentValueVo();
		vo4.setName("FL项目号");
		vo4.setValue(FL_proNo);

		// 添加单行输入框到表单
		formComponentValues.add(vo1);
		formComponentValues.add(vo2);
		formComponentValues.add(vo3);
		formComponentValues.add(vo4);

		request.setFormComponentValues(formComponentValues);
		request.setApprovers(shenPiUserId); // 审批人id
		request.setOriginatorUserId(fqUserId);// 申请人id

		// 获取所在部门ID
		DingTalkClient client1 = new DefaultDingTalkClient("https://oapi.dingtalk.com/user/get");
		OapiUserGetRequest request1 = new OapiUserGetRequest();
		request1.setUserid(fqUserId);
		request1.setHttpMethod("GET");
		OapiUserGetResponse response1 = client1.execute(request1, accceToken);
		request.setDeptId(response1.getDepartment().get(0));

		OapiProcessinstanceCreateResponse response = client.execute(request, accceToken);

		return response.getProcessInstanceId();

	}
}
