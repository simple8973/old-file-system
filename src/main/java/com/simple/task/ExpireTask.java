package com.simple.task;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.simple.ding.DingUtil;

public class ExpireTask implements Runnable{
	/**
	 * 临时发布文件到期提醒
	 */
	@Override
	public void run() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
		SimpleDateFormat sdf0 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date d= new Date();
		Calendar nowCalendar=Calendar.getInstance();
		nowCalendar.add(Calendar.DATE, -3);
		String lastThreeDate=sdf0.format(nowCalendar.getTime());
		String date = sdf.format(d);
		//距离到期时间还有一周（生产文件）
		String sql_expire="SELECT a.* FROM tech_files a WHERE a.sp_status=4 and a.release_type=2 and DATE_ADD(a.create_time,INTERVAL ((SELECT b.use_cycle FROM tech_files b WHERE a.id=b.id)-7)  DAY)<NOW()";
		List<Record> taskList=Db.use("dc").find(sql_expire);
		int len=taskList.size();
		if (len!=0) {
			for (int i = 0; i < len; i++) {
				//通知接收者即将作废
				String sql_receive="select * from tech_files_receive where tech_files_id="+taskList.get(i).getStr("id");
				List<Record> ReceiveUser=Db.use("dc").find(sql_receive);
				for (int j = 0; j < ReceiveUser.size(); j++) {
					DingUtil.sendText(ReceiveUser.get(j).getStr("receive_userId"), "到期提醒：临时生产文件： "+taskList.get(i).getStr("file_name")+"，文件号： "+taskList.get(i).getStr("nb_fileCode")+"即将到期，请知悉！"+date);
				}
				//通知发布者即将作废
				DingUtil.sendText(taskList.get(i).getStr("create_user"), "到期提醒：临时生产文件： "+taskList.get(i).getStr("file_name")+"，文件号： "+taskList.get(i).getStr("nb_fileCode")+"即将到期，请及时通知回收！"+date);
			}
		}
		//距离到期时间还有一周（其他四类文件）通知即将作废
		String sql_caigou="SELECT a.* FROM caigou_tech_file a WHERE a.sp_status=4 and a.release_type=2 and DATE_ADD(a.create_time,INTERVAL ((SELECT b.use_cycle FROM caigou_tech_file b WHERE a.id=b.id)-7)  DAY)<NOW()";
		List<Record> taskList_caigou=Db.use("dc").find(sql_caigou);
		if (taskList_caigou.size()!=0) {
			for (int i = 0; i < taskList_caigou.size(); i++) {
				//通知接收者即将作废
				String sql_receive="select * from caigou_tech_files_receive where caigou_tech_files_id="+taskList_caigou.get(i).getStr("id");
				List<Record> ReceiveUser=Db.use("dc").find(sql_receive);
				for (int j = 0; j < ReceiveUser.size(); j++) {
					DingUtil.sendText(ReceiveUser.get(j).getStr("receive_userId"), "到期提醒：临时采购文件： "+taskList_caigou.get(i).getStr("file_name")+"，文件号： "+taskList_caigou.get(i).getStr("nb_fileCode")+"即将到期，请知悉！"+date);
				}
				//通知发布者即将作废
				DingUtil.sendText(taskList_caigou.get(i).getStr("create_user"), "到期提醒：临时采购文件： "+taskList_caigou.get(i).getStr("file_name")+"，文件号： "+taskList_caigou.get(i).getStr("nb_fileCode")+"即将到期，请及时发布新文件！"+date);
			}
		}
		String sql_kehu="SELECT a.* FROM kehu_tech_file a WHERE a.sp_status=4 and a.release_type=2 and DATE_ADD(a.create_time,INTERVAL ((SELECT b.use_cycle FROM kehu_tech_file b WHERE a.id=b.id)-7)  DAY)<NOW()";
		List<Record> taskList_kehu=Db.use("dc").find(sql_kehu);
		if (taskList_kehu.size()!=0) {
			for (int i = 0; i < taskList_kehu.size(); i++) {
				//通知接收者即将作废
				String sql_receive="select * from kehu_tech_files_receive where kehu_tech_files_id="+taskList_kehu.get(i).getStr("id");
				List<Record> ReceiveUser=Db.use("dc").find(sql_receive);
				for (int j = 0; j < ReceiveUser.size(); j++) {
					DingUtil.sendText(ReceiveUser.get(j).getStr("receive_userId"), "到期提醒：临时客户文件： "+taskList_kehu.get(i).getStr("file_name")+"，文件号： "+taskList_kehu.get(i).getStr("nb_fileCode")+"即将到期，请知悉！"+date);
				}
				//通知发布者即将作废
				DingUtil.sendText(taskList_kehu.get(i).getStr("create_user"), "到期提醒：临时客户文件： "+taskList_kehu.get(i).getStr("file_name")+"，文件号： "+taskList_kehu.get(i).getStr("nb_fileCode")+"即将到期，请及时发布新文件！"+date);
			}
		}
		String sql_xiangmu="SELECT a.* FROM xiangmu_tech_file a WHERE a.sp_status=4 and a.release_type=2 and DATE_ADD(a.create_time,INTERVAL ((SELECT b.use_cycle FROM xiangmu_tech_file b WHERE a.id=b.id)-7)  DAY)<NOW()";
		List<Record> taskList_xiangmu=Db.use("dc").find(sql_xiangmu);
		if (taskList_xiangmu.size()!=0) {
			for (int i = 0; i < taskList_xiangmu.size(); i++) {
				//通知接收者即将作废
				String sql_receive="select * from xiangmu_tech_files_receive where receive_userId="+taskList_xiangmu.get(i).getStr("id");
				List<Record> ReceiveUser=Db.use("dc").find(sql_receive);
				for (int j = 0; j < ReceiveUser.size(); j++) {
					DingUtil.sendText(ReceiveUser.get(j).getStr("receive_userId"), "到期提醒：临时项目文件： "+taskList_xiangmu.get(i).getStr("file_name")+"，文件号： "+taskList_xiangmu.get(i).getStr("nb_fileCode")+"即将到期，请知悉！"+date);
				}
				//通知发布者即将作废
				DingUtil.sendText(taskList_xiangmu.get(i).getStr("create_user"), "到期提醒：临时项目文件： "+taskList_xiangmu.get(i).getStr("file_name")+"，文件号： "+taskList_xiangmu.get(i).getStr("nb_fileCode")+"即将到期，请及时发布新文件！"+date);
			}
		}
		String sql_zhiliang="SELECT a.* FROM zhiliang_tech_file a WHERE a.sp_status=4 and a.release_type=2 and DATE_ADD(a.create_time,INTERVAL ((SELECT b.use_cycle FROM zhiliang_tech_file b WHERE a.id=b.id)-7)  DAY)<NOW()";
		List<Record> taskList_zhiliang=Db.use("dc").find(sql_zhiliang);
		if (taskList_zhiliang.size()!=0) {
			for (int i = 0; i < taskList_zhiliang.size(); i++) {
				//通知接收者即将作废
				String sql_receive="select * from zhiliang_tech_files_receive where zhiliang_tech_files_id="+taskList_zhiliang.get(i).getStr("id");
				List<Record> ReceiveUser=Db.use("dc").find(sql_receive);
				for (int j = 0; j < ReceiveUser.size(); j++) {
					DingUtil.sendText(ReceiveUser.get(j).getStr("receive_userId"), "到期提醒：临时质量文件： "+taskList_zhiliang.get(i).getStr("file_name")+"，文件号： "+taskList_zhiliang.get(i).getStr("nb_fileCode")+"即将到期，请知悉！"+date);
				}
				//通知发布者即将作废
				DingUtil.sendText(taskList_zhiliang.get(i).getStr("create_user"), "到期提醒：临时质量文件： "+taskList_zhiliang.get(i).getStr("file_name")+"，文件号： "+taskList_zhiliang.get(i).getStr("nb_fileCode")+"即将到期，请及时发布新文件！"+date);
			}
		}
		
		//距离到期时间还有一周（其他四类文件）自动作废并通知使用人禁止使用
		String sql_caigou1="SELECT a.* FROM caigou_tech_file a WHERE a.sp_status=4 and a.release_type=2 and DATE_ADD(a.create_time,INTERVAL (SELECT b.use_cycle FROM caigou_tech_file b WHERE a.id=b.id) DAY)<=NOW()";
		List<Record> taskList_caigou1=Db.use("dc").find(sql_caigou1);
		if (taskList_caigou1.size()!=0) {
			for (int i = 0; i < taskList_caigou1.size(); i++) {
				//文件状态变为作废
				taskList_caigou1.get(i).set("sp_status", 8);
				Db.use("dc").update("caigou_tech_file", taskList_caigou1.get(i));
				//通知接收者即将作废
				String sql_receive="select * from caigou_tech_files_receive where caigou_tech_files_id="+taskList_caigou1.get(i).getStr("id");
				List<Record> ReceiveUser=Db.use("dc").find(sql_receive);
				for (int j = 0; j < ReceiveUser.size(); j++) {
					DingUtil.sendText(ReceiveUser.get(j).getStr("receive_userId"), "到期提醒：临时采购文件： "+taskList_caigou1.get(i).getStr("file_name")+"，文件号： "+taskList_caigou1.get(i).getStr("nb_fileCode")+"已到期，禁止使用，请知悉！"+date);
				}
				//通知发布者即将作废
				DingUtil.sendText(taskList_caigou1.get(i).getStr("create_user"), "到期提醒：临时采购文件： "+taskList_caigou1.get(i).getStr("file_name")+"，文件号： "+taskList_caigou1.get(i).getStr("nb_fileCode")+"已到期，自动作废！"+date);
			}
		}
		String sql_kehu1="SELECT a.* FROM kehu_tech_file a WHERE a.sp_status=4 and a.release_type=2 and DATE_ADD(a.create_time,INTERVAL (SELECT b.use_cycle FROM kehu_tech_file b WHERE a.id=b.id)  DAY)<NOW()";
		List<Record> taskList_kehu1=Db.use("dc").find(sql_kehu1);
		if (taskList_kehu1.size()!=0) {
			for (int i = 0; i < taskList_kehu1.size(); i++) {
				//文件状态变为作废
				taskList_kehu1.get(i).set("sp_status", 8);
				Db.use("dc").update("kehu_tech_file", taskList_kehu1.get(i));
				//通知接收者即将作废
				String sql_receive="select * from kehu_tech_files_receive where kehu_tech_files_id="+taskList_kehu1.get(i).getStr("id");
				List<Record> ReceiveUser=Db.use("dc").find(sql_receive);
				for (int j = 0; j < ReceiveUser.size(); j++) {
					DingUtil.sendText(ReceiveUser.get(j).getStr("receive_userId"), "到期提醒：临时客户文件： "+taskList_kehu1.get(i).getStr("file_name")+"，文件号： "+taskList_kehu1.get(i).getStr("nb_fileCode")+"已到期，禁止使用，请知悉！"+date);
				}
				//通知发布者即将作废
				DingUtil.sendText(taskList_kehu1.get(i).getStr("create_user"), "到期提醒：临时客户文件： "+taskList_kehu1.get(i).getStr("file_name")+"，文件号： "+taskList_kehu1.get(i).getStr("nb_fileCode")+"已到期，自动作废！"+date);
			}
		}
		String sql_xiangmu1="SELECT a.* FROM xiangmu_tech_file a WHERE a.sp_status=4 and a.release_type=2 and DATE_ADD(a.create_time,INTERVAL ((SELECT b.use_cycle FROM xiangmu_tech_file b WHERE a.id=b.id)-7)  DAY)<NOW()";
		List<Record> taskList_xiangmu1=Db.use("dc").find(sql_xiangmu1);
		if (taskList_xiangmu1.size()!=0) {
			for (int i = 0; i < taskList_xiangmu1.size(); i++) {
				//文件状态变为作废
				taskList_xiangmu1.get(i).set("sp_status", 8);
				Db.use("dc").update("xiangmu_tech_file", taskList_xiangmu1.get(i));
				//通知接收者即将作废
				String sql_receive="select * from xiangmu_tech_files_receive where receive_userId="+taskList_xiangmu1.get(i).getStr("id");
				List<Record> ReceiveUser=Db.use("dc").find(sql_receive);
				for (int j = 0; j < ReceiveUser.size(); j++) {
					DingUtil.sendText(ReceiveUser.get(j).getStr("receive_userId"), "到期提醒：临时项目文件： "+taskList_xiangmu1.get(i).getStr("file_name")+"，文件号： "+taskList_xiangmu1.get(i).getStr("nb_fileCode")+"已到期，禁止使用，请知悉！"+date);
				}
				//通知发布者即将作废
				DingUtil.sendText(taskList_xiangmu1.get(i).getStr("create_user"), "到期提醒：临时项目文件： "+taskList_xiangmu1.get(i).getStr("file_name")+"，文件号： "+taskList_xiangmu1.get(i).getStr("nb_fileCode")+"已到期，自动作废！"+date);
			}
		}
		String sql_zhiliang1="SELECT a.* FROM zhiliang_tech_file a WHERE a.sp_status=4 and a.release_type=2 and DATE_ADD(a.create_time,INTERVAL ((SELECT b.use_cycle FROM zhiliang_tech_file b WHERE a.id=b.id)-7)  DAY)<NOW()";
		List<Record> taskList_zhiliang1=Db.use("dc").find(sql_zhiliang1);
		if (taskList_zhiliang1.size()!=0) {
			for (int i = 0; i < taskList_zhiliang1.size(); i++) {
				//文件状态变为作废
				taskList_zhiliang1.get(i).set("sp_status", 8);
				Db.use("dc").update("zhiliang_tech_file", taskList_zhiliang1.get(i));
				//通知接收者即将作废
				String sql_receive="select * from zhiliang_tech_files_receive where zhiliang_tech_files_id="+taskList_zhiliang1.get(i).getStr("id");
				List<Record> ReceiveUser=Db.use("dc").find(sql_receive);
				for (int j = 0; j < ReceiveUser.size(); j++) {
					DingUtil.sendText(ReceiveUser.get(j).getStr("receive_userId"), "到期提醒：临时质量文件： "+taskList_zhiliang1.get(i).getStr("file_name")+"，文件号： "+taskList_zhiliang1.get(i).getStr("nb_fileCode")+"已到期，禁止使用，请知悉！"+date);
				}
				//通知发布者即将作废
				DingUtil.sendText(taskList_zhiliang1.get(i).getStr("create_user"), "到期提醒：临时质量文件： "+taskList_zhiliang1.get(i).getStr("file_name")+"，文件号： "+taskList_zhiliang1.get(i).getStr("nb_fileCode")+"已到期，自动作废！"+date);
			}
		}
		//所有文件未领取超过两天执行提醒任务
		//采购文件
		List<Record> caigouList=Db.use("dc").find("SELECT b.receive_userId,COUNT(*) AS total_num FROM caigou_tech_file a,caigou_tech_files_receive b WHERE a.sp_status=3 and a.create_time<='"+lastThreeDate+"' AND a.id=b.caigou_tech_files_id AND b.receive=0 GROUP BY b.receive_userId");
		for (int i = 0; i < caigouList.size(); i++) {
			DingUtil.sendText(caigouList.get(i).getStr("receive_userId"), "采购文件领取通知：您有"+caigouList.get(i).getStr("total_num")+"份采购文件已超过3天未领取，请及时领取！"+date);
		}
		//客户文件
		List<Record> kehuList=Db.use("dc").find("SELECT b.receive_userId,COUNT(*) AS total_num FROM kehu_tech_file a,kehu_tech_files_receive b WHERE a.sp_status=3 and a.create_time<='"+lastThreeDate+"' AND a.id=b.kehu_tech_files_id AND b.receive=0 GROUP BY b.receive_userId");
		for (int i = 0; i < kehuList.size(); i++) {
			DingUtil.sendText(kehuList.get(i).getStr("receive_userId"), "客户文件领取通知：您有"+kehuList.get(i).getStr("total_num")+"份客户文件已超过3天未领取，请及时领取！"+date);
		}
		//生产文件
		List<Record> shengchanList=Db.use("dc").find("SELECT b.receive_userId,COUNT(*) AS total_num FROM tech_files a,tech_files_receive b WHERE a.sp_status=3 and a.create_time<='"+lastThreeDate+"' AND a.id=b.tech_files_id AND b.receive=0 GROUP BY b.receive_userId");
		for (int i = 0; i < shengchanList.size(); i++) {
			DingUtil.sendText(shengchanList.get(i).getStr("receive_userId"), "生产文件领取通知：您有"+shengchanList.get(i).getStr("total_num")+"份生产文件已超过3天未领取，请及时领取！"+date);
		}
		//质量文件
		List<Record> zhiliangList=Db.use("dc").find("SELECT b.receive_userId,COUNT(*) AS total_num FROM zhiliang_tech_file a,zhiliang_tech_files_receive b WHERE a.sp_status=3 and a.create_time<='"+lastThreeDate+"' AND a.id=b.zhiliang_tech_files_id AND b.receive=0 GROUP BY b.receive_userId");
		for (int i = 0; i < zhiliangList.size(); i++) {
			DingUtil.sendText(zhiliangList.get(i).getStr("receive_userId"), "质量文件领取通知：您有"+zhiliangList.get(i).getStr("total_num")+"份质量文件已超过3天未领取，请及时领取！"+date);
		}
		//项目文件
		List<Record> xiangmuList=Db.use("dc").find("SELECT b.receive_userId,COUNT(*) AS total_num FROM xiangmu_tech_file a,xiangmu_tech_files_receive b WHERE a.sp_status=3 and a.create_time<='"+lastThreeDate+"' AND a.id=b.xiangmu_tech_files_id AND b.receive=0 GROUP BY b.receive_userId");
		for (int i = 0; i < xiangmuList.size(); i++) {
			DingUtil.sendText(xiangmuList.get(i).getStr("receive_userId"), "项目文件领取通知：您有"+xiangmuList.get(i).getStr("total_num")+"份项目文件已超过3天未领取，请及时领取！"+date);
		}
		
		
	}

}
