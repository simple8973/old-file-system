package com.simple.excelModel;

import com.github.liaochong.myexcel.core.annotation.ExcelColumn;

public class ProExport {
	@ExcelColumn(order = 0, title = "客户名称")
    private String customer_name;
	public String getCustomer_name() {
		return customer_name;
	}
	public void setCustomer_name(String customer_name) {
		this.customer_name=customer_name;
	}
	@ExcelColumn(order = 1, title = "客户项目名称")
    private String customer_pro_name;
	public String getCustomer_pro_name() {
		return customer_pro_name;
	}
	public void setCustomer_pro_name(String customer_pro_name) {
		this.customer_pro_name=customer_pro_name;
	}
	@ExcelColumn(order = 2, title = "FL项目号")
    private String FL_pro_no;
	public String getFL_pro_no() {
		return FL_pro_no;
	}
	public void setFL_pro_no(String FL_pro_no) {
		this.FL_pro_no=FL_pro_no;
	}
	@ExcelColumn(order = 3, title = "顾客代码")
    private String customer_code;
	public String getCustomer_code() {
		return customer_code;
	}
	public void setCustomer_code(String customer_code) {
		this.customer_code=customer_code;
	}
	@ExcelColumn(order = 4, title = "客户机型")
    private String model;
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model=model;
	}
	@ExcelColumn(order = 5, title = "事业部门")
    private String depart_name;
	public String getDepart_name() {
		return depart_name;
	}
	public void setDepart_name(String depart_name) {
		this.depart_name=depart_name;
	}
	@ExcelColumn(order = 6, title = "产品类别")
    private String module_name;
	public String getModule_name() {
		return module_name;
	}
	public void setModule_name(String module_name) {
		this.module_name=module_name;
	}
	@ExcelColumn(order = 7, title = "产品名称")
    private String category_name;
	public String getCategory_name() {
		return category_name;
	}
	public void setCategory_name(String category_name) {
		this.category_name=category_name;
	}
	@ExcelColumn(order = 8, title = "创建人")
    private String create_user_name;
	public String getCreate_user_name() {
		return create_user_name;
	}
	public void setCreate_user_name(String create_user_name) {
		this.create_user_name=create_user_name;
	}
	@ExcelColumn(order = 9, title = "内部顾客号")
    private String cost_code;
	public String getCost_code() {
		return cost_code;
	}
	public void setCost_code(String cost_code) {
		this.cost_code=cost_code;
	}
	@ExcelColumn(order = 10, title = "核算主体")
    private String cost_center;
	public String getCost_center() {
		return cost_center;
	}
	public void setCost_center(String cost_center) {
		this.cost_center=cost_center;
	}
}
