package com.simple.ding;

import com.simple.common.controller.BaseController;
import com.taobao.api.ApiException;

import static com.simple.common.config.URLConstant.URL_GET_TOKEN;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpServletRequest;

import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.request.OapiGetJsapiTicketRequest;
import com.dingtalk.api.request.OapiGettokenRequest;
import com.dingtalk.api.response.OapiGetJsapiTicketResponse;
import com.dingtalk.api.response.OapiGettokenResponse;
import com.jfinal.plugin.activerecord.Record;
import com.simple.ding.Env;
import com.simple.ding.DingTalkApiSignature;
import com.simple.ding.OApiException;

/**
 * 钉钉微应用JSAPI鉴权处理类
 * @author simple
 *
 */
public class DingController extends BaseController{
	//获取access_token
	public static String getAccessToken() {
		DefaultDingTalkClient client =new DefaultDingTalkClient(URL_GET_TOKEN);
		OapiGettokenRequest request=new OapiGettokenRequest();
		request.setAppkey(Env.APP_KEY);
		request.setAppsecret(Env.APP_SECRET);
		request.setHttpMethod("GET");
		OapiGettokenResponse response=null;
		try {
			response=client.execute(request);
			return response.getAccessToken();
		}catch(ApiException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static String getJsapiTicket(String accessToken) {
		DefaultDingTalkClient client=new DefaultDingTalkClient("https://oapi.dingtalk.com/get_jsapi_ticket");
		OapiGetJsapiTicketRequest req=new OapiGetJsapiTicketRequest();
		req.setTopHttpMethod("GET");
		try {
			OapiGetJsapiTicketResponse execute=client.execute(req,accessToken);
			return execute.getTicket();
		}catch(ApiException e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	
	public static String sign(String ticket, String nonceStr, long timeStamp, String url)throws OApiException{
		try {
			return DingTalkApiSignature.getJsApiSingnature(url, nonceStr, timeStamp, ticket);
		}catch(Exception ex) {
			throw new OApiException(0, ex.getMessage());
		}
	}
	
	public static String sign1(String ticket,String nonceStr,long timeStamp,String url) throws OApiException{
		String plain="jsapi_ticket="+ticket+"&noncestr="+nonceStr+"&timestamp="+String.valueOf(timeStamp)+"&url="+url;
		try {
			MessageDigest sha1=MessageDigest.getInstance("SHA-1");
			sha1.reset();
			sha1.update(plain.getBytes("UTF-8"));
			return sha1.digest().toString();
		}catch(NoSuchAlgorithmException e) {
			throw new OApiException(0, e.getMessage());
		}catch(UnsupportedEncodingException e) {
			throw new OApiException(0, e.getMessage());
		}
	}
	
	public static String getConfig(HttpServletRequest request) {
		String urlString =request.getRequestURL().toString();
		String nonceStr = "abcdefgqweqweqwe";
        long timeStamp = System.currentTimeMillis() / 1000;
        String signedUrl = urlString;
        String accessToken = null;
        String ticket = null;
        String signature = null;
        String agentid = null;
        try {
        	accessToken = getAccessToken();

            ticket = getJsapiTicket(accessToken);
            signature = sign(ticket, nonceStr, timeStamp, signedUrl);
            agentid = "335467234";
        }catch(OApiException e) {
        	e.printStackTrace();
        }
        String configValue = "{jsticket:'" + ticket + "',signature:'" + signature + "',nonceStr:'" + nonceStr + "',timeStamp:'"
                + timeStamp + "',corpId:'" + Env.CORP_ID + "',agentid:'" + agentid + "'}";
        System.out.println(configValue);
        Record resp = new Record();
        resp.set("code", 200);
        resp.set("config", configValue);
        return configValue;
	}

}
