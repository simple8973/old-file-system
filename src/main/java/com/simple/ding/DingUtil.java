package com.simple.ding;

import java.util.Arrays;

import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.OapiCallBackRegisterCallBackRequest;
import com.dingtalk.api.request.OapiCallBackUpdateCallBackRequest;
import com.dingtalk.api.request.OapiMessageCorpconversationAsyncsendV2Request;
import com.dingtalk.api.response.OapiCallBackRegisterCallBackResponse;
import com.dingtalk.api.response.OapiCallBackUpdateCallBackResponse;
import com.dingtalk.api.response.OapiMessageCorpconversationAsyncsendV2Response;
import com.simple.common.config.Constant;
import com.simple.common.config.URLConstant;
import com.simple.ding.Env;
import com.simple.util.AccessTokenUtil;
import com.taobao.api.ApiException;

public class DingUtil {
	/**
	 * sendText 发送文本类消息
	 * @author Ray
	 * @param useridList 钉钉用户ID
	 * @param message 消息内容
	 * @return
	 */
	public static String sendText(String useridList,String message) {
		String accessToken = AccessTokenUtil.getToken();
		DingTalkClient client = new DefaultDingTalkClient(URLConstant.URL_MESSAGE_ASYNCSEND_V2);
		
		OapiMessageCorpconversationAsyncsendV2Request request = new OapiMessageCorpconversationAsyncsendV2Request();
		request.setUseridList(useridList);
		request.setAgentId(Constant.AGENT_ID);
		request.setToAllUser(false);
		
		OapiMessageCorpconversationAsyncsendV2Request.Msg msg = new OapiMessageCorpconversationAsyncsendV2Request.Msg();
		msg.setMsgtype("text");
		msg.setText(new OapiMessageCorpconversationAsyncsendV2Request.Text());
		msg.getText().setContent(message);
		request.setMsg(msg);
		
		OapiMessageCorpconversationAsyncsendV2Response response = null;
		try {
			response = client.execute(request,accessToken);
		} catch (ApiException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return response.getBody();
	}

}
