package com.simple.shiro;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;

public class ShiroDbRealm extends AuthorizingRealm{
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authcToken)throws AuthenticationException{
		UsernamePasswordToken token = (UsernamePasswordToken)authcToken;
		return new SimpleAuthenticationInfo(token.getUsername(), token.getPassword(), getName());
	}
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		Collection<String> roleNameList = new ArrayList();
		Collection<String> permissionNameList = new ArrayList();
		String loginName = (String)principals.fromRealm(getName()).iterator().next();
		Record user = Db.findFirst("select * from users where username = ?", new Object[] { loginName });
		if (user != null) {
			SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
			List<Record> roleList = Db.find("select * from user_role where user_id = ?", new Object[] { user.get("user_id") });
			for (int i = 0; i < roleList.size(); i++) {
				Record role = Db.findById("roles", ((Record)roleList.get(i)).get("role_id"));
				roleNameList.add(role.getStr("role_name"));
				List<Record> permissionList = Db.find("select * from menu_permission where role_id = ?", new Object[] { ((Record)roleList.get(i)).getInt("role_id") });
			}
			info.addRoles(roleNameList);
			info.addStringPermissions(permissionNameList);
			return info;
		}
		return null;
	}
	
	public void clearCachedAuthorizationInfo(String principal) {
		SimplePrincipalCollection principals = new SimplePrincipalCollection(principal, getName());
		clearCachedAuthorizationInfo(principals);
	}
	
	public void clearAllCachedAuthorizationInfo() {
		Cache<Object, AuthorizationInfo> cache = getAuthorizationCache();
		if (cache != null) {
			for (Object key : cache.keys()) {
				cache.remove(key);
			}
		}
	}

}
