package com.simple.common.config;

import com.jfinal.config.Routes;
import com.simple.controller.system.OsController;
import com.simple.controller.system.SystemController;
import com.simple.controller.MainController;
import com.simple.controller.FileSendReceive.ShenPiInterFace;
import com.simple.controller.system.DingLoginController;

public class AdminRoutes extends Routes {

	@Override
	public void config() {
		//针对一组路由配置baseViewPath
		this.setBaseViewPath("/page");
		this.add("/dl",DingLoginController.class);
		this.add("/os",OsController.class,"sys");
		this.add("/sys",SystemController.class);
		this.add("/",MainController.class);
		this.add("/sp",ShenPiInterFace.class);
	}

}