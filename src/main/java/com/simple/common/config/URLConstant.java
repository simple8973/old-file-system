package com.simple.common.config;

public class URLConstant {
	//钉钉网关gettoken
	public static final String URL_GET_TOKEN="https://oapi.dingtalk.com/gettoken";
	
	//获取用户在企业内userId的接口URL
	public static final String URL_GET_USER_INFO="https://oapi.dingtalk.com/user/getuserinfo";
	
	//获取用户姓名的接口url
	public static final String URL_USER_GET="https://oapi.dingtalk.com/user/get";

	/**
     * 发送消息的接口
     */
    public static final String URL_MESSAGE_ASYNCSEND_V2 = "https://oapi.dingtalk.com/topapi/message/corpconversation/asyncsend_v2";
}
