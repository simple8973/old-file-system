package com.simple.common.config;

import com.jfinal.config.Routes;
import com.simple.controller.CaiGouFile.CaiGouPrint;
import com.simple.controller.CaiGouFile.CaiGouPublish;
import com.simple.controller.techFileReceive.FileReceiveController;
import com.simple.controller.techFileShenPi.FileShenPiController;
import com.simple.controller.techFileZhiHui.FileZhiHuiController;
import com.simple.controller.KehuFile.KeHuPrint;
import com.simple.controller.KehuFile.KeHuPublish;
import com.simple.controller.OutCheckFile.OutCheckFilePublishController;
import com.simple.controller.OutCheckFile.OutCheckFileReceiveController;
import com.simple.controller.ProjectShenHe.ProjectShenHeController;
import com.simple.controller.ShareAuth.ShareAuthController;
import com.simple.controller.XiangMuFile.XiangMuPrint;
import com.simple.controller.XiangMuFile.XiangMuPublish;
import com.simple.controller.ZhiLiangFile.ZhiLiangPrint;
import com.simple.controller.ZhiLiangFile.ZhiLiangPublish;
import com.simple.controller.customerInput.CustomerJieshouController;
import com.simple.controller.customerInput.CustomerZhihuiController;
import com.simple.controller.pro_build.pro_buildContorller;
import com.simple.controller.techFile.techPrintController;
import com.simple.controller.techFile.techPublishController;

public class PCRoutes extends Routes{
	
	@Override
	public void config() {
		this.setBaseViewPath("/page");
		this.add("pro_build",pro_buildContorller.class);
		this.add("customerInput",com.simple.controller.customerInput.CustomerInputController.class,"/customerInput");//顾客输入
		this.add("cusJiehsou",CustomerJieshouController.class,"/customerInput");//顾客输入接收
		this.add("cusZhihui",CustomerZhihuiController.class,"/customerInput");//顾客输入知会
		
		this.add("techPublish",techPublishController.class,"/shengchan/techPublish");//生产发布
		this.add("techPrint",techPrintController.class,"/shengchan/techPrint");//生产打印
		
		this.add("zhiliangPublish",ZhiLiangPublish.class,"/zhiliang/zhiliangPublish");//质量发布
		this.add("zhiliangPrint",ZhiLiangPrint.class,"/zhiliang/zhiliangPrint");//质量打印
		
		this.add("xiangmuPublish",XiangMuPublish.class,"/xiangmu/xiangmuPublish");//项目发布
		this.add("xiangmuPrint",XiangMuPrint.class,"/xiangmu/xiangmuPrint");//项目打印
		
		this.add("kehuPublish",KeHuPublish.class,"/kehu/kehuPublish");//客户发布
		this.add("kehuPrint",KeHuPrint.class,"/kehu/kehuPrint");//客户打印
		
		this.add("caigouPublish",CaiGouPublish.class,"/caigou/caigouPublish");//采购发布
		this.add("caigouPrint",CaiGouPrint.class,"/caigou/caigouPrint");//采购打印
		
		this.add("fileShenPi",FileShenPiController.class,"/techFileShenPi");//技术文件审批
		this.add("fileReceive",FileReceiveController.class,"/techFileReceive");//技术文件接收
		this.add("fileZhiHui",FileZhiHuiController.class,"/techFileZhiHui");//技术文件知会
		this.add("xmShenHe",ProjectShenHeController.class,"/xmShenHe");//项目审核
		
		this.add("outCheckPublish",OutCheckFilePublishController.class,"/outCheck");//外检站文件下发
		this.add("outCheckReceive",OutCheckFileReceiveController.class,"/outCheck");//外检站文件查看
		
		this.add("shareAuth",ShareAuthController.class,"/shareAuth");//权限共享
	}

}
