package com.simple.common.config;

import com.jfinal.config.Routes;
import com.simple.controller.app.AppController;
import com.simple.controller.app.AppMyFileController;

/**
 * 钉钉小程序后台业务路由
 * @author FL00024996
 *
 */
public class AppRoutes extends Routes{
	@Override
	public void config() {
		this.add("/fileApp",AppMyFileController.class);
		this.add("/app",AppController.class);
	}

}
