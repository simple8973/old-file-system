package com.simple.common.model.base;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * 
 * Generated by JBolt, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseProBuild<M extends BaseProBuild<M>> extends Model<M> implements IBean {

	/**
	 * 项目代码列表
	 */
	public M setId(java.lang.Integer id) {
		set("id", id);
		return (M)this;
	}
	
	/**
	 * 项目代码列表
	 */
	public java.lang.Integer getId() {
		return getInt("id");
	}

	/**
	 * 项目类型0-新品项目（含预研）；1-量产
	 */
	public M setProType(java.lang.Integer proType) {
		set("pro_type", proType);
		return (M)this;
	}
	
	/**
	 * 项目类型0-新品项目（含预研）；1-量产
	 */
	public java.lang.Integer getProType() {
		return getInt("pro_type");
	}

	public M setDepartNo(java.lang.String departNo) {
		set("depart_no", departNo);
		return (M)this;
	}
	
	public java.lang.String getDepartNo() {
		return getStr("depart_no");
	}

	public M setCustomerName(java.lang.String customerName) {
		set("customer_name", customerName);
		return (M)this;
	}
	
	public java.lang.String getCustomerName() {
		return getStr("customer_name");
	}

	public M setCustomerCode(java.lang.String customerCode) {
		set("customer_code", customerCode);
		return (M)this;
	}
	
	public java.lang.String getCustomerCode() {
		return getStr("customer_code");
	}

	/**
	 * 客户机型
	 */
	public M setModel(java.lang.String model) {
		set("model", model);
		return (M)this;
	}
	
	/**
	 * 客户机型
	 */
	public java.lang.String getModel() {
		return getStr("model");
	}

	public M setCustomerProName(java.lang.String customerProName) {
		set("customer_pro_name", customerProName);
		return (M)this;
	}
	
	public java.lang.String getCustomerProName() {
		return getStr("customer_pro_name");
	}

	public M setModuleId(java.lang.Integer moduleId) {
		set("module_id", moduleId);
		return (M)this;
	}
	
	public java.lang.Integer getModuleId() {
		return getInt("module_id");
	}

	public M setCategoryId(java.lang.Integer categoryId) {
		set("category_id", categoryId);
		return (M)this;
	}
	
	public java.lang.Integer getCategoryId() {
		return getInt("category_id");
	}

	/**
	 * 项目号
	 */
	public M setFlProNo(java.lang.String flProNo) {
		set("FL_pro_no", flProNo);
		return (M)this;
	}
	
	/**
	 * 项目号
	 */
	public java.lang.String getFlProNo() {
		return getStr("FL_pro_no");
	}

	public M setCreateTime(java.util.Date createTime) {
		set("create_time", createTime);
		return (M)this;
	}
	
	public java.util.Date getCreateTime() {
		return getDate("create_time");
	}

	/**
	 * 创建人id
	 */
	public M setUserId(java.lang.String userId) {
		set("user_id", userId);
		return (M)this;
	}
	
	/**
	 * 创建人id
	 */
	public java.lang.String getUserId() {
		return getStr("user_id");
	}

	/**
	 * 创建人姓名
	 */
	public M setCreateUserName(java.lang.String createUserName) {
		set("create_user_name", createUserName);
		return (M)this;
	}
	
	/**
	 * 创建人姓名
	 */
	public java.lang.String getCreateUserName() {
		return getStr("create_user_name");
	}

	/**
	 * 0-待审批；1-审批通过；2-审批驳回
	 */
	public M setSpStatus(java.lang.Integer spStatus) {
		set("sp_status", spStatus);
		return (M)this;
	}
	
	/**
	 * 0-待审批；1-审批通过；2-审批驳回
	 */
	public java.lang.Integer getSpStatus() {
		return getInt("sp_status");
	}

	/**
	 * 审批人id
	 */
	public M setSpUserId(java.lang.String spUserId) {
		set("sp_user_id", spUserId);
		return (M)this;
	}
	
	/**
	 * 审批人id
	 */
	public java.lang.String getSpUserId() {
		return getStr("sp_user_id");
	}

	/**
	 * 财务编码
	 */
	public M setCostCode(java.lang.String costCode) {
		set("cost_code", costCode);
		return (M)this;
	}
	
	/**
	 * 财务编码
	 */
	public java.lang.String getCostCode() {
		return getStr("cost_code");
	}

	/**
	 * 成本中心
	 */
	public M setCostCenter(java.lang.String costCenter) {
		set("cost_center", costCenter);
		return (M)this;
	}
	
	/**
	 * 成本中心
	 */
	public java.lang.String getCostCenter() {
		return getStr("cost_center");
	}

	/**
	 * 1-更新项目
	 */
	public M setIsUpdate(java.lang.Integer isUpdate) {
		set("is_update", isUpdate);
		return (M)this;
	}
	
	/**
	 * 1-更新项目
	 */
	public java.lang.Integer getIsUpdate() {
		return getInt("is_update");
	}

}

