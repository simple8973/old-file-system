package com.simple.common.model.base;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * 
 * Generated by JBolt, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseZhiliangTechFile<M extends BaseZhiliangTechFile<M>> extends Model<M> implements IBean {

	public M setId(java.lang.Integer id) {
		set("id", id);
		return (M)this;
	}
	
	public java.lang.Integer getId() {
		return getInt("id");
	}

	/**
	 * 发布类型：0-首次发布；1-替换升版；2-临时发布
	 */
	public M setReleaseType(java.lang.Integer releaseType) {
		set("release_type", releaseType);
		return (M)this;
	}
	
	/**
	 * 发布类型：0-首次发布；1-替换升版；2-临时发布
	 */
	public java.lang.Integer getReleaseType() {
		return getInt("release_type");
	}

	/**
	 * 文件类型
	 */
	public M setFileType(java.lang.String fileType) {
		set("file_type", fileType);
		return (M)this;
	}
	
	/**
	 * 文件类型
	 */
	public java.lang.String getFileType() {
		return getStr("file_type");
	}

	/**
	 * 内部项目总成号
	 */
	public M setNbZcCode(java.lang.String nbZcCode) {
		set("nb_zc_Code", nbZcCode);
		return (M)this;
	}
	
	/**
	 * 内部项目总成号
	 */
	public java.lang.String getNbZcCode() {
		return getStr("nb_zc_Code");
	}

	/**
	 * 内部文件号
	 */
	public M setNbFilecode(java.lang.String nbFilecode) {
		set("nb_fileCode", nbFilecode);
		return (M)this;
	}
	
	/**
	 * 内部文件号
	 */
	public java.lang.String getNbFilecode() {
		return getStr("nb_fileCode");
	}

	/**
	 * 文件名称
	 */
	public M setFileName(java.lang.String fileName) {
		set("file_name", fileName);
		return (M)this;
	}
	
	/**
	 * 文件名称
	 */
	public java.lang.String getFileName() {
		return getStr("file_name");
	}

	/**
	 * 版本号
	 */
	public M setEdition(java.lang.String edition) {
		set("edition", edition);
		return (M)this;
	}
	
	/**
	 * 版本号
	 */
	public java.lang.String getEdition() {
		return getStr("edition");
	}

	/**
	 * 用途
	 */
	public M setPurpose(java.lang.String purpose) {
		set("purpose", purpose);
		return (M)this;
	}
	
	/**
	 * 用途
	 */
	public java.lang.String getPurpose() {
		return getStr("purpose");
	}

	/**
	 * 打印变更说明
	 */
	public M setPrintExplain(java.lang.String printExplain) {
		set("print_explain", printExplain);
		return (M)this;
	}
	
	/**
	 * 打印变更说明
	 */
	public java.lang.String getPrintExplain() {
		return getStr("print_explain");
	}

	/**
	 * 文件发放数量
	 */
	public M setGiveNum(java.lang.Integer giveNum) {
		set("give_num", giveNum);
		return (M)this;
	}
	
	/**
	 * 文件发放数量
	 */
	public java.lang.Integer getGiveNum() {
		return getInt("give_num");
	}

	/**
	 * 5-质量文件
	 */
	public M setStatus(java.lang.Integer status) {
		set("status", status);
		return (M)this;
	}
	
	/**
	 * 5-质量文件
	 */
	public java.lang.Integer getStatus() {
		return getInt("status");
	}

	/**
	 * 临时文件使用周期
	 */
	public M setUseCycle(java.lang.Integer useCycle) {
		set("use_cycle", useCycle);
		return (M)this;
	}
	
	/**
	 * 临时文件使用周期
	 */
	public java.lang.Integer getUseCycle() {
		return getInt("use_cycle");
	}

	/**
	 * 发布人
	 */
	public M setCreateUser(java.lang.String createUser) {
		set("create_user", createUser);
		return (M)this;
	}
	
	/**
	 * 发布人
	 */
	public java.lang.String getCreateUser() {
		return getStr("create_user");
	}

	/**
	 * 创建人姓名
	 */
	public M setCreateUserName(java.lang.String createUserName) {
		set("create_user_name", createUserName);
		return (M)this;
	}
	
	/**
	 * 创建人姓名
	 */
	public java.lang.String getCreateUserName() {
		return getStr("create_user_name");
	}

	/**
	 * 创建时间
	 */
	public M setCreateTime(java.util.Date createTime) {
		set("create_time", createTime);
		return (M)this;
	}
	
	/**
	 * 创建时间
	 */
	public java.util.Date getCreateTime() {
		return getDate("create_time");
	}

	/**
	 * 审批人id
	 */
	public M setSpUserId(java.lang.String spUserId) {
		set("sp_user_id", spUserId);
		return (M)this;
	}
	
	/**
	 * 审批人id
	 */
	public java.lang.String getSpUserId() {
		return getStr("sp_user_id");
	}

	/**
	 * 0-未审批；20-待审核；21-审核通过；22-审核驳回；1-审批通过；2-审批驳回；3-已下发，待领取；4-使用中；5-已升版；13-已签章，待下发
	 */
	public M setSpStatus(java.lang.Integer spStatus) {
		set("sp_status", spStatus);
		return (M)this;
	}
	
	/**
	 * 0-未审批；20-待审核；21-审核通过；22-审核驳回；1-审批通过；2-审批驳回；3-已下发，待领取；4-使用中；5-已升版；13-已签章，待下发
	 */
	public java.lang.Integer getSpStatus() {
		return getInt("sp_status");
	}

	/**
	 * 印章
	 */
	public M setSeal(java.lang.String seal) {
		set("seal", seal);
		return (M)this;
	}
	
	/**
	 * 印章
	 */
	public java.lang.String getSeal() {
		return getStr("seal");
	}

	/**
	 * 附件表id
	 */
	public M setFujianId(java.lang.Integer fujianId) {
		set("fujian_id", fujianId);
		return (M)this;
	}
	
	/**
	 * 附件表id
	 */
	public java.lang.Integer getFujianId() {
		return getInt("fujian_id");
	}

	public M setPrintUserId(java.lang.String printUserId) {
		set("print_user_id", printUserId);
		return (M)this;
	}
	
	public java.lang.String getPrintUserId() {
		return getStr("print_user_id");
	}

	public M setXmSpUserId(java.lang.String xmSpUserId) {
		set("xm_sp_user_id", xmSpUserId);
		return (M)this;
	}
	
	public java.lang.String getXmSpUserId() {
		return getStr("xm_sp_user_id");
	}

	/**
	 * 项目审批人姓名
	 */
	public M setXmSpUserName(java.lang.String xmSpUserName) {
		set("xm_sp_user_name", xmSpUserName);
		return (M)this;
	}
	
	/**
	 * 项目审批人姓名
	 */
	public java.lang.String getXmSpUserName() {
		return getStr("xm_sp_user_name");
	}

	/**
	 * 量产、项目
	 */
	public M setXmStage(java.lang.String xmStage) {
		set("xm_stage", xmStage);
		return (M)this;
	}
	
	/**
	 * 量产、项目
	 */
	public java.lang.String getXmStage() {
		return getStr("xm_stage");
	}

	/**
	 * 0-否；1-是
	 */
	public M setIsChange(java.lang.Integer isChange) {
		set("is_change", isChange);
		return (M)this;
	}
	
	/**
	 * 0-否；1-是
	 */
	public java.lang.Integer getIsChange() {
		return getInt("is_change");
	}

	/**
	 * 变更附件
	 */
	public M setChangFujianId(java.lang.Long changFujianId) {
		set("chang_fujian_id", changFujianId);
		return (M)this;
	}
	
	/**
	 * 变更附件
	 */
	public java.lang.Long getChangFujianId() {
		return getLong("chang_fujian_id");
	}

}

