package com.simple.common.model.base;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * 外检站文件权限分配
 * Generated by JBolt, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseOutCheckAuth<M extends BaseOutCheckAuth<M>> extends Model<M> implements IBean {

	public M setId(java.lang.Long id) {
		set("id", id);
		return (M)this;
	}
	
	public java.lang.Long getId() {
		return getLong("id");
	}

	/**
	 * 0-外检；1-SQE
	 */
	public M setType(java.lang.Integer type) {
		set("type", type);
		return (M)this;
	}
	
	/**
	 * 0-外检；1-SQE
	 */
	public java.lang.Integer getType() {
		return getInt("type");
	}

	public M setUserId(java.lang.String userId) {
		set("user_id", userId);
		return (M)this;
	}
	
	public java.lang.String getUserId() {
		return getStr("user_id");
	}

	/**
	 * 员工
	 */
	public M setUserName(java.lang.String userName) {
		set("user_name", userName);
		return (M)this;
	}
	
	/**
	 * 员工
	 */
	public java.lang.String getUserName() {
		return getStr("user_name");
	}

	public M setDepId(java.lang.Long depId) {
		set("dep_id", depId);
		return (M)this;
	}
	
	public java.lang.Long getDepId() {
		return getLong("dep_id");
	}

	/**
	 * 部门
	 */
	public M setDepName(java.lang.String depName) {
		set("dep_name", depName);
		return (M)this;
	}
	
	/**
	 * 部门
	 */
	public java.lang.String getDepName() {
		return getStr("dep_name");
	}

	/**
	 * 状态
	 */
	public M setStatus(java.lang.Integer status) {
		set("status", status);
		return (M)this;
	}
	
	/**
	 * 状态
	 */
	public java.lang.Integer getStatus() {
		return getInt("status");
	}

}

