package com.simple.common.model.base;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * 
 * Generated by JBolt, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseCustomerInput<M extends BaseCustomerInput<M>> extends Model<M> implements IBean {

	/**
	 * 顾客输入类
	 */
	public M setId(java.lang.Long id) {
		set("id", id);
		return (M)this;
	}
	
	/**
	 * 顾客输入类
	 */
	public java.lang.Long getId() {
		return getLong("id");
	}

	/**
	 * 项目号
	 */
	public M setProCode(java.lang.String proCode) {
		set("pro_code", proCode);
		return (M)this;
	}
	
	/**
	 * 项目号
	 */
	public java.lang.String getProCode() {
		return getStr("pro_code");
	}

	public M setCustomerCode(java.lang.String customerCode) {
		set("customer_code", customerCode);
		return (M)this;
	}
	
	public java.lang.String getCustomerCode() {
		return getStr("customer_code");
	}

	/**
	 * 客户名称
	 */
	public M setCustomerName(java.lang.String customerName) {
		set("customer_name", customerName);
		return (M)this;
	}
	
	/**
	 * 客户名称
	 */
	public java.lang.String getCustomerName() {
		return getStr("customer_name");
	}

	/**
	 * 机型
	 */
	public M setJixing(java.lang.String jixing) {
		set("jixing", jixing);
		return (M)this;
	}
	
	/**
	 * 机型
	 */
	public java.lang.String getJixing() {
		return getStr("jixing");
	}

	/**
	 * 下发日期
	 */
	public M setSendTime(java.util.Date sendTime) {
		set("send_time", sendTime);
		return (M)this;
	}
	
	/**
	 * 下发日期
	 */
	public java.util.Date getSendTime() {
		return getDate("send_time");
	}

	/**
	 * 文件类型:01-技术类；02-质量类；03-物流类；04-其他类
	 */
	public M setFileType(java.lang.String fileType) {
		set("file_type", fileType);
		return (M)this;
	}
	
	/**
	 * 文件类型:01-技术类；02-质量类；03-物流类；04-其他类
	 */
	public java.lang.String getFileType() {
		return getStr("file_type");
	}

	/**
	 * 外来文件名称
	 */
	public M setWlFileName(java.lang.String wlFileName) {
		set("wl_file_name", wlFileName);
		return (M)this;
	}
	
	/**
	 * 外来文件名称
	 */
	public java.lang.String getWlFileName() {
		return getStr("wl_file_name");
	}

	/**
	 * 外来文件编码
	 */
	public M setWlFileCode(java.lang.String wlFileCode) {
		set("wl_file_code", wlFileCode);
		return (M)this;
	}
	
	/**
	 * 外来文件编码
	 */
	public java.lang.String getWlFileCode() {
		return getStr("wl_file_code");
	}

	/**
	 * 发布类型：0-首版，1-升版
	 */
	public M setPublishType(java.lang.Integer publishType) {
		set("publish_type", publishType);
		return (M)this;
	}
	
	/**
	 * 发布类型：0-首版，1-升版
	 */
	public java.lang.Integer getPublishType() {
		return getInt("publish_type");
	}

	/**
	 * 内部文件编码
	 */
	public M setNbFileCode(java.lang.String nbFileCode) {
		set("nb_file_code", nbFileCode);
		return (M)this;
	}
	
	/**
	 * 内部文件编码
	 */
	public java.lang.String getNbFileCode() {
		return getStr("nb_file_code");
	}

	/**
	 * 覆盖文件id
	 */
	public M setCoverFileId(java.lang.Long coverFileId) {
		set("cover_file_id", coverFileId);
		return (M)this;
	}
	
	/**
	 * 覆盖文件id
	 */
	public java.lang.Long getCoverFileId() {
		return getLong("cover_file_id");
	}

	/**
	 * 覆盖文件内部文件号
	 */
	public M setCoverNbfileCode(java.lang.String coverNbfileCode) {
		set("cover_nbfile_code", coverNbfileCode);
		return (M)this;
	}
	
	/**
	 * 覆盖文件内部文件号
	 */
	public java.lang.String getCoverNbfileCode() {
		return getStr("cover_nbfile_code");
	}

	/**
	 * 创建人id
	 */
	public M setInputUserId(java.lang.String inputUserId) {
		set("input_user_id", inputUserId);
		return (M)this;
	}
	
	/**
	 * 创建人id
	 */
	public java.lang.String getInputUserId() {
		return getStr("input_user_id");
	}

	/**
	 * 创建人
	 */
	public M setInputUserName(java.lang.String inputUserName) {
		set("input_user_name", inputUserName);
		return (M)this;
	}
	
	/**
	 * 创建人
	 */
	public java.lang.String getInputUserName() {
		return getStr("input_user_name");
	}

	/**
	 * 创建日期
	 */
	public M setCreateTime(java.util.Date createTime) {
		set("create_time", createTime);
		return (M)this;
	}
	
	/**
	 * 创建日期
	 */
	public java.util.Date getCreateTime() {
		return getDate("create_time");
	}

	/**
	 * 接受部门
	 */
	public M setJieshouDep(java.lang.String jieshouDep) {
		set("jieshou_dep", jieshouDep);
		return (M)this;
	}
	
	/**
	 * 接受部门
	 */
	public java.lang.String getJieshouDep() {
		return getStr("jieshou_dep");
	}

	/**
	 * 分发说明
	 */
	public M setPublishRemarks(java.lang.String publishRemarks) {
		set("publish_remarks", publishRemarks);
		return (M)this;
	}
	
	/**
	 * 分发说明
	 */
	public java.lang.String getPublishRemarks() {
		return getStr("publish_remarks");
	}

	/**
	 * 附加说明
	 */
	public M setRemarksFujian(java.lang.Long remarksFujian) {
		set("remarks_fujian", remarksFujian);
		return (M)this;
	}
	
	/**
	 * 附加说明
	 */
	public java.lang.Long getRemarksFujian() {
		return getLong("remarks_fujian");
	}

	/**
	 * 文件状态：0-待领取；1-使用中；2-已升版作废
	 */
	public M setFileStatus(java.lang.Integer fileStatus) {
		set("file_status", fileStatus);
		return (M)this;
	}
	
	/**
	 * 文件状态：0-待领取；1-使用中；2-已升版作废
	 */
	public java.lang.Integer getFileStatus() {
		return getInt("file_status");
	}

	/**
	 * 文件附件id
	 */
	public M setFileFujian(java.lang.Long fileFujian) {
		set("file_fujian", fileFujian);
		return (M)this;
	}
	
	/**
	 * 文件附件id
	 */
	public java.lang.Long getFileFujian() {
		return getLong("file_fujian");
	}

}

