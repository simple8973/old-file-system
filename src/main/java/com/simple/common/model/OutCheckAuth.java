package com.simple.common.model;

import com.simple.common.model.base.BaseOutCheckAuth;

/**
 * 外检站文件权限分配
 * Generated by JBolt.
 */
@SuppressWarnings("serial")
public class OutCheckAuth extends BaseOutCheckAuth<OutCheckAuth> {
	//建议将dao放在Service中只用作查询 
	public static final OutCheckAuth dao = new OutCheckAuth().dao();
	//在Service中声明 可直接复制过去使用
	//private OutCheckAuth dao = new OutCheckAuth().dao();  
}

