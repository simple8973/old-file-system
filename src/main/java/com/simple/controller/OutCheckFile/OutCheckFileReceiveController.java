package com.simple.controller.OutCheckFile;

import java.util.List;

import com.dingtalk.api.response.OapiUserGetResponse;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.simple.common.model.OutCheckAuth;
import com.simple.common.model.OutCheckFujian;
import com.simple.file.MyFileRender;
/**
 * 外检站文件查看 outCheckReceive/toOutCheckFileReceive
 * @author FL00024996
 *
 */
public class OutCheckFileReceiveController extends Controller {
	public void toOutCheckFileReceive() {
		render("outCheckReceive.html");
	}
	/**获取当前有效版本文件-----外检站提出需求 旧版本文件、过期临时单独页面展示---2021.10.21
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年10月21日 下午4:30:44
	 */
	public void getOutCheckFiles() {
		String selPartZcCode=get("selPartZcCode");
		String selNbFileCode=get("selNbFileCode");
		String selFileName=get("selFileName");
		String selDep=get("selDep");
		int page=getInt("currentPage");
		int limit=getInt("pageSize");
		StringBuilder sb=new StringBuilder();
		sb.append(" from out_check_file a, caigou_tech_file b where a.status=0 and b.id=a.caigou_file_id "
				+ "and ((b.release_type<>2 and (b.sp_status=3 or b.sp_status=4)) or (b.release_type=2 and DATEDIFF(now(),b.create_time)<=b.use_cycle and b.sp_status<>5))");//正式待领取、在使用文件 或 临时有效期内文件
		if (selDep!=null&&!"".equals(selDep)) {
			sb.append(" and a.dep_id="+selDep);
		}
		if (selPartZcCode!=null&&!"".equals(selPartZcCode)) {
			sb.append(" and a.nb_zc_code like '%"+selPartZcCode+"%'");
		}
		if (selNbFileCode!=null&&!"".equals(selNbFileCode)) {
			sb.append(" and a.nb_file_code like '%"+selNbFileCode+"%'");
		}
		if (selFileName!=null&&!"".equals(selFileName)) {
			sb.append(" and a.file_name like '%"+selFileName+"%'");
		}
		//判断当前人员角色---如果是外检人员，查看全部，如果是SQE查看自己事业部
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		OutCheckAuth outCheckAuth=OutCheckAuth.dao.findFirst("select * from out_check_auth where status=0 and type=0 and user_id='"+user.getUserid()+"'");
		int userType=0;
		if (outCheckAuth==null) {//非外检人员
			List<OutCheckAuth> sqeAuths=OutCheckAuth.dao.find("select * from out_check_auth where status=0 and type=1 and user_id='"+user.getUserid()+"'");
			if (!sqeAuths.isEmpty()) {//SQE
				userType=1;//SQE
				sb.append(" and ( ");
				for (int i = 0; i < sqeAuths.size(); i++) {
					sb.append(" a.dep_id="+sqeAuths.get(i).getDepId()+" or ");
				}
				sb.append(" 1<>1 ) ");
			}else {
				sb.append(" and 1<>1 ");
			}
		}
		Page<Record> files=Db.use("dc").paginate(page, limit, "select a.* ",sb.toString());
		for (int i = 0; i < files.getList().size(); i++) {
			//图纸
			OutCheckFujian drawFujian=OutCheckFujian.dao.findFirst("select * from out_check_fujian where file_id="+files.getList().get(i).getLong("id")+" and type=0 and status=0");
			files.getList().get(i).set("draw_id", drawFujian.getId());
			//作业指导书
			OutCheckFujian workFujian=OutCheckFujian.dao.findFirst("select * from out_check_fujian where file_id="+files.getList().get(i).getLong("id")+" and type=1 and status=0");
			if (workFujian==null) {
				files.getList().get(i).set("work_id", 0);
			}else {
				files.getList().get(i).set("work_id", workFujian.getId());
			}
		}
		Record record=new Record();
		record.set("code", 0);
		record.set("msg", "获取成功");
		record.set("total", files.getTotalRow());
		record.set("list", files.getList());
		record.set("user_type", userType);
		renderJson(record);
	}
//	/**获取数据列表-----外检站提出需求 旧版本文件、过期临时单独页面展示---2021.10.21
//	 * @author simple
//	 * @contact 15228717200
//	 * @time 2021年8月6日 上午11:12:28
//	 */
//	public void getOutCheckFiles() {
//		String selPartZcCode=get("selPartZcCode");
//		String selNbFileCode=get("selNbFileCode");
//		String selFileName=get("selFileName");
//		String selDep=get("selDep");
//		int page=getInt("currentPage");
//		int limit=getInt("pageSize");
//		StringBuilder sb=new StringBuilder();
//		sb.append(" from out_check_file where status=0 ");
//		if (selDep!=null&&!"".equals(selDep)) {
//			sb.append(" and dep_id="+selDep);
//		}
//		if (selPartZcCode!=null&&!"".equals(selPartZcCode)) {
//			sb.append(" and nb_zc_code like '%"+selPartZcCode+"%'");
//		}
//		if (selNbFileCode!=null&&!"".equals(selNbFileCode)) {
//			sb.append(" and nb_file_code like '%"+selNbFileCode+"%'");
//		}
//		if (selFileName!=null&&!"".equals(selFileName)) {
//			sb.append(" and file_name like '%"+selFileName+"%'");
//		}
//		//判断当前人员角色---如果是外检人员，查看全部，如果是SQE查看自己事业部
//		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
//		OutCheckAuth outCheckAuth=OutCheckAuth.dao.findFirst("select * from out_check_auth where status=0 and type=0 and user_id='"+user.getUserid()+"'");
//		int userType=0;
//		if (outCheckAuth==null) {//非外检人员
//			List<OutCheckAuth> sqeAuths=OutCheckAuth.dao.find("select * from out_check_auth where status=0 and type=1 and user_id='"+user.getUserid()+"'");
//			if (!sqeAuths.isEmpty()) {//SQE
//				userType=1;//SQE
//				sb.append(" and ( ");
//				for (int i = 0; i < sqeAuths.size(); i++) {
//					sb.append(" dep_id="+sqeAuths.get(i).getDepId()+" or ");
//				}
//				sb.append(" 1<>1 ) ");
//			}else {
//				sb.append(" and 1<>1 ");
//			}
//		}
//		Page<Record> files=Db.use("dc").paginate(page, limit, "select * ",sb.toString());
//		for (int i = 0; i < files.getList().size(); i++) {
//			//图纸
//			OutCheckFujian drawFujian=OutCheckFujian.dao.findFirst("select * from out_check_fujian where file_id="+files.getList().get(i).getLong("id")+" and type=0 and status=0");
//			files.getList().get(i).set("draw_id", drawFujian.getId());
//			//作业指导书
//			OutCheckFujian workFujian=OutCheckFujian.dao.findFirst("select * from out_check_fujian where file_id="+files.getList().get(i).getLong("id")+" and type=1 and status=0");
//			if (workFujian==null) {
//				files.getList().get(i).set("work_id", 0);
//			}else {
//				files.getList().get(i).set("work_id", workFujian.getId());
//			}
//		}
//		Record record=new Record();
//		record.set("code", 0);
//		record.set("msg", "获取成功");
//		record.set("total", files.getTotalRow());
//		record.set("list", files.getList());
//		record.set("user_type", userType);
//		renderJson(record);
//	}
	public void toOutCheckOldFile() {
		render("outCheckOld.html");
	}
	/**作废文件获取
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年10月21日 下午5:57:11
	 */
	public void getOldFile() {
		String selPartZcCode=get("selPartZcCode");
		String selNbFileCode=get("selNbFileCode");
		String selFileName=get("selFileName");
		String selDep=get("selDep");
		int page=getInt("currentPage");
		int limit=getInt("pageSize");
		StringBuilder sb=new StringBuilder();
		sb.append(" from out_check_file a, caigou_tech_file b where a.status=0 and b.id=a.caigou_file_id "
				+ "and ((b.release_type<>2 and b.sp_status=5 ) or (b.release_type=2 and (DATEDIFF(now(),b.create_time)>b.use_cycle or b.sp_status=5)))");//正式待领取、在使用文件 或 临时有效期内文件
		if (selDep!=null&&!"".equals(selDep)) {
			sb.append(" and a.dep_id="+selDep);
		}
		if (selPartZcCode!=null&&!"".equals(selPartZcCode)) {
			sb.append(" and a.nb_zc_code like '%"+selPartZcCode+"%'");
		}
		if (selNbFileCode!=null&&!"".equals(selNbFileCode)) {
			sb.append(" and a.nb_file_code like '%"+selNbFileCode+"%'");
		}
		if (selFileName!=null&&!"".equals(selFileName)) {
			sb.append(" and a.file_name like '%"+selFileName+"%'");
		}
		//判断当前人员角色---如果是外检人员，查看全部，如果是SQE查看自己事业部
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		OutCheckAuth outCheckAuth=OutCheckAuth.dao.findFirst("select * from out_check_auth where status=0 and type=0 and user_id='"+user.getUserid()+"'");
		int userType=0;
		if (outCheckAuth==null) {//非外检人员
			List<OutCheckAuth> sqeAuths=OutCheckAuth.dao.find("select * from out_check_auth where status=0 and type=1 and user_id='"+user.getUserid()+"'");
			if (!sqeAuths.isEmpty()) {//SQE
				userType=1;//SQE
				sb.append(" and ( ");
				for (int i = 0; i < sqeAuths.size(); i++) {
					sb.append(" a.dep_id="+sqeAuths.get(i).getDepId()+" or ");
				}
				sb.append(" 1<>1 ) ");
			}else {
				sb.append(" and 1<>1 ");
			}
		}
		Page<Record> files=Db.use("dc").paginate(page, limit, "select a.* ",sb.toString());
		for (int i = 0; i < files.getList().size(); i++) {
			//图纸
			OutCheckFujian drawFujian=OutCheckFujian.dao.findFirst("select * from out_check_fujian where file_id="+files.getList().get(i).getLong("id")+" and type=0 and status=0");
			files.getList().get(i).set("draw_id", drawFujian.getId());
			//作业指导书
			OutCheckFujian workFujian=OutCheckFujian.dao.findFirst("select * from out_check_fujian where file_id="+files.getList().get(i).getLong("id")+" and type=1 and status=0");
			if (workFujian==null) {
				files.getList().get(i).set("work_id", 0);
			}else {
				files.getList().get(i).set("work_id", workFujian.getId());
			}
		}
		Record record=new Record();
		record.set("code", 0);
		record.set("msg", "获取成功");
		record.set("total", files.getTotalRow());
		record.set("list", files.getList());
		record.set("user_type", userType);
		renderJson(record);
	}
	/**下载
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年8月9日 上午10:21:59
	 */
	public void fujianDownload() {
		Record rd=new Record();
		long file_id=getParaToLong(0);
		OutCheckFujian fujian=OutCheckFujian.dao.findById(file_id);
		try {
			render(new MyFileRender(fujian.getOldFujianName()));
		}catch (Exception e){
			rd.set("code", 1);
			rd.set("msg", "暂无附件下载");
			rd.set("data", "");
			renderJson(rd);
		}
	}
}
