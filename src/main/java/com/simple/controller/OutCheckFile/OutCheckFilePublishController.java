package com.simple.controller.OutCheckFile;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageOutputStream;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;

import com.jfinal.core.Controller;
import com.jfinal.json.FastJson;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.dingtalk.api.response.OapiUserGetResponse;
import com.jfinal.kit.PropKit;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.upload.UploadFile;
import com.simple.common.model.Codeaparts;
import com.simple.common.model.OutCheckAuth;
import com.simple.common.model.OutCheckDep;
import com.simple.common.model.OutCheckFile;
import com.simple.common.model.OutCheckFujian;
import com.simple.file.MyFileRender;
/**
 * 外检站文件上传 outCheckPublish/toOutCheckFilePublish
 * @author FL00024996
 *
 */
public class OutCheckFilePublishController extends Controller {
	public void toOutCheckFilePublish() {
		render("outCheckPublish.html");
	}
	/**获取数据列表
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年8月6日 上午11:12:28
	 */
	public void getOutCheckFiles() {
		String selPartZcCode=get("selPartZcCode");
		String selNbFileCode=get("selNbFileCode");
		String selFileName=get("selFileName");
		int page=getInt("currentPage");
		int limit=getInt("pageSize");
		StringBuilder sb=new StringBuilder();
		sb.append(" from out_check_file where status=0 ");
		if (selPartZcCode!=null&&!"".equals(selPartZcCode)) {
			sb.append(" and nb_zc_code like '%"+selPartZcCode+"%'");
		}
		if (selNbFileCode!=null&&!"".equals(selNbFileCode)) {
			sb.append(" and nb_file_code like '%"+selNbFileCode+"%'");
		}
		if (selFileName!=null&&!"".equals(selFileName)) {
			sb.append(" and file_name like '%"+selFileName+"%'");
		}
		Page<Record> files=Db.use("dc").paginate(page, limit, "select * ",sb.toString());
		Record record=new Record();
		record.set("code", 0);
		record.set("msg", "获取成功");
		record.set("total", files.getTotalRow());
		record.set("list", files.getList());
		renderJson(record);
	}
	/**新增编辑外检文件
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年8月6日 上午11:46:34
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void saveAddEditPublish() {
		Db.tx(() -> {
			Map map=FastJson.getJson().parse(get("formData"), Map.class);
			long fileId=Long.valueOf(get("fileId"));
			if (fileId==0) {
				OutCheckFile checkFile=new OutCheckFile();
				checkFile._setAttrs(map);
				checkFile.setCreateTime(new Date());
				checkFile.save();
				fileId=checkFile.getId();
			}else {
				OutCheckFile checkFile=OutCheckFile.dao.findById(fileId);
				checkFile._setAttrs(map);
				checkFile.update();
				
			}
			JSONArray fujianList=JSONArray.parseArray(get("fileList"));
			Db.use("dc").update("update out_check_fujian set status=1 where file_id="+fileId);
			for (int i = 0; i < fujianList.size(); i++) {
				JSONObject fujiannObject=fujianList.getJSONObject(i);
				OutCheckFujian fujian=OutCheckFujian.dao.findById(fujiannObject.get("id"));
				fujian.setFileId(fileId).setStatus(0);
				fujian.update();
			}
			renderJson(Ret.ok("msg", "操作成功！"));
			return true;
		});
	}
	/**
	 * 上传作业指导书
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年8月6日 上午11:01:17
	 */
	public void fujianUploadWork() {
		Record rd=new Record();
		try {
			long checkFileId=getParaToLong(0);
			UploadFile file = getFile("file", "");
			String oldfileName=file.getFileName();
			String pngName1=oldfileName.substring(0,oldfileName.lastIndexOf("."))+".png";
			String pngName=pngName1.replace("#", "");
			String pdfPath="D:/FilesFuJian/upload/"+oldfileName;
			String targetPath="D:/FilesFuJian/upload/"+pngName;
			//转为png
			FileInputStream instream = new FileInputStream(pdfPath);
            InputStream byteInputStream=null;
            try {
                PDDocument doc = PDDocument.load(instream);
                PDFRenderer renderer = new PDFRenderer(doc);
                int pageCount = doc.getNumberOfPages();
                if (pageCount > 0) {
                    BufferedImage image = renderer.renderImage(0, 2.0f);
                    image.flush();
                    ByteArrayOutputStream bs = new ByteArrayOutputStream();
                    ImageOutputStream imOut;
                    imOut = ImageIO.createImageOutputStream(bs);
                    ImageIO.write(image, "png", imOut);
                    byteInputStream = new ByteArrayInputStream(bs.toByteArray());
                    byteInputStream.close();
                }
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            File uploadFile = new File(targetPath);
            FileOutputStream fops;
            fops = new FileOutputStream(uploadFile);
            fops.write(readInputStream(byteInputStream));
            fops.flush();
            fops.close();
            //查询是否有旧作业指导书
            OutCheckFujian oldfujian=OutCheckFujian.dao.findFirst("select * from out_check_fujian where file_id="+checkFileId+" and type=1 and status=0");
            if (oldfujian!=null) {
            	oldfujian.setStatus(1);
            	oldfujian.update();
			}
			 //保存
            OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
			OutCheckFujian fujian=new OutCheckFujian();
			fujian.setFileId(checkFileId)
			.setOldFujianName(oldfileName).setNewFujianName(pngName)
			.setCreateTime(new Date())
			.setCreateUserId(user.getUserid()).setCreateUserName(user.getName());
			fujian.save();
			
			rd.set("msg","附件上传成功！");
			rd.set("code",0);
			rd.set("data", fujian);
		} catch (Exception e) {
			e.printStackTrace();
			rd.set("code",1);
			rd.set("msg","附件上传失败！"+e.getMessage());
			rd.set("data", "");
		}
		renderJson(rd);
	}
	public static byte[] readInputStream(InputStream inStream) throws Exception {
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len = 0;
        while ((len = inStream.read(buffer)) != -1) {
            outStream.write(buffer, 0, len);
        }
        inStream.close();
        return outStream.toByteArray();
    }
	/**
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年8月6日 上午11:28:05
	 */
	public void downLoad() {
		Record rd=new Record();
		long fujianId=getParaToLong(0);
		OutCheckFujian fujian=OutCheckFujian.dao.findById(fujianId);
		try {
			render(new MyFileRender(fujian.getOldFujianName()));
		}catch (Exception e){
			rd.set("code", 1);
			rd.set("msg", "暂无附件下载");
			rd.set("data", "");
			renderJson(rd);
		}
	}
	/**获取文件附件列表
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年8月6日 上午11:28:19
	 */
	public void getFileFujian() {
		long fileId=getParaToLong("fileid");
		List<OutCheckFujian> fujians=OutCheckFujian.dao.find("select * from out_check_fujian where status=0 and file_id="+fileId);
		Record record=new Record();
		record.set("data", fujians);
		record.set("msg", "获取成功");
		renderJson(record);
	}
	/**获取零件总成号列表
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年8月6日 上午11:07:51
	 */
	public void getZcPartCodeList() {
		List<Codeaparts> zcCodeList=Codeaparts.dao.find("select zc_part_code from codeaparts group by zc_part_code");
		renderJson(Ret.ok("data",zcCodeList));
	}
	/**删除外检文件台账
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年8月6日 下午4:20:45
	 */
	public void delOutCheckFile() {
		Db.tx(()->{
			long fileId=getParaToLong("id");
			OutCheckFile file=OutCheckFile.dao.findById(fileId);
			file.setStatus(1);
			file.update();
			renderJson(Ret.ok("msg", "删除成功！"));
			return true;
		});
	}
	/**附件预览
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年8月6日 下午5:30:04
	 */
	public void pdfPreview() {
		long fujianid=getParaToLong("fujianid");
		OutCheckFujian fujian=OutCheckFujian.dao.findById(fujianid);
		String pdfUrl=PropKit.get("domin_url")+"/"+fujian.getNewFujianName();
		renderJson(Ret.ok("data",pdfUrl));
	}
	/**获取部门列表
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年8月16日 下午4:49:25
	 */
	public void getDepList() {
		List<OutCheckDep> depList=OutCheckDep.dao.find("select * from out_check_dep");
		renderJson(Ret.ok("data",depList));
	}
	/**
	 * 权限设置分配
	 */
	public void toOutCheckAuth() {
		render("outCheckAuth.html");
	}
	/**
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年8月17日 上午9:37:13
	 */
	public void getCheckAuthUser() {
		int page=getInt("currentPage");
		int limit=getInt("pageSize");
		StringBuilder sb=new StringBuilder();
		String selUserName=get("selUserName");
		sb.append(" from out_check_auth where status=0 ");
		if (selUserName!=null&&!"".equals(selUserName)) {
			sb.append(" and user_name like '%"+selUserName+"%'");
		}
		Page<Record> userAuthList=Db.use("dc").paginate(page, limit, "select * ",sb.toString());
		Record record=new Record();
		record.set("code", 0);
		record.set("msg", "获取成功");
		record.set("total", userAuthList.getTotalRow());
		record.set("list", userAuthList.getList());
		renderJson(record);
	}
	/**新增
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年8月17日 上午9:58:42
	 */
	@SuppressWarnings("rawtypes")
	public void addOutAuth() {
		Db.tx(()->{
			Map map=FastJson.getJson().parse(get("formData"), Map.class);
			OutCheckDep dep=OutCheckDep.dao.findFirst("select * from out_check_dep where dep_id="+Long.valueOf(map.get("dep_id").toString()));
			String user_ids=map.get("user_ids").toString();
			String user_names=map.get("user_names").toString();
			String[] userIds=user_ids.split(",");
			String[] userNames=user_names.split(",");
			int type=Integer.valueOf(map.get("type").toString());
			for (int i = 0; i < userIds.length; i++) {
				OutCheckAuth checkAuth=OutCheckAuth.dao.findFirst("select * from out_check_auth where user_id='"+userIds[i]+"' and type="+type+" and dep_id="+Long.valueOf(map.get("dep_id").toString()));
				if (checkAuth!=null) {
					checkAuth.setStatus(0);
					checkAuth.update();
				}else {
					checkAuth=new OutCheckAuth();
					checkAuth.setUserId(userIds[i]).setUserName(userNames[i])
					.setDepId(dep.getDepId()).setDepName(dep.getDepName())
					.setStatus(0).setType(type);
					checkAuth.save();
				}
			}
			renderJson(Ret.ok("msg","新增成功！"));
			return true;
		});
	}
	/**删除权限
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年8月17日 上午10:20:57
	 */
	public void delAuth() {
		Db.tx(()->{
			long id=getParaToLong("id");
			OutCheckAuth auth=OutCheckAuth.dao.findById(id);
			auth.setStatus(1);
			auth.update();
			renderJson(Ret.ok("msg","删除成功！"));
			return true;
		});
	}
	
}
