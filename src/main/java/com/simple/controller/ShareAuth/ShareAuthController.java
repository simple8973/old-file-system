package com.simple.controller.ShareAuth;

import java.util.List;

import com.dingtalk.api.response.OapiUserGetResponse;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.simple.common.model.SameAuth;
/**
 * 权限共享/shareAuth/toShareAuth
 * @author FL00024996
 *
 */
public class ShareAuthController extends Controller {
	public void toShareAuth(){
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		set("login_user", user.getUserid());
		render("shareAuth.html");
	}
	public void getSameAuth() {
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		StringBuilder sb=new StringBuilder();
		int page=getInt("page");
		int limit=getInt("limit");
		Record jsp=new Record();
		jsp.set("code", 0);
		String sql_userRole="select * from user_role  where user_id='"+user.getUserid()+"' and role_id=1";
		List<Record> userRoleList=Db.use("dc").find(sql_userRole);
		if (userRoleList.size()>0) {
			sb.append(" from same_auth where status=0 ");
		}else {
			sb.append(" from same_auth where original_user_id='"+user.getUserid()+"' and status=0 or share_user_id='"+user.getUserid()+"'");
		}
		Page<Record> allAuth=Db.use("dc").paginate(page, limit, "select * ",sb.toString()+" order by id desc");
		jsp.set("count", allAuth.getTotalRow());
		jsp.set("data", allAuth.getList());
		jsp.set("msg", "获取成功");
		renderJson(jsp);
	}
	public void doAddAuth() {
		String share_user_id=get("share_user_id");
		String share_user_name=get("share_user_name");
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		SameAuth auth=SameAuth.dao.findFirst("select * from same_auth where original_user_id='"+user.getUserid()+"' and share_user_id='"+share_user_id+"'");
		if (auth==null) {
			auth=new SameAuth();
			auth.setOriginalUserId(user.getUserid()).setOriginalUserName(user.getName())
			.setShareUserId(share_user_id).setShareUserName(share_user_name).setStatus(0);
			auth.save();
		}else {
			auth.setStatus(0);
			auth.update();
		}
		Record jsp=new Record();
		jsp.set("code", 0);
		jsp.set("data", "");
		jsp.set("msg", "添加成功！");
		renderJson(jsp);
	}
	public void deleteAuth() {
		long authId=getParaToLong(0);
		SameAuth auth=SameAuth.dao.findById(authId);
		auth.setStatus(1);
		auth.update();
		Record jsp=new Record();
		jsp.set("code", 0);
		jsp.set("data", "");
		jsp.set("msg", "删除");
		renderJson(jsp);
	}
}
