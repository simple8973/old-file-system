package com.simple.controller.pro_build;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.poi.ss.usermodel.Workbook;
import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.OapiUserGetRequest;
import com.dingtalk.api.response.OapiUserGetResponse;
import com.github.liaochong.myexcel.core.DefaultExcelBuilder;
import com.github.liaochong.myexcel.utils.FileExportUtil;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.simple.ding.DingController;
import com.simple.ding.DingUtil;
import com.simple.excelModel.ProExport;
import com.taobao.api.ApiException;
import com.simple.common.model.UserRole;

public class pro_buildContorller extends Controller{
	//指向项目代码建号页面
	public void to_proList() {
		//获取产品类别
		List<Record> moduleList=Db.use("dc").find("select * from pro_modules");
		set("moduleList", moduleList);
		//当前用户角色
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		setAttr("login_user", user.getUserid());
		String sql_userRole="select * from user_role where user_id='"+user.getUserid()+"'";
		List<Record> userRole=Db.use("dc").find(sql_userRole);
		if (userRole.size()==0) {
			setAttr("role_type", 0);//普通用户
		} else {
			int back_role=0;
			for (int i = 0; i < userRole.size(); i++) {
				int role_type=userRole.get(i).getInt("role_id");
				if (role_type==10) {
					back_role=1; //项目人员
				}else if (role_type==11) {
					back_role=2;//项目经理
					break;
				}else if (role_type==12) {
					back_role=3;//财务
					break;
				}
			}
			setAttr("role_type", back_role);//0-非项目角色人员；1-项目人员；2-项目经理;3-财务
		}
		render("proList.html");
	}
	//通过选择的 部门、模块、类别 调出对应的项目列表
	public void get_proList() {
		StringBuilder sb=new StringBuilder();
		sb.append(" from pro_build a,pro_modules b,pro_categories c,pro_depart f  where a.module_id=b.id and a.category_id=c.id and a.depart_no=f.depart_no ");
		int page=getInt("page");
		int limit=getInt("limit");
		Record jsp=new Record();
		jsp.set("code", 0);
		
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		String sql_userRole="select * from user_role where user_id='"+user.getUserid()+"'";
		List<Record> userRole=Db.use("dc").find(sql_userRole);
		int back_role=0;//0-非项目角色人员；1-项目人员；2-项目经理
		if (userRole.size()!=0) {
			for (int i = 0; i < userRole.size(); i++) {
				int role_type=userRole.get(i).getInt("role_id");
				if (role_type==10) {
					back_role=1; //项目人员
				}else if (role_type==11) {
					back_role=2;//项目经理
					break;
				}
			}
		}
		if (back_role==0) {
			//非项目人员
			sb.append(" and a.sp_status=1");
		} else if (back_role==1){
			//项目人员
			sb.append(" and (a.user_id='"+user.getUserid()+"' or (a.user_id<>'"+user.getUserid()+"' and  a.sp_status=1))");
		}else if (back_role==2) {
			//项目经理
			sb.append(" and (a.sp_user_id='"+user.getUserid()+"' or (a.sp_user_id<>'"+user.getUserid()+"' and  a.sp_status=1) or a.user_id='"+user.getUserid()+"')");
		}
		
		if(!"".equals(get("fl_code"))&&get("fl_code")!=null){
			//sb.append(" and  a.depart_no='"+get("pro_depNo")+"' and a.module_id=b.id and a.category_id=c.id and a.depart_no=f.depart_no");
			sb.append(" and a.FL_pro_no like '%"+get("fl_code")+"%'");
		}
		if(!"".equals(get("customer_code"))&&get("customer_code")!=null) {
			sb.append(" and a.customer_name like '%"+get("customer_code")+"%'");
		}
		if(!"".equals(get("model"))&&get("model")!=null) {
			sb.append(" and a.model like '%"+get("model")+"%'");
		}
		if (getInt("module")!=null) {
			sb.append(" and a.module_id ="+getInt("module")+" and b.id=a.module_id");
		}
		if (getInt("category")!=null) {
			sb.append(" and a.category_id="+getInt("category")+" and c.id=a.category_id");
		}
		if(!"".equals(get("cost_code"))&&get("cost_code")!=null) {
			sb.append(" and a.cost_code like '%"+get("cost_code")+"%'");
		}
		
		if(!"".equals(get("zcPartCode"))&&get("zcPartCode")!=null) {
			List<Record> zcList=Db.use("dc").find("SELECT *  FROM codeaparts WHERE zc_part_code like '%"+get("zcPartCode")+"%'");
			if (!zcList.isEmpty()) {
				sb.append(" and ( ");
				for (int i = 0; i < zcList.size(); i++) {
					sb.append(" a.FL_pro_no='"+zcList.get(i).getStr("pro_code")+"' or ");
				}
				sb.append(" 1=2)");
			}
		}
		Page<Record> allPro=Db.use("dc").paginate(page, limit, "select a.*,b.module_name,c.category_name,f.depart_name  ",sb.toString()+" order by a.id desc");
		jsp.set("count", allPro.getTotalRow());
		jsp.set("data", allPro.getList());
		jsp.set("msg", "");
		renderJson(jsp);
	}
	//根据模块获取类别
	public void getCategory() {
		int moduleNo=getInt("moduleNo");
		String sql_category="select * from pro_categories where module_id="+moduleNo;
		List<Record> categoryList=Db.use("dc").find(sql_category);
		Record rd=new Record();
		rd.set("code", 0);
		rd.set("msg", "获取类别成功！");
		rd.set("data", categoryList);
		renderJson(rd);
	}
	//指向项目代码建号页面
	public void to_addProBuild() {
		//事业部列表
		List<Record> pro_depList=Db.use("dc").find("select * from pro_depart");
		setAttr("depList", pro_depList);
		//获取产品类别
		List<Record> moduleList=Db.use("dc").find("select * from pro_modules");
		setAttr("moduleList", moduleList);
		//获取客户姓名、代码
		String sql_ora_customer="select *,code as customer_id from customer_info where is_delete=0 ";
		List<Record> customerList=Db.use("dc").find(sql_ora_customer);
		setAttr("customerList", customerList);
		
		setAttr("shenpi", getXmMage());
		//零件列表
		String sql_part="select * from part_lists";
		List<Record> partList=Db.use("dc").find(sql_part);
		set("partList", partList);
		render("addProBuild.html");
	}
	//获取客户信息
	public void getCustomer() {
		String customerName=get("customer");
		String sql_customer="select *,code as customer_id from customer_info where name='"+customerName+"' ";
		Record customer=Db.use("dc").findFirst(sql_customer);
		Record rd=new Record();
		rd.set("code", 0);
		rd.set("msg", "获取客户信息成功！");
		rd.set("data",customer );
		renderJson(rd);
	}
	//获取总成代码列表
	public void getZcList1() {
		String sql="select * from zc_codeList";
		List<Record> zcList=Db.use("dc").find(sql);
		List<String> zc_no1=new ArrayList<String>();
		for (int i = 0; i < zcList.size(); i++) {
			String[] zc=zcList.get(i).getStr("zc_part_code").split("\\.");
			String[] no1=zc[0].split("L");
			zc_no1.add(no1[1]);
		}
		//去重
		List<String> zc_no11=new ArrayList<String>();
		for (int i = 0; i < zc_no1.size(); i++) {
			boolean b=false;
			String zci=zc_no1.get(i);
			for (int j = i+1; j < zc_no1.size(); j++) {
				String zcj=zc_no1.get(j);
				if (zcj.equals(zci)) {
					b=true;
					break;
				}
			}
			if (b==false) {
				zc_no11.add(zc_no1.get(i));
			}
		}
		Record rd=new Record();
		rd.set("code", 0);
		rd.set("msg", "获取总成列表1成功！");
		rd.set("zc_no1",zc_no11 );
		renderJson(rd);
	}
	//获取zcNo2
	public void getZcList2() {
		String fl_no1=get("fl_no1");
		String zcString="FL"+fl_no1+".";
		String sql="select * from zc_codeList where zc_part_code like '"+zcString+"%'";
		List<Record> zcList=Db.use("dc").find(sql);
		List<String> zc_no2=new ArrayList<String>();
		for (int i = 0; i < zcList.size(); i++) {
			String[] zc=zcList.get(i).getStr("zc_part_code").split("\\.");
			zc_no2.add(zc[1]);
		}
		Record rd=new Record();
		rd.set("code", 0);
		rd.set("msg", "获取总成列表1成功！");
		rd.set("data",zc_no2 );
		renderJson(rd);
	}
	//获取zcNo3
	public void getZcList3() {
		String fl_no1=get("fl_no1");
		String fl_no2=get("fl_no2");
		String zcString="FL"+fl_no1+"."+fl_no2+".";
		String sql="select * from zc_codeList where zc_part_code like '"+zcString+"%'";
		List<Record> zcList=Db.use("dc").find(sql);
		List<String> zc_no3=new ArrayList<String>();
		for (int i = 0; i < zcList.size(); i++) {
			String[] zc=zcList.get(i).getStr("zc_part_code").split("\\.");
			zc_no3.add(zc[2]);
		}
		Record rd=new Record();
		rd.set("code", 0);
		rd.set("msg", "获取总成列表1成功！");
		rd.set("data",zc_no3 );
		renderJson(rd);
	}

	//提交项目建号，逻辑检验
	public void ProBuild() {
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		Date date=new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
		String d = sdf.format(date);
		
		String customer_name=get("customer_name"); //客户名称
		String customer_id=get("customer_id");
		int pro_type=getParaToInt("pro_type");
		String pro_depNo=get("pro_depNo"); //事业部code
		int module_id=getParaToInt("module"); //模块id
		int category_id=getParaToInt("category"); //类别id
		String model=get("model");//客户机型
		String customer_pro_name=get("customer_pro_name");//客户项目名称
		
		String sp_userId=get("shenpi");//审批人id
		String zhiHuiUserId=get("zhiHuiUserId");//知会人id
		String pro_code=get("pro_code");
		String pro_no=get("pro_no");
		String pro_no2=get("pro_no2");
		
		int index_num=getParaToInt("partNum");
		List<Record> partList=new ArrayList<Record>();
		for (int i = 0; i < index_num; i++) {
			String customer_part_no="customer_part_no"+i;//客户零件号			
			String part_id="part_id"+i; //零件id
			String flNo0="flNo0";  //FL
			String fl_no1="fl_no1"+i;
			String fl_no2="fl_no2"+i;
			String fl_no3="fl_no3"+i;
			Record partRecord=new Record();
			partRecord.set("customer_part_no", get(customer_part_no));
			partRecord.set("part_id", getParaToInt(part_id));
			partRecord.set("flNo0", get(flNo0));
			partRecord.set("fl_no1", get(fl_no1));
			partRecord.set("fl_no2", get(fl_no2));
			partRecord.set("fl_no3", get(fl_no3));
			partList.add(partRecord); 
		}
		//pro_no输入长度处理
		int pro_no_len=pro_no.length();
		if(pro_no_len==1) {
			pro_no="000"+pro_no;
		}else if(pro_no_len==2) {
			pro_no="00"+pro_no;
		}else if(pro_no_len==3){
			pro_no="0"+pro_no;
		}
		//pro_no2输入长度处理
		int pro_no2_len=pro_no2.length();
		if(pro_no2_len==1) {
			pro_no2="0"+pro_no2;
		}
		
		String FL_pro_no=pro_code+pro_no+"."+pro_no2; //合成fl项目代号
		String sql_flProNo="select * from pro_build where FL_pro_no='"+FL_pro_no+"'";
		List<Record> fl_ProNo=Db.use("dc").find(sql_flProNo);
		Record rd=new Record();
		if (fl_ProNo.size()==0) {
			for (int i = 0; i < partList.size(); i++) {
				Record code_part=new Record();
				String zc1=partList.get(i).get("flNo0");
				String zc2=partList.get(i).get("fl_no1");
				String zc3=partList.get(i).get("fl_no2");
				String zc4=partList.get(i).get("fl_no3");
				//zc2长度处理
				int zc2_len=zc2.length();
				if(zc2_len==1) {
					zc2="0"+zc2;
				}
				//zc3长度处理
				int zc3_len=zc3.length();
				if(zc3_len==1) {
					zc3="000"+zc3;
				}else if(zc3_len==2) {
					zc3="00"+zc3;
				}else if(zc3_len==3){
					zc3="0"+zc3;
				}
				//zc4长度处理
				int zc4_len=zc4.length();
				if(zc4_len==1) {
					zc4="0"+zc4;
				}
				
				String zc_part_code=zc1+zc2+"."+zc3+'.'+zc4;
				code_part.set("customer_part_no", partList.get(i).get("customer_part_no"));
				code_part.set("part_id", partList.get(i).getInt("part_id"));
				code_part.set("zc_part_code", zc_part_code);
				code_part.set("pro_code",FL_pro_no );
				code_part.set("create_time", date);
				Db.use("dc").save("codeAparts",code_part);
			}
			Record build=new Record();
			build.set("pro_type", pro_type);
			build.set("customer_name", customer_name);
			build.set("customer_code", customer_id);
			build.set("depart_no", pro_depNo);
			build.set("module_id", module_id);
			build.set("category_id", category_id);
			build.set("model", model);//客户机型
			build.set("customer_pro_name", customer_pro_name);
			build.set("FL_pro_no", FL_pro_no);
			build.set("create_time", date);
			build.set("user_id", user.getUserid());
			build.set("create_user_name", user.getName());
			build.set("sp_user_id", sp_userId);//审批
			build.set("sp_status", 0);//待审批
			Db.use("dc").save("pro_build", build);
			//获取建号表id
			int pro_id=build.getInt("id");
			String proId=Integer.valueOf(pro_id).toString();
			//前端页面选择的知会人保存
			if (!"0".equals(zhiHuiUserId)) {
				String zhihuiUserSel_rec=get("zhihuiUserSel_rec");
				String[] zhiHuiUserIds=zhiHuiUserId.split("\\,");
				String[] zhihuiUserSel_recs=zhihuiUserSel_rec.split("\\,");
				for (int i = 0; i < zhiHuiUserIds.length; i++) {
					Record zhiHuiUser=new Record();
					zhiHuiUser.set("pro_build_id", pro_id);
					zhiHuiUser.set("zhihui_userId", zhiHuiUserIds[i]);
					zhiHuiUser.set("zhihui_userName", zhihuiUserSel_recs[i]);
					Db.use("dc").save("pro_zhihui_users", zhiHuiUser);
				}
			}
			//通知知会人项目建号待审批
			String sql_zhihui="select * from pro_zhihui_users where pro_build_id="+pro_id;
			List<Record> zhiHuiList=Db.use("dc").find(sql_zhihui);
			for (int i = 0; i < zhiHuiList.size(); i++) {
				DingUtil.sendText(zhiHuiList.get(i).getStr("zhihui_userId"), "知会消息：FL项目号“"+FL_pro_no+"”建号成功，等待审批，请知悉！"+d);
			}
			//通知审批人审批
			DingUtil.sendText(sp_userId, "审批消息：FL项目号“"+FL_pro_no+"”建号成功，请登录文件管理系统进行审批，请知悉！"+d);
			rd.set("code", 0);
			rd.set("msg", "创建项目成功！");
		}else {
			rd.set("code", 1);
			rd.set("msg", "项目号已存在！");
		}
		renderJson(rd);
	}
	
	//指向编辑项目创建信息
	public void to_editProBuildList() {
		setAttr("shenpi", getXmMage());
		//当前数据
		int pro_id=getParaToInt(0);
		String sql_pro="select a.*,b.depart_name,c.module_name,d.category_name from pro_build a,  pro_depart b,pro_modules c,pro_categories d where a.id="+pro_id+" and a.depart_no=b.depart_no and a.module_id=c.id and a.category_id=d.id";
		Record proRecord=Db.use("dc").findFirst(sql_pro);
		int pro_type=proRecord.getInt("pro_type");
		if (pro_type==0) {
			proRecord.set("pro_type_name", "新品项目（含预研）");
		}else {
			proRecord.set("pro_type_name", "量产项目");
		}
		setAttr("proInfo", proRecord);
		
		String FL_pro_no=proRecord.getStr("FL_pro_no");
		String[] string_pro=FL_pro_no.split("\\.");
		String no2=string_pro[1];
		setAttr("no2", no2);
		//获取客户
		String sql_ora_customer="select *,code as customer_id from customer_info where is_delete=0 ";
		List<Record> customerList=Db.use("dc").find(sql_ora_customer);
		setAttr("customerList", customerList);
		//获取部门
		List<Record> pro_depList=Db.use("dc").find("select * from pro_depart");
		setAttr("depList", pro_depList);
		//获取产品类别
		List<Record> moduleList=Db.use("dc").find("select * from pro_modules");
		setAttr("moduleList", moduleList);
		//获取模块编码
		String sql_module="select * from pro_modules where id="+proRecord.getInt("module_id");
		Record module=Db.use("dc").findFirst(sql_module);
		setAttr("module", module);
		//获取当前设置的总成编码
		String sql_zcPart="select * from codeaparts where status=0 and pro_code='"+proRecord.getStr("FL_pro_no")+"'";
		List<Record> zcPartList=Db.use("dc").find(sql_zcPart);
		List<String> zc_no2=new ArrayList<String>();
		for (int i = 0; i < zcPartList.size(); i++) {
			String string=zcPartList.get(i).getStr("zc_part_code");
			String[] strings=string.split("\\.");
			zc_no2.add(strings[1]);
		}
		setAttr("zc_no2", zc_no2);
		//零件列表
		String sql_part="select * from part_lists";
		List<Record> partList=Db.use("dc").find(sql_part);
		set("partList", partList);
		//总成零件号列表
		String sql_zcpart="SELECT a.*,b.part_name FROM codeaparts a,part_lists b ,pro_build  c WHERE a.pro_code=c.FL_pro_no AND c.id='"+pro_id+"' AND a.part_id=b.id and a.status=0";
		List<Record> zcPartListRecord=Db.use("dc").find(sql_zcpart);
		for (int i = 0; i < zcPartListRecord.size(); i++) {
			String zcPartString=zcPartListRecord.get(i).get("zc_part_code");
			String zcPart1=zcPartString.split("FL")[1];
			String[] zcPart1s=zcPart1.split("\\.");
			zcPartListRecord.get(i).set("fl_no1", zcPart1s[0]);
			zcPartListRecord.get(i).set("fl_no2", zcPart1s[1]);
			zcPartListRecord.get(i).set("fl_no3", zcPart1s[2]);
		}
		set("zcPartList", zcPartListRecord);
		set("zcPartNum", zcPartListRecord.size());
		render("editProBuildList.html");
	}
	//获取当前项目对应的零件号
	public void getPro_PartList() {
		int pro_id=getParaToInt(0);
		String sql_part="SELECT a.*,b.part_name FROM codeaparts a,part_lists b ,pro_build  c WHERE a.pro_code=c.FL_pro_no AND c.id='"+pro_id+"' AND a.part_id=b.id and a.status=0";
		List<Record> partList=Db.use("dc").find(sql_part);
		Record rd=new Record();
		rd.set("code", 0);
		rd.set("msg", "获取零件成功！");
		rd.set("data",partList );
		renderJson(rd);
	}
	//删除零件
	public void delPro_Par() {
		int zcpart_id=getParaToInt(0);
		Record zcPartRecord=Db.use("dc").findById("codeAparts", zcpart_id);
		Db.use("dc").delete("codeAparts", zcPartRecord );
		Record rd=new Record();
		rd.set("code", 0);
		rd.set("msg", "删除成功！");
		rd.set("data","" );
		renderJson(rd);
	}
	//指向修改零件页面
	public void toEditPart() {
		int id=getParaToInt(0); //总成表id
		setAttr("id", id);
		String sql_part="select *  from part_lists";
		List<Record> partList=Db.use("dc").find(sql_part);
		setAttr("partList", partList);
		render("editPart.html");
	}
	//修改零件逻辑
	public void doEditPart() {
		int id=getParaToInt("id");
		int part_id=getParaToInt("fieldValue");
		Db.use("dc").update("update codeaparts set part_id="+part_id+" where id="+id);
		Record rd=new Record();
		rd.set("code", 0);
		rd.set("msg", "修改成功！");
		rd.set("data","" );
		renderJson(rd);
	}
	//获取总成编码
	public void getEditZcPart() {
		int pro_id=getParaToInt(0);
		Record proInfo=Db.use("dc").findById("pro_build", pro_id);
		String sql_zcPart="select * from codeaparts where status=0 and pro_code='"+proInfo.getStr("FL_pro_no")+"'";
		List<Record> zcPartList=Db.use("dc").find(sql_zcPart);
		List<String> zc_no2=new ArrayList<String>();
		for (int i = 0; i < zcPartList.size(); i++) {
			String string=zcPartList.get(i).getStr("zc_part_code");
			String[] strings=string.split("\\.");
			zc_no2.add(strings[1]);
		}	
		Record rd=new Record();
		rd.set("code", 0);
		rd.set("msg", "获取总成编码成功！");
		rd.set("data",zc_no2 );
		renderJson(rd);
	}
	//编辑当前项目逻辑提交
	public void doProEdit() {
		Date date=new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
		String d = sdf.format(date);
		int pro_id=getParaToInt("pro_id");
		int pro_type=getParaToInt("pro_type");
		String customer_name=get("customer_name");
		String customer_id=get("customer_id");
		String pro_depNo=get("pro_depNo");
		int module=getParaToInt("module");
		int category=getParaToInt("category");
		String model=get("model");
		String customer_pro_name=get("customer_pro_name");//客户项目名称
		String sp_userId=get("shenpi");//审批人id
		String zhiHuiUserId=get("zhiHuiUserId");//知会人id
		int index_num=getParaToInt("partNum")-1;
		
		String pro_code=get("pro_code");
		String pro_no=get("pro_no");
		String pro_no2=get("pro_no2");
		//pro_no输入长度处理
		int pro_no_len=pro_no.length();
		if(pro_no_len==1) {
			pro_no="000"+pro_no;
		}else if(pro_no_len==2) {
			pro_no="00"+pro_no;
		}else if(pro_no_len==3){
			pro_no="0"+pro_no;
		}
		//pro_no2输入长度处理
		int pro_no2_len=pro_no2.length();
		if(pro_no2_len==1) {
			pro_no2="0"+pro_no2;
		}
		//合成新的fl项目代号
		String FL_pro_no=pro_code+pro_no+"."+pro_no2; 
		//查询新项目编码是否重复
		String sql_flProNo="select * from pro_build where FL_pro_no='"+FL_pro_no+"' and id<>"+pro_id;
		List<Record> fl_ProNo=Db.use("dc").find(sql_flProNo);
		
		Record rd=new Record();
		if (fl_ProNo.size()==0) {//不重复
			//修改pro_build表数据
			Record upPro=Db.use("dc").findById("pro_build", pro_id);
			upPro.set("pro_type", pro_type);
			upPro.set("customer_name", customer_name);
			upPro.set("customer_code", customer_id);
			upPro.set("depart_no", pro_depNo);
			upPro.set("module_id", module);
			upPro.set("category_id", category);
			upPro.set("customer_pro_name", customer_pro_name);
			upPro.set("model", model);
			upPro.set("FL_pro_no", FL_pro_no);
			upPro.set("sp_user_id", sp_userId);
			upPro.set("sp_status", 0);
			Db.use("dc").update("pro_build",upPro);
			//删除原来知会人
			String sql_old_zhihui="select * from pro_zhihui_users where pro_build_id="+pro_id;
			List<Record> oldZhiHui=Db.use("dc").find(sql_old_zhihui);
			if(oldZhiHui.size()!=0) {
				for (int i = 0; i < oldZhiHui.size(); i++) {
					Db.use("dc").delete("pro_zhihui_users", oldZhiHui.get(i));
				}
			}
			//新增知会人记录
			if (!"0".equals(zhiHuiUserId)) {
				String zhihuiUserSel_rec=get("zhihuiUserSel_rec");
				String[] zhiHuiUserIds=zhiHuiUserId.split("\\,");
				String[] zhihuiUserSel_recs=zhihuiUserSel_rec.split("\\,");
				for (int i = 0; i < zhiHuiUserIds.length; i++) {
					Record zhiHuiUser=new Record();
					zhiHuiUser.set("pro_build_id", pro_id);
					zhiHuiUser.set("zhihui_userId", zhiHuiUserIds[i]);
					zhiHuiUser.set("zhihui_userName", zhihuiUserSel_recs[i]);
					Db.use("dc").save("pro_zhihui_users", zhiHuiUser);
				}
			}
			//新添加零件
			if (index_num!=0) {
				List<Record> partList=new ArrayList<Record>();
				for (int i = 0; i <index_num; i++) {
					String zcId="zcId"+i;
					String customer_part_no="customer_part_no"+i;//客户零件号
					String part_id="part_id"+i; //零件id
					String flNo0="flNo0";  //FL
					String fl_no1="fl_no1"+i;
					String fl_no2="fl_no2"+i;
					String fl_no3="fl_no3"+i;
					Record partRecord=new Record();
					partRecord.set("zcId", get(zcId));
					partRecord.set("customer_part_no", get(customer_part_no));
					partRecord.set("part_id", getParaToInt(part_id));
					partRecord.set("flNo0", get(flNo0));
					partRecord.set("fl_no1", get(fl_no1));
					partRecord.set("fl_no2", get(fl_no2));
					partRecord.set("fl_no3", get(fl_no3));
					partList.add(partRecord); 
				}
				//零件处理
				for (int i = 0; i < partList.size(); i++) {
					String zc1=partList.get(i).get("flNo0");
					String zc2=partList.get(i).get("fl_no1");
					String zc3=partList.get(i).get("fl_no2");
					String zc4=partList.get(i).get("fl_no3");
					//zc2长度处理
					int zc2_len=zc2.length();
					if(zc2_len==1) {
						zc2="0"+zc2;
					}
					//zc3长度处理
					int zc3_len=zc3.length();
					if(zc3_len==1) {
						zc3="000"+zc3;
					}else if(zc3_len==2) {
						zc3="00"+zc3;
					}else if(zc3_len==3){
						zc3="0"+zc3;
					}
					//zc4长度处理
					int zc4_len=zc4.length();
					if(zc4_len==1) {
						zc4="0"+zc4;
					}
					//合成总成零件号
					String zc_part_code=zc1+zc2+"."+zc3+'.'+zc4;
					//新添加零件
					int zcId=Integer.valueOf(partList.get(i).get("zcId"));
					if (zcId!=0) {
						Record code_part=Db.use("dc").findById("codeAparts", zcId);
						code_part.set("customer_part_no", partList.get(i).get("customer_part_no"));
						code_part.set("part_id", partList.get(i).getInt("part_id"));
						code_part.set("zc_part_code", zc_part_code);
						code_part.set("pro_code",FL_pro_no );
						code_part.set("create_time", date);
						Db.use("dc").update("codeAparts", code_part);
					}else {
						Record code_part=new Record();
						code_part.set("customer_part_no", partList.get(i).get("customer_part_no"));
						code_part.set("part_id", partList.get(i).getInt("part_id"));
						code_part.set("zc_part_code", zc_part_code);
						code_part.set("pro_code",FL_pro_no );
						code_part.set("create_time", date);
						Db.use("dc").save("codeAparts",code_part);
					}
				}
			}
			//通知知会人
			String sql_zhihui="select * from pro_zhihui_users where pro_build_id="+pro_id;
			List<Record> zhiHuiList=Db.use("dc").find(sql_zhihui);
			for (int i = 0; i < zhiHuiList.size(); i++) {
				DingUtil.sendText(zhiHuiList.get(i).getStr("zhihui_userId"), "知会消息：FL项目号“"+FL_pro_no+"”已重新编辑，等待审批，请知悉！"+d);
			}
			//通知审批人审批
			DingUtil.sendText(sp_userId, "审批消息：FL项目号“"+FL_pro_no+"”建号成功，请登录文件管理系统进行审批，请知悉！"+d);
//			String proId=Integer.valueOf(pro_id).toString();
//			String process_instance_id="";
//			try {
//				process_instance_id=ShenPiUtil.proBuildShenPi(user.getUserid(),proId,customer_name,FL_pro_no,sp_userId);
//			}catch(Exception e) {
//				e.printStackTrace();
//			}
			rd.set("code", 0);
			rd.set("msg", "项目代码修改成功！");
		}else {//重复
			rd.set("code", 1);
			rd.set("msg", "项目号已存在！");
		}
		renderJson(rd);
	}
	//删除项目号以及项目号下面的零件号
	public void deleteProNo() {
		int pro_id=getParaToInt(0);
		Record pro=Db.use("dc").findById("pro_build", pro_id);
		Db.use("dc").delete("pro_build", pro);
		//删除项目号对应的零件号
		String sql_part="select * from codeaparts where pro_code='"+pro.get("FL_pro_no")+"'";
		List<Record> partList=Db.use("dc").find(sql_part);
		Record rd=new Record();
		rd.set("code", 0);
		if (partList.size()==0) {
			rd.set("msg", "项目号下无零件号！");
		} else {
			for (int j = 0; j < partList.size(); j++) {
				Db.use("dc").delete("codeaparts", partList.get(j));
			}
			rd.set("msg", "项目号删除成功！");
		}
		rd.set("data","" );
		renderJson(rd);
	}
	//项目代号详情
	public void getProInfo() {
		//当前数据
		int pro_id=getParaToInt(0);
		String sql_pro="select a.*,b.depart_name,c.module_name,d.category_name from pro_build a,  pro_depart b,pro_modules c,pro_categories d where a.id="+pro_id+" and a.depart_no=b.depart_no and a.module_id=c.id and a.category_id=d.id";
		Record proRecord=Db.use("dc").findFirst(sql_pro);
		int pro_type=proRecord.getInt("pro_type");
		if (pro_type==0) {
			proRecord.set("pro_type_name", "新品项目（含预研）");
		}else {
			proRecord.set("pro_type_name", "量产项目");
		}
		setAttr("proInfo", proRecord);
		//获取审批人姓名
		String sp_user=proRecord.getStr("sp_user_id");
		String accessToken_user=DingController.getAccessToken();
		DingTalkClient client_user = new DefaultDingTalkClient("https://oapi.dingtalk.com/user/get");
		OapiUserGetRequest request_user = new OapiUserGetRequest();
		request_user.setUserid(sp_user);
		request_user.setHttpMethod("GET");
		OapiUserGetResponse  response_user = null;
		try {
			response_user = client_user.execute(request_user, accessToken_user);
		}catch(ApiException e) {
			e.printStackTrace();
		}
		setAttr("sp_name", response_user.getName());
		//获取知会人
		String sql_zhihui="select * from pro_zhihui_users where pro_build_id="+pro_id;
		List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
		String zhihuiUser="";
		if (zhihuiList.size()!=0) {
			for (int i = 0; i < zhihuiList.size(); i++) {
				zhihuiUser+=zhihuiList.get(i).getStr("zhihui_userName")+",";
			}
		}
		setAttr("zhihui", zhihuiUser);
		//获取当前设置的总成编码
		String sql_zcPart="select * from codeaparts where status=0 and pro_code='"+proRecord.getStr("FL_pro_no")+"'";
		List<Record> zcPartList=Db.use("dc").find(sql_zcPart);
		List<String> zc_no2=new ArrayList<String>();
		for (int i = 0; i < zcPartList.size(); i++) {
			String string=zcPartList.get(i).getStr("zc_part_code");
			String[] strings=string.split("\\.");
			zc_no2.add(strings[1]);
		}
		setAttr("zc_no2", zc_no2);
		render("ProXiangQing.html");
	}
	//审批通过
	public void spPass() {
		Record rd=new Record();
		try {
			OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
			Date d= new Date();
			String date = sdf.format(d);
			int pro_id=getParaToInt(0);
			Record proInfo=Db.use("dc").findById("pro_build", pro_id);
			proInfo.set("sp_status", 1);
			Db.use("dc").update("pro_build", proInfo);
			DingUtil.sendText(user.getUserid(), "审批通知：项目代码“"+proInfo.getStr("FL_pro_no")+"”已在PC端完成审批，请知悉！"+date);
			//通知知会人
			String sql_zhihui="select * from pro_zhihui_users where pro_build_id="+pro_id;
			List<Record> zhiHuiList=Db.use("dc").find(sql_zhihui);
			for (int i = 0; i < zhiHuiList.size(); i++) {
				DingUtil.sendText(zhiHuiList.get(i).getStr("zhihui_userId"), "知会消息：FL项目号“"+proInfo.getStr("FL_pro_no")+"”已审批通过，请知悉！"+date);
			}
			//通知财务审批通过，编辑成本中心-内部顾客号
			List<UserRole> caiwus=UserRole.dao.find("select * from user_role where role_id=12");
			for (int i = 0; i < caiwus.size(); i++) {
				DingUtil.sendText(caiwus.get(i).getUserId(), "知会消息：FL项目号“"+proInfo.getStr("FL_pro_no")+"”已审批通过，请及时添加成本中心信息！"+date);
			}
			rd.set("code", 0);
			rd.set("msg", "审批成功！");
			rd.set("data","" );
		} catch (Exception e) {
			e.printStackTrace();
			rd.set("code", 1);
			rd.set("msg", "审批失败！");
			rd.set("data","" );
		}
		renderJson(rd);
	}
	//审批驳回
	public void spBack() {
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
		Date d= new Date();
		String date = sdf.format(d);
		int pro_id=getParaToInt(0);
		Record proInfo=Db.use("dc").findById("pro_build", pro_id);
		proInfo.set("sp_status", 2);
		Db.use("dc").update("pro_build", proInfo);
		DingUtil.sendText(user.getUserid(), "审批通知：项目代码“"+proInfo.getStr("FL_pro_no")+"”已在PC端完成审批，请知悉！"+date);
		//通知知会人
		String sql_zhihui="select * from pro_zhihui_users where pro_build_id="+pro_id;
		List<Record> zhiHuiList=Db.use("dc").find(sql_zhihui);
		for (int i = 0; i < zhiHuiList.size(); i++) {
			DingUtil.sendText(zhiHuiList.get(i).getStr("zhihui_userId"), "知会消息：FL项目号“"+proInfo.getStr("FL_pro_no")+"”被审批驳回，请知悉！"+date);
		}
		Record rd=new Record();
		rd.set("code", 0);
		rd.set("msg", "审批成功！");
		rd.set("data","" );
		renderJson(rd);
	}
	/**
	 * 公共模块
	 */
	//获取项目经理
	public List<Record> getXmMage(){
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		//String sql_sp="select * from user_role where role_id=11 and user_id<>'"+user.getUserid()+"'";
		String sql_sp="select * from user_role where role_id=11 ";
		List<Record> spList=Db.use("dc").find(sql_sp);
		return spList;
	}
	/**
	 * 零件总成号
	 */
	//指向总成号列表页面
	public void toZcList() {
		//当前用户角色
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		setAttr("login_user", user.getUserid());
		String sql_userRole="select * from user_role where user_id='"+user.getUserid()+"'";
		List<Record> userRole=Db.use("dc").find(sql_userRole);
		if (userRole.size()==0) {
			setAttr("role_type", 0);//普通用户
		} else {
			int back_role=0;
			for (int i = 0; i < userRole.size(); i++) {
				int role_type=userRole.get(i).getInt("role_id");
				if (role_type==10) {
					back_role=1; //项目人员
				}else if (role_type==11) {
					back_role=2;//项目经理
					break;
				}
			}
			setAttr("role_type", back_role);//0-非项目角色人员；1-项目人员；2-项目经理
		}
		render("zcProList.html");
	}
	//获取总成零件号列表
	public void getZcList() {
		StringBuilder sb=new StringBuilder();
		int page=getInt("page");
		int limit=getInt("limit");
		Record jsp=new Record();
		jsp.set("code", 0);
		sb.append(" from zc_codelist where 1=1 ");
		if (!"".equals(get("zcPro")) && get("zcPro")!=null) {
			sb.append(" and zc_part_code like '%"+get("zcPro")+"%' ");
		}
		Page<Record> allZcPro=Db.use("dc").paginate(page, limit, "select * ",sb.toString()+" order by id desc");
		jsp.set("count", allZcPro.getTotalRow());
		jsp.set("data", allZcPro.getList());
		jsp.set("msg", "查询总成号成功");
		renderJson(jsp);
	}
	//指向创建零件总成号页面
	public void toAddZcPro() {
		String sql_module="select * from zc_create_module";
		List<Record> moduleList=Db.use("dc").find(sql_module);
		setAttr("moduleList", moduleList);
		render("addZcPro.html");
	}
	//获取总成列表中getZcNo2数据
	public void getZcNo2() {
		String zc_no1=get("zc_no1");
		String zcNo="FL"+zc_no1+".";
		String zc_no2=get("zc_no2");
		Record record=new Record();
		String sql_zc="select * from zc_codelist where zc_part_code like '"+zcNo+"%"+zc_no2+"%.%'";
		List<Record> zcList=Db.use("dc").find(sql_zc);
		if (zcList.size()!=0) {
			List<String> zcStrings=new ArrayList<String>();
			for (int i = 0; i < zcList.size(); i++) {
				String zc_part_code=zcList.get(i).getStr("zc_part_code");
				String s=zc_part_code.split("\\.")[1];
				zcStrings.add(s);
			}
			record.set("data", zcStrings);
		}else {
			record.set("data", "");
		}
		record.set("code", 0);
		record.set("msg", "获取成功！");
		renderJson(record);
	}
	//获取总成列表中getZcNo3数据
	public void getZcNo3() {
		String zc_no1=get("zc_no1");
		String zcNo="FL"+zc_no1+".";
		String zc_no2=get("zc_no2");
		String zc_no3=get("zc_no3");
		Record record=new Record();
		
		String sql_zc="select * from zc_codelist where zc_part_code like '"+zcNo+""+zc_no2+".%"+zc_no3+"%' ";
		List<Record> zcList=Db.use("dc").find(sql_zc);
		if (zcList.size()!=0) {
			List<String> zcStrings=new ArrayList<String>();
			for (int i = 0; i < zcList.size(); i++) {
				String zc_part_code=zcList.get(i).getStr("zc_part_code");
				String s=zc_part_code.split("\\.")[2];
				zcStrings.add(s);
			}
			record.set("data", zcStrings);
		}else {
			record.set("data", "");
		}
		record.set("code", 0);
		record.set("msg", "获取成功！");
		renderJson(record);
	}
	//创建零件总成号逻辑
	public void doAddZcCode() {
		String zc_no1=get("zc_no1");
		String zc_no2=get("zc_no2");
		String zc_no3=get("zc_no3");
		//zc1长度处理
		int zc1_len=zc_no1.length();
		if(zc1_len==1) {
			zc_no1="0"+zc_no1;
		}
		//zc2长度处理
		int zc2_len=zc_no2.length();
		if(zc2_len==1) {
			zc_no2="000"+zc_no2;
		}else if(zc2_len==2) {
			zc_no2="00"+zc_no2;
		}else if(zc2_len==3){
			zc_no2="0"+zc_no2;
		}
		//zc3长度处理
		int zc3_len=zc_no3.length();
		if(zc3_len==1) {
			zc_no3="0"+zc_no3;
		}
		//合成总成码
		String zcNo="FL"+zc_no1+"."+zc_no2+"."+zc_no3;
		//唯一性校验
		String sql_zc="select * from zc_codelist where zc_part_code='"+zcNo+"'";
		List<Record> zcList=Db.use("dc").find(sql_zc);
		Record record=new Record();
		if (zcList.size()==0) {
			Record zcRecord=new Record();
			zcRecord.set("zc_part_code", zcNo);
			Db.use("dc").save("zc_codelist",zcRecord);
			record.set("code", 0);
			record.set("msg", "创建成功！");
		}else {
			record.set("code", 1);
			record.set("msg", "总成零件号已存在。请重新输入！");
		}
		renderJson(record);
	}
	/**
	 * 成本中心-内部顾客号
	 */
	public void toCostCenter() {
		int id=getParaToInt(0);
		Record proRecord=Db.use("dc").findById("pro_build", id);
		setAttr("proDetail", proRecord);
		render("costCenter.html");
	}
	//添加或编辑成本中心-内部顾客号
	public void AddCostCenter() {
		int proId=getParaToInt("pro_id");
		String cost_code=get("cost_code");
		String cost_center=get("cost_center");
		Record proRecord=Db.use("dc").findById("pro_build", proId);
		proRecord.set("cost_code", cost_code);
		proRecord.set("cost_center", cost_center);
		Db.use("dc").update("pro_build", proRecord);
		Record record=new Record();
		record.set("code", 0);
		record.set("data", "");
		record.set("msg", "成本中心编辑成功");
		renderJson(record);
	}
	/**
	 * 更新项目
	 */
	public void to_updateProBuildList() {
		setAttr("shenpi", getXmMage());
		//当前数据
		int pro_id=getParaToInt(0);
		String sql_pro="select a.*,b.depart_name,c.module_name,d.category_name from pro_build a,  pro_depart b,pro_modules c,pro_categories d where a.id="+pro_id+" and a.depart_no=b.depart_no and a.module_id=c.id and a.category_id=d.id";
		Record proRecord=Db.use("dc").findFirst(sql_pro);
		int pro_type=proRecord.getInt("pro_type");
		if (pro_type==0) {
			proRecord.set("pro_type_name", "新品项目（含预研）");
		}else {
			proRecord.set("pro_type_name", "量产项目");
		}
		setAttr("proInfo", proRecord);
		
		String FL_pro_no=proRecord.getStr("FL_pro_no");
		String[] string_pro=FL_pro_no.split("\\.");
		String no2=string_pro[1];
		setAttr("no2", no2);
		//获取客户
		String sql_ora_customer="select *,code as customer_id from customer_info where is_delete=0 ";
		List<Record> customerList=Db.use("dc").find(sql_ora_customer);
		setAttr("customerList", customerList);
		//获取部门
		List<Record> pro_depList=Db.use("dc").find("select * from pro_depart");
		setAttr("depList", pro_depList);
		//获取产品类别
		List<Record> moduleList=Db.use("dc").find("select * from pro_modules");
		setAttr("moduleList", moduleList);
		//获取模块编码
		String sql_module="select * from pro_modules where id="+proRecord.getInt("module_id");
		Record module=Db.use("dc").findFirst(sql_module);
		setAttr("module", module);
		//获取当前设置的总成编码
		String sql_zcPart="select * from codeaparts where status=0 and pro_code='"+proRecord.getStr("FL_pro_no")+"'";
		List<Record> zcPartList=Db.use("dc").find(sql_zcPart);
		List<String> zc_no2=new ArrayList<String>();
		for (int i = 0; i < zcPartList.size(); i++) {
			String string=zcPartList.get(i).getStr("zc_part_code");
			String[] strings=string.split("\\.");
			zc_no2.add(strings[1]);
		}
		setAttr("zc_no2", zc_no2);
		//零件列表
		String sql_part="select * from part_lists";
		List<Record> partList=Db.use("dc").find(sql_part);
		set("partList", partList);
		//总成零件号列表
		String sql_zcpart="SELECT a.*,b.part_name FROM codeaparts a,part_lists b ,pro_build  c WHERE a.pro_code=c.FL_pro_no AND c.id='"+pro_id+"' AND a.part_id=b.id and a.status=0";
		List<Record> zcPartListRecord=Db.use("dc").find(sql_zcpart);
		for (int i = 0; i < zcPartListRecord.size(); i++) {
			String zcPartString=zcPartListRecord.get(i).get("zc_part_code");
			String zcPart1=zcPartString.split("FL")[1];
			String[] zcPart1s=zcPart1.split("\\.");
			zcPartListRecord.get(i).set("fl_no1", zcPart1s[0]);
			zcPartListRecord.get(i).set("fl_no2", zcPart1s[1]);
			zcPartListRecord.get(i).set("fl_no3", zcPart1s[2]);
		}
		set("zcPartList", zcPartListRecord);
		set("zcPartNum", zcPartListRecord.size());
		render("updateProBuildList.html");
	}
	public void doProUpdate() {
		Date date=new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
		String d = sdf.format(date);
		int pro_id=getParaToInt("pro_id");
		int pro_type=getParaToInt("pro_type");
		String customer_name=get("customer_name");
		String customer_id=get("customer_id");
		String pro_depNo=get("pro_depNo");
		int category=getParaToInt("category");
		String model=get("model");
		String customer_pro_name=get("customer_pro_name");//客户项目名称
		String sp_userId=get("shenpi");//审批人id
		String zhiHuiUserId=get("zhiHuiUserId");//知会人id
		int index_num=getParaToInt("partNum")-1;
		
		Record rd=new Record();
		//修改pro_build表数据
		Record upPro=Db.use("dc").findById("pro_build", pro_id);
		upPro.set("pro_type", pro_type);
		upPro.set("customer_name", customer_name);
		upPro.set("customer_code", customer_id);
		upPro.set("depart_no", pro_depNo);
		upPro.set("category_id", category);
		upPro.set("customer_pro_name", customer_pro_name);
		upPro.set("model", model);
		upPro.set("sp_user_id", sp_userId);
		upPro.set("sp_status", 0);
		upPro.set("is_update", 1);
		Db.use("dc").update("pro_build",upPro);
		//删除原来知会人
		String sql_old_zhihui="select * from pro_zhihui_users where pro_build_id="+pro_id;
		List<Record> oldZhiHui=Db.use("dc").find(sql_old_zhihui);
		if(oldZhiHui.size()!=0) {
			for (int i = 0; i < oldZhiHui.size(); i++) {
				Db.use("dc").delete("pro_zhihui_users", oldZhiHui.get(i));
			}
		}
		//新增知会人记录
		if (!"0".equals(zhiHuiUserId)) {
			String zhihuiUserSel_rec=get("zhihuiUserSel_rec");
			String[] zhiHuiUserIds=zhiHuiUserId.split("\\,");
			String[] zhihuiUserSel_recs=zhihuiUserSel_rec.split("\\,");
			for (int i = 0; i < zhiHuiUserIds.length; i++) {
				Record zhiHuiUser=new Record();
				zhiHuiUser.set("pro_build_id", pro_id);
				zhiHuiUser.set("zhihui_userId", zhiHuiUserIds[i]);
				zhiHuiUser.set("zhihui_userName", zhihuiUserSel_recs[i]);
				Db.use("dc").save("pro_zhihui_users", zhiHuiUser);
			}
		}
		//默认知会人-马健
		Record zhiHuiUser=new Record();
		zhiHuiUser.set("pro_build_id", pro_id);
		zhiHuiUser.set("zhihui_userId", "20100438571246073");
		zhiHuiUser.set("zhihui_userName", "马健");
		Db.use("dc").save("pro_zhihui_users", zhiHuiUser);
		//新添加零件
		if (index_num!=0) {
			List<Record> partList=new ArrayList<Record>();
			for (int i = 0; i <index_num; i++) {
				String zcId="zcId"+i;
				String customer_part_no="customer_part_no"+i;//客户零件号
				String part_id="part_id"+i; //零件id
				String flNo0="flNo0";  //FL
				String fl_no1="fl_no1"+i;
				String fl_no2="fl_no2"+i;
				String fl_no3="fl_no3"+i;
				Record partRecord=new Record();
				partRecord.set("zcId", get(zcId));
				partRecord.set("customer_part_no", get(customer_part_no));
				partRecord.set("part_id", getParaToInt(part_id));
				partRecord.set("flNo0", get(flNo0));
				partRecord.set("fl_no1", get(fl_no1));
				partRecord.set("fl_no2", get(fl_no2));
				partRecord.set("fl_no3", get(fl_no3));
				partList.add(partRecord); 
			}
			//零件处理
			for (int i = 0; i < partList.size(); i++) {
				String zc1=partList.get(i).get("flNo0");
				String zc2=partList.get(i).get("fl_no1");
				String zc3=partList.get(i).get("fl_no2");
				String zc4=partList.get(i).get("fl_no3");
				//zc2长度处理
				int zc2_len=zc2.length();
				if(zc2_len==1) {
					zc2="0"+zc2;
				}
				//zc3长度处理
				int zc3_len=zc3.length();
				if(zc3_len==1) {
					zc3="000"+zc3;
				}else if(zc3_len==2) {
					zc3="00"+zc3;
				}else if(zc3_len==3){
					zc3="0"+zc3;
				}
				//zc4长度处理
				int zc4_len=zc4.length();
				if(zc4_len==1) {
					zc4="0"+zc4;
				}
				//合成总成零件号
				String zc_part_code=zc1+zc2+"."+zc3+'.'+zc4;
				//新添加零件
				int zcId=Integer.valueOf(partList.get(i).get("zcId"));
				if (zcId!=0) {
					Record code_part=Db.use("dc").findById("codeAparts", zcId);
					code_part.set("customer_part_no", partList.get(i).get("customer_part_no"));
					code_part.set("part_id", partList.get(i).getInt("part_id"));
					code_part.set("zc_part_code", zc_part_code);
					code_part.set("pro_code",upPro.getStr("FL_pro_no") );
					code_part.set("create_time", date);
					Db.use("dc").update("codeAparts", code_part);
				}else {
					Record code_part=new Record();
					code_part.set("customer_part_no", partList.get(i).get("customer_part_no"));
					code_part.set("part_id", partList.get(i).getInt("part_id"));
					code_part.set("zc_part_code", zc_part_code);
					code_part.set("pro_code",upPro.getStr("FL_pro_no") );
					code_part.set("create_time", date);
					Db.use("dc").save("codeAparts",code_part);
				}
			}
		}
		//通知知会人
		String sql_zhihui="select * from pro_zhihui_users where pro_build_id="+pro_id;
		List<Record> zhiHuiList=Db.use("dc").find(sql_zhihui);
		for (int i = 0; i < zhiHuiList.size(); i++) {
			DingUtil.sendText(zhiHuiList.get(i).getStr("zhihui_userId"), "知会消息：FL项目号“"+upPro.getStr("FL_pro_no")+"”已重新更新，等待审批，请知悉！"+d);
		}
		//通知审批人审批
		DingUtil.sendText(sp_userId, "审批消息：FL项目号“"+upPro.getStr("FL_pro_no")+"”更新成功，请登录文件管理系统进行审批，请知悉！"+d);
		rd.set("code", 0);
		rd.set("msg", "项目代码更新成功！");
		renderJson(rd);
	}
//	public void updateProStatus() {
//		int proId=getParaToInt(0);
//		ProBuild proBuild=ProBuild.dao.findById(proId);
//		proBuild.setSpStatus(2);
//		proBuild.update();
//		Record record=new Record();
//		record.set("code", 0);
//		record.set("data", "");
//		record.set("msg", "状态修改成功，可重新编辑");
//		renderJson(record);
//	}
	/**
	 * 导出
	 */
	public void downLoadPro() {
		String sqlPro="select a.*,b.module_name,c.category_name,f.depart_name from pro_build a,pro_modules b,pro_categories c,pro_depart f  where a.module_id=b.id and a.category_id=c.id and a.depart_no=f.depart_no and a.sp_status=1 ";
		
		if(!"".equals(get("fl_code"))&&get("fl_code")!=null){
			sqlPro=sqlPro+" and a.FL_pro_no like '%"+get("fl_code")+"%'";
		}
		if(!"".equals(get("customer_code"))&&get("customer_code")!=null) {
			sqlPro=sqlPro+" and a.customer_name like '%"+get("customer_code")+"%'";
		}
		if(!"".equals(get("model"))&&get("model")!=null) {
			sqlPro=sqlPro+" and a.model like '%"+get("model")+"%'";
		}
		if (getInt("module")!=null) {
			sqlPro=sqlPro+" and a.module_id ="+getInt("module")+" and b.id=a.module_id";
		}
		if (getInt("category")!=null) {
			sqlPro=sqlPro+" and a.category_id="+getInt("category")+" and c.id=a.category_id";
		}
		if(!"".equals(get("cost_code"))&&get("cost_code")!=null) {
			sqlPro=sqlPro+" and a.cost_code like '%"+get("cost_code")+"%'";
		}
		if(!"".equals(get("zcPartCode"))&&get("zcPartCode")!=null) {
			sqlPro=sqlPro+" and a.customer_name like '%"+get("customer_code")+"%'";
			
			
			List<Record> zcList=Db.use("dc").find("SELECT *  FROM codeaparts WHERE zc_part_code like '%"+get("zcPartCode")+"%'");
			if (!zcList.isEmpty()) {
				sqlPro=sqlPro+" and ( ";
				for (int i = 0; i < zcList.size(); i++) {
					sqlPro=sqlPro+" a.FL_pro_no='"+zcList.get(i).getStr("pro_code")+"' or ";
				}
				sqlPro=sqlPro+" 1=2)";
			}
		}
		List<Record> allProList=Db.use("dc").find(sqlPro);
		Record req=new Record();
		Db.tx(() -> {
			try {
				List<ProExport> dataList = this.getProList(allProList);
				Workbook workbook = DefaultExcelBuilder.of(ProExport.class)
				         .build(dataList);				
				FileExportUtil.export(workbook, new File("D:/FilesFuJian/upload/moban/项目列表.xlsx"));
				req.set("code", 0);
				req.set("msg", "成功");
				return true;
			} catch (Exception e) {
				e.printStackTrace();
				req.set("code", 1);
				req.set("msg", "失败！");
				return false;
			}
		});
		renderJson(req);
	}
	public List<ProExport> getProList(List<Record> proList) {
		List<ProExport> dataList=new ArrayList<ProExport>(proList.size());
	    for (int i = 0; i < proList.size(); i++) {
			ProExport exportModel=new ProExport();
			exportModel.setCustomer_name(proList.get(i).getStr("customer_name"));
			exportModel.setCustomer_pro_name(proList.get(i).getStr("customer_pro_name"));
			exportModel.setFL_pro_no(proList.get(i).getStr("FL_pro_no"));
			exportModel.setCustomer_code(proList.get(i).getStr("customer_code"));
			exportModel.setModel(proList.get(i).getStr("model"));
			exportModel.setDepart_name(proList.get(i).getStr("depart_name"));
			exportModel.setModule_name(proList.get(i).getStr("module_name"));
			exportModel.setCategory_name(proList.get(i).getStr("category_name"));
			exportModel.setCreate_user_name(proList.get(i).getStr("create_user_name"));
			exportModel.setCost_code(proList.get(i).getStr("cost_code"));
			exportModel.setCost_center(proList.get(i).getStr("cost_center"));
	    	dataList.add(exportModel);
	    }
	    return dataList;
	}
	public void downLoadProExcel() {
		Record rd=new Record();
		try {
			renderFile("/moban/项目列表.xlsx");
		} catch (Exception e) {
			rd.set("code", 1);
			rd.set("msg", "暂无！");
			renderJson(rd);
		}
	}
}
