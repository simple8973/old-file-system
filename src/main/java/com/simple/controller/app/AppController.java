package com.simple.controller.app;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;

import com.alibaba.fastjson.JSONObject;
import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.OapiUserGetRequest;
import com.dingtalk.api.request.OapiUserGetuserinfoRequest;
import com.dingtalk.api.response.OapiUserGetResponse;
import com.dingtalk.api.response.OapiUserGetuserinfoResponse;
import com.jfinal.core.Controller;
import com.jfinal.core.NotAction;
import com.jfinal.plugin.activerecord.Record;
import com.simple.common.config.URLConstant;
import com.simple.util.AccessTokenUtil;
import com.taobao.api.ApiException;

//E应用端处理类
public class AppController extends Controller{
	//开发测试用
	public void test() throws ApiException{ 
		String dingtoken = AccessTokenUtil.getToken();
		//获取用户详情
		DingTalkClient client3 = new DefaultDingTalkClient("https://oapi.dingtalk.com/user/get");
		OapiUserGetRequest request = new OapiUserGetRequest();
		request.setUserid("233262610339770583");//FL00014866项目经理
		request.setHttpMethod("GET");
		OapiUserGetResponse response3 = client3.execute(request, dingtoken);
		
		UsernamePasswordToken token = new UsernamePasswordToken("233262610339770583", "233262610339770583");//091765545335792299贾翠珍
		Subject subject = SecurityUtils.getSubject();
		subject.login(token);
		subject.getSession().setAttribute("user", response3);
		//redirect("/index");
		Record rsp=new Record();
		JSONObject jb = JSONObject.parseObject(response3.getBody());
		rsp.set("userinfo", jb.toString());
		rsp.set("code", 0);
		rsp.set("msg", "登陆成功");
        renderJson(rsp);
	}
	//钉钉E应用免登
	public void login() throws ApiException{
		//获取access_token
		String accessToken=AccessTokenUtil.getToken();
		//获取用户信息
		DingTalkClient client=new DefaultDingTalkClient(URLConstant.URL_GET_USER_INFO);
		OapiUserGetuserinfoRequest request=new OapiUserGetuserinfoRequest();
		request.setCode(getPara("authCode"));
		request.setHttpMethod("GET");
		
		OapiUserGetuserinfoResponse response=new OapiUserGetuserinfoResponse();
		try {
			response=client.execute(request,accessToken);
		}catch(ApiException e) {
			e.printStackTrace();
			renderNull();
		}
		//查询得到当前用户的userId
		//获取得到userId之后应用应该处理自身的登陆绘画管理（session），避免后续业务交互（前端到应用服务端）每次都要重新获取用户身份，提升用户体验
		String userId=response.getUserid();
		DingTalkClient client2= new DefaultDingTalkClient(URLConstant.URL_USER_GET);
		OapiUserGetRequest request2=new OapiUserGetRequest();
		request2.setUserid(userId);
		request2.setHttpMethod("GET");
		
		OapiUserGetResponse userinfo = null;
		try {
			userinfo = client2.execute(request2, accessToken);
		} catch (ApiException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			renderNull();
		}
		
        Record rsp=new Record();
		JSONObject jb = JSONObject.parseObject(userinfo.getBody());
		rsp.set("userinfo", jb.toString());
		rsp.set("code", 0);
		rsp.set("msg", "登陆成功");
        renderJson(rsp);
	}
	
	//获取用户详情
	@NotAction
	private OapiUserGetResponse getUserInfo(String accessToken,String userId) {
		try {
			DingTalkClient client =new DefaultDingTalkClient(URLConstant.URL_USER_GET);
			OapiUserGetRequest request=new OapiUserGetRequest();
			request.setUserid(userId);
			request.setHttpMethod("GET");
			OapiUserGetResponse response=client.execute(request,accessToken);
			return response;
		}catch(ApiException e) {
			e.printStackTrace();
			return null;
		}
	}

}
