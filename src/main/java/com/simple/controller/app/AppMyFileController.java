package com.simple.controller.app;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.simple.ding.DingUtil;

public class AppMyFileController extends Controller{	
	//获取文件发布记录
	public void ShengChan() {
		String user_id=get("user_id");
		String sql_shengchan="select * from tech_files  where create_user='"+user_id+"' order by create_time desc";//生产文件
		List<Record> allRecords=Db.use("dc").find(sql_shengchan);
		Record rd=new Record();
		if(allRecords.size()==0) {
			rd.set("code", 1);
			rd.set("msg", "当前没有任何生产文件发布噢！");
		}else {
			rd.set("data", allRecords);
			rd.set("code", 0);
			rd.set("msg", "获取文件列表成功！");
		}
		renderJson(rd);
	}
	public void KeHu() {
		String user_id=get("user_id");
		String sql_kehu="select * from kehu_tech_file  where create_user='"+user_id+"' order by create_time desc";//客户文件
		List<Record> allRecords=Db.use("dc").find(sql_kehu);
		Record rd=new Record();
		if(allRecords.size()==0) {
			rd.set("code", 1);
			rd.set("msg", "当前没有任何客户文件发布噢！");
		}else {
			rd.set("data", allRecords);
			rd.set("code", 0);
			rd.set("msg", "获取文件列表成功！");
		}
		renderJson(rd);
	}
	public void ZhiLiang() {
		String user_id=get("user_id");
		String sql_zhiliang="select * from zhiliang_tech_file  where create_user='"+user_id+"' order by create_time desc";//质量
		List<Record> allRecords=Db.use("dc").find(sql_zhiliang);
		Record rd=new Record();
		if(allRecords.size()==0) {
			rd.set("code", 1);
			rd.set("msg", "当前没有任何质量文件发布噢！");
		}else {
			rd.set("data", allRecords);
			rd.set("code", 0);
			rd.set("msg", "获取文件列表成功！");
		}
		renderJson(rd);
	}
	public void XiangMu() {
		String user_id=get("user_id");
		String sql_xiangmu="select * from xiangmu_tech_file  where create_user='"+user_id+"' order by create_time desc";//项目
		List<Record> allRecords=Db.use("dc").find(sql_xiangmu);
		Record rd=new Record();
		if(allRecords.size()==0) {
			rd.set("code", 1);
			rd.set("msg", "当前没有任何项目文件发布噢！");
		}else {
			rd.set("data", allRecords);
			rd.set("code", 0);
			rd.set("msg", "获取文件列表成功！");
		}
		renderJson(rd);
	}
	public void CaiGou() {
		String user_id=get("user_id");
		String sql_caigou="select * from caigou_tech_file  where create_user='"+user_id+"' order by create_time desc";//采购
		List<Record> allRecords=Db.use("dc").find(sql_caigou);
		Record rd=new Record();
		if(allRecords.size()==0) {
			rd.set("code", 1);
			rd.set("msg", "当前没有任何采购文件发布噢！");
		}else {
			rd.set("data", allRecords);
			rd.set("code", 0);
			rd.set("msg", "获取文件列表成功！");
		}
		renderJson(rd);
	}
	//获取文件接收列表
	public void getReceiveList() {
		String user_id=get("user_id");
		String sql_file="("
				+ "select b.*,a.receive,a.id as bid from caigou_tech_files_receive a, caigou_tech_file b where a.receive_userId='"+user_id+"' and b.id=a.caigou_tech_files_id and b.sp_status>=3 and b.sp_status<13"
				+ " union all select b.*,a.receive,a.id as bid from kehu_tech_files_receive a, kehu_tech_file b where a.receive_userId='"+user_id+"' and b.id=a.kehu_tech_files_id and b.sp_status>=3 and b.sp_status<13"
				+ " union all select b.*,a.receive,a.id as bid from tech_files_receive a, tech_files b where a.receive_userId='"+user_id+"' and b.id=a.tech_files_id and b.sp_status>=3 and b.sp_status<>6"
				+ " union all select b.*,a.receive,a.id as bid from xiangmu_tech_files_receive a,  xiangmu_tech_file b where  a.receive_userId='"+user_id+"' and b.id=a.xiangmu_tech_files_id and b.sp_status>=3 and b.sp_status<13"
				+ " union all select b.*,a.receive,a.id as bid from zhiliang_tech_files_receive a,zhiliang_tech_file b where  a.receive_userId='"+user_id+"' and b.id=a.zhiliang_tech_files_id and b.sp_status>=3 and b.sp_status<13"
				+ ") order by receive asc, create_time desc";
		List<Record> fileList=Db.use("dc").find(sql_file);
		Record rd=new Record();
		if (fileList.size()==0) {
			rd.set("code", 1);
			rd.set("msg", "当前没有任何接收文件噢！");
		}else {
			rd.set("data", fileList);
			rd.set("code", 0);
			rd.set("msg", "获取文件列表成功！");
		}
		renderJson(rd);
	}
	//接收文件
	public void fileReceive() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
		Date d= new Date();
		String date = sdf.format(d);
		int id=getParaToInt("receive_id");//接收表id
		int type=getParaToInt("type");//文件种类
		
		if (type==1) {
			Record receive=Db.use("dc").findById("caigou_tech_files_receive", id);
			receive.set("receive", 1);
			Db.use("dc").update("caigou_tech_files_receive", receive);
			//判断是否所有人都已经接收
			String sql_user="select * from caigou_tech_files_receive where caigou_tech_files_id="+receive.getInt("caigou_tech_files_id");
			List<Record> userList=Db.use("dc").find(sql_user);
			int a=0;
			for (int i = 0; i < userList.size(); i++) {
				int status=userList.get(i).getInt("receive");
				if (status==0) {
					a=1; //有未接收的人员时,跳出循环,文件状态仍然是"待领取"
					break;
				}
			}
			//根据a的值判断文件是否已经全部接受
			Record file=Db.use("dc").findById("caigou_tech_file", receive.getInt("caigou_tech_files_id"));
			if (a==0) {
				file.set("sp_status", 4);//全部接收,状态变为4-使用中
				Db.use("dc").update("caigou_tech_file", file);
				//全部接收时，通知知会人
				String sql_zhihui="select * from caigou_zhihui_users where caigou_tech_files_id="+file.getInt("id");
				List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
				String zhiHui="";
				for (int i = 0; i < zhihuiList.size(); i++) {
					zhiHui+=zhihuiList.get(i).getStr("zhihui_userId")+",";
				}
				DingUtil.sendText(zhiHui, "知会通知：采购文件“ "+file.getStr("file_name")+" ” 已经全部领取，请知悉！"+date);
			}
		}else if(type==2) {
			Record receive=Db.use("dc").findById("kehu_tech_files_receive", id);
			receive.set("receive", 1);
			Db.use("dc").update("kehu_tech_files_receive", receive);
			//判断是否所有人都已经接收
			String sql_user="select * from kehu_tech_files_receive where kehu_tech_files_id="+receive.getInt("kehu_tech_files_id");
			List<Record> userList=Db.use("dc").find(sql_user);
			int a=0;
			for (int i = 0; i < userList.size(); i++) {
				int status=userList.get(i).getInt("receive");
				if (status==0) {
					a=1; //有未接收的人员时,跳出循环,文件状态仍然是"待领取"
					break;
				}
			}
			//根据a的值判断文件是否已经全部接受
			Record file=Db.use("dc").findById("kehu_tech_file", receive.getInt("kehu_tech_files_id"));
			if (a==0) {
				file.set("sp_status", 4);//全部接收,状态变为4-使用中
				Db.use("dc").update("kehu_tech_file", file);
				//全部接收时，通知知会人
				String sql_zhihui="select * from kehu_zhihui_users where kehu_tech_files_id="+file.getInt("id");
				List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
				String zhiHui="";
				for (int i = 0; i < zhihuiList.size(); i++) {
					zhiHui+=zhihuiList.get(i).getStr("zhihui_userId")+",";
				}
				DingUtil.sendText(zhiHui, "知会通知：客户文件“ "+file.getStr("file_name")+" ” 已经全部领取，请知悉！"+date);
			}
		}else if(type==3) {
			Record receive=Db.use("dc").findById("tech_files_receive", id);
			receive.set("receive", 1);
			Db.use("dc").update("tech_files_receive", receive);
			//判断是否所有人都已经接收
			String sql_user="select * from tech_files_receive where tech_files_id="+receive.getInt("tech_files_id");
			List<Record> userList=Db.use("dc").find(sql_user);
			int a=0;
			for (int i = 0; i < userList.size(); i++) {
				int status=userList.get(i).getInt("receive");
				if (status==0) {
					a=1; //有未接收的人员时,跳出循环,文件状态仍然是"待领取"
					break;
				}
			}
			//根据a的值判断文件是否已经全部接受
			Record file=Db.use("dc").findById("tech_files", receive.getInt("tech_files_id"));
			if (a==0) {
				file.set("sp_status", 4);//全部接收,状态变为4-使用中
				Db.use("dc").update("tech_files", file);
				//全部接收时，通知知会人
				String sql_zhihui="select * from zhihui_users where tech_files_id="+file.getInt("id");
				List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
				String zhiHui="";
				for (int i = 0; i < zhihuiList.size(); i++) {
					zhiHui+=zhihuiList.get(i).getStr("zhihui_userId")+",";
				}
				DingUtil.sendText(zhiHui, "知会通知：生产文件“ "+file.getStr("file_name")+" ” 已经全部领取，请知悉！"+date);
			}
		}else if(type==4) {
			Record receive=Db.use("dc").findById("xiangmu_tech_files_receive", id);
			receive.set("receive", 1);
			Db.use("dc").update("xiangmu_tech_files_receive", receive);
			//判断是否所有人都已经接收
			String sql_user="select * from xiangmu_tech_files_receive where xiangmu_tech_files_id="+receive.getInt("xiangmu_tech_files_id");
			List<Record> userList=Db.use("dc").find(sql_user);
			int a=0;
			for (int i = 0; i < userList.size(); i++) {
				int status=userList.get(i).getInt("receive");
				if (status==0) {
					a=1; //有未接收的人员时,跳出循环,文件状态仍然是"待领取"
					break;
				}
			}
			//根据a的值判断文件是否已经全部接受
			Record file=Db.use("dc").findById("xiangmu_tech_file", receive.getInt("xiangmu_tech_files_id"));
			if (a==0) {
				file.set("sp_status", 4);//全部接收,状态变为4-使用中
				Db.use("dc").update("xiangmu_tech_file", file);
				
				//全部接收时，通知知会人
				String sql_zhihui="select * from xiangmu_zhihui_users where xiangmu_tech_files_id="+file.getInt("id");
				List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
				String zhiHui="";
				for (int i = 0; i < zhihuiList.size(); i++) {
					zhiHui+=zhihuiList.get(i).getStr("zhihui_userId")+",";
				}
				DingUtil.sendText(zhiHui, "知会通知：项目文件“ "+file.getStr("file_name")+" ” 已经全部领取，请知悉！"+date);
			}
		}else if(type==5) {
			Record receive=Db.use("dc").findById("zhiliang_tech_files_receive", id);
			receive.set("receive", 1);
			Db.use("dc").update("zhiliang_tech_files_receive", receive);
			//判断是否所有人都已经接收
			String sql_user="select * from zhiliang_tech_files_receive where zhiliang_tech_files_id="+receive.getInt("zhiliang_tech_files_id");
			List<Record> userList=Db.use("dc").find(sql_user);
			int a=0;
			for (int i = 0; i < userList.size(); i++) {
				int status=userList.get(i).getInt("receive");
				if (status==0) {
					a=1; //有未接收的人员时,跳出循环,文件状态仍然是"待领取"
					break;
				}
			}
			//根据a的值判断文件是否已经全部接受
			Record file=Db.use("dc").findById("zhiliang_tech_file", receive.getInt("zhiliang_tech_files_id"));
			if (a==0) {
				file.set("sp_status", 4);//全部接收,状态变为4-使用中
				Db.use("dc").update("zhiliang_tech_file", file);
				//全部接收时，通知知会人
				String sql_zhihui="select * from zhiliang_zhihui_users where zhiliang_tech_files_id="+file.getInt("id");
				List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
				String zhiHui="";
				for (int i = 0; i < zhihuiList.size(); i++) {
					zhiHui+=zhihuiList.get(i).getStr("zhihui_userId")+",";
				}
				DingUtil.sendText(zhiHui, "知会通知：质量文件“ "+file.getStr("file_name")+" ” 已经全部领取，请知悉！"+date);
			}
		}
		Record record=new Record();
		record.set("code", 0);
		record.set("msg", "文件接收成功！");
		record.set("data", "");
		renderJson(record);
	}
	//生产文件接收
	public void scfileReceive() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
		Date d= new Date();
		String date = sdf.format(d);
		int id=getParaToInt("receiveId");//接收表id
		int receiveNo=getParaToInt("receiveNo");
		
		Record receive=Db.use("dc").findById("tech_files_receive", id);
		receive.set("receive", 1);
		receive.set("receiveNum", receiveNo);
		Db.use("dc").update("tech_files_receive", receive);
		//判断是否所有人都已经接收
		String sql_user="select * from tech_files_receive where tech_files_id="+receive.getInt("tech_files_id");
		List<Record> userList=Db.use("dc").find(sql_user);
		int a=0;
		for (int i = 0; i < userList.size(); i++) {
			int status=userList.get(i).getInt("receive");
			if (status==0) {
				a=1; //有未接收的人员时,跳出循环,文件状态仍然是"待领取"
				break;
			}
		}
		//根据a的值判断文件是否已经全部接受
		Record file=Db.use("dc").findById("tech_files", receive.getInt("tech_files_id"));
		if (a==0) {
			file.set("sp_status", 4);//全部接收,状态变为4-使用中
			Db.use("dc").update("tech_files", file);
			//全部接收时，通知知会人
			String sql_zhihui="select * from zhihui_users where tech_files_id="+file.getInt("id");
			List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
			String zhiHui="";
			for (int i = 0; i < zhihuiList.size(); i++) {
				zhiHui+=zhihuiList.get(i).getStr("zhihui_userId")+",";
			}
			DingUtil.sendText(zhiHui, "知会通知：生产文件“ "+file.getStr("file_name")+" ” 已经全部领取，请知悉！"+date);
		}
		Record record=new Record();
		record.set("code", 0);
		record.set("msg", "文件接收成功！");
		record.set("data", "");
		renderJson(record);
	}
	//获取文件详情
	public void getScFile() {
		int receiveId=getParaToInt("receiveId");
		Record receive=Db.use("dc").findById("tech_files_receive", receiveId);
		Record fileInfo=Db.use("dc").findById("tech_files", receive.getInt("tech_files_id"));
		Record record=new Record();
		record.set("code", 0);
		record.set("msg", "文件获取成功！");
		record.set("data", fileInfo);
		renderJson(record);
	}

}
