package com.simple.controller.XiangMuFile;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.OapiUserGetRequest;
import com.dingtalk.api.response.OapiUserGetResponse;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.upload.UploadFile;
import com.simple.common.model.XiangmuZhihuiUsers;
import com.simple.ding.DingController;
import com.simple.ding.DingUtil;
import com.taobao.api.ApiException;

public class XiangMuPublish extends Controller{
	//指向项目文件列表页面
	public void toXiangMuList() {
		render("xiangmuFileList.html");
	}
	//获取质量文件列表数据
	public void getXiangMuFileList() {
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		StringBuilder sb=new StringBuilder();
		int page=getInt("page");
		int limit=getInt("limit");
		Record jsp=new Record();
		jsp.set("code", 0);
		String sql_userRole="select * from user_role  where user_id='"+user.getUserid()+"' and role_id=1";
		List<Record> userRoleList=Db.use("dc").find(sql_userRole);
		if (userRoleList.size()>0) {
			sb.append(" from xiangmu_tech_file where 1=1 ");
		}else {
			sb.append(" from xiangmu_tech_file where (create_user in (select original_user_id from same_auth where share_user_id='"+user.getUserid()+"' and status=0) or create_user='"+user.getUserid()+"')");
		}
		int isShenban=getParaToInt("isShenban");
		if (isShenban==1) {
			sb.append(" and (release_type=0 or release_type=1) and sp_status=4 ");
		}
		//sb.append(" from xiangmu_tech_file where create_user='"+user.getUserid()+"'");
		if (!"".equals(get("file_name")) && get("file_name")!=null) {
			sb.append(" and file_name like '%"+get("file_name")+"%'");
		}
		if (!"".equals(get("zc_pro_code")) && get("zc_pro_code")!=null) {
			sb.append(" and nb_zc_Code like '%"+get("zc_pro_code")+"%'");
		}
		if (!"".equals(get("nb_file_code")) && get("nb_file_code")!=null) {
			sb.append(" and nb_fileCode like '%"+get("nb_file_code")+"%'");
		}
		Page<Record> allFile=Db.use("dc").paginate(page, limit, "select *,use_cycle-DATEDIFF(NOW(),create_time) AS sy ",sb.toString()+" order by id desc");
		jsp.set("count", allFile.getTotalRow());
		jsp.set("data", allFile.getList());
		jsp.set("msg", "查询文件成功");
		renderJson(jsp);
	}
	//删除发布文件
	public void deletePublishFile() {
		int file_id=getParaToInt(0);
		Record file=Db.use("dc").findById("xiangmu_tech_file", file_id);
		Db.use("dc").delete("xiangmu_tech_file", file);
		//删除接收人表数据
		String sql_user="select * from xiangmu_tech_files_receive where xiangmu_tech_files_id="+file_id;
		List<Record> userList=Db.use("dc").find(sql_user);
		
		for (int i = 0; i < userList.size(); i++) {
			String sql_delUser="delete from xiangmu_tech_files_receive where id="+userList.get(i).getInt("id");
			Db.use("dc").delete(sql_delUser);
		}
		//删除知会人表数据
		String sql_zhihui="select * from xiangmu_zhihui_users where xiangmu_tech_files_id="+file_id;
		List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
		for (int i = 0; i < zhihuiList.size(); i++) {
			String sql_delZhi="delete from xiangmu_zhihui_users where id ='"+zhihuiList.get(i).getInt("id")+"'";
			Db.use("dc").delete(sql_delZhi);
		}
		Record rd=new Record();
		rd.set("code", 0);
		rd.set("msg", "文件已删除！");
		rd.set("data","" );
		renderJson(rd);
	}
	//指向历史版本记录页面
	public void historyList() {
		int id=getParaToInt(0);
		Record file=Db.use("dc").findById("xiangmu_tech_file", id);
		setAttr("file", file);
		render("xiangmuFile_history.html");
	}
	//获取历史版本信息
	public void getHistory() {
		int id=getParaToInt(0);
		Record file=Db.use("dc").findById("xiangmu_tech_file",id);
		String nb_fileCode=file.getStr("nb_fileCode");
		String sql_file=" from xiangmu_tech_file where nb_fileCode='"+nb_fileCode+"'";
		int page=getInt("page");
		int limit=getInt("limit");
		Page<Record> fileList=Db.use("dc").paginate(page, limit, "select * ",sql_file);
		Record rd=new Record();
		rd.set("code", 0);
		rd.set("msg", "获取历史文件成功！");
		rd.set("count", fileList.getTotalRow());
		rd.set("data",fileList.getList());
		renderJson(rd);
	}
	//正式文件详情
	public void zsfileDetail() {
		int id=getParaToInt(0);
		Record file=Db.use("dc").findById("xiangmu_tech_file", id);
		int is_change=file.getInt("is_change");
		if (is_change==0) {
			setAttr("changeName", "否");
		}else {
			setAttr("changeName", "是");
		}
		long chang_fujian_id=file.getInt("chang_fujian_id");
		if (chang_fujian_id==0) {
			setAttr("changeFujian", "非变更文件");
		}else {
			setAttr("changeFujian", fujian(file.getInt("chang_fujian_id")));			
		}
		setAttr("file", file);
		String accessToken_user=DingController.getAccessToken();
		DingTalkClient client_user = new DefaultDingTalkClient("https://oapi.dingtalk.com/user/get");
		//创建人
		String create_userId=file.getStr("create_user");
		OapiUserGetRequest create_user=new OapiUserGetRequest();
		create_user.setUserid(create_userId);
		create_user.setHttpMethod("GET");
		OapiUserGetResponse  response_create=null;
		try {
			response_create=client_user.execute(create_user,accessToken_user);
		} catch (ApiException e) {
			e.printStackTrace();
		}
		setAttr("create_user", response_create.getName());
		//审批人
		String sp_userId=file.getStr("sp_user_id");
		OapiUserGetRequest request_user = new OapiUserGetRequest();
		request_user.setUserid(sp_userId);
		request_user.setHttpMethod("GET");
		OapiUserGetResponse  response_user = null;
		try {
			response_user = client_user.execute(request_user, accessToken_user);
		}catch(ApiException e) {
			e.printStackTrace();
		}
		//打印人
		String print_user_id=file.getStr("print_user_id");
		OapiUserGetRequest request_print = new OapiUserGetRequest();
		request_print.setUserid(print_user_id);
		request_print.setHttpMethod("GET");
		OapiUserGetResponse  response_print = null;
		try {
			response_print = client_user.execute(request_print, accessToken_user);
		}catch(ApiException e) {
			e.printStackTrace();
		}
		setAttr("sp_name", response_user.getName());
		setAttr("print_user", response_print.getName());
		setAttr("fujianName", fujian(file.getInt("fujian_id")));
		render("zsfileDetail.html");
	}
	//获取旧版文件接收人
	public void getOldReceiver() {
		int id=getParaToInt(0);
		String sql_user="select * from xiangmu_tech_files_receive where xiangmu_tech_files_id="+id;
		List<Record> file_receive=Db.use("dc").find(sql_user);
		Record rd=new Record();
		rd.set("code", 0);
		rd.set("msg", "获取接收人列表成功！");
		rd.set("data",file_receive );
		renderJson(rd);
	}
	//获取旧版文件知会人
	public void getOldZhiHui() {
		int id=getParaToInt(0);
		String sql_user="select * from xiangmu_zhihui_users where xiangmu_tech_files_id="+id;
		List<Record> file_zhihui=Db.use("dc").find(sql_user);
		Record rd=new Record();
		rd.set("code", 0);
		rd.set("msg", "获取知会人列表成功！");
		rd.set("data",file_zhihui );
		renderJson(rd);
	}
	//临时文件详情
	public void lsfileDetail() {
		int id=getParaToInt(0);
		Record file=Db.use("dc").findById("xiangmu_tech_file", id);
		int is_change=file.getInt("is_change");
		if (is_change==0) {
			setAttr("changeName", "否");
		}else {
			setAttr("changeName", "是");
		}
		long chang_fujian_id=file.getInt("chang_fujian_id");
		if (chang_fujian_id==0) {
			setAttr("changeFujian", "非变更文件");
		}else {
			setAttr("changeFujian", fujian(file.getInt("chang_fujian_id")));			
		}
		setAttr("file", file);
		String accessToken_user=DingController.getAccessToken();
		DingTalkClient client_user = new DefaultDingTalkClient("https://oapi.dingtalk.com/user/get");
		//创建人
		String create_userId=file.getStr("create_user");
		OapiUserGetRequest create_user=new OapiUserGetRequest();
		create_user.setUserid(create_userId);
		create_user.setHttpMethod("GET");
		OapiUserGetResponse  response_create=null;
		try {
			response_create=client_user.execute(create_user,accessToken_user);
		} catch (ApiException e) {
			e.printStackTrace();
		}
		setAttr("create_user", response_create.getName());
		//审批人
		String sp_userId=file.getStr("sp_user_id");
		OapiUserGetRequest request_user = new OapiUserGetRequest();
		request_user.setUserid(sp_userId);
		request_user.setHttpMethod("GET");
		OapiUserGetResponse  response_user = null;
		try {
			response_user = client_user.execute(request_user, accessToken_user);
		}catch(ApiException e) {
			e.printStackTrace();
		}
		//打印人
		String print_user_id=file.getStr("print_user_id");
		OapiUserGetRequest request_print = new OapiUserGetRequest();
		request_print.setUserid(print_user_id);
		request_print.setHttpMethod("GET");
		OapiUserGetResponse  response_print = null;
		try {
			response_print = client_user.execute(request_print, accessToken_user);
		}catch(ApiException e) {
			e.printStackTrace();
		}
		setAttr("sp_name", response_user.getName());
		setAttr("print_user", response_print.getName());
		setAttr("fujianName", fujian(file.getInt("fujian_id")));
		render("lsfileDetail.html");
	}
	//驳回之后发起审批
	public void faqiShenPi() {
		int id=getParaToInt(0);
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		String tech_fileId=Integer.valueOf(id).toString();
		Record file=Db.use("dc").findById("xiangmu_tech_file", id);
		file.set("sp_status", 0);
		Db.use("dc").update("xiangmu_tech_file",file);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
		Date d= new Date();
		String date = sdf.format(d);
		//发起审批
		DingUtil.sendText(file.getStr("sp_user_id"), "审批通知：项目文件“ "+file.getStr("file_name")+" ” 已发布，请登录电脑端文件管理系统进行审批，请知悉！"+date);
//		String process_instance_id="";
//		String fbFile_type="项目文件";
//		try {
//			process_instance_id=ShenPiUtil.printShenPi(user.getUserid(),tech_fileId,file.getStr("nb_fileCode"),file.getStr("file_name"),file.getStr("edition"),file.getStr("sp_user_id"),fbFile_type,printUser(id));
//		}catch(Exception e) {
//			e.printStackTrace();
//		}
		//通知知会人
		String sql_zhihui="select * from xiangmu_zhihui_users where xiangmu_tech_files_id="+file;
		List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
		for (int i = 0; i < zhihuiList.size(); i++) {
			DingUtil.sendText(zhihuiList.get(i).getStr("zhihui_userId"), "知会通知：项目文件“ "+file.getStr("file_name")+" ” 已重新发起审批，请知悉！"+date);
		}
		Record rd=new Record();
		rd.set("code", 0);
		rd.set("msg", "发起审批成功！");
		rd.set("data","" );
		renderJson(rd);
	}
	//正式-首次发布文件编辑
	public void toEditZsTech() {
		int id=getParaToInt(0);
		Record file=Db.use("dc").findById("xiangmu_tech_file", id);
		int is_change=file.getInt("is_change");
		if (is_change==0) {
			setAttr("changeName", "否");
		}else {
			setAttr("changeName", "是");
		}
		long chang_fujian_id=file.getInt("chang_fujian_id");
		if (chang_fujian_id==0) {
			setAttr("changeFujian", "非变更文件");
		}else {
			setAttr("changeFujian", fujian(file.getInt("chang_fujian_id")));			
		}
		setAttr("file", file);
		setAttr("zcProCodeList", getZcProCode());
		setAttr("shenpi", getShenPi());
		setAttr("dayin", wkUserList());
		setAttr("fujianName", fujian(file.getInt("fujian_id")));
		render("EditZsFile.html");
	}
	//删除原有接收人
	public void delReceiveUser() {
		int id=getParaToInt(0);
		Record user=Db.use("dc").findById("xiangmu_tech_files_receive", id);
		Db.use("dc").delete("xiangmu_tech_files_receive",user);
		Record rd=new Record();
		rd.set("code", 0);
		rd.set("msg", "删除接收人成功！");
		rd.set("data","" );
		renderJson(rd);
	}
	//删除原有知会人
	public void delZhiHuiUser() {
		int id=getParaToInt(0);
		Record user=Db.use("dc").findById("xiangmu_zhihui_users", id);
		Db.use("dc").delete("xiangmu_zhihui_users",user);
		Record rd=new Record();
		rd.set("code", 0);
		rd.set("msg", "删除知会人成功！");
		rd.set("data","" );
		renderJson(rd);
	}
	//正式-首次发布文件编辑处理
	public void doEditZsTechFile() {
		Date date=new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
		String d = sdf.format(date);
		Record rd=new Record();
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		int id=getParaToInt("id");
		String file_type=get("file_type");
		String zc_pro_code=get("zc_pro_code");
		String nb_file_code=get("nb_file_code");
		String file_name=get("file_name");
		String edition=get("edition");
		int is_change=getParaToInt("is_change");
		int chang_fujian_id=getParaToInt("changePz_id");
		String purpose=get("purpose");
		String print_explain=get("print_explain");
		String sp_user_id=get("shenpi");
//		String xm_spUserId=get("xm_spUserId");//项目审批人id
//		String xm_spUserName=get("xm_spUserSel_rec");//项目审批人姓名
		int give_num=getParaToInt("give_num");
		int fujianId=getParaToInt("fujian_id");
		String print_user_id=get("dayin");
		//验证文件编码唯一性
		String sql_fileCode="select * from xiangmu_tech_file where nb_fileCode='"+nb_file_code+"' and sp_status=0 and id<>"+id;
		List<Record> files=Db.use("dc").find(sql_fileCode);
		if (files.size()==0) {
			rd.set("code", 0);
			rd.set("msg", "编辑文件成功！");
			rd.set("data","" );
			//修改tech_files表数据
			Record editZsFile=Db.use("dc").findById("xiangmu_tech_file", id);
			editZsFile.set("file_type", file_type);
			editZsFile.set("nb_zc_Code", zc_pro_code);
			editZsFile.set("nb_fileCode", nb_file_code);
			editZsFile.set("file_name", file_name);
			editZsFile.set("edition", edition);
			editZsFile.set("is_change", is_change);
			editZsFile.set("chang_fujian_id", chang_fujian_id);
			editZsFile.set("purpose", purpose);
			editZsFile.set("print_explain", print_explain);
			editZsFile.set("give_num", give_num);
			editZsFile.set("create_time", date);
			editZsFile.set("sp_user_id", sp_user_id);
			editZsFile.set("print_user_id", print_user_id);
			editZsFile.set("fujian_id",fujianId); //附件id
//			editZsFile.set("xm_sp_user_id", xm_spUserId);//项目管理审核
//			editZsFile.set("xm_sp_user_name", xm_spUserName);//项目管理审核
//			editZsFile.set("sp_status", 20);//待审核
			editZsFile.set("sp_status", 0);
			Db.use("dc").update("xiangmu_tech_file", editZsFile);
			//获取接收员
			String userIds=get("userId");
			if(!"0".equals(userIds)) {
				String userNames=get("userSel_rec");
				String[] userId=userIds.split("\\,");
				String[] userName=userNames.split("\\,");
				for (int i = 0; i < userId.length; i++) {
					Record receiveUser=new Record();
					receiveUser.set("xiangmu_tech_files_id", id);
					receiveUser.set("receive_userId", userId[i]);
					receiveUser.set("receive_userName", userName[i]);
					Db.use("dc").save("xiangmu_tech_files_receive", receiveUser);
				}
			}
//			//获取知会人员
//			String zhiHuiUserId=get("zhiHuiUserId");
//			if (!"0".equals(zhiHuiUserId)) {
//				String zhihuiUserSel_rec=get("zhihuiUserSel_rec");
//				String[] zhiHuiUserIds=zhiHuiUserId.split("\\,");
//				String[] zhihuiUserSel_recs=zhihuiUserSel_rec.split("\\,");
//				for (int i = 0; i < zhiHuiUserIds.length; i++) {
//					Record zhiHuiUser=new Record();
//					zhiHuiUser.set("xiangmu_tech_files_id", id);
//					zhiHuiUser.set("zhihui_userId", zhiHuiUserIds[i]);
//					zhiHuiUser.set("zhihui_userName", zhihuiUserSel_recs[i]);
//					Db.use("dc").save("xiangmu_zhihui_users", zhiHuiUser);
//				}
//			}
			String zhiHuiUserId1=get("zhiHuiUserId1");
			if (!"0".equals(zhiHuiUserId1)) {
				String zhihuiUserSel_rec1=get("zhihuiUserSel_rec1");
				String[] zhiHuiIds=zhiHuiUserId1.split(",");
				String[] zhiHuiNames=zhihuiUserSel_rec1.split(",");
				for (int i = 0; i < zhiHuiNames.length; i++) {
					XiangmuZhihuiUsers xiangmuZhihuiUsers =getModel(XiangmuZhihuiUsers.class);
					xiangmuZhihuiUsers.setXiangmuTechFilesId(id)
					.setZhihuiUserid(zhiHuiIds[i]).setZhihuiUsername(zhiHuiNames[i])
					.setType(1);
					xiangmuZhihuiUsers.save();
				}
			}
			//不可查看附件
			String zhiHuiUserId2=get("zhiHuiUserId2");
			if (!"0".equals(zhiHuiUserId2)) {
				String zhihuiUserSel_rec2=get("zhihuiUserSel_rec2");
				String[] zhiHuiIds=zhiHuiUserId2.split(",");
				String[] zhiHuiNames=zhihuiUserSel_rec2.split(",");
				for (int i = 0; i < zhiHuiNames.length; i++) {
					XiangmuZhihuiUsers xiangmuZhihuiUsers =getModel(XiangmuZhihuiUsers.class);
					xiangmuZhihuiUsers.setXiangmuTechFilesId(id)
					.setZhihuiUserid(zhiHuiIds[i]).setZhihuiUsername(zhiHuiNames[i])
					.setType(0);
					xiangmuZhihuiUsers.save();
				}
			}
			//编辑记录表中新增记录
			Record editRecord=new Record();
			editRecord.set("edit_userId", user.getUserid());
			editRecord.set("edit_userName", user.getName());
			editRecord.set("xiangmu_tech_file_id", id);
			editRecord.set("xiangmu_tech_file_code", nb_file_code);
			editRecord.set("create_time", date);
			Db.use("dc").save("xiangmu_edit_tech_file", editRecord);
			//发起审批
			DingUtil.sendText(editZsFile.getStr("sp_user_id"), "审批通知：项目文件“ "+editZsFile.getStr("file_name")+" ” 已发布，请登录电脑端文件管理系统进行审批，请知悉！"+date);
//			String tech_fileId=Integer.valueOf(id).toString();
//			String process_instance_id="";
//			String fbFile_type="项目文件";
//			try {
//				process_instance_id=ShenPiUtil.printShenPi(user.getUserid(),tech_fileId,nb_file_code,file_name,edition,sp_user_id,fbFile_type,print_user_id);
//			}catch(Exception e) {
//				e.printStackTrace();
//			}
			//通知项目管理审核
//			DingUtil.sendText(xm_spUserId, "项目审核通知：项目文件“ "+editZsFile.getStr("file_name")+" ” 已重新编辑并发起项目审核，请登陆文件管理系统进行审核！"+d);
			//编辑完成之后知会
			String sql_zhihui="select * from xiangmu_zhihui_users where xiangmu_tech_files_id="+id;
			List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
			for (int i = 0; i < zhihuiList.size(); i++) {
				DingUtil.sendText(zhihuiList.get(i).getStr("zhihui_userId"), "知会通知：项目文件“ "+editZsFile.getStr("file_name")+" ” 已重新发起审批，请知悉！"+d);
			}
		}else {
			rd.set("code", 1);
			rd.set("msg", "内部文件号已存在，请重新输入！");
			rd.set("data","" );
		}
		renderJson(rd);
	}
	//替换升版发布文件编辑
	public void toEditSbTech() {
		int id=getParaToInt(0);
		Record file=Db.use("dc").findById("xiangmu_tech_file", id);
		int is_change=file.getInt("is_change");
		if (is_change==0) {
			setAttr("changeName", "否");
		}else {
			setAttr("changeName", "是");
		}
		long chang_fujian_id=file.getInt("chang_fujian_id");
		if (chang_fujian_id==0) {
			setAttr("changeFujian", "非变更文件");
		}else {
			setAttr("changeFujian", fujian(file.getInt("chang_fujian_id")));			
		}
		setAttr("file", file);
		setAttr("shenpi", getShenPi());
		setAttr("dayin", wkUserList());
		setAttr("fujianName", fujian(file.getInt("fujian_id")));
		render("EditSbFile.html");
	}
	//正式-升版文件发布编辑处理
	public void doEditSbTechFile() {
		Date date=new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
		String d = sdf.format(date);
		Record rd=new Record();
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		int id=getParaToInt("id");
		String file_type=get("file_type");
		String zc_pro_code=get("zc_pro_code");
		String nb_file_code=get("nb_file_code");
		String file_name=get("file_name");
		String edition=get("edition");
		int is_change=getParaToInt("is_change");
		int chang_fujian_id=getParaToInt("changePz_id");
		String purpose=get("purpose");
		String print_explain=get("print_explain");
		String sp_user_id=get("shenpi");
//		String xm_spUserId=get("xm_spUserId");//项目审批人id
//		String xm_spUserName=get("xm_spUserSel_rec");//项目审批人姓名
		String print_user_id=get("dayin");
		int give_num=getParaToInt("give_num");
		int fujianId=getParaToInt("fujian_id");
		rd.set("code", 0);
		rd.set("msg", "编辑文件成功！");
		rd.set("data","" );
		//修改tech_files表数据
		Record editSbFile=Db.use("dc").findById("xiangmu_tech_file", id);
		editSbFile.set("file_type", file_type);
		editSbFile.set("nb_zc_Code", zc_pro_code);
		editSbFile.set("nb_fileCode", nb_file_code);
		editSbFile.set("file_name", file_name);
		editSbFile.set("edition", edition);
		editSbFile.set("is_change", is_change);
		editSbFile.set("chang_fujian_id", chang_fujian_id);
		editSbFile.set("purpose", purpose);
		editSbFile.set("print_explain", print_explain);
		editSbFile.set("give_num", give_num);
		editSbFile.set("create_time", date);
		editSbFile.set("sp_user_id", sp_user_id);
		editSbFile.set("print_user_id", print_user_id);
		editSbFile.set("fujian_id",fujianId); //附件id
//		editSbFile.set("xm_sp_user_id", xm_spUserId);//项目管理审核
//		editSbFile.set("xm_sp_user_name", xm_spUserName);//项目管理审核
//		editSbFile.set("sp_status", 20);//待审核
		editSbFile.set("sp_status", 0);
		Db.use("dc").update("xiangmu_tech_file", editSbFile);
		//获取接收员
		String userIds=get("userId");
		if(!"0".equals(userIds)) {
			String userNames=get("userSel_rec");
			String[] userId=userIds.split("\\,");
			String[] userName=userNames.split("\\,");
			for (int i = 0; i < userId.length; i++) {
				Record receiveUser=new Record();
				receiveUser.set("xiangmu_tech_files_id", id);
				receiveUser.set("receive_userId", userId[i]);
				receiveUser.set("receive_userName", userName[i]);
				Db.use("dc").save("xiangmu_tech_files_receive", receiveUser);
			}
		}
//		//获取知会人员
//		String zhiHuiUserId=get("zhiHuiUserId");
//		if (!"0".equals(zhiHuiUserId)) {
//			String zhihuiUserSel_rec=get("zhihuiUserSel_rec");
//			String[] zhiHuiUserIds=zhiHuiUserId.split("\\,");
//			String[] zhihuiUserSel_recs=zhihuiUserSel_rec.split("\\,");
//			for (int i = 0; i < zhiHuiUserIds.length; i++) {
//				Record zhiHuiUser=new Record();
//				zhiHuiUser.set("xiangmu_tech_files_id", id);
//				zhiHuiUser.set("zhihui_userId", zhiHuiUserIds[i]);
//				zhiHuiUser.set("zhihui_userName", zhihuiUserSel_recs[i]);
//				Db.use("dc").save("xiangmu_zhihui_users", zhiHuiUser);
//			}
//		}
		String zhiHuiUserId1=get("zhiHuiUserId1");
		if (!"0".equals(zhiHuiUserId1)) {
			String zhihuiUserSel_rec1=get("zhihuiUserSel_rec1");
			String[] zhiHuiIds=zhiHuiUserId1.split(",");
			String[] zhiHuiNames=zhihuiUserSel_rec1.split(",");
			for (int i = 0; i < zhiHuiNames.length; i++) {
				XiangmuZhihuiUsers xiangmuZhihuiUsers =getModel(XiangmuZhihuiUsers.class);
				xiangmuZhihuiUsers.setXiangmuTechFilesId(id)
				.setZhihuiUserid(zhiHuiIds[i]).setZhihuiUsername(zhiHuiNames[i])
				.setType(1);
				xiangmuZhihuiUsers.save();
			}
		}
		//不可查看附件
		String zhiHuiUserId2=get("zhiHuiUserId2");
		if (!"0".equals(zhiHuiUserId2)) {
			String zhihuiUserSel_rec2=get("zhihuiUserSel_rec2");
			String[] zhiHuiIds=zhiHuiUserId2.split(",");
			String[] zhiHuiNames=zhihuiUserSel_rec2.split(",");
			for (int i = 0; i < zhiHuiNames.length; i++) {
				XiangmuZhihuiUsers xiangmuZhihuiUsers =getModel(XiangmuZhihuiUsers.class);
				xiangmuZhihuiUsers.setXiangmuTechFilesId(id)
				.setZhihuiUserid(zhiHuiIds[i]).setZhihuiUsername(zhiHuiNames[i])
				.setType(0);
				xiangmuZhihuiUsers.save();
			}
		}
		//编辑记录表中新增记录
		Record editRecord=new Record();
		editRecord.set("edit_userId", user.getUserid());
		editRecord.set("edit_userName", user.getName());
		editRecord.set("xiangmu_tech_file_id", id);
		editRecord.set("xiangmu_tech_file_code", nb_file_code);
		editRecord.set("create_time", date);
		Db.use("dc").save("xiangmu_edit_tech_file", editRecord);
		//发起审批
		DingUtil.sendText(editSbFile.getStr("sp_user_id"), "审批通知：项目文件“ "+editSbFile.getStr("file_name")+" ” 已发布，请登录电脑端文件管理系统进行审批，请知悉！"+date);
//		String tech_fileId=Integer.valueOf(id).toString();
//		String process_instance_id="";
//		String fbFile_type="项目文件";
//		try {
//			process_instance_id=ShenPiUtil.printShenPi(user.getUserid(),tech_fileId,nb_file_code,file_name,edition,sp_user_id,fbFile_type,print_user_id);
//		}catch(Exception e) {
//			e.printStackTrace();
//		}
		//通知项目管理审核
//		DingUtil.sendText(xm_spUserId, "项目审核通知：质量文件“ "+editSbFile.getStr("file_name")+" ” 已重新编辑并发起项目审核，请登陆文件管理系统进行审核！"+d);
		//编辑完成之后知会
		String sql_zhihui="select * from xiangmu_zhihui_users where xiangmu_tech_files_id="+id;
		List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
		for (int i = 0; i < zhihuiList.size(); i++) {
			DingUtil.sendText(zhihuiList.get(i).getStr("zhihui_userId"),  "知会通知：项目文件“ "+editSbFile.getStr("file_name")+" ” 已重新编辑并发起审批，请知悉！"+d);
		}
	renderJson(rd);
	}
	//临时文件编辑
	public void toEditLsTech() {
		int id=getParaToInt(0);
		Record file=Db.use("dc").findById("xiangmu_tech_file", id);
		int is_change=file.getInt("is_change");
		if (is_change==0) {
			setAttr("changeName", "否");
		}else {
			setAttr("changeName", "是");
		}
		long chang_fujian_id=file.getInt("chang_fujian_id");
		if (chang_fujian_id==0) {
			setAttr("changeFujian", "非变更文件");
		}else {
			setAttr("changeFujian", fujian(file.getInt("chang_fujian_id")));			
		}
		setAttr("file", file);
		setAttr("zcProCodeList", getZcProCode());
		setAttr("shenpi", getShenPi());
		setAttr("dayin", wkUserList());
		setAttr("fujianName", fujian(file.getInt("fujian_id")));
		render("EditLsFile.html");
	}
	//临时文件编辑处理
	public void doEditLsTechFile() {
		Date date=new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
		String d = sdf.format(date);
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		Record rd=new Record();
		//获取页面数据
		int id=getParaToInt("id");
		String file_type=get("file_type");
		String zc_pro_code=get("zc_pro_code");
		String nb_file_code=get("nb_file_code");
		String file_name=get("file_name");
		String edition=get("edition");
		int is_change=getParaToInt("is_change");
		int chang_fujian_id=getParaToInt("changePz_id");
		String purpose=get("purpose");
		String print_explain=get("print_explain");
		int int_use_cycle=getParaToInt("int_use_cycle");
		String unit_use_cycle=get("unit_use_cycle");
		int give_num=getParaToInt("give_num");
		int fujianId=getParaToInt("fujian_id");
		String sp_user_id=get("shenpi");
//		String xm_spUserId=get("xm_spUserId");//项目审批人id
//		String xm_spUserName=get("xm_spUserSel_rec");//项目审批人姓名
		String print_user_id=get("dayin");
		//验证文件编码唯一性
		String sql_fileCode="select * from xiangmu_tech_file where nb_fileCode='"+nb_file_code+"' and sp_status=0 and id<>"+id;
		List<Record> files=Db.use("dc").find(sql_fileCode);
		if (files.size()!=0) {
			rd.set("code", 1);
			rd.set("msg", "内部文件号已存在，请重新输入！");
			rd.set("data","" );
		}else {
			rd.set("code", 0);
			rd.set("msg", "临时文件发布成功！");
			rd.set("data","" );
			//修改tech_files表数据
			Record editLsFile=Db.use("dc").findById("xiangmu_tech_file", id);
			editLsFile.set("file_type", file_type);
			editLsFile.set("nb_zc_Code", zc_pro_code);
			editLsFile.set("nb_fileCode", nb_file_code);
			editLsFile.set("file_name", file_name);
			editLsFile.set("edition", edition);
			editLsFile.set("is_change", is_change);
			editLsFile.set("chang_fujian_id", chang_fujian_id);
			editLsFile.set("purpose", purpose);
			editLsFile.set("print_explain", print_explain);
			editLsFile.set("give_num", give_num);
			editLsFile.set("sp_user_id", sp_user_id);
			editLsFile.set("print_user_id", print_user_id);
			editLsFile.set("fujian_id",fujianId); //附件id
//			editLsFile.set("xm_sp_user_id", xm_spUserId);//项目管理审核
//			editLsFile.set("xm_sp_user_name", xm_spUserName);//项目管理审核
//			editLsFile.set("sp_status", 20);//待审核
			editLsFile.set("sp_status", 0);
			int use_cycle=0;
			if(unit_use_cycle.equals("天")) {
				use_cycle=int_use_cycle;
			}else if (unit_use_cycle.equals("周")) {
				use_cycle=int_use_cycle*7;
			}else if (unit_use_cycle.equals("月")) {
				use_cycle=int_use_cycle*30;
			}else if (unit_use_cycle.equals("年")) {
				use_cycle=int_use_cycle*365;
			}
			editLsFile.set("use_cycle", use_cycle);
			editLsFile.set("create_time", date);
			Db.use("dc").update("xiangmu_tech_file", editLsFile);
			//获取接收员
			String userIds=get("userId");
			if(!"0".equals(userIds)) {
				String userNames=get("userSel_rec");
				String[] userId=userIds.split("\\,");
				String[] userName=userNames.split("\\,");
				for (int i = 0; i < userId.length; i++) {
					Record receiveUser=new Record();
					receiveUser.set("xiangmu_tech_files_id", id);
					receiveUser.set("receive_userId", userId[i]);
					receiveUser.set("receive_userName", userName[i]);
					Db.use("dc").save("xiangmu_tech_files_receive", receiveUser);
				}
			}
//			//获取知会人员
//			String zhiHuiUserId=get("zhiHuiUserId");
//			if (!"0".equals(zhiHuiUserId)) {
//				String zhihuiUserSel_rec=get("zhihuiUserSel_rec");
//				String[] zhiHuiUserIds=zhiHuiUserId.split("\\,");
//				String[] zhihuiUserSel_recs=zhihuiUserSel_rec.split("\\,");
//				for (int i = 0; i < zhiHuiUserIds.length; i++) {
//					Record zhiHuiUser=new Record();
//					zhiHuiUser.set("xiangmu_tech_files_id", id);
//					zhiHuiUser.set("zhihui_userId", zhiHuiUserIds[i]);
//					zhiHuiUser.set("zhihui_userName", zhihuiUserSel_recs[i]);
//					Db.use("dc").save("xiangmu_zhihui_users", zhiHuiUser);
//				}
//			}
			String zhiHuiUserId1=get("zhiHuiUserId1");
			if (!"0".equals(zhiHuiUserId1)) {
				String zhihuiUserSel_rec1=get("zhihuiUserSel_rec1");
				String[] zhiHuiIds=zhiHuiUserId1.split(",");
				String[] zhiHuiNames=zhihuiUserSel_rec1.split(",");
				for (int i = 0; i < zhiHuiNames.length; i++) {
					XiangmuZhihuiUsers xiangmuZhihuiUsers =getModel(XiangmuZhihuiUsers.class);
					xiangmuZhihuiUsers.setXiangmuTechFilesId(id)
					.setZhihuiUserid(zhiHuiIds[i]).setZhihuiUsername(zhiHuiNames[i])
					.setType(1);
					xiangmuZhihuiUsers.save();
				}
			}
			//不可查看附件
			String zhiHuiUserId2=get("zhiHuiUserId2");
			if (!"0".equals(zhiHuiUserId2)) {
				String zhihuiUserSel_rec2=get("zhihuiUserSel_rec2");
				String[] zhiHuiIds=zhiHuiUserId2.split(",");
				String[] zhiHuiNames=zhihuiUserSel_rec2.split(",");
				for (int i = 0; i < zhiHuiNames.length; i++) {
					XiangmuZhihuiUsers xiangmuZhihuiUsers =getModel(XiangmuZhihuiUsers.class);
					xiangmuZhihuiUsers.setXiangmuTechFilesId(id)
					.setZhihuiUserid(zhiHuiIds[i]).setZhihuiUsername(zhiHuiNames[i])
					.setType(0);
					xiangmuZhihuiUsers.save();
				}
			}
			//编辑记录表中新增记录
			Record editRecord=new Record();
			editRecord.set("edit_userId", user.getUserid());
			editRecord.set("edit_userName", user.getName());
			editRecord.set("xiangmu_tech_file_id", id);
			editRecord.set("xiangmu_tech_file_code", nb_file_code);
			editRecord.set("create_time", date);
			Db.use("dc").save("xiangmu_edit_tech_file", editRecord);
			//发起审批
			DingUtil.sendText(editLsFile.getStr("sp_user_id"), "审批通知：项目文件“ "+editLsFile.getStr("file_name")+" ” 已发布，请登录电脑端文件管理系统进行审批，请知悉！"+date);
//			String tech_fileId=Integer.valueOf(id).toString();
//			String process_instance_id="";
//			String fbFile_type="质量文件";
//			try {
//				process_instance_id=ShenPiUtil.printShenPi(user.getUserid(),tech_fileId,nb_file_code,file_name,edition,sp_user_id,fbFile_type,print_user_id);
//			}catch(Exception e) {
//				e.printStackTrace();
//			}
			//通知项目管理审核
//			DingUtil.sendText(xm_spUserId, "项目审核通知：项目文件“ "+editLsFile.getStr("file_name")+" ” 已重新编辑并发起项目审核，请登陆文件管理系统进行审核！"+d);
			//编辑完成之后知会
			String sql_zhihui="select * from xiangmu_zhihui_users where xiangmu_tech_files_id="+id;
			List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
			for (int i = 0; i < zhihuiList.size(); i++) {
				DingUtil.sendText(zhihuiList.get(i).getStr("zhihui_userId"), "知会通知：项目文件“ "+editLsFile.getStr("file_name")+" ” 已重新编辑并发起审批，请知悉！"+d);
			}
		}
		renderJson(rd);
	}
	//指向升版页面
	public void toSB() {
		int id=getParaToInt(0);
		Record file=Db.use("dc").findById("xiangmu_tech_file", id);
		setAttr("file", file);
		setAttr("shenpi", getShenPi());
		setAttr("dayin", wkUserList());
		render("sbTechFile.html");
	}
	//处理升版逻辑
	public void doSbTechFile() {
		Date date=new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
		String d = sdf.format(date);
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		Record rd=new Record();
		String file_type=get("file_type");
		String nb_zc_Code=get("nb_zc_Code");
		String nb_fileCode=get("nb_fileCode");
		String file_name=get("file_name");
		String edition=get("edition");
		int is_change=getParaToInt("is_change");
		int chang_fujian_id=getParaToInt("changePz_id");
		String purpose=get("purpose");
		String print_explain=get("print_explain");
		int give_num=getParaToInt("give_num");
		int fujianId=getParaToInt("fujian_id");
		String sp_user_id=get("shenpi"); //审批人
//		String xm_spUserId=get("xm_spUserId");//项目审批人id
//		String xm_spUserName=get("xm_spUserSel_rec");//项目审批人姓名
		String print_user_id=get("dayin");
		String sql_can="select * from xiangmu_tech_file where nb_fileCode='"+nb_fileCode+"' and sp_status=0";
		List<Record> can_file=Db.use("dc").find(sql_can);
		if (can_file.size()>=1) {
			rd.set("code", 1);
			rd.set("msg", "已有相同文件编码的文件待审批,请勿重复提交！");
			rd.set("data","" );
		} else {
			//新增新版文件行
			Record sbFile=new Record();
			sbFile.set("release_type", 1);
			sbFile.set("file_type",file_type );
			sbFile.set("nb_zc_Code",nb_zc_Code );
			sbFile.set("nb_fileCode",nb_fileCode );
			sbFile.set("file_name",file_name );
			sbFile.set("is_change", is_change);
			sbFile.set("chang_fujian_id", chang_fujian_id);
			sbFile.set("edition",edition );
			sbFile.set("purpose",purpose );
			sbFile.set("print_explain",print_explain );
			sbFile.set("give_num",give_num );
			sbFile.set("create_user",user.getUserid() );
			sbFile.set("create_user_name",user.getName() );
			sbFile.set("create_time",date );
			sbFile.set("sp_user_id",sp_user_id );
//			sbFile.set("xm_sp_user_id", xm_spUserId);//项目管理审核
//			sbFile.set("xm_sp_user_name", xm_spUserName);//项目管理审核
//			sbFile.set("sp_status", 20);//待审核
			sbFile.set("sp_status", 0);
			sbFile.set("print_user_id", print_user_id);
			sbFile.set("fujian_id",fujianId); //附件id
			Db.use("dc").save("xiangmu_tech_file", sbFile);
			int sjFile_id=sbFile.getInt("id");
			String sj_file_id=Integer.valueOf(sjFile_id).toString();
			//获取接收员
			String userIds=get("userId");
			if(!"0".equals(userIds)) {
				String userNames=get("userSel_rec");
				String[] userId=userIds.split("\\,");
				String[] userName=userNames.split("\\,");
				for (int i = 0; i < userId.length; i++) {
					Record receiveUser=new Record();
					receiveUser.set("xiangmu_tech_files_id", sjFile_id);
					receiveUser.set("receive_userId", userId[i]);
					receiveUser.set("receive_userName", userName[i]);
					Db.use("dc").save("xiangmu_tech_files_receive", receiveUser);
				}
			}
//			//获取知会人员
//			String zhiHuiUserId=get("zhiHuiUserId");
//			if (!"0".equals(zhiHuiUserId)) {
//				String zhihuiUserSel_rec=get("zhihuiUserSel_rec");
//				String[] zhiHuiUserIds=zhiHuiUserId.split("\\,");
//				String[] zhihuiUserSel_recs=zhihuiUserSel_rec.split("\\,");
//				for (int i = 0; i < zhiHuiUserIds.length; i++) {
//					Record zhiHuiUser=new Record();
//					zhiHuiUser.set("xiangmu_tech_files_id", sjFile_id);
//					zhiHuiUser.set("zhihui_userId", zhiHuiUserIds[i]);
//					zhiHuiUser.set("zhihui_userName", zhihuiUserSel_recs[i]);
//					Db.use("dc").save("xiangmu_zhihui_users", zhiHuiUser);
//				}
//			}
			String zhiHuiUserId1=get("zhiHuiUserId1");
			if (!"0".equals(zhiHuiUserId1)) {
				String zhihuiUserSel_rec1=get("zhihuiUserSel_rec1");
				String[] zhiHuiIds=zhiHuiUserId1.split(",");
				String[] zhiHuiNames=zhihuiUserSel_rec1.split(",");
				for (int i = 0; i < zhiHuiNames.length; i++) {
					XiangmuZhihuiUsers xiangmuZhihuiUsers =getModel(XiangmuZhihuiUsers.class);
					xiangmuZhihuiUsers.setXiangmuTechFilesId(sjFile_id)
					.setZhihuiUserid(zhiHuiIds[i]).setZhihuiUsername(zhiHuiNames[i])
					.setType(1);
					xiangmuZhihuiUsers.save();
				}
			}
			//不可查看附件
			String zhiHuiUserId2=get("zhiHuiUserId2");
			if (!"0".equals(zhiHuiUserId2)) {
				String zhihuiUserSel_rec2=get("zhihuiUserSel_rec2");
				String[] zhiHuiIds=zhiHuiUserId2.split(",");
				String[] zhiHuiNames=zhihuiUserSel_rec2.split(",");
				for (int i = 0; i < zhiHuiNames.length; i++) {
					XiangmuZhihuiUsers xiangmuZhihuiUsers =getModel(XiangmuZhihuiUsers.class);
					xiangmuZhihuiUsers.setXiangmuTechFilesId(sjFile_id)
					.setZhihuiUserid(zhiHuiIds[i]).setZhihuiUsername(zhiHuiNames[i])
					.setType(0);
					xiangmuZhihuiUsers.save();
				}
			}
			//发起审批
			DingUtil.sendText(sbFile.getStr("sp_user_id"), "审批通知：项目文件“ "+sbFile.getStr("file_name")+" ” 已发布，请登录电脑端文件管理系统进行审批，请知悉！"+date);
//			String process_instance_id="";
//			String fbFile_type="质量文件";
//			try {
//				process_instance_id=ShenPiUtil.printShenPi(user.getUserid(),sj_file_id,nb_fileCode,file_name,edition,sp_user_id,fbFile_type,print_user_id);
//			}catch(Exception e) {
//				e.printStackTrace();
//			}
			//通知项目管理审核
//			DingUtil.sendText(xm_spUserId, "项目审核通知：新版本项目文件“ "+sbFile.getStr("file_name")+" ” 已发布成功，请登陆文件管理系统进行审核！"+d);
			//升版完成之后通知知会人
			String sql_zhihui="select * from xiangmu_zhihui_users where xiangmu_tech_files_id="+sjFile_id;
			List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
			for (int i = 0; i < zhihuiList.size(); i++) {
				DingUtil.sendText(zhihuiList.get(i).getStr("zhihui_userId"), "知会通知：新版本项目文件“ "+sbFile.getStr("file_name")+" ” 已发布等待审批，请知悉！"+d);
			}
			rd.set("code", 0);
			rd.set("msg", "替换升版成功,等待审批！");
			rd.set("data","" );
		}
		renderJson(rd);
	}
	//指向新增正式发布技术文件页面
	public void toAddTechFile() {
		setAttr("zcProCodeList", getZcProCode());
		setAttr("shenpi", getShenPi());
		setAttr("dayin", wkUserList());
		render("addXiangMuFile.html");
	}
	//附件上传
	public void fujianUpload() {
		Record rd=new Record();
		Date date=new Date();
		try {
			UploadFile file = getFile("file", "");
			String fileName=file.getFileName();
			OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
			Record fujian=new Record();
			fujian.set("fujian_name", fileName);
			fujian.set("create_userId", user.getUserid());
			fujian.set("create_userName", user.getName());
			fujian.set("create_time", date);
			Db.use("dc").save("xiangmu_tech_file_fujian", fujian);
			
			rd.set("msg","附件上传成功！");
			rd.set("code",0);
			rd.set("data", fujian);
		} catch (Exception e) {
			rd.set("code",1);
			rd.set("msg","附件上传失败！");
			rd.set("data", "");
		}
		renderJson(rd);
	}
	//正式文件首次发布处理逻辑
	public void doAddTechFile(){
		Date date=new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
		String d = sdf.format(date);
		Record rd=new Record();
		//当前用户
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		String file_type=get("file_type");
		String zc_pro_code=get("zc_pro_code");
		String nb_file_code=get("nb_file_code");
		String file_name=get("file_name");
		String edition=get("edition");
		int is_change=getParaToInt("is_change");
		int chang_fujian_id=getParaToInt("changePz_id");
		String purpose=get("purpose");
		String print_explain=get("print_explain");
		int give_num=getParaToInt("give_num");
//		String xm_spUserId=get("xm_spUserId");//项目审批人id
//		String xm_spUserName=get("xm_spUserSel_rec");//项目审批人姓名
		String sp_user_id=get("shenpi");
		String print_user_id=get("dayin");
		int fujianId=getParaToInt("fujian_id");
		//验证文件编码唯一性
		String sql_fileCode="select * from xiangmu_tech_file where nb_fileCode='"+nb_file_code+"'";
		List<Record> files=Db.use("dc").find(sql_fileCode);
		if (files.size()!=0) {
			rd.set("code", 1);
			rd.set("msg", "内部文件号已存在，请重新输入！");
			rd.set("data","" );
		}else {
			rd.set("code", 0);
			rd.set("msg", "正式文件首次发布成功！");
			rd.set("data","" );
			//技术文件表新增数据tech_files
			Record addFile=new Record();
			addFile.set("release_type", 0);
			addFile.set("file_type", file_type);
			addFile.set("nb_zc_Code", zc_pro_code);
			addFile.set("nb_fileCode", nb_file_code);
			addFile.set("file_name", file_name);
			addFile.set("edition", edition);
			addFile.set("is_change", is_change);
			addFile.set("chang_fujian_id", chang_fujian_id);
			addFile.set("purpose", purpose);
			addFile.set("print_explain", print_explain);
			addFile.set("give_num", give_num);
			addFile.set("create_time", date);
			addFile.set("sp_user_id", sp_user_id);
//			addFile.set("xm_sp_user_id", xm_spUserId);//项目管理审核
//			addFile.set("xm_sp_user_name", xm_spUserName);//项目管理审核
//			addFile.set("sp_status", 20);//待审核
			addFile.set("sp_status", 0);
			addFile.set("print_user_id", print_user_id);
			addFile.set("fujian_id",fujianId); //附件id
			addFile.set("create_user", user.getUserid());
			addFile.set("create_user_name",user.getName() );
			Db.use("dc").save("xiangmu_tech_file", addFile);
			//获取新增数据id
			int addFile_id=addFile.getInt("id");
			String tech_fileId=Integer.valueOf(addFile_id).toString();
			//获取接收员
			String userIds=get("userId");
			if(!"0".equals(userIds)) {
				String userNames=get("userSel_rec");
				String[] userId=userIds.split("\\,");
				String[] userName=userNames.split("\\,");
				for (int i = 0; i < userId.length; i++) {
					Record receiveUser=new Record();
					receiveUser.set("xiangmu_tech_files_id", addFile_id);
					receiveUser.set("receive_userId", userId[i]);
					receiveUser.set("receive_userName", userName[i]);
					Db.use("dc").save("xiangmu_tech_files_receive", receiveUser);
				}
			}
//			//获取知会人员
//			String zhiHuiUserId=get("zhiHuiUserId");
//			if (!"0".equals(zhiHuiUserId)) {
//				String zhihuiUserSel_rec=get("zhihuiUserSel_rec");
//				String[] zhiHuiUserIds=zhiHuiUserId.split("\\,");
//				String[] zhihuiUserSel_recs=zhihuiUserSel_rec.split("\\,");
//				for (int i = 0; i < zhiHuiUserIds.length; i++) {
//					Record zhiHuiUser=new Record();
//					zhiHuiUser.set("xiangmu_tech_files_id", addFile_id);
//					zhiHuiUser.set("zhihui_userId", zhiHuiUserIds[i]);
//					zhiHuiUser.set("zhihui_userName", zhihuiUserSel_recs[i]);
//					Db.use("dc").save("xiangmu_zhihui_users", zhiHuiUser);
//				}
//			}
			String zhiHuiUserId1=get("zhiHuiUserId1");
			if (!"0".equals(zhiHuiUserId1)) {
				String zhihuiUserSel_rec1=get("zhihuiUserSel_rec1");
				String[] zhiHuiIds=zhiHuiUserId1.split(",");
				String[] zhiHuiNames=zhihuiUserSel_rec1.split(",");
				for (int i = 0; i < zhiHuiNames.length; i++) {
					XiangmuZhihuiUsers xiangmuZhihuiUsers =getModel(XiangmuZhihuiUsers.class);
					xiangmuZhihuiUsers.setXiangmuTechFilesId(addFile_id)
					.setZhihuiUserid(zhiHuiIds[i]).setZhihuiUsername(zhiHuiNames[i])
					.setType(1);
					xiangmuZhihuiUsers.save();
				}
			}
			//不可查看附件
			String zhiHuiUserId2=get("zhiHuiUserId2");
			if (!"0".equals(zhiHuiUserId2)) {
				String zhihuiUserSel_rec2=get("zhihuiUserSel_rec2");
				String[] zhiHuiIds=zhiHuiUserId2.split(",");
				String[] zhiHuiNames=zhihuiUserSel_rec2.split(",");
				for (int i = 0; i < zhiHuiNames.length; i++) {
					XiangmuZhihuiUsers xiangmuZhihuiUsers =getModel(XiangmuZhihuiUsers.class);
					xiangmuZhihuiUsers.setXiangmuTechFilesId(addFile_id)
					.setZhihuiUserid(zhiHuiIds[i]).setZhihuiUsername(zhiHuiNames[i])
					.setType(0);
					xiangmuZhihuiUsers.save();
				}
			}
			//发起审批
			DingUtil.sendText(addFile.getStr("sp_user_id"), "审批通知：项目文件“ "+addFile.getStr("file_name")+" ” 已发布，请登录电脑端文件管理系统进行审批，请知悉！"+date);
//			String process_instance_id="";
//			String fbFile_type="项目文件";
//			try {
//				process_instance_id=ShenPiUtil.printShenPi(user.getUserid(),tech_fileId,nb_file_code,file_name,edition,sp_user_id,fbFile_type,print_user_id);
//			}catch(Exception e) {
//				e.printStackTrace();
//			}
			//通知项目管理审核
//			DingUtil.sendText(xm_spUserId, "项目审核通知：项目文件“ "+addFile.getStr("file_name")+" ” 已发布成功，请登陆文件管理系统进行审核！"+d);
			//发布之后通知知会人
			String sql_zhihui="select * from xiangmu_zhihui_users where xiangmu_tech_files_id="+addFile_id;
			List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
			for (int i = 0; i < zhihuiList.size(); i++) {
				DingUtil.sendText(zhihuiList.get(i).getStr("zhihui_userId"), "知会通知：项目文件“ "+addFile.getStr("file_name")+" ” 已发布等待审批，请知悉！"+d);
			}
		}
		renderJson(rd);
	}
	//指向新增临时发布技术文件页面
	public void toAddTempTechFile() {
		setAttr("zcProCodeList", getZcProCode());
		setAttr("shenpi", getShenPi());
		setAttr("dayin", wkUserList());
		render("addTempXiangMuFile.html");
	}
	//发布临时文件处理逻辑
	public void doAddTempTechFile() {
		Date date=new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
		String d = sdf.format(date);
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		Record rd=new Record();
		//获取页面数据
		String file_type=get("file_type");
		String zc_pro_code=get("zc_pro_code");
		String nb_file_code=get("nb_file_code");
		String file_name=get("file_name");
		String edition=get("edition");
		int is_change=getParaToInt("is_change");
		int chang_fujian_id=getParaToInt("changePz_id");
		String purpose=get("purpose");
		String print_explain=get("print_explain");
		int int_use_cycle=getParaToInt("int_use_cycle");
		String unit_use_cycle=get("unit_use_cycle");
		int give_num=getParaToInt("give_num");
//		String xm_spUserId=get("xm_spUserId");//项目审批人id
//		String xm_spUserName=get("xm_spUserSel_rec");//项目审批人姓名
		String sp_user_id=get("shenpi");
		String print_user_id=get("dayin");
		int fujianId=getParaToInt("fujian_id");
		//验证文件编码唯一性
		String sql_fileCode="select * from xiangmu_tech_file where nb_fileCode='"+nb_file_code+"'";
		List<Record> files=Db.use("dc").find(sql_fileCode);
		if (files.size()!=0) {
			rd.set("code", 1);
			rd.set("msg", "内部文件号已存在，请重新输入！");
			rd.set("data","" );
		}else {
			rd.set("code", 0);
			rd.set("msg", "临时文件发布成功！");
			rd.set("data","" );
			Record tempFile=new Record();
			//tech_files表新增临时发布文件
			tempFile.set("file_type", file_type);
			tempFile.set("nb_zc_Code", zc_pro_code);
			tempFile.set("nb_fileCode", nb_file_code);
			tempFile.set("file_name", file_name);
			tempFile.set("edition", edition);
			tempFile.set("is_change", is_change);
			tempFile.set("chang_fujian_id", chang_fujian_id);
			tempFile.set("purpose", purpose);
			tempFile.set("give_num", give_num);
			tempFile.set("print_explain", print_explain);
			tempFile.set("release_type", 2); //临时文件
//			tempFile.set("xm_sp_user_id", xm_spUserId);//项目管理审核
//			tempFile.set("xm_sp_user_name", xm_spUserName);//项目管理审核
//			tempFile.set("sp_status", 20);//待审核
			tempFile.set("sp_status", 0);
			tempFile.set("sp_user_id", sp_user_id);
			tempFile.set("print_user_id", print_user_id);
			tempFile.set("fujian_id",fujianId); //附件id
			tempFile.set("create_user", user.getUserid());
			tempFile.set("create_user_name",user.getName() );
			int use_cycle=0;
			if(unit_use_cycle.equals("天")) {
				use_cycle=int_use_cycle;
			}else if (unit_use_cycle.equals("周")) {
				use_cycle=int_use_cycle*7;
			}else if (unit_use_cycle.equals("月")) {
				use_cycle=int_use_cycle*30;
			}else if (unit_use_cycle.equals("年")) {
				use_cycle=int_use_cycle*365;
			}
			tempFile.set("use_cycle", use_cycle);
			tempFile.set("create_time", date);
			Db.use("dc").save("xiangmu_tech_file", tempFile);
			//获取新增数据id
			int fileId=tempFile.getInt("id");
			String tech_fileId=Integer.valueOf(fileId).toString();
			//获取接收员
			String userIds=get("userId");
			if(!"0".equals(userIds)) {
				String userNames=get("userSel_rec");
				String[] userId=userIds.split("\\,");
				String[] userName=userNames.split("\\,");
				for (int i = 0; i < userId.length; i++) {
					Record receiveUser=new Record();
					receiveUser.set("xiangmu_tech_files_id", fileId);
					receiveUser.set("receive_userId", userId[i]);
					receiveUser.set("receive_userName", userName[i]);
					Db.use("dc").save("xiangmu_tech_files_receive", receiveUser);
				}
			}
//			//获取知会人员
//			String zhiHuiUserId=get("zhiHuiUserId");
//			if (!"0".equals(zhiHuiUserId)) {
//				String zhihuiUserSel_rec=get("zhihuiUserSel_rec");
//				String[] zhiHuiUserIds=zhiHuiUserId.split("\\,");
//				String[] zhihuiUserSel_recs=zhihuiUserSel_rec.split("\\,");
//				for (int i = 0; i < zhiHuiUserIds.length; i++) {
//					Record zhiHuiUser=new Record();
//					zhiHuiUser.set("xiangmu_tech_files_id", fileId);
//					zhiHuiUser.set("zhihui_userId", zhiHuiUserIds[i]);
//					zhiHuiUser.set("zhihui_userName", zhihuiUserSel_recs[i]);
//					Db.use("dc").save("xiangmu_zhihui_users", zhiHuiUser);
//				}
//			}
			String zhiHuiUserId1=get("zhiHuiUserId1");
			if (!"0".equals(zhiHuiUserId1)) {
				String zhihuiUserSel_rec1=get("zhihuiUserSel_rec1");
				String[] zhiHuiIds=zhiHuiUserId1.split(",");
				String[] zhiHuiNames=zhihuiUserSel_rec1.split(",");
				for (int i = 0; i < zhiHuiNames.length; i++) {
					XiangmuZhihuiUsers xiangmuZhihuiUsers =getModel(XiangmuZhihuiUsers.class);
					xiangmuZhihuiUsers.setXiangmuTechFilesId(fileId)
					.setZhihuiUserid(zhiHuiIds[i]).setZhihuiUsername(zhiHuiNames[i])
					.setType(1);
					xiangmuZhihuiUsers.save();
				}
			}
			//不可查看附件
			String zhiHuiUserId2=get("zhiHuiUserId2");
			if (!"0".equals(zhiHuiUserId2)) {
				String zhihuiUserSel_rec2=get("zhihuiUserSel_rec2");
				String[] zhiHuiIds=zhiHuiUserId2.split(",");
				String[] zhiHuiNames=zhihuiUserSel_rec2.split(",");
				for (int i = 0; i < zhiHuiNames.length; i++) {
					XiangmuZhihuiUsers xiangmuZhihuiUsers =getModel(XiangmuZhihuiUsers.class);
					xiangmuZhihuiUsers.setXiangmuTechFilesId(fileId)
					.setZhihuiUserid(zhiHuiIds[i]).setZhihuiUsername(zhiHuiNames[i])
					.setType(0);
					xiangmuZhihuiUsers.save();
				}
			}
			//发起审批
			DingUtil.sendText(tempFile.getStr("sp_user_id"), "审批通知：项目文件“ "+tempFile.getStr("file_name")+" ” 已发布，请登录电脑端文件管理系统进行审批，请知悉！"+date);
//			String process_instance_id="";
//			String fbFile_type="项目文件";
//			try {
//				process_instance_id=ShenPiUtil.printShenPi(user.getUserid(),tech_fileId,nb_file_code,file_name,edition,sp_user_id,fbFile_type,print_user_id);
//			}catch(Exception e) {
//				e.printStackTrace();
//			}
			//通知项目管理审核
//			DingUtil.sendText(xm_spUserId, "项目审核通知：项目文件“ "+tempFile.getStr("file_name")+" ” 已发布成功，请登陆文件管理系统进行审核！"+d);
			//发布之后通知知会人
			String sql_zhihui="select * from xiangmu_zhihui_users where xiangmu_tech_files_id="+fileId;
			List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
			for (int i = 0; i < zhihuiList.size(); i++) {
				DingUtil.sendText(zhihuiList.get(i).getStr("zhihui_userId"),  "知会通知：项目文件“ "+tempFile.getStr("file_name")+" ” 已发布等待审批，请知悉！"+d);
			}
		}
		renderJson(rd);
	}
	//校验版本
	public void jyEdition() {
		String newEdition=get("newEdition");
		String nb_fileCode=get("nb_fileCode");
		String sql_file="select * from xiangmu_tech_file where nb_fileCode='"+nb_fileCode+"' and sp_status<>2";
		List<Record> fileList=Db.use("dc").find(sql_file);
		Record rd=new Record();
		rd.set("code", 0);
		rd.set("msg", "无相同版本号！");
		for (int i = 0; i < fileList.size(); i++) {
			if (newEdition.equals(fileList.get(i).get("edition"))) {
				rd.set("code", 1);
				rd.set("msg", "版本号不能和历史版本相同！");
				break;
			}
		}
		renderJson(rd);
	}
	/**
	 * 公共模块
	 */
	//获取附件
	public String fujian(int fujianId) {
		Record fujian=Db.use("dc").findById("xiangmu_tech_file_fujian", fujianId);
		String fujianName=fujian.getStr("fujian_name");
		return fujianName;
	}
	//获取文控角色用户列表
	public List<Record> wkUserList() {
		String sql_wkUser="select * from user_role where role_id=3";
		List<Record> wk_users=Db.use("dc").find(sql_wkUser);
		return wk_users;
	}
	//获取文件对应的文控打印人
	public String printUser(int fileId) {
		Record file=Db.use("dc").findById("xiangmu_tech_file", fileId);
		String print_user_id=file.getStr("print_user_id");
		return print_user_id;
	}
	//获取审批人
	public List<Record> getShenPi(){
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		String sql_sp="select * from user_role where role_id=8 and user_id<>'"+user.getUserid()+"'";
		//String sql_sp="select * from user_role where role_id=8 ";
		List<Record> spList=Db.use("dc").find(sql_sp);
		return spList;
	}
	//获取项目总成号
	public List<Record> getZcProCode(){
		String sql_zc="select * from codeaparts";
		List<Record> zcProList=Db.use("dc").find(sql_zc);
		return zcProList;
	}
}
