package com.simple.controller.techFileZhiHui;

import java.util.List;

import com.dingtalk.api.response.OapiUserGetResponse;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

public class FileZhiHuiController extends Controller{
	//指向当前用户知会列表页面
	public void toZhiHuiList() {
		render("zhihuiList.html");
	}
	//获取当前用户所有被知会的文件
	public void getZhiHuiList() {
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		StringBuilder sb=new StringBuilder();
		int page=getInt("page");
		int limit=getInt("limit");
		String sql_userRole="select * from user_role  where user_id='"+user.getUserid()+"' and role_id=1";
		List<Record> userRoleList=Db.use("dc").find(sql_userRole);
		if (userRoleList.size()>0) {
			sb.append(" from  (select b.*,a.type from caigou_zhihui_users a, caigou_tech_file b where  b.id=a.caigou_tech_files_id");
		}else {
			sb.append(" from  (select b.*,a.type from caigou_zhihui_users a, caigou_tech_file b where a.zhihui_userId='"+user.getUserid()+"' and b.id=a.caigou_tech_files_id");
		}
		//sb.append(" from  (select b.* from caigou_zhihui_users a, caigou_tech_file b where a.zhihui_userId='"+user.getUserid()+"' and b.id=a.caigou_tech_files_id");
		if (!"".equals(get("file_name")) && get("file_name")!=null) {
			sb.append(" and file_name like '%"+get("file_name")+"%'");
		}
		if (!"".equals(get("zc_pro_code")) && get("zc_pro_code")!=null) {
			sb.append(" and nb_zc_Code like '%"+get("zc_pro_code")+"%'");
		}
		if (!"".equals(get("nb_file_code")) && get("nb_file_code")!=null) {
			sb.append(" and nb_fileCode like '%"+get("nb_file_code")+"%'");
		}
		if (userRoleList.size()>0) {
			sb.append(" union all select b.*,a.type from kehu_zhihui_users a, kehu_tech_file b where  b.id=a.kehu_tech_files_id");
		}else {
			sb.append(" union all select b.*,a.type from kehu_zhihui_users a, kehu_tech_file b where a.zhihui_userId='"+user.getUserid()+"' and b.id=a.kehu_tech_files_id");
		}
		//sb.append(" union all select b.* from kehu_zhihui_users a, kehu_tech_file b where a.zhihui_userId='"+user.getUserid()+"' and b.id=a.kehu_tech_files_id");
		if (!"".equals(get("file_name")) && get("file_name")!=null) {
			sb.append(" and file_name like '%"+get("file_name")+"%'");
		}
		if (!"".equals(get("zc_pro_code")) && get("zc_pro_code")!=null) {
			sb.append(" and nb_zc_Code like '%"+get("zc_pro_code")+"%'");
		}
		if (!"".equals(get("nb_file_code")) && get("nb_file_code")!=null) {
			sb.append(" and nb_fileCode like '%"+get("nb_file_code")+"%'");
		}
		if (userRoleList.size()>0) {
			sb.append(" union all select b.*,a.type from zhihui_users a, tech_files b where  b.id=a.tech_files_id");
		}else {
			sb.append(" union all select b.*,a.type from zhihui_users a, tech_files b where a.zhihui_userId='"+user.getUserid()+"' and b.id=a.tech_files_id");
		}
		//sb.append(" union all select b.* from zhihui_users a, tech_files b where a.zhihui_userId='"+user.getUserid()+"' and b.id=a.tech_files_id");
		if (!"".equals(get("file_name")) && get("file_name")!=null) {
			sb.append(" and file_name like '%"+get("file_name")+"%'");
		}
		if (!"".equals(get("zc_pro_code")) && get("zc_pro_code")!=null) {
			sb.append(" and nb_zc_Code like '%"+get("zc_pro_code")+"%'");
		}
		if (!"".equals(get("nb_file_code")) && get("nb_file_code")!=null) {
			sb.append(" and nb_fileCode like '%"+get("nb_file_code")+"%'");
		}
		if (userRoleList.size()>0) {
			sb.append(" union all select b.*,a.type from xiangmu_zhihui_users a,  xiangmu_tech_file b where  b.id=a.xiangmu_tech_files_id");
		}else {
			sb.append(" union all select b.*,a.type from xiangmu_zhihui_users a,  xiangmu_tech_file b where  a.zhihui_userId='"+user.getUserid()+"' and b.id=a.xiangmu_tech_files_id");
		}
		//sb.append(" union all select b.* from xiangmu_zhihui_users a,  xiangmu_tech_file b where  a.zhihui_userId='"+user.getUserid()+"' and b.id=a.xiangmu_tech_files_id");
		if (!"".equals(get("file_name")) && get("file_name")!=null) {
			sb.append(" and file_name like '%"+get("file_name")+"%'");
		}
		if (!"".equals(get("zc_pro_code")) && get("zc_pro_code")!=null) {
			sb.append(" and nb_zc_Code like '%"+get("zc_pro_code")+"%'");
		}
		if (!"".equals(get("nb_file_code")) && get("nb_file_code")!=null) {
			sb.append(" and nb_fileCode like '%"+get("nb_file_code")+"%'");
		}
		if (userRoleList.size()>0) {
			sb.append(" union all select b.*,a.type from zhiliang_zhihui_users a,zhiliang_tech_file b where  b.id=a.zhiliang_tech_files_id");
		}else {
			sb.append(" union all select b.*,a.type from zhiliang_zhihui_users a,zhiliang_tech_file b where  a.zhihui_userId='"+user.getUserid()+"' and b.id=a.zhiliang_tech_files_id");
		}
		//sb.append(" union all select b.* from zhiliang_zhihui_users a,zhiliang_tech_file b where  a.zhihui_userId='"+user.getUserid()+"' and b.id=a.zhiliang_tech_files_id");
		if (!"".equals(get("file_name")) && get("file_name")!=null) {
			sb.append(" and file_name like '%"+get("file_name")+"%'");
		}
		if (!"".equals(get("zc_pro_code")) && get("zc_pro_code")!=null) {
			sb.append(" and nb_zc_Code like '%"+get("zc_pro_code")+"%'");
		}
		if (!"".equals(get("nb_file_code")) && get("nb_file_code")!=null) {
			sb.append(" and nb_fileCode like '%"+get("nb_file_code")+"%'");
		}
		Page<Record> ReceiveList=Db.use("dc").paginate(page, limit, "select *,  use_cycle-DATEDIFF(NOW(),create_time) AS sy ",sb.toString()+") temp order by create_time desc");
		Record record=new Record();
		record.set("code", 0);
		record.set("msg", "获取知会列表成功！");
		record.set("count", ReceiveList.getTotalRow());
		record.set("data", ReceiveList.getList());
		renderJson(record);
	}
}
