package com.simple.controller.ProjectShenHe;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.OapiUserGetRequest;
import com.dingtalk.api.response.OapiUserGetResponse;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.simple.ding.DingController;
import com.simple.ding.DingUtil;
import com.simple.file.MyFileRender;
import com.simple.util.ShenPiUtil;
import com.taobao.api.ApiException;

public class ProjectShenHeController extends Controller{
	//指向项目审核页面
	public void toShenHeList() {
		render("shenHeList.html");
	}
	//获取当前用户所有审核文件
	public void getShenHeList() {
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		StringBuilder sb=new StringBuilder();
		int page=getInt("page");
		int limit=getInt("limit");
		Record jsp=new Record();
		jsp.set("code", 0);
		String sql_userRole="select * from user_role  where user_id='"+user.getUserid()+"' and role_id=1";
		List<Record> userRoleList=Db.use("dc").find(sql_userRole);
		if (userRoleList.size()>0) {
			sb.append(" from  (select * from caigou_tech_file where 1=1 ");
		}else {
			sb.append(" from  (select * from caigou_tech_file where xm_sp_user_id='"+user.getUserid()+"' ");
		}
		//sb.append(" from  (select * from caigou_tech_file where xm_sp_user_id='"+user.getUserid()+"' ");
		if (!"".equals(get("file_name")) && get("file_name")!=null) {
			sb.append(" and file_name like '%"+get("file_name")+"%'");
		}
		if (!"".equals(get("zc_pro_code")) && get("zc_pro_code")!=null) {
			sb.append(" and nb_zc_Code like '%"+get("zc_pro_code")+"%'");
		}
		if (!"".equals(get("nb_file_code")) && get("nb_file_code")!=null) {
			sb.append(" and nb_fileCode like '%"+get("nb_file_code")+"%'");
		}
		if (userRoleList.size()>0) {
			sb.append(" union all select * from kehu_tech_file where 1=1 ");
		}else {
			sb.append(" union all select * from kehu_tech_file where xm_sp_user_id='"+user.getUserid()+"' ");
		}
		//sb.append(" union all select * from kehu_tech_file where xm_sp_user_id='"+user.getUserid()+"' ");
		if (!"".equals(get("file_name")) && get("file_name")!=null) {
			sb.append(" and file_name like '%"+get("file_name")+"%'");
		}
		if (!"".equals(get("zc_pro_code")) && get("zc_pro_code")!=null) {
			sb.append(" and nb_zc_Code like '%"+get("zc_pro_code")+"%'");
		}
		if (!"".equals(get("nb_file_code")) && get("nb_file_code")!=null) {
			sb.append(" and nb_fileCode like '%"+get("nb_file_code")+"%'");
		}
		if (userRoleList.size()>0) {
			sb.append(" union all select * from xiangmu_tech_file where 1=1 ");
		}else {
			sb.append(" union all select * from xiangmu_tech_file where xm_sp_user_id='"+user.getUserid()+"' ");
		}
		//sb.append(" union all select * from xiangmu_tech_file where xm_sp_user_id='"+user.getUserid()+"' ");
		if (!"".equals(get("file_name")) && get("file_name")!=null) {
			sb.append(" and file_name like '%"+get("file_name")+"%'");
		}
		if (!"".equals(get("zc_pro_code")) && get("zc_pro_code")!=null) {
			sb.append(" and nb_zc_Code like '%"+get("zc_pro_code")+"%'");
		}
		if (!"".equals(get("nb_file_code")) && get("nb_file_code")!=null) {
			sb.append(" and nb_fileCode like '%"+get("nb_file_code")+"%'");
		}
		if (userRoleList.size()>0) {
			sb.append(" union all select * from zhiliang_tech_file where 1=1 ");
		}else {
			sb.append(" union all select * from zhiliang_tech_file where xm_sp_user_id='"+user.getUserid()+"' ");
		}
		//sb.append(" union all select * from zhiliang_tech_file where xm_sp_user_id='"+user.getUserid()+"' ");
		if (!"".equals(get("file_name")) && get("file_name")!=null) {
			sb.append(" and file_name like '%"+get("file_name")+"%'");
		}
		if (!"".equals(get("zc_pro_code")) && get("zc_pro_code")!=null) {
			sb.append(" and nb_zc_Code like '%"+get("zc_pro_code")+"%'");
		}
		if (!"".equals(get("nb_file_code")) && get("nb_file_code")!=null) {
			sb.append(" and nb_fileCode like '%"+get("nb_file_code")+"%'");
		}
		Page<Record> shenHeList=Db.use("dc").paginate(page, limit, "select * ,use_cycle-DATEDIFF(NOW(),create_time) AS sy ",sb.toString()+") temp order by create_time desc");
		Record record=new Record();
		record.set("code", 0);
		record.set("msg", "获取审批表成功！");
		record.set("count", shenHeList.getTotalRow());
		record.set("data", shenHeList.getList());
		renderJson(record);
	}
	//审批列表页面获取历史记录
	public void historyList() {
		int id=getParaToInt("id");
		int status=getParaToInt("status");
		if (status==1) {
			Record file=Db.use("dc").findById("caigou_tech_file", id);
			setAttr("file", file);
			render("/page/caigou/caigouPublish/caigouFile_history.html");
		} else if (status==2){
			Record file=Db.use("dc").findById("kehu_tech_file", id);
			setAttr("file", file);
			render("/page/kehu/kehuPublish/kehuFile_history.html");
		}else if (status==4){
			Record file=Db.use("dc").findById("xiangmu_tech_file", id);
			setAttr("file", file);
			render("/page/xiangmu/xiangmuPublish/xiangmuFile_history.html");
		}else if (status==5){
			Record file=Db.use("dc").findById("zhiliang_tech_file", id);
			setAttr("file", file);
			render("/page/zhiliang/zhiliangPublish/zhiliangFile_history.html");
		}
	}
	//正式文件详情
	public void zsfileDetail() {
		int id=getParaToInt("id");
		int status=getParaToInt("status");
		if (status==1) {
			Record file=Db.use("dc").findById("caigou_tech_file", id);
			setAttr("file", file);
			String accessToken_user=DingController.getAccessToken();
			DingTalkClient client_user = new DefaultDingTalkClient("https://oapi.dingtalk.com/user/get");
			//创建人
			String create_userId=file.getStr("create_user");
			OapiUserGetRequest create_user=new OapiUserGetRequest();
			create_user.setUserid(create_userId);
			create_user.setHttpMethod("GET");
			OapiUserGetResponse  response_create=null;
			try {
				response_create=client_user.execute(create_user,accessToken_user);
			} catch (ApiException e) {
				e.printStackTrace();
			}
			setAttr("create_user", response_create.getName());
			//审批人
			String sp_userId=file.getStr("sp_user_id");
			OapiUserGetRequest request_user = new OapiUserGetRequest();
			request_user.setUserid(sp_userId);
			request_user.setHttpMethod("GET");
			OapiUserGetResponse  response_user = null;
			try {
				response_user = client_user.execute(request_user, accessToken_user);
			}catch(ApiException e) {
				e.printStackTrace();
			}
			//打印人
			String print_user_id=file.getStr("print_user_id");
			OapiUserGetRequest request_print = new OapiUserGetRequest();
			request_print.setUserid(print_user_id);
			request_print.setHttpMethod("GET");
			OapiUserGetResponse  response_print = null;
			try {
				response_print = client_user.execute(request_print, accessToken_user);
			}catch(ApiException e) {
				e.printStackTrace();
			}
			setAttr("sp_name", response_user.getName());
			setAttr("print_user", response_print.getName());
			Record fujian=Db.use("dc").findById("caigou_tech_file_fujian", file.getInt("fujian_id"));
			setAttr("fujianName", fujian.get("fujian_name"));
			render("/page/caigou/caigouPublish/zsfileDetail.html");
		} else if (status==2){
			Record file=Db.use("dc").findById("kehu_tech_file", id);
			setAttr("file", file);
			String accessToken_user=DingController.getAccessToken();
			DingTalkClient client_user = new DefaultDingTalkClient("https://oapi.dingtalk.com/user/get");
			//审批人
			String sp_userId=file.getStr("sp_user_id");
			OapiUserGetRequest request_user = new OapiUserGetRequest();
			request_user.setUserid(sp_userId);
			request_user.setHttpMethod("GET");
			OapiUserGetResponse  response_user = null;
			try {
				response_user = client_user.execute(request_user, accessToken_user);
			}catch(ApiException e) {
				e.printStackTrace();
			}
			//打印人
			String print_user_id=file.getStr("print_user_id");
			OapiUserGetRequest request_print = new OapiUserGetRequest();
			request_print.setUserid(print_user_id);
			request_print.setHttpMethod("GET");
			OapiUserGetResponse  response_print = null;
			try {
				response_print = client_user.execute(request_print, accessToken_user);
			}catch(ApiException e) {
				e.printStackTrace();
			}
			setAttr("sp_name", response_user.getName());
			setAttr("print_user", response_print.getName());
			Record fujian=Db.use("dc").findById("kehu_tech_file_fujian", file.getInt("fujian_id"));
			setAttr("fujianName", fujian.get("fujian_name"));
			render("/page/kehu/kehuPublish/zsfileDetail.html");
		}else if (status==4){
			Record file=Db.use("dc").findById("xiangmu_tech_file", id);
			setAttr("file", file);
			String accessToken_user=DingController.getAccessToken();
			DingTalkClient client_user = new DefaultDingTalkClient("https://oapi.dingtalk.com/user/get");
			//审批人
			String sp_userId=file.getStr("sp_user_id");
			OapiUserGetRequest request_user = new OapiUserGetRequest();
			request_user.setUserid(sp_userId);
			request_user.setHttpMethod("GET");
			OapiUserGetResponse  response_user = null;
			try {
				response_user = client_user.execute(request_user, accessToken_user);
			}catch(ApiException e) {
				e.printStackTrace();
			}
			//打印人
			String print_user_id=file.getStr("print_user_id");
			OapiUserGetRequest request_print = new OapiUserGetRequest();
			request_print.setUserid(print_user_id);
			request_print.setHttpMethod("GET");
			OapiUserGetResponse  response_print = null;
			try {
				response_print = client_user.execute(request_print, accessToken_user);
			}catch(ApiException e) {
				e.printStackTrace();
			}
			setAttr("sp_name", response_user.getName());
			setAttr("print_user", response_print.getName());
			Record fujian=Db.use("dc").findById("xiangmu_tech_file_fujian", file.getInt("fujian_id"));
			setAttr("fujianName", fujian.get("fujian_name"));
			render("/page/xiangmu/xiangmuPublish/zsfileDetail.html");
		}else if (status==5){
			Record file=Db.use("dc").findById("zhiliang_tech_file", id);
			setAttr("file", file);
			String accessToken_user=DingController.getAccessToken();
			DingTalkClient client_user = new DefaultDingTalkClient("https://oapi.dingtalk.com/user/get");
			//审批人
			String sp_userId=file.getStr("sp_user_id");
			OapiUserGetRequest request_user = new OapiUserGetRequest();
			request_user.setUserid(sp_userId);
			request_user.setHttpMethod("GET");
			OapiUserGetResponse  response_user = null;
			try {
				response_user = client_user.execute(request_user, accessToken_user);
			}catch(ApiException e) {
				e.printStackTrace();
			}
			//打印人
			String print_user_id=file.getStr("print_user_id");
			OapiUserGetRequest request_print = new OapiUserGetRequest();
			request_print.setUserid(print_user_id);
			request_print.setHttpMethod("GET");
			OapiUserGetResponse  response_print = null;
			try {
				response_print = client_user.execute(request_print, accessToken_user);
			}catch(ApiException e) {
				e.printStackTrace();
			}
			setAttr("sp_name", response_user.getName());
			setAttr("print_user", response_print.getName());
			Record fujian=Db.use("dc").findById("zhiliang_tech_file_fujian", file.getInt("fujian_id"));
			setAttr("fujianName", fujian.get("fujian_name"));
			render("/page/zhiliang/zhiliangPublish/zsfileDetail.html");
		}
	}
	//临时文件详情
	public void lsfileDetail() {
		int id=getParaToInt("id");
		int status=getParaToInt("status");
		if (status==1) {
			Record file=Db.use("dc").findById("caigou_tech_file", id);
			setAttr("file", file);
			String accessToken_user=DingController.getAccessToken();
			DingTalkClient client_user = new DefaultDingTalkClient("https://oapi.dingtalk.com/user/get");
			//创建人
			String create_userId=file.getStr("create_user");
			OapiUserGetRequest create_user=new OapiUserGetRequest();
			create_user.setUserid(create_userId);
			create_user.setHttpMethod("GET");
			OapiUserGetResponse  response_create=null;
			try {
				response_create=client_user.execute(create_user,accessToken_user);
			} catch (ApiException e) {
				e.printStackTrace();
			}
			setAttr("create_user", response_create.getName());
			//审批人
			String sp_userId=file.getStr("sp_user_id");
			OapiUserGetRequest request_user = new OapiUserGetRequest();
			request_user.setUserid(sp_userId);
			request_user.setHttpMethod("GET");
			OapiUserGetResponse  response_user = null;
			try {
				response_user = client_user.execute(request_user, accessToken_user);
			}catch(ApiException e) {
				e.printStackTrace();
			}
			//打印人
			String print_user_id=file.getStr("print_user_id");
			OapiUserGetRequest request_print = new OapiUserGetRequest();
			request_print.setUserid(print_user_id);
			request_print.setHttpMethod("GET");
			OapiUserGetResponse  response_print = null;
			try {
				response_print = client_user.execute(request_print, accessToken_user);
			}catch(ApiException e) {
				e.printStackTrace();
			}
			setAttr("sp_name", response_user.getName());
			setAttr("print_user", response_print.getName());
			Record fujian=Db.use("dc").findById("caigou_tech_file_fujian", file.getInt("fujian_id"));
			setAttr("fujianName", fujian.get("fujian_name"));
			render("/page/caigou/caigouPublish/lsfileDetail.html");
		} else if (status==2){
			Record file=Db.use("dc").findById("kehu_tech_file", id);
			setAttr("file", file);
			String accessToken_user=DingController.getAccessToken();
			DingTalkClient client_user = new DefaultDingTalkClient("https://oapi.dingtalk.com/user/get");
			//审批人
			String sp_userId=file.getStr("sp_user_id");
			OapiUserGetRequest request_user = new OapiUserGetRequest();
			request_user.setUserid(sp_userId);
			request_user.setHttpMethod("GET");
			OapiUserGetResponse  response_user = null;
			try {
				response_user = client_user.execute(request_user, accessToken_user);
			}catch(ApiException e) {
				e.printStackTrace();
			}
			//打印人
			String print_user_id=file.getStr("print_user_id");
			OapiUserGetRequest request_print = new OapiUserGetRequest();
			request_print.setUserid(print_user_id);
			request_print.setHttpMethod("GET");
			OapiUserGetResponse  response_print = null;
			try {
				response_print = client_user.execute(request_print, accessToken_user);
			}catch(ApiException e) {
				e.printStackTrace();
			}
			setAttr("sp_name", response_user.getName());
			setAttr("print_user", response_print.getName());
			Record fujian=Db.use("dc").findById("kehu_tech_file_fujian", file.getInt("fujian_id"));
			setAttr("fujianName", fujian.get("fujian_name"));
			render("/page/kehu/kehuPublish/lsfileDetail.html");
		}else if (status==4){
			Record file=Db.use("dc").findById("xiangmu_tech_file", id);
			setAttr("file", file);
			String accessToken_user=DingController.getAccessToken();
			DingTalkClient client_user = new DefaultDingTalkClient("https://oapi.dingtalk.com/user/get");
			//审批人
			String sp_userId=file.getStr("sp_user_id");
			OapiUserGetRequest request_user = new OapiUserGetRequest();
			request_user.setUserid(sp_userId);
			request_user.setHttpMethod("GET");
			OapiUserGetResponse  response_user = null;
			try {
				response_user = client_user.execute(request_user, accessToken_user);
			}catch(ApiException e) {
				e.printStackTrace();
			}
			//打印人
			String print_user_id=file.getStr("print_user_id");
			OapiUserGetRequest request_print = new OapiUserGetRequest();
			request_print.setUserid(print_user_id);
			request_print.setHttpMethod("GET");
			OapiUserGetResponse  response_print = null;
			try {
				response_print = client_user.execute(request_print, accessToken_user);
			}catch(ApiException e) {
				e.printStackTrace();
			}
			setAttr("sp_name", response_user.getName());
			setAttr("print_user", response_print.getName());
			Record fujian=Db.use("dc").findById("xiangmu_tech_file_fujian", file.getInt("fujian_id"));
			setAttr("fujianName", fujian.get("fujian_name"));
			render("/page/xiangmu/xiangmuPublish/lsfileDetail.html");
		}else if (status==5){
			Record file=Db.use("dc").findById("zhiliang_tech_file", id);
			setAttr("file", file);
			String accessToken_user=DingController.getAccessToken();
			DingTalkClient client_user = new DefaultDingTalkClient("https://oapi.dingtalk.com/user/get");
			//审批人
			String sp_userId=file.getStr("sp_user_id");
			OapiUserGetRequest request_user = new OapiUserGetRequest();
			request_user.setUserid(sp_userId);
			request_user.setHttpMethod("GET");
			OapiUserGetResponse  response_user = null;
			try {
				response_user = client_user.execute(request_user, accessToken_user);
			}catch(ApiException e) {
				e.printStackTrace();
			}
			//打印人
			String print_user_id=file.getStr("print_user_id");
			OapiUserGetRequest request_print = new OapiUserGetRequest();
			request_print.setUserid(print_user_id);
			request_print.setHttpMethod("GET");
			OapiUserGetResponse  response_print = null;
			try {
				response_print = client_user.execute(request_print, accessToken_user);
			}catch(ApiException e) {
				e.printStackTrace();
			}
			setAttr("sp_name", response_user.getName());
			setAttr("print_user", response_print.getName());
			Record fujian=Db.use("dc").findById("zhiliang_tech_file_fujian", file.getInt("fujian_id"));
			setAttr("fujianName", fujian.get("fujian_name"));
			render("/page/zhiliang/zhiliangPublish/lsfileDetail.html");
		}
	}
	//项目审核通过
	public void spPass() {
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
		Date d= new Date();
		String date = sdf.format(d);
		int id=getParaToInt("id");
		String file_idString=Integer.valueOf(id).toString();
		int status=getParaToInt("status");
		if (status==1) {
			Record file=Db.use("dc").findById("caigou_tech_file", id);
			file.set("sp_status", 0);//审核通过待审批
			Db.use("dc").update("caigou_tech_file", file);
			//发起领导审批
			String nb_file_code=file.getStr("nb_fileCode");
			String file_name=file.getStr("file_name");
			String edition=file.getStr("edition");
			String sp_user_id=file.getStr("sp_user_id");
			String print_user_id=file.getStr("print_user_id");
			String process_instance_id="";
			String fbFile_type="采购文件";
			try {
				process_instance_id=ShenPiUtil.printShenPi(file.getStr("create_user"),file_idString,nb_file_code,file_name,edition,sp_user_id,fbFile_type,print_user_id);
			}catch(Exception e) {
				e.printStackTrace();
			}
			//获取文件知会人
			String sql_zhihui="select * from caigou_zhihui_users where caigou_tech_files_id="+id;
			List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
			String zhihuiUsers="";
			for (int i = 0; i < zhihuiList.size(); i++) {
				zhihuiUsers+=zhihuiList.get(i).getStr("zhihui_userId")+",";
			}
			DingUtil.sendText(zhihuiUsers, "知会消息：采购文件“"+file.getStr("file_name")+"”项目审核通过，等待领导审批，请知悉！" +date);
			
		} else if (status==2){
			Record file=Db.use("dc").findById("kehu_tech_file", id);
			file.set("sp_status", 0);//审核通过待审批
			Db.use("dc").update("kehu_tech_file", file);
			//发起领导审批
			String nb_file_code=file.getStr("nb_fileCode");
			String file_name=file.getStr("file_name");
			String edition=file.getStr("edition");
			String sp_user_id=file.getStr("sp_user_id");
			String print_user_id=file.getStr("print_user_id");
			String process_instance_id="";
			String fbFile_type="客户文件";
			try {
				process_instance_id=ShenPiUtil.printShenPi(file.getStr("create_user"),file_idString,nb_file_code,file_name,edition,sp_user_id,fbFile_type,print_user_id);
			}catch(Exception e) {
				e.printStackTrace();
			}
			//获取文件知会人
			String sql_zhihui="select * from kehu_zhihui_users where kehu_tech_files_id="+id;
			List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
			String zhihuiUsers="";
			for (int i = 0; i < zhihuiList.size(); i++) {
				zhihuiUsers+=zhihuiList.get(i).getStr("zhihui_userId")+",";
			}
			DingUtil.sendText(zhihuiUsers, "知会消息：客户文件“"+file.getStr("file_name")+"”项目审核通过，等待领导审批，请知悉！" +date);
		}else if (status==4){
			Record file=Db.use("dc").findById("xiangmu_tech_file", id);
			file.set("sp_status", 0);//审核通过待审批
			Db.use("dc").update("xiangmu_tech_file", file);
			//发起领导审批
			String nb_file_code=file.getStr("nb_fileCode");
			String file_name=file.getStr("file_name");
			String edition=file.getStr("edition");
			String sp_user_id=file.getStr("sp_user_id");
			String print_user_id=file.getStr("print_user_id");
			String process_instance_id="";
			String fbFile_type="项目文件";
			try {
				process_instance_id=ShenPiUtil.printShenPi(file.getStr("create_user"),file_idString,nb_file_code,file_name,edition,sp_user_id,fbFile_type,print_user_id);
			}catch(Exception e) {
				e.printStackTrace();
			}
			//获取文件知会人
			String sql_zhihui="select * from xiangmu_zhihui_users where xiangmu_tech_files_id="+id;
			List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
			String zhihuiUsers="";
			for (int i = 0; i < zhihuiList.size(); i++) {
				zhihuiUsers+=zhihuiList.get(i).getStr("zhihui_userId")+",";
			}
			DingUtil.sendText(zhihuiUsers, "知会消息：项目文件“"+file.getStr("file_name")+"”项目审核通过，等待领导审批，请知悉！" +date);
		}else if (status==5){
			Record file=Db.use("dc").findById("zhiliang_tech_file", id);
			file.set("sp_status", 0);//审核通过待审批
			Db.use("dc").update("zhiliang_tech_file", file);
			//发起领导审批
			String nb_file_code=file.getStr("nb_fileCode");
			String file_name=file.getStr("file_name");
			String edition=file.getStr("edition");
			String sp_user_id=file.getStr("sp_user_id");
			String print_user_id=file.getStr("print_user_id");
			String process_instance_id="";
			String fbFile_type="质量文件";
			try {
				process_instance_id=ShenPiUtil.printShenPi(file.getStr("create_user"),file_idString,nb_file_code,file_name,edition,sp_user_id,fbFile_type,print_user_id);
			}catch(Exception e) {
				e.printStackTrace();
			}
			//获取文件知会人
			String sql_zhihui="select * from zhiliang_zhihui_users where zhiliang_tech_files_id="+id;
			List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
			String zhihuiUsers="";
			for (int i = 0; i < zhihuiList.size(); i++) {
				zhihuiUsers+=zhihuiList.get(i).getStr("zhihui_userId")+",";
			}
			DingUtil.sendText(zhihuiUsers, "知会消息：质量文件“"+file.getStr("file_name")+"”项目审核通过，等待领导审批，请知悉！" +date);
		}
		Record record=new Record();
		record.set("code", 0);
		record.set("msg", "项目审核通过！");
		renderJson(record);
	}
	//项目审核驳回
	public void spBack() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
		Date d= new Date();
		String date = sdf.format(d);
		int id=getParaToInt("id");
		int status=getParaToInt("status");
		if (status==1) {
			Record file=Db.use("dc").findById("caigou_tech_file", id);
			file.set("sp_status", 22);
			Db.use("dc").update("caigou_tech_file", file);
			//通知发布人
			DingUtil.sendText(file.getStr("create_user"), "项目审核消息：采购文件“"+file.getStr("file_name")+"”，项目审核驳回，请知悉！" +date);
			//获取文件知会人
			String sql_zhihui="select * from  caigou_zhihui_users where caigou_tech_files_id="+id;
			List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
			String zhihuiUsers="";
			for (int i = 0; i < zhihuiList.size(); i++) {
				zhihuiUsers+=zhihuiList.get(i).getStr("zhihui_userId")+",";
			}
			DingUtil.sendText(zhihuiUsers, "知会消息：采购文件“"+file.getStr("file_name")+"”，项目审核驳回，请知悉！" +date);
		} else if (status==2){
			Record file=Db.use("dc").findById("kehu_tech_file", id);
			file.set("sp_status", 22);
			Db.use("dc").update("kehu_tech_file", file);
			//通知发布人
			DingUtil.sendText(file.getStr("create_user"), "项目审核消息：采购文件“"+file.getStr("file_name")+"”，项目审核驳回，请知悉！" +date);
			//获取文件知会人
			String sql_zhihui="select * from kehu_zhihui_users where kehu_tech_files_id="+id;
			List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
			String zhihuiUsers="";
			for (int i = 0; i < zhihuiList.size(); i++) {
				zhihuiUsers+=zhihuiList.get(i).getStr("zhihui_userId")+",";
			}
			DingUtil.sendText(zhihuiUsers, "知会消息：客户文件“"+file.getStr("file_name")+"”，项目审核驳回，请知悉！" +date);
		}else if (status==4){
			Record file=Db.use("dc").findById("xiangmu_tech_file", id);
			file.set("sp_status", 22);
			Db.use("dc").update("xiangmu_tech_file", file);
			//通知发布人
			DingUtil.sendText(file.getStr("create_user"), "项目审核消息：采购文件“"+file.getStr("file_name")+"”，项目审核驳回，请知悉！" +date);
			//获取文件知会人
			String sql_zhihui="select * from xiangmu_zhihui_users where xiangmu_tech_files_id="+id;
			List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
			String zhihuiUsers="";
			for (int i = 0; i < zhihuiList.size(); i++) {
				zhihuiUsers+=zhihuiList.get(i).getStr("zhihui_userId")+",";
			}
			DingUtil.sendText(zhihuiUsers, "知会消息：项目文件“"+file.getStr("file_name")+"”，项目审核驳回，请知悉！" +date);
		}else if (status==5){
			Record file=Db.use("dc").findById("zhiliang_tech_file", id);
			file.set("sp_status", 22);
			Db.use("dc").update("zhiliang_tech_file", file);
			//通知发布人
			DingUtil.sendText(file.getStr("create_user"), "项目审核消息：采购文件“"+file.getStr("file_name")+"”，项目审核驳回，请知悉！" +date);
			//获取文件知会人
			String sql_zhihui="select * from zhiliang_zhihui_users where zhiliang_tech_files_id="+id;
			List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
			String zhihuiUsers="";
			for (int i = 0; i < zhihuiList.size(); i++) {
				zhihuiUsers+=zhihuiList.get(i).getStr("zhihui_userId")+",";
			}
			DingUtil.sendText(zhihuiUsers, "知会消息：质量文件“"+file.getStr("file_name")+"”，项目审核驳回，请知悉！" +date);
		}
		Record record=new Record();
		record.set("code", 0);
		record.set("msg", "项目审核驳回成功！");
		renderJson(record);
	}
	//附件下载
	public void fujianDownload() {
		Record rd=new Record();
		int file_id=getParaToInt("id");
		int file_status=getParaToInt("status");
		String sql_fj="";
		if (file_status==1) {
			Record file=Db.use("dc").findById("caigou_tech_file", file_id);
			sql_fj="select * from caigou_tech_file_fujian where id="+file.getInt("fujian_id");
		} else if (file_status==2){
			Record file=Db.use("dc").findById("kehu_tech_file", file_id);
			sql_fj="select * from kehu_tech_file_fujian where id="+file.getInt("fujian_id");
		}else if (file_status==3){
			Record file=Db.use("dc").findById("tech_files", file_id);
			sql_fj="select * from tech_file_fujian where id="+file.getInt("fujian_id");
		}else if (file_status==4){
			Record file=Db.use("dc").findById("xiangmu_tech_file", file_id);
			sql_fj="select * from xiangmu_tech_file_fujian where id="+file.getInt("fujian_id");
		}else if (file_status==5){
			Record file=Db.use("dc").findById("zhiliang_tech_file", file_id);
			sql_fj="select * from zhiliang_tech_file_fujian where id="+file.getInt("fujian_id");
		}
		Record fujian=Db.use("dc").findFirst(sql_fj);
		try {
//			renderFile(fujian.getStr("fujian_name"));
			render(new MyFileRender(fujian.getStr("fujian_name")));
		}catch (Exception e){
			rd.set("code", 1);
			rd.set("msg", "暂无附件下载");
			rd.set("data", "");
			renderJson(rd);
		}
	}
}
