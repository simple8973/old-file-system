package com.simple.controller.techFileReceive;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.dingtalk.api.response.OapiUserGetResponse;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.simple.common.model.CaigouTechFile;
import com.simple.common.model.CaigouTechFileFujian;
import com.simple.common.model.CaigouTechFilesReceive;
import com.simple.common.model.KehuTechFile;
import com.simple.common.model.KehuTechFileFujian;
import com.simple.common.model.KehuTechFilesReceive;
import com.simple.common.model.XiangmuTechFile;
import com.simple.common.model.XiangmuTechFileFujian;
import com.simple.common.model.XiangmuTechFilesReceive;
import com.simple.common.model.ZhiliangTechFile;
import com.simple.common.model.ZhiliangTechFileFujian;
import com.simple.common.model.ZhiliangTechFilesReceive;
import com.simple.ding.DingUtil;
import com.simple.file.MyFileRender;

public class FileReceiveController extends Controller{
	//指向接收列表页面
	public void toReceiveList() {
		render("ReceiveList.html");
	}
	//获取当前用户接收的所有文件
	public void getReceiveList() {
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		StringBuilder sb=new StringBuilder();
		int page=getInt("page");
		int limit=getInt("limit");
		String sql_userRole="select * from user_role  where user_id='"+user.getUserid()+"' and role_id=1";
		List<Record> userRoleList=Db.use("dc").find(sql_userRole);
		if (userRoleList.size()>0) {
			sb.append(" from  (select b.*,a.receive,a.id as bid from caigou_tech_files_receive a, caigou_tech_file b where  b.id=a.caigou_tech_files_id and b.sp_status>=3 and b.sp_status<13 ");
		}else {
			sb.append(" from  (select b.*,a.receive,a.id as bid from caigou_tech_files_receive a, caigou_tech_file b where a.receive_userId='"+user.getUserid()+"' and b.id=a.caigou_tech_files_id and b.sp_status>=3 and b.sp_status<13 ");
		}
		//sb.append(" from  (select b.*,a.receive,a.id as bid from caigou_tech_files_receive a, caigou_tech_file b where a.receive_userId='"+user.getUserid()+"' and b.id=a.caigou_tech_files_id and b.sp_status>=3 and b.sp_status<20 ");
		if (!"".equals(get("file_name")) && get("file_name")!=null) {
			sb.append(" and file_name like '%"+get("file_name")+"%'");
		}
		if (!"".equals(get("zc_pro_code")) && get("zc_pro_code")!=null) {
			sb.append(" and nb_zc_Code like '%"+get("zc_pro_code")+"%'");
		}
		if (!"".equals(get("nb_file_code")) && get("nb_file_code")!=null) {
			sb.append(" and nb_fileCode like '%"+get("nb_file_code")+"%'");
		}
		if (userRoleList.size()>0) {
			sb.append(" union all select b.*,a.receive,a.id as bid from kehu_tech_files_receive a, kehu_tech_file b where  b.id=a.kehu_tech_files_id and b.sp_status>=3 and b.sp_status<13 ");
		}else {
			sb.append(" union all select b.*,a.receive,a.id as bid from kehu_tech_files_receive a, kehu_tech_file b where a.receive_userId='"+user.getUserid()+"' and b.id=a.kehu_tech_files_id and b.sp_status>=3 and b.sp_status<13 ");
		}
		//sb.append(" union all select b.*,a.receive,a.id as bid from kehu_tech_files_receive a, kehu_tech_file b where a.receive_userId='"+user.getUserid()+"' and b.id=a.kehu_tech_files_id and b.sp_status>=3 and b.sp_status<20 ");
		if (!"".equals(get("file_name")) && get("file_name")!=null) {
			sb.append(" and file_name like '%"+get("file_name")+"%'");
		}
		if (!"".equals(get("zc_pro_code")) && get("zc_pro_code")!=null) {
			sb.append(" and nb_zc_Code like '%"+get("zc_pro_code")+"%'");
		}
		if (!"".equals(get("nb_file_code")) && get("nb_file_code")!=null) {
			sb.append(" and nb_fileCode like '%"+get("nb_file_code")+"%'");
		}
		if (userRoleList.size()>0) {
			sb.append(" union all select b.*,a.receive,a.id as bid from tech_files_receive a, tech_files b where b.id=a.tech_files_id and b.sp_status>=3 and b.sp_status<>6 ");
		}else {
			sb.append(" union all select b.*,a.receive,a.id as bid from tech_files_receive a, tech_files b where a.receive_userId='"+user.getUserid()+"' and b.id=a.tech_files_id and b.sp_status>=3 and b.sp_status<>6 ");
		}
		//sb.append(" union all select b.*,a.receive,a.id as bid from tech_files_receive a, tech_files b where a.receive_userId='"+user.getUserid()+"' and b.id=a.tech_files_id and b.sp_status>=3 and b.sp_status<>6 ");
		if (!"".equals(get("file_name")) && get("file_name")!=null) {
			sb.append(" and file_name like '%"+get("file_name")+"%'");
		}
		if (!"".equals(get("zc_pro_code")) && get("zc_pro_code")!=null) {
			sb.append(" and nb_zc_Code like '%"+get("zc_pro_code")+"%'");
		}
		if (!"".equals(get("nb_file_code")) && get("nb_file_code")!=null) {
			sb.append(" and nb_fileCode like '%"+get("nb_file_code")+"%'");
		}
		if (userRoleList.size()>0) {
			sb.append(" union all select b.*,a.receive,a.id as bid from xiangmu_tech_files_receive a,  xiangmu_tech_file b where  b.id=a.xiangmu_tech_files_id and b.sp_status>=3 and b.sp_status<13 ");
		}else {
			sb.append(" union all select b.*,a.receive,a.id as bid from xiangmu_tech_files_receive a,  xiangmu_tech_file b where  a.receive_userId='"+user.getUserid()+"' and b.id=a.xiangmu_tech_files_id and b.sp_status>=3 and b.sp_status<13 ");
		}
		//sb.append(" union all select b.*,a.receive,a.id as bid from xiangmu_tech_files_receive a,  xiangmu_tech_file b where  a.receive_userId='"+user.getUserid()+"' and b.id=a.xiangmu_tech_files_id and b.sp_status>=3 and b.sp_status<20 ");
		if (!"".equals(get("file_name")) && get("file_name")!=null) {
			sb.append(" and file_name like '%"+get("file_name")+"%'");
		}
		if (!"".equals(get("zc_pro_code")) && get("zc_pro_code")!=null) {
			sb.append(" and nb_zc_Code like '%"+get("zc_pro_code")+"%'");
		}
		if (!"".equals(get("nb_file_code")) && get("nb_file_code")!=null) {
			sb.append(" and nb_fileCode like '%"+get("nb_file_code")+"%'");
		}
		if (userRoleList.size()>0) {
			sb.append(" union all select b.*,a.receive,a.id as bid from zhiliang_tech_files_receive a,zhiliang_tech_file b where  b.id=a.zhiliang_tech_files_id and b.sp_status>=3 and b.sp_status<13 ");
		}else {
			sb.append(" union all select b.*,a.receive,a.id as bid from zhiliang_tech_files_receive a,zhiliang_tech_file b where  a.receive_userId='"+user.getUserid()+"' and b.id=a.zhiliang_tech_files_id and b.sp_status>=3 and b.sp_status<13 ");
		}
		//sb.append(" union all select b.*,a.receive,a.id as bid from zhiliang_tech_files_receive a,zhiliang_tech_file b where  a.receive_userId='"+user.getUserid()+"' and b.id=a.zhiliang_tech_files_id and b.sp_status>=3 and b.sp_status<20 ");
		if (!"".equals(get("file_name")) && get("file_name")!=null) {
			sb.append(" and file_name like '%"+get("file_name")+"%'");
		}
		if (!"".equals(get("zc_pro_code")) && get("zc_pro_code")!=null) {
			sb.append(" and nb_zc_Code like '%"+get("zc_pro_code")+"%'");
		}
		if (!"".equals(get("nb_file_code")) && get("nb_file_code")!=null) {
			sb.append(" and nb_fileCode like '%"+get("nb_file_code")+"%'");
		}
		Page<Record> ReceiveList=Db.use("dc").paginate(page, limit, "select * ,use_cycle-DATEDIFF(NOW(),create_time) AS sy ",sb.toString()+") temp order by create_time desc");
		Record record=new Record();
		record.set("code", 0);
		record.set("msg", "获取审批表成功！");
		record.set("count", ReceiveList.getTotalRow());
		record.set("data", ReceiveList.getList());
		renderJson(record);
	}
	//其他四类文件接收
	public void otherReceive() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
		Date d= new Date();
		String date = sdf.format(d);
		Record rd=new Record();
		int bid=getParaToInt("bid");//接受表id
		int fileType=getParaToInt("file_type");
		if (fileType==1) {
			CaigouTechFilesReceive caigouTechReceive=CaigouTechFilesReceive.dao.findById(bid);
			CaigouTechFile caigouTechFile=CaigouTechFile.dao.findById(caigouTechReceive.getCaigouTechFilesId());
			int caigouReceive=caigouTechReceive.getReceive();
			if (caigouReceive==0) {//未接收
				caigouTechReceive.setReceive(1);
				caigouTechReceive.update();
				//判断是否所有人已接受
				List<CaigouTechFilesReceive> allcaigouReceive=CaigouTechFilesReceive.dao.find("select * from caigou_tech_files_receive where caigou_tech_files_id="+caigouTechReceive.getCaigouTechFilesId());
				boolean isReceive=true;
				for (int i = 0; i < allcaigouReceive.size(); i++) {
					int receiveStatus=allcaigouReceive.get(i).getReceive();
					if (receiveStatus==0) {//有人未接受
						isReceive=false;
						break;
					}
				}
				if (isReceive) {
					caigouTechFile.setSpStatus(4);
					caigouTechFile.update();
					//全部接收时，通知知会人
					String sql_zhihui="select * from caigou_zhihui_users where caigou_tech_files_id="+caigouTechFile.getId();
					List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
					String zhiHui="";
					for (int i = 0; i < zhihuiList.size(); i++) {
						zhiHui+=zhihuiList.get(i).getStr("zhihui_userId")+",";
					}
					DingUtil.sendText(zhiHui, "知会通知：采购文件“ "+caigouTechFile.getFileName()+" ” 已经全部领取，请知悉！"+date);
				}
			}
			//文件下载
			CaigouTechFileFujian caigouFile=CaigouTechFileFujian.dao.findById(caigouTechFile.getFujianId());
			try {
				render(new MyFileRender(caigouFile.getFujianName()));
//				renderFile(caigouFile.getFujianName());
			} catch (Exception e) {
				rd.set("code", 1);
				rd.set("msg", "暂无文件下载");
				rd.set("data", "");
				renderJson(rd);
			}
		}else if (fileType==2) {
			KehuTechFilesReceive kehuTechReceive=KehuTechFilesReceive.dao.findById(bid);
			KehuTechFile kehuTechFile=KehuTechFile.dao.findById(kehuTechReceive.getKehuTechFilesId());
			int kehuReceive=kehuTechReceive.getReceive();
			if (kehuReceive==0) {//未接收
				kehuTechReceive.setReceive(1);
				kehuTechReceive.update();
				//判断是否所有人已接受
				List<KehuTechFilesReceive> allKehuReceive=KehuTechFilesReceive.dao.find("select * from kehu_tech_files_receive where kehu_tech_files_id="+kehuTechReceive.getKehuTechFilesId());
				boolean isReceive=true;
				for (int i = 0; i < allKehuReceive.size(); i++) {
					int receiveStatus=allKehuReceive.get(i).getReceive();
					if (receiveStatus==0) {//有人未接受
						isReceive=false;
						break;
					}
				}
				if (isReceive) {
					kehuTechFile.setSpStatus(4);
					kehuTechFile.update();
					//全部接收时，通知知会人
					String sql_zhihui="select * from kehu_zhihui_users where kehu_tech_files_id="+kehuTechFile.getId();
					List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
					String zhiHui="";
					for (int i = 0; i < zhihuiList.size(); i++) {
						zhiHui+=zhihuiList.get(i).getStr("zhihui_userId")+",";
					}
					DingUtil.sendText(zhiHui, "知会通知：客户文件“ "+kehuTechFile.getFileName()+" ” 已经全部领取，请知悉！"+date);
				}
			}
			//文件下载
			KehuTechFileFujian kehuFile=KehuTechFileFujian.dao.findById(kehuTechFile.getFujianId());
			try {
				render(new MyFileRender(kehuFile.getFujianName()));
//				renderFile(kehuFile.getFujianName());
			} catch (Exception e) {
				rd.set("code", 1);
				rd.set("msg", "暂无文件下载");
				rd.set("data", "");
				renderJson(rd);
			}
		}else if (fileType==4) {
			XiangmuTechFilesReceive xmTechReceive=XiangmuTechFilesReceive.dao.findById(bid);
			XiangmuTechFile xmTechFile=XiangmuTechFile.dao.findById(xmTechReceive.getXiangmuTechFilesId());
			int xmReceive=xmTechReceive.getReceive();
			if (xmReceive==0) {//未接收
				xmTechReceive.setReceive(1);
				xmTechReceive.update();
				//判断是否所有人已接受
				List<XiangmuTechFilesReceive> allxmReceive=XiangmuTechFilesReceive.dao.find("select * from xiangmu_tech_files_receive where xiangmu_tech_files_id="+xmTechReceive.getXiangmuTechFilesId());
				boolean isReceive=true;
				for (int i = 0; i < allxmReceive.size(); i++) {
					int receiveStatus=allxmReceive.get(i).getReceive();
					if (receiveStatus==0) {//有人未接受
						isReceive=false;
						break;
					}
				}
				if (isReceive) {
					xmTechFile.setSpStatus(4);
					xmTechFile.update();
					//全部接收时，通知知会人
					String sql_zhihui="select * from xiangmu_zhihui_users where xiangmu_tech_files_id="+xmTechFile.getId();
					List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
					String zhiHui="";
					for (int i = 0; i < zhihuiList.size(); i++) {
						zhiHui+=zhihuiList.get(i).getStr("zhihui_userId")+",";
					}
					DingUtil.sendText(zhiHui, "知会通知：项目文件“ "+xmTechFile.getFileName()+" ” 已经全部领取，请知悉！"+date);
				}
			}
			//文件下载
			XiangmuTechFileFujian xmFile=XiangmuTechFileFujian.dao.findById(xmTechFile.getFujianId());
			try {
				render(new MyFileRender(xmFile.getFujianName()));
//				renderFile(xmFile.getFujianName());
			} catch (Exception e) {
				rd.set("code", 1);
				rd.set("msg", "暂无文件下载");
				rd.set("data", "");
				renderJson(rd);
			}
		}else if (fileType==5) {
			ZhiliangTechFilesReceive zlTechReceive=ZhiliangTechFilesReceive.dao.findById(bid);
			ZhiliangTechFile zlTechFile=ZhiliangTechFile.dao.findById(zlTechReceive.getZhiliangTechFilesId());
			int zlReceive=zlTechReceive.getReceive();
			if (zlReceive==0) {//未接收
				zlTechReceive.setReceive(1);
				zlTechReceive.update();
				//判断是否所有人已接受
				List<ZhiliangTechFilesReceive> allzlReceive=ZhiliangTechFilesReceive.dao.find("select * from zhiliang_tech_files_receive where zhiliang_tech_files_id="+zlTechReceive.getZhiliangTechFilesId());
				boolean isReceive=true;
				for (int i = 0; i < allzlReceive.size(); i++) {
					int receiveStatus=allzlReceive.get(i).getReceive();
					if (receiveStatus==0) {//有人未接受
						isReceive=false;
						break;
					}
				}
				if (isReceive) {
					zlTechFile.setSpStatus(4);
					zlTechFile.update();
					//全部接收时，通知知会人
					String sql_zhihui="select * from zhiliang_zhihui_users where zhiliang_tech_files_id="+zlTechFile.getId();
					List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
					String zhiHui="";
					for (int i = 0; i < zhihuiList.size(); i++) {
						zhiHui+=zhihuiList.get(i).getStr("zhihui_userId")+",";
					}
					DingUtil.sendText(zhiHui, "知会通知：质量文件“ "+zlTechFile.getFileName()+" ” 已经全部领取，请知悉！"+date);
				}
			}
			//文件下载
			ZhiliangTechFileFujian zlFile=ZhiliangTechFileFujian.dao.findById(zlTechFile.getFujianId());
			try {
				render(new MyFileRender(zlFile.getFujianName()));
//				renderFile(zlFile.getFujianName());
			} catch (Exception e) {
				rd.set("code", 1);
				rd.set("msg", "暂无文件下载");
				rd.set("data", "");
				renderJson(rd);
			}
		}
	}
//	//确认接收文件
//	public void receiveTech() {
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
//		Date d= new Date();
//		String date = sdf.format(d);
//		
//		int id=getParaToInt("bid");//员工接收表id
//		int file_type=getParaToInt("file_type");
//		if (file_type==1) {
//			Record receive=Db.use("dc").findById("caigou_tech_files_receive", id);
//			receive.set("receive", 1);
//			Db.use("dc").update("caigou_tech_files_receive", receive);
//			//判断是否所有人都已经接收
//			String sql_user="select * from caigou_tech_files_receive where caigou_tech_files_id="+receive.getInt("caigou_tech_files_id");
//			List<Record> userList=Db.use("dc").find(sql_user);
//			int a=0;
//			for (int i = 0; i < userList.size(); i++) {
//				int status=userList.get(i).getInt("receive");
//				if (status==0) {
//					a=1; //有未接收的人员时,跳出循环,文件状态仍然是"待领取"
//					break;
//				}
//			}
//			//根据a的值判断文件是否已经全部接受
//			Record file=Db.use("dc").findById("caigou_tech_file", receive.getInt("caigou_tech_files_id"));
//			if (a==0) {
//				file.set("sp_status", 4);//全部接收,状态变为4-使用中
//				Db.use("dc").update("caigou_tech_file", file);
//				//全部接收时，通知知会人
//				String sql_zhihui="select * from caigou_zhihui_users where caigou_tech_files_id="+file.getInt("id");
//				List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
//				String zhiHui="";
//				for (int i = 0; i < zhihuiList.size(); i++) {
//					zhiHui+=zhihuiList.get(i).getStr("zhihui_userId")+",";
//				}
//				DingUtil.sendText(zhiHui, "知会通知：采购文件“ "+file.getStr("file_name")+" ” 已经全部领取，请知悉！"+date);
//			}
//		}else if(file_type==2) {
//			Record receive=Db.use("dc").findById("kehu_tech_files_receive", id);
//			receive.set("receive", 1);
//			Db.use("dc").update("kehu_tech_files_receive", receive);
//			//判断是否所有人都已经接收
//			String sql_user="select * from kehu_tech_files_receive where kehu_tech_files_id="+receive.getInt("kehu_tech_files_id");
//			List<Record> userList=Db.use("dc").find(sql_user);
//			int a=0;
//			for (int i = 0; i < userList.size(); i++) {
//				int status=userList.get(i).getInt("receive");
//				if (status==0) {
//					a=1; //有未接收的人员时,跳出循环,文件状态仍然是"待领取"
//					break;
//				}
//			}
//			//根据a的值判断文件是否已经全部接受
//			Record file=Db.use("dc").findById("kehu_tech_file", receive.getInt("kehu_tech_files_id"));
//			if (a==0) {
//				file.set("sp_status", 4);//全部接收,状态变为4-使用中
//				Db.use("dc").update("kehu_tech_file", file);
//				//全部接收时，通知知会人
//				String sql_zhihui="select * from kehu_zhihui_users where kehu_tech_files_id="+file.getInt("id");
//				List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
//				String zhiHui="";
//				for (int i = 0; i < zhihuiList.size(); i++) {
//					zhiHui+=zhihuiList.get(i).getStr("zhihui_userId")+",";
//				}
//				DingUtil.sendText(zhiHui, "知会通知：客户文件“ "+file.getStr("file_name")+" ” 已经全部领取，请知悉！"+date);
//			}
//		}else if(file_type==3) {
//			Record receive=Db.use("dc").findById("tech_files_receive", id);
//			receive.set("receive", 1);
//			Db.use("dc").update("tech_files_receive", receive);
//			//判断是否所有人都已经接收
//			String sql_user="select * from tech_files_receive where tech_files_id="+receive.getInt("tech_files_id");
//			List<Record> userList=Db.use("dc").find(sql_user);
//			int a=0;
//			for (int i = 0; i < userList.size(); i++) {
//				int status=userList.get(i).getInt("receive");
//				if (status==0) {
//					a=1; //有未接收的人员时,跳出循环,文件状态仍然是"待领取"
//					break;
//				}
//			}
//			//根据a的值判断文件是否已经全部接受
//			Record file=Db.use("dc").findById("tech_files", receive.getInt("tech_files_id"));
//			if (a==0) {
//				file.set("sp_status", 4);//全部接收,状态变为4-使用中
//				Db.use("dc").update("tech_files", file);
//				//全部接收时，通知知会人
//				String sql_zhihui="select * from zhihui_users where tech_files_id="+file.getInt("id");
//				List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
//				String zhiHui="";
//				for (int i = 0; i < zhihuiList.size(); i++) {
//					zhiHui+=zhihuiList.get(i).getStr("zhihui_userId")+",";
//				}
//				DingUtil.sendText(zhiHui, "知会通知：生产文件“ "+file.getStr("file_name")+" ” 已经全部领取，请知悉！"+date);
//			}
//		}else if(file_type==4) {
//			Record receive=Db.use("dc").findById("xiangmu_tech_files_receive", id);
//			receive.set("receive", 1);
//			Db.use("dc").update("xiangmu_tech_files_receive", receive);
//			//判断是否所有人都已经接收
//			String sql_user="select * from xiangmu_tech_files_receive where xiangmu_tech_files_id="+receive.getInt("xiangmu_tech_files_id");
//			List<Record> userList=Db.use("dc").find(sql_user);
//			int a=0;
//			for (int i = 0; i < userList.size(); i++) {
//				int status=userList.get(i).getInt("receive");
//				if (status==0) {
//					a=1; //有未接收的人员时,跳出循环,文件状态仍然是"待领取"
//					break;
//				}
//			}
//			//根据a的值判断文件是否已经全部接受
//			Record file=Db.use("dc").findById("xiangmu_tech_file", receive.getInt("xiangmu_tech_files_id"));
//			if (a==0) {
//				file.set("sp_status", 4);//全部接收,状态变为4-使用中
//				Db.use("dc").update("xiangmu_tech_file", file);
//				
//				//全部接收时，通知知会人
//				String sql_zhihui="select * from xiangmu_zhihui_users where xiangmu_tech_files_id="+file.getInt("id");
//				List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
//				String zhiHui="";
//				for (int i = 0; i < zhihuiList.size(); i++) {
//					zhiHui+=zhihuiList.get(i).getStr("zhihui_userId")+",";
//				}
//				DingUtil.sendText(zhiHui, "知会通知：项目文件“ "+file.getStr("file_name")+" ” 已经全部领取，请知悉！"+date);
//			}
//		}else if(file_type==5) {
//			Record receive=Db.use("dc").findById("zhiliang_tech_files_receive", id);
//			receive.set("receive", 1);
//			Db.use("dc").update("zhiliang_tech_files_receive", receive);
//			//判断是否所有人都已经接收
//			String sql_user="select * from zhiliang_tech_files_receive where zhiliang_tech_files_id="+receive.getInt("zhiliang_tech_files_id");
//			List<Record> userList=Db.use("dc").find(sql_user);
//			int a=0;
//			for (int i = 0; i < userList.size(); i++) {
//				int status=userList.get(i).getInt("receive");
//				if (status==0) {
//					a=1; //有未接收的人员时,跳出循环,文件状态仍然是"待领取"
//					break;
//				}
//			}
//			//根据a的值判断文件是否已经全部接受
//			Record file=Db.use("dc").findById("zhiliang_tech_file", receive.getInt("zhiliang_tech_files_id"));
//			if (a==0) {
//				file.set("sp_status", 4);//全部接收,状态变为4-使用中
//				Db.use("dc").update("zhiliang_tech_file", file);
//				//全部接收时，通知知会人
//				String sql_zhihui="select * from zhiliang_zhihui_users where zhiliang_tech_files_id="+file.getInt("id");
//				List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
//				String zhiHui="";
//				for (int i = 0; i < zhihuiList.size(); i++) {
//					zhiHui+=zhihuiList.get(i).getStr("zhihui_userId")+",";
//				}
//				DingUtil.sendText(zhiHui, "知会通知：质量文件“ "+file.getStr("file_name")+" ” 已经全部领取，请知悉！"+date);
//			}
//		}
//		Record record=new Record();
//		record.set("code", 0);
//		record.set("msg", "文件接收成功！");
//		record.set("data", "");
//		renderJson(record);
//	}
	//指向生产文件接受页面
	public void ReceiveShengChan(){
		int Receive_id=getParaToInt(0);
		setAttr("id", Receive_id);
		render("ReceiveShengChan.html");
	}
	//接收成功
	public void doReceiveSc() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
		Date d= new Date();
		String date = sdf.format(d);
		
		int Receive_id=getParaToInt("id");
		int receive_num=getParaToInt("receive_num");
		Record receive=Db.use("dc").findById("tech_files_receive", Receive_id);
		receive.set("receive", 1);
		receive.set("receiveNum", receive_num);
		Db.use("dc").update("tech_files_receive", receive);
		//判断是否所有人都已经接收
		String sql_user="select * from tech_files_receive where tech_files_id="+receive.getInt("tech_files_id");
		List<Record> userList=Db.use("dc").find(sql_user);
		int a=0;
		for (int i = 0; i < userList.size(); i++) {
			int status=userList.get(i).getInt("receive");
			if (status==0) {
				a=1; //有未接收的人员时,跳出循环,文件状态仍然是"待领取"
				break;
			}
		}
		//根据a的值判断文件是否已经全部接受
		Record file=Db.use("dc").findById("tech_files", receive.getInt("tech_files_id"));
		if (a==0) {
			file.set("sp_status", 4);//全部接收,状态变为4-使用中
			Db.use("dc").update("tech_files", file);
			//全部接收时，通知知会人
			String sql_zhihui="select * from zhihui_users where tech_files_id="+file.getInt("id");
			List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
			String zhiHui="";
			for (int i = 0; i < zhihuiList.size(); i++) {
				zhiHui+=zhihuiList.get(i).getStr("zhihui_userId")+",";
			}
			DingUtil.sendText(zhiHui, "知会通知：生产文件“ "+file.getStr("file_name")+" ” 已经全部领取，请知悉！"+date);
		}
		Record record=new Record();
		record.set("code", 0);
		record.set("msg", "文件接收成功！");
		record.set("data", "");
		renderJson(record);
	}
}
