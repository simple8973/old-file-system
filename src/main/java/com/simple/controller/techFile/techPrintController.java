package com.simple.controller.techFile;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.OapiUserGetRequest;
import com.dingtalk.api.response.OapiUserGetResponse;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.simple.common.model.TechFiles;
import com.simple.common.model.ZhihuiUsers;
import com.simple.ding.DingController;
import com.simple.ding.DingUtil;
import com.simple.file.MyFileRender;
import com.taobao.api.ApiException;

public class techPrintController extends Controller{
	//指向技术文件文控打印页面
	public void toWkPrint() {
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		String sql_userRole="select * from user_role  where user_id='"+user.getUserid()+"' and (role_id=1 or role_id=3)";
		List<Record> userRoleList=Db.use("dc").find(sql_userRole);
		if (userRoleList.size()>0) {
			set("isPrint", 1);//是打印
		}else {
			set("isPrint", 0);
		}
		render("wkPrintList.html");
	}
	//获取审批通过的打印文件列表
	public void getPrintList() {
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		StringBuilder sb=new StringBuilder();
		int page=getInt("page");
		int limit=getInt("limit");
		int fileStatus=getParaToInt("fileStatus");
		String sql_userRole="select * from user_role  where user_id='"+user.getUserid()+"' and (role_id=1 or role_id=3)";
		List<Record> userRoleList=Db.use("dc").find(sql_userRole);
		if (userRoleList.size()>0) {
			sb.append(" from tech_files where (sp_status=1 or sp_status>=3) ");
			int printUser=getParaToInt("printUser");
			if (printUser==1) {
				sb.append(" and print_user_id='314250561633681831' ");
			}
			if (printUser==2) {
				sb.append(" and print_user_id='04123429531145674' ");
			}
		}else {
			sb.append(" from tech_files where (sp_status=1 or sp_status>=3) and print_user_id='"+user.getUserid()+"'");
		}
		//sb.append(" from tech_files where (sp_status=1 or sp_status>=3) and print_user_id='"+user.getUserid()+"'");
		if (!"".equals(get("file_name")) && get("file_name")!=null) {
			sb.append(" and file_name like '%"+get("file_name")+"%'");
		}
		if (!"".equals(get("zc_pro_code")) && get("zc_pro_code")!=null) {
			sb.append(" and nb_zc_Code like '%"+get("zc_pro_code")+"%'");
		}
		if (!"".equals(get("nb_file_code")) && get("nb_file_code")!=null) {
			sb.append(" and nb_fileCode like '%"+get("nb_file_code")+"%'");
		}
		if (fileStatus==1) {
			sb.append(" and sp_status=1 ");
		}else if (fileStatus==3) {
			sb.append(" and sp_status=3 ");
		}else if (fileStatus==4) {
			sb.append(" and sp_status=4 ");
		}else if (fileStatus==579) {
			sb.append(" and (sp_status=5 or  sp_status=7 or sp_status=9) ");
		}else if (fileStatus==10) {
			sb.append(" and sp_status=10 ");
		}else if (fileStatus==8) {
			sb.append(" and sp_status=8 ");
		}
		Page<Record> printList=Db.use("dc").paginate(page, limit, "select * ,use_cycle-DATEDIFF(NOW(),create_time) AS sy ",sb.toString()+" order by id desc");
		Record record=new Record();
		record.set("code", 0);
		record.set("msg", "获取打印文件列表成功！");
		record.set("count", printList.getTotalRow());
		record.set("data", printList.getList());
		renderJson(record);
	}
	//临时文件详情
	public void lsfileDetail() {
		int id=getParaToInt(0);
		Record file=Db.use("dc").findById("tech_files", id);
		int is_change=file.getInt("is_change");
		if (is_change==0) {
			setAttr("changeName", "否");
		}else {
			setAttr("changeName", "是");
		}
		setAttr("file", file);
		String accessToken_user=DingController.getAccessToken();
		DingTalkClient client_user = new DefaultDingTalkClient("https://oapi.dingtalk.com/user/get");
		//创建人
		String create_userId=file.getStr("create_user");
		OapiUserGetRequest create_user=new OapiUserGetRequest();
		create_user.setUserid(create_userId);
		create_user.setHttpMethod("GET");
		OapiUserGetResponse  response_create=null;
		try {
			response_create=client_user.execute(create_user,accessToken_user);
		} catch (ApiException e) {
			e.printStackTrace();
		}
		setAttr("create_user", response_create.getName());
		//审批人
		String sp_userId=file.getStr("sp_user_id");
		OapiUserGetRequest request_user = new OapiUserGetRequest();
		request_user.setUserid(sp_userId);
		request_user.setHttpMethod("GET");
		OapiUserGetResponse  response_user = null;
		try {
			response_user = client_user.execute(request_user, accessToken_user);
		}catch(ApiException e) {
			e.printStackTrace();
		}
		//打印人
		String print_user_id=file.getStr("print_user_id");
		OapiUserGetRequest request_print = new OapiUserGetRequest();
		request_print.setUserid(print_user_id);
		request_print.setHttpMethod("GET");
		OapiUserGetResponse  response_print = null;
		try {
			response_print = client_user.execute(request_print, accessToken_user);
		}catch(ApiException e) {
			e.printStackTrace();
		}
		setAttr("sp_name", response_user.getName());
		setAttr("print_user", response_print.getName());
		setAttr("fujianName", fujian(file.getInt("fujian_id")));
		render("lsfileDetail.html");
	}
	//正式文件详情
	public void zsfileDetail() {
		int id=getParaToInt(0);
		Record file=Db.use("dc").findById("tech_files", id);
		int is_change=file.getInt("is_change");
		if (is_change==0) {
			setAttr("changeName", "否");
		}else {
			setAttr("changeName", "是");
		}
		setAttr("file", file);
		String accessToken_user=DingController.getAccessToken();
		DingTalkClient client_user = new DefaultDingTalkClient("https://oapi.dingtalk.com/user/get");
		//创建人
		String create_userId=file.getStr("create_user");
		OapiUserGetRequest create_user=new OapiUserGetRequest();
		create_user.setUserid(create_userId);
		create_user.setHttpMethod("GET");
		OapiUserGetResponse  response_create=null;
		try {
			response_create=client_user.execute(create_user,accessToken_user);
		} catch (ApiException e) {
			e.printStackTrace();
		}
		setAttr("create_user", response_create.getName());
		//审批人
		String sp_userId=file.getStr("sp_user_id");
		OapiUserGetRequest request_user = new OapiUserGetRequest();
		request_user.setUserid(sp_userId);
		request_user.setHttpMethod("GET");
		OapiUserGetResponse  response_user = null;
		try {
			response_user = client_user.execute(request_user, accessToken_user);
		}catch(ApiException e) {
			e.printStackTrace();
		}
		//打印人
		String print_user_id=file.getStr("print_user_id");
		OapiUserGetRequest request_print = new OapiUserGetRequest();
		request_print.setUserid(print_user_id);
		request_print.setHttpMethod("GET");
		OapiUserGetResponse  response_print = null;
		try {
			response_print = client_user.execute(request_print, accessToken_user);
		}catch(ApiException e) {
			e.printStackTrace();
		}
		setAttr("sp_name", response_user.getName());
		setAttr("print_user", response_print.getName());
		setAttr("fujianName", fujian(file.getInt("fujian_id")));
		render("zsfileDetail.html");
	}
	//指向下发文件页面
	public void PrintFile(){
		int file_id=getParaToInt(0);
		setAttr("id", file_id);
		render("printFile.html");
	}
	//打印文件逻辑处理
	public void doPrint() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
		Date d= new Date();
		String date = sdf.format(d);
		int file_id=getParaToInt("id");
		String seal=get("fieldValue");
		Record file=Db.use("dc").findById("tech_files", file_id);
		file.set("seal", seal);
		file.set("sp_status", 3);//下发
		Db.use("dc").update("tech_files", file);
		//通知接收人领取文件
		String sql_users="select * from tech_files_receive where tech_files_id="+file_id;
		List<Record> receive_user=Db.use("dc").find(sql_users);
		String usersId="";
		for (int i = 0; i < receive_user.size(); i++) {
			usersId+=receive_user.get(i).getStr("receive_userId")+",";
		}
		DingUtil.sendText(usersId, "文件领用通知：文件“"+file.get("file_name")+"”已打印，请及时到文控处领取！"+date);
		//下发之后通知知会人
		String sql_zhihui="select * from zhihui_users where tech_files_id="+file_id;
		List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
		for (int i = 0; i < zhihuiList.size(); i++) {
			DingUtil.sendText(zhihuiList.get(i).getStr("zhihui_userId"), "知会通知：生产文件“ "+file.getStr("file_name")+" ” 已打印等待领取，请知悉！"+date);
		}
		Record rd=new Record();
		rd.set("code", 0);
		rd.set("msg", "文件已打印！");
		rd.set("data","" );
		renderJson(rd);
	}
	//文控专员确认回收作废文件
	public void Recovery() {
		int file_id=getParaToInt(0);
		Record file=Db.use("dc").findById("tech_files", file_id);
		//判断回收文件是正式文件还是临时文件
		int release_type=file.getInt("release_type");
		if (release_type==2) {
			//临时文件作废
			file.set("sp_status", 8);
			Db.use("dc").update("tech_files", file);
		} else {
			//老版文件作废
			file.set("sp_status", 8);
			Db.use("dc").update("tech_files", file);
			//新版文件可以打印
			String nb_fileCode=file.getStr("nb_fileCode");
			Record new_file=Db.use("dc").findFirst("select * from tech_files where nb_fileCode='"+nb_fileCode+"' and sp_status=6");
			new_file.set("sp_status", 1); //1-审批通过待打印
			Db.use("dc").update("tech_files", new_file);
		}
		//用户接收表中状态改为已回收-2
		String sql_user="select * from tech_files_receive where tech_files_id="+file_id;
		List<Record> userList=Db.use("dc").find(sql_user);
		for (int i = 0; i < userList.size(); i++) {
			Record usersList=new Record();
			usersList=Db.use("dc").findById("tech_files_receive", userList.get(i).getInt("id"));
			usersList.set("receive", 2);
			Db.use("dc").update("tech_files_receive", usersList);
		}
		Record rd=new Record();
		rd.set("code", 0);
		rd.set("msg", "文件已作废！");
		renderJson(rd);

	}
	//附件下载
	public void fujianDownload() {
		Record rd=new Record();
		int file_id=getParaToInt(0);
		Record file=Db.use("dc").findById("tech_files", file_id);
		String sql_fj="select * from tech_file_fujian where id="+file.getInt("fujian_id");
		Record fujian=Db.use("dc").findFirst(sql_fj);
		try {
			render(new MyFileRender(fujian.getStr("fujian_name")));
		}catch (Exception e){
			rd.set("code", 1);
			rd.set("msg", "暂无附件下载");
			rd.set("data", "");
			renderJson(rd);
		}
	}
	public void pingZhengDownload() {//变更凭证下载
		Record rd=new Record();
		int file_id=getParaToInt(0);
		Record file=Db.use("dc").findById("tech_files", file_id);
		String sql_fj="select * from tech_file_fujian where id="+file.getInt("chang_fujian_id");
		Record fujian=Db.use("dc").findFirst(sql_fj);
		try {
			render(new MyFileRender(fujian.getStr("fujian_name")));
		}catch (Exception e){
			rd.set("code", 1);
			rd.set("msg", "暂无附件下载");
			rd.set("data", "");
			renderJson(rd);
		}
	}
	//文控专员通知所有接收人回收即将作废文件
	public void noticeUser() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
		Date d= new Date();
		String date = sdf.format(d);
		
		int file_id=getParaToInt(0);
		Record file=Db.use("dc").findById("tech_files", file_id);
		file.set("sp_status", 9);//已通知回收，等待接收人回交文件
		Db.use("dc").update("tech_files",file);
		String sql_user="select * from tech_files_receive where tech_files_id="+file_id;
		List<Record> receive_user=Db.use("dc").find(sql_user);
		String usersId="";
		for (int i = 0; i < receive_user.size(); i++) {
			usersId+=receive_user.get(i).getStr("receive_userId")+",";
		}
		DingUtil.sendText(usersId, "文件回收通知：生产文件“"+file.get("file_name")+"”,版本号“ "+file.get("edition")+"”需销毁，将文件提交至文控处，请知悉~"+date);
		Record rd=new Record();
		rd.set("code", 0);
		rd.set("msg", "已通知接收人交回文件,且文件不可用!");
		rd.set("data", "");
		renderJson(rd);
	}
	//编辑正式文件
	public void toEditZsTech() {
		int id=getParaToInt(0);
		Record file=Db.use("dc").findById("tech_files", id);
		setAttr("file", file);
		setAttr("zcProCodeList", getZcProCode());
		setAttr("fujianName", fujian(file.getInt("fujian_id")));
		setAttr("dayin", wkUserList());
		render("EditZsFile.html");
	}
	public void doEditZsTechFile() {
		Date date=new Date();
		Record rd=new Record();
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		int id=getParaToInt("id");
		String file_type=get("file_type");
		String zc_pro_code=get("zc_pro_code");
		String nb_file_code=get("nb_file_code");
		String file_name=get("file_name");
		String edition=get("edition");
		String purpose=get("purpose");
		String print_explain=get("print_explain");
		int give_num=getParaToInt("give_num");
		int fujianId=getParaToInt("fujian_id");
		String print_user_id=get("dayin");
		rd.set("code", 0);
		rd.set("msg", "编辑文件成功！");
		rd.set("data","" );
		//修改tech_files表数据
		Record editZsFile=Db.use("dc").findById("tech_files", id);
		editZsFile.set("file_type", file_type);
		editZsFile.set("nb_zc_Code", zc_pro_code);
		editZsFile.set("nb_fileCode", nb_file_code);
		editZsFile.set("file_name", file_name);
		editZsFile.set("edition", edition);
		editZsFile.set("purpose", purpose);
		editZsFile.set("print_explain", print_explain);
		editZsFile.set("give_num", give_num);
		editZsFile.set("print_user_id", print_user_id);
		editZsFile.set("fujian_id",fujianId); //附件id
		Db.use("dc").update("tech_files", editZsFile);
		//获取接收员
		String userIds=get("userId");
		if(!"0".equals(userIds)) {
			String userNames=get("userSel_rec");
			String[] userId=userIds.split("\\,");
			String[] userName=userNames.split("\\,");
			for (int i = 0; i < userId.length; i++) {
				Record receiveUser=new Record();
				receiveUser.set("tech_files_id", id);
				receiveUser.set("receive_userId", userId[i]);
				receiveUser.set("receive_userName", userName[i]);
				Db.use("dc").save("tech_files_receive", receiveUser);
			}
		}
//		//获取知会人员
//		String zhiHuiUserId=get("zhiHuiUserId");
//		if (!"0".equals(zhiHuiUserId)) {
//			String zhihuiUserSel_rec=get("zhihuiUserSel_rec");
//			String[] zhiHuiUserIds=zhiHuiUserId.split("\\,");
//			String[] zhihuiUserSel_recs=zhihuiUserSel_rec.split("\\,");
//			for (int i = 0; i < zhiHuiUserIds.length; i++) {
//				Record zhiHuiUser=new Record();
//				zhiHuiUser.set("tech_files_id", id);
//				zhiHuiUser.set("zhihui_userId", zhiHuiUserIds[i]);
//				zhiHuiUser.set("zhihui_userName", zhihuiUserSel_recs[i]);
//				Db.use("dc").save("zhihui_users", zhiHuiUser);
//			}
//		}
		String zhiHuiUserId1=get("zhiHuiUserId1");
		if (!"0".equals(zhiHuiUserId1)) {
			String zhihuiUserSel_rec1=get("zhihuiUserSel_rec1");
			String[] zhiHuiIds=zhiHuiUserId1.split(",");
			String[] zhiHuiNames=zhihuiUserSel_rec1.split(",");
			for (int i = 0; i < zhiHuiNames.length; i++) {
				ZhihuiUsers zhihuiUsers=getModel(ZhihuiUsers.class);
				zhihuiUsers.setTechFilesId(id)
				.setZhihuiUserid(zhiHuiIds[i]).setZhihuiUsername(zhiHuiNames[i])
				.setType(1);
				zhihuiUsers.save();
			}
		}
		//不可查看附件
		String zhiHuiUserId2=get("zhiHuiUserId2");
		if (!"0".equals(zhiHuiUserId2)) {
			String zhihuiUserSel_rec2=get("zhihuiUserSel_rec2");
			String[] zhiHuiIds=zhiHuiUserId2.split(",");
			String[] zhiHuiNames=zhihuiUserSel_rec2.split(",");
			for (int i = 0; i < zhiHuiNames.length; i++) {
				ZhihuiUsers zhihuiUsers=getModel(ZhihuiUsers.class);
				zhihuiUsers.setTechFilesId(id)
				.setZhihuiUserid(zhiHuiIds[i]).setZhihuiUsername(zhiHuiNames[i])
				.setType(0);
				zhihuiUsers.save();
			}
		}
		//编辑记录表中新增记录
		Record editRecord=new Record();
		editRecord.set("edit_userId", user.getUserid());
		editRecord.set("edit_userName", user.getName());
		editRecord.set("tech_file_id", id);
		editRecord.set("tech_file_code", nb_file_code);
		editRecord.set("create_time", date);
		Db.use("dc").save("edit_tech_file", editRecord);
		
		renderJson(rd);
	}
	//编辑临时文件
	public void toEditLsTech() {
		int id=getParaToInt(0);
		Record file=Db.use("dc").findById("tech_files", id);
		setAttr("file", file);
		setAttr("zcProCodeList", getZcProCode());
		setAttr("fujianName", fujian(file.getInt("fujian_id")));
		setAttr("dayin", wkUserList());
		render("EditLsFile.html");
	}
	public void doEditLsTechFile() {
		Date date=new Date();
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		Record rd=new Record();
		//获取页面数据
		int id=getParaToInt("id");
		String file_type=get("file_type");
		String zc_pro_code=get("zc_pro_code");
		String nb_file_code=get("nb_file_code");
		String file_name=get("file_name");
		String edition=get("edition");
		String purpose=get("purpose");
		String print_explain=get("print_explain");
		int int_use_cycle=getParaToInt("int_use_cycle");
		String unit_use_cycle=get("unit_use_cycle");
		int give_num=getParaToInt("give_num");
		int fujianId=getParaToInt("fujian_id");
		String print_user_id=get("dayin");
		rd.set("code", 0);
		rd.set("msg", "临时文件发布成功！");
		rd.set("data","" );
		//修改tech_files表数据
		Record editLsFile=Db.use("dc").findById("tech_files", id);
		editLsFile.set("file_type", file_type);
		editLsFile.set("nb_zc_Code", zc_pro_code);
		editLsFile.set("nb_fileCode", nb_file_code);
		editLsFile.set("file_name", file_name);
		editLsFile.set("edition", edition);
		editLsFile.set("print_explain", print_explain);
		editLsFile.set("purpose", purpose);
		editLsFile.set("give_num", give_num);
		editLsFile.set("print_user_id", print_user_id);
		editLsFile.set("fujian_id",fujianId); //附件id
		int use_cycle=0;
		if(unit_use_cycle.equals("天")) {
			use_cycle=int_use_cycle;
		}else if (unit_use_cycle.equals("周")) {
			use_cycle=int_use_cycle*7;
		}else if (unit_use_cycle.equals("月")) {
			use_cycle=int_use_cycle*30;
		}else if (unit_use_cycle.equals("年")) {
			use_cycle=int_use_cycle*365;
		}
		editLsFile.set("use_cycle", use_cycle);
		Db.use("dc").update("tech_files", editLsFile);
		//获取接收人员
		String userIds=get("userId");
		if(!"0".equals(userIds)) {
			String userNames=get("userSel_rec");
			String[] userId=userIds.split("\\,");
			String[] userName=userNames.split("\\,");
			for (int i = 0; i < userId.length; i++) {
				Record receiveUser=new Record();
				receiveUser.set("tech_files_id", id);
				receiveUser.set("receive_userId", userId[i]);
				receiveUser.set("receive_userName", userName[i]);
				Db.use("dc").save("tech_files_receive", receiveUser);
			}
		}
//		//获取知会人员
//		String zhiHuiUserId=get("zhiHuiUserId");
//		if (!"0".equals(zhiHuiUserId)) {
//			String zhihuiUserSel_rec=get("zhihuiUserSel_rec");
//			String[] zhiHuiUserIds=zhiHuiUserId.split("\\,");
//			String[] zhihuiUserSel_recs=zhihuiUserSel_rec.split("\\,");
//			for (int i = 0; i < zhiHuiUserIds.length; i++) {
//				Record zhiHuiUser=new Record();
//				zhiHuiUser.set("tech_files_id", id);
//				zhiHuiUser.set("zhihui_userId", zhiHuiUserIds[i]);
//				zhiHuiUser.set("zhihui_userName", zhihuiUserSel_recs[i]);
//				Db.use("dc").save("zhihui_users", zhiHuiUser);
//			}
//		}
		String zhiHuiUserId1=get("zhiHuiUserId1");
		if (!"0".equals(zhiHuiUserId1)) {
			String zhihuiUserSel_rec1=get("zhihuiUserSel_rec1");
			String[] zhiHuiIds=zhiHuiUserId1.split(",");
			String[] zhiHuiNames=zhihuiUserSel_rec1.split(",");
			for (int i = 0; i < zhiHuiNames.length; i++) {
				ZhihuiUsers zhihuiUsers=getModel(ZhihuiUsers.class);
				zhihuiUsers.setTechFilesId(id)
				.setZhihuiUserid(zhiHuiIds[i]).setZhihuiUsername(zhiHuiNames[i])
				.setType(1);
				zhihuiUsers.save();
			}
		}
		//不可查看附件
		String zhiHuiUserId2=get("zhiHuiUserId2");
		if (!"0".equals(zhiHuiUserId2)) {
			String zhihuiUserSel_rec2=get("zhihuiUserSel_rec2");
			String[] zhiHuiIds=zhiHuiUserId2.split(",");
			String[] zhiHuiNames=zhihuiUserSel_rec2.split(",");
			for (int i = 0; i < zhiHuiNames.length; i++) {
				ZhihuiUsers zhihuiUsers=getModel(ZhihuiUsers.class);
				zhihuiUsers.setTechFilesId(id)
				.setZhihuiUserid(zhiHuiIds[i]).setZhihuiUsername(zhiHuiNames[i])
				.setType(0);
				zhihuiUsers.save();
			}
		}
		//编辑记录表中新增记录
		Record editRecord=new Record();
		editRecord.set("edit_userId", user.getUserid());
		editRecord.set("edit_userName", user.getName());
		editRecord.set("tech_file_id", id);
		editRecord.set("tech_file_code", nb_file_code);
		editRecord.set("create_time", date);
		Db.use("dc").save("edit_tech_file", editRecord);
		renderJson(rd);
	}
	//通知接收人领取文件
	public void lingqu() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
		Date d= new Date();
		String date = sdf.format(d);
		int id=getParaToInt(0);
		Record user=Db.use("dc").findById("tech_files_receive", id);
		String userid=user.getStr("receive_userId");
		DingUtil.sendText(userid,"文件领用通知：您有文件待领取，请及时领取！"+date);
		Record rd=new Record();
		rd.set("code", 0);
		rd.set("msg", "通知成功");
		rd.set("data", "");
		renderJson(rd);
	}
	//通知某个接收人回收文件
	public void huishou() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
		Date d= new Date();
		String date = sdf.format(d);
		int id=getParaToInt(0);
		Record user=Db.use("dc").findById("tech_files_receive", id);
		Record file=Db.use("dc").findById("tech_files", user.getInt("tech_files_id"));
		String userid=user.getStr("receive_userId");
		DingUtil.sendText(userid,"文件回收通知：生产文件“"+file.get("file_name")+"”,版本号“ "+file.get("edition")+"”需销毁，将文件提交至文控处，请知悉~"+date);
		Record rd=new Record();
		rd.set("code", 0);
		rd.set("msg", "通知成功");
		rd.set("data", "");
		renderJson(rd);
	}
	//确认已上交文件至文控
	public void hasBack() {
		int id=getParaToInt("id");
		int receive_num=getParaToInt("receive_num");
		String remark=getPara("remark");
		Record receive=Db.use("dc").findById("tech_files_receive", id);
		receive.set("receive", 2);//确认已回收文件
		receive.set("backNum", receive_num);
		receive.set("backRemark", remark);
		Db.use("dc").update("tech_files_receive", receive);
		//判断是否所有人已经上交文件
		String sql_user="select * from tech_files_receive where tech_files_id="+receive.getInt("tech_files_id");
		List<Record> userList=Db.use("dc").find(sql_user);
		int a=0;
		for (int i = 0; i < userList.size(); i++) {
			int status=userList.get(i).getInt("receive");
			if (status==1) {
				a=1; //有未上交的人员时,跳出循环,文件状态仍然是"等待回收"
				break;
			}
		}
		//根据a的值判断文件是否已经全部上交
		Record file=Db.use("dc").findById("tech_files", receive.getInt("tech_files_id"));
		if (a==0) {
			file.set("sp_status", 10);//全部上交,状态变为10-等待销毁
			Db.use("dc").update("tech_files", file);
		}
		Record record=new Record();
		record.set("code", 0);
		record.set("msg", "文件回收成功！");
		record.set("data", "");
		renderJson(record);
	}
	//指向确认回收页面
	public void HuiShou() {
		int file_id=getParaToInt(0);
		setAttr("id", file_id);
		render("HuiShou.html");
	}
	//指向打印驳回页面
	public void toPrintBohui() {
		int id=getParaToInt(0);
		TechFiles file=TechFiles.dao.findById(id);
		set("file", file);
		set("id", id);
		render("bohuiReason.html");
	}
	//处理驳回
	public void doPrintBohui() {
		int id=getParaToInt("id");
		String bohuiReason=get("bohuiReason");
		TechFiles techFile=TechFiles.dao.findById(id);
		techFile.setSpStatus(2);//审批驳回
		techFile.update();
		DingUtil.sendText(techFile.getCreateUser(), "生产文件【"+techFile.getFileName()+"】被驳回，驳回理由："+bohuiReason+"。请知悉！");
		Record record=new Record();
		record.set("code", 0);
		record.set("msg", "驳回成功");
		record.set("data", "");
		renderJson(record);
	}
	/**
	 * 公用模块
	 * @return
	 */
	//获取项目总成号
	public List<Record> getZcProCode(){
		String sql_zc="select * from codeaparts";
		List<Record> zcProList=Db.use("dc").find(sql_zc);
		return zcProList;
	}
	//获取附件
	public String fujian(int fujianId) {
		Record fujian=Db.use("dc").findById("tech_file_fujian", fujianId);
		String fujianName=fujian.getStr("fujian_name");
		return fujianName;
	}
	//获取文控角色用户列表
	public List<Record> wkUserList() {
		String sql_wkUser="select * from user_role where role_id=3";
		List<Record> wk_users=Db.use("dc").find(sql_wkUser);
		return wk_users;
	}

}
