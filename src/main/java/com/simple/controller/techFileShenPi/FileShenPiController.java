package com.simple.controller.techFileShenPi;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.OapiUserGetRequest;
import com.dingtalk.api.response.OapiUserGetResponse;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.simple.ding.DingController;
import com.simple.ding.DingUtil;
import com.simple.file.MyFileRender;
import com.taobao.api.ApiException;

public class FileShenPiController extends Controller{
	//指向审批文件列表页面
	public void toShenPiList() {
		render("shenpiList.html");
	}
	//获取当前用户所有审批文件
	public void getShenPiList() {
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		StringBuilder sb=new StringBuilder();
		int page=getInt("page");
		int limit=getInt("limit");
		Record jsp=new Record();
		jsp.set("code", 0);
		String sql_userRole="select * from user_role  where user_id='"+user.getUserid()+"' and role_id=1";
		List<Record> userRoleList=Db.use("dc").find(sql_userRole);
		if (userRoleList.size()>0) {
			sb.append(" from  (select * from caigou_tech_file where  sp_status<>20 and sp_status<>22 ");
		}else {
			sb.append(" from  (select * from caigou_tech_file where sp_user_id='"+user.getUserid()+"' and sp_status<>20 and sp_status<>22 ");
		}
		//sb.append(" from  (select * from caigou_tech_file where sp_user_id='"+user.getUserid()+"' and sp_status<>20 and sp_status<>22 ");
		if (!"".equals(get("file_name")) && get("file_name")!=null) {
			sb.append(" and file_name like '%"+get("file_name")+"%'");
		}
		if (!"".equals(get("zc_pro_code")) && get("zc_pro_code")!=null) {
			sb.append(" and nb_zc_Code like '%"+get("zc_pro_code")+"%'");
		}
		if (!"".equals(get("nb_file_code")) && get("nb_file_code")!=null) {
			sb.append(" and nb_fileCode like '%"+get("nb_file_code")+"%'");
		}
		if (userRoleList.size()>0) {
			sb.append(" union all select * from kehu_tech_file where sp_status<>20 and sp_status<>22 ");
		}else {
			sb.append(" union all select * from kehu_tech_file where sp_user_id='"+user.getUserid()+"' and sp_status<>20 and sp_status<>22 ");
		}
		//sb.append(" union all select * from kehu_tech_file where sp_user_id='"+user.getUserid()+"' and sp_status<>20 and sp_status<>22 ");
		if (!"".equals(get("file_name")) && get("file_name")!=null) {
			sb.append(" and file_name like '%"+get("file_name")+"%'");
		}
		if (!"".equals(get("zc_pro_code")) && get("zc_pro_code")!=null) {
			sb.append(" and nb_zc_Code like '%"+get("zc_pro_code")+"%'");
		}
		if (!"".equals(get("nb_file_code")) && get("nb_file_code")!=null) {
			sb.append(" and nb_fileCode like '%"+get("nb_file_code")+"%'");
		}
		if (userRoleList.size()>0) {
			sb.append(" union all select * from xiangmu_tech_file where  sp_status<>20 and sp_status<>22 ");
		}else {
			sb.append(" union all select * from xiangmu_tech_file where sp_user_id='"+user.getUserid()+"' and sp_status<>20 and sp_status<>22 ");
		}
		//sb.append(" union all select * from xiangmu_tech_file where sp_user_id='"+user.getUserid()+"' and sp_status<>20 and sp_status<>22 ");
		if (!"".equals(get("file_name")) && get("file_name")!=null) {
			sb.append(" and file_name like '%"+get("file_name")+"%'");
		}
		if (!"".equals(get("zc_pro_code")) && get("zc_pro_code")!=null) {
			sb.append(" and nb_zc_Code like '%"+get("zc_pro_code")+"%'");
		}
		if (!"".equals(get("nb_file_code")) && get("nb_file_code")!=null) {
			sb.append(" and nb_fileCode like '%"+get("nb_file_code")+"%'");
		}
		if (userRoleList.size()>0) {
			sb.append(" union all select * from tech_files where 1=1 ");
		}else {
			sb.append(" union all select * from tech_files where sp_user_id='"+user.getUserid()+"' ");
		}
		//sb.append(" union all select * from tech_files where sp_user_id='"+user.getUserid()+"' ");
		if (!"".equals(get("file_name")) && get("file_name")!=null) {
			sb.append(" and file_name like '%"+get("file_name")+"%'");
		}
		if (!"".equals(get("zc_pro_code")) && get("zc_pro_code")!=null) {
			sb.append(" and nb_zc_Code like '%"+get("zc_pro_code")+"%'");
		}
		if (!"".equals(get("nb_file_code")) && get("nb_file_code")!=null) {
			sb.append(" and nb_fileCode like '%"+get("nb_file_code")+"%'");
		}
		if (userRoleList.size()>0) {
			sb.append(" union all select * from zhiliang_tech_file where  sp_status<>20 and sp_status<>22");
		}else {
			sb.append(" union all select * from zhiliang_tech_file where sp_user_id='"+user.getUserid()+"' and sp_status<>20 and sp_status<>22");
		}
		//sb.append(" union all select * from zhiliang_tech_file where sp_user_id='"+user.getUserid()+"' and sp_status<>20 and sp_status<>22");
		if (!"".equals(get("file_name")) && get("file_name")!=null) {
			sb.append(" and file_name like '%"+get("file_name")+"%'");
		}
		if (!"".equals(get("zc_pro_code")) && get("zc_pro_code")!=null) {
			sb.append(" and nb_zc_Code like '%"+get("zc_pro_code")+"%'");
		}
		if (!"".equals(get("nb_file_code")) && get("nb_file_code")!=null) {
			sb.append(" and nb_fileCode like '%"+get("nb_file_code")+"%'");
		}
		Page<Record> shenpiList=Db.use("dc").paginate(page, limit, "select * ,use_cycle-DATEDIFF(NOW(),create_time) AS sy ",sb.toString()+") temp order by create_time desc");
		Record record=new Record();
		record.set("code", 0);
		record.set("msg", "获取审批表成功！");
		record.set("count", shenpiList.getTotalRow());
		record.set("data", shenpiList.getList());
		renderJson(record);
	}
	//审批列表页面获取历史记录
	public void historyList() {
		int id=getParaToInt("id");
		int status=getParaToInt("status");
		if (status==1) {
			Record file=Db.use("dc").findById("caigou_tech_file", id);
			setAttr("file", file);
			render("/page/caigou/caigouPublish/caigouFile_history.html");
		} else if (status==2){
			Record file=Db.use("dc").findById("kehu_tech_file", id);
			setAttr("file", file);
			render("/page/kehu/kehuPublish/kehuFile_history.html");
		}else if (status==3){
			Record file=Db.use("dc").findById("tech_files", id);
			setAttr("file", file);
			render("/page/shengchan/techPublish/tech_history.html");
		}else if (status==4){
			Record file=Db.use("dc").findById("xiangmu_tech_file", id);
			setAttr("file", file);
			render("/page/xiangmu/xiangmuPublish/xiangmuFile_history.html");
		}else if (status==5){
			Record file=Db.use("dc").findById("zhiliang_tech_file", id);
			setAttr("file", file);
			render("/page/zhiliang/zhiliangPublish/zhiliangFile_history.html");
		}
	}
	//正式文件详情
	public void zsfileDetail() {
		int id=getParaToInt("id");
		int status=getParaToInt("status");
		if (status==1) {
			Record file=Db.use("dc").findById("caigou_tech_file", id);
			int is_change=file.getInt("is_change");
			if (is_change==0) {
				setAttr("changeName", "否");
			}else {
				setAttr("changeName", "是");
			}
			long chang_fujian_id=file.getInt("chang_fujian_id");
			if (chang_fujian_id==0) {
				setAttr("changeFujian", "");
			}else {
				Record fujian=Db.use("dc").findById("caigou_tech_file_fujian", file.getInt("chang_fujian_id"));
				setAttr("changeFujian", fujian.get("fujian_name"));
			}
			setAttr("file", file);
			String accessToken_user=DingController.getAccessToken();
			DingTalkClient client_user = new DefaultDingTalkClient("https://oapi.dingtalk.com/user/get");
			//创建人
			String create_userId=file.getStr("create_user");
			OapiUserGetRequest create_user=new OapiUserGetRequest();
			create_user.setUserid(create_userId);
			create_user.setHttpMethod("GET");
			OapiUserGetResponse  response_create=null;
			try {
				response_create=client_user.execute(create_user,accessToken_user);
			} catch (ApiException e) {
				e.printStackTrace();
			}
			setAttr("create_user", response_create.getName());
			//审批人
			String sp_userId=file.getStr("sp_user_id");
			OapiUserGetRequest request_user = new OapiUserGetRequest();
			request_user.setUserid(sp_userId);
			request_user.setHttpMethod("GET");
			OapiUserGetResponse  response_user = null;
			try {
				response_user = client_user.execute(request_user, accessToken_user);
			}catch(ApiException e) {
				e.printStackTrace();
			}
			//打印人
			String print_user_id=file.getStr("print_user_id");
			OapiUserGetRequest request_print = new OapiUserGetRequest();
			request_print.setUserid(print_user_id);
			request_print.setHttpMethod("GET");
			OapiUserGetResponse  response_print = null;
			try {
				response_print = client_user.execute(request_print, accessToken_user);
			}catch(ApiException e) {
				e.printStackTrace();
			}
			setAttr("sp_name", response_user.getName());
			setAttr("print_user", response_print.getName());
			Record fujian=Db.use("dc").findById("caigou_tech_file_fujian", file.getInt("fujian_id"));
			setAttr("fujianName", fujian.get("fujian_name"));
			render("/page/caigou/caigouPublish/zsfileDetail.html");
		} else if (status==2){
			Record file=Db.use("dc").findById("kehu_tech_file", id);
			int is_change=file.getInt("is_change");
			if (is_change==0) {
				setAttr("changeName", "否");
			}else {
				setAttr("changeName", "是");
			}
			long chang_fujian_id=file.getInt("chang_fujian_id");
			if (chang_fujian_id==0) {
				setAttr("changeFujian", "");
			}else {
				Record fujian=Db.use("dc").findById("kehu_tech_file_fujian", file.getInt("chang_fujian_id"));
				setAttr("changeFujian", fujian.get("fujian_name"));
			}
			setAttr("file", file);
			String accessToken_user=DingController.getAccessToken();
			DingTalkClient client_user = new DefaultDingTalkClient("https://oapi.dingtalk.com/user/get");
			//创建人
			String create_userId=file.getStr("create_user");
			OapiUserGetRequest create_user=new OapiUserGetRequest();
			create_user.setUserid(create_userId);
			create_user.setHttpMethod("GET");
			OapiUserGetResponse  response_create=null;
			try {
				response_create=client_user.execute(create_user,accessToken_user);
			} catch (ApiException e) {
				e.printStackTrace();
			}
			setAttr("create_user", response_create.getName());
			//审批人
			String sp_userId=file.getStr("sp_user_id");
			OapiUserGetRequest request_user = new OapiUserGetRequest();
			request_user.setUserid(sp_userId);
			request_user.setHttpMethod("GET");
			OapiUserGetResponse  response_user = null;
			try {
				response_user = client_user.execute(request_user, accessToken_user);
			}catch(ApiException e) {
				e.printStackTrace();
			}
			//打印人
			String print_user_id=file.getStr("print_user_id");
			OapiUserGetRequest request_print = new OapiUserGetRequest();
			request_print.setUserid(print_user_id);
			request_print.setHttpMethod("GET");
			OapiUserGetResponse  response_print = null;
			try {
				response_print = client_user.execute(request_print, accessToken_user);
			}catch(ApiException e) {
				e.printStackTrace();
			}
			setAttr("sp_name", response_user.getName());
			setAttr("print_user", response_print.getName());
			Record fujian=Db.use("dc").findById("kehu_tech_file_fujian", file.getInt("fujian_id"));
			setAttr("fujianName", fujian.get("fujian_name"));
			render("/page/kehu/kehuPublish/zsfileDetail.html");
		}else if (status==3){
			Record file=Db.use("dc").findById("tech_files", id);
			int is_change=file.getInt("is_change");
			if (is_change==0) {
				setAttr("changeName", "否");
			}else {
				setAttr("changeName", "是");
			}
			long chang_fujian_id=file.getInt("chang_fujian_id");
			if (chang_fujian_id==0) {
				setAttr("changeFujian", "");
			}else {
				Record fujian=Db.use("dc").findById("tech_file_fujian", file.getInt("chang_fujian_id"));
				setAttr("changeFujian", fujian.get("fujian_name"));
			}
			setAttr("file", file);
			String accessToken_user=DingController.getAccessToken();
			DingTalkClient client_user = new DefaultDingTalkClient("https://oapi.dingtalk.com/user/get");
			//创建人
			String create_userId=file.getStr("create_user");
			OapiUserGetRequest create_user=new OapiUserGetRequest();
			create_user.setUserid(create_userId);
			create_user.setHttpMethod("GET");
			OapiUserGetResponse  response_create=null;
			try {
				response_create=client_user.execute(create_user,accessToken_user);
			} catch (ApiException e) {
				e.printStackTrace();
			}
			setAttr("create_user", response_create.getName());
			//审批人
			String sp_userId=file.getStr("sp_user_id");
			OapiUserGetRequest request_user = new OapiUserGetRequest();
			request_user.setUserid(sp_userId);
			request_user.setHttpMethod("GET");
			OapiUserGetResponse  response_user = null;
			try {
				response_user = client_user.execute(request_user, accessToken_user);
			}catch(ApiException e) {
				e.printStackTrace();
			}
			//打印人
			String print_user_id=file.getStr("print_user_id");
			OapiUserGetRequest request_print = new OapiUserGetRequest();
			request_print.setUserid(print_user_id);
			request_print.setHttpMethod("GET");
			OapiUserGetResponse  response_print = null;
			try {
				response_print = client_user.execute(request_print, accessToken_user);
			}catch(ApiException e) {
				e.printStackTrace();
			}
			setAttr("sp_name", response_user.getName());
			setAttr("print_user", response_print.getName());
			Record fujian=Db.use("dc").findById("tech_file_fujian", file.getInt("fujian_id"));
			setAttr("fujianName", fujian.get("fujian_name"));
			render("/page/shengchan/techPublish/zsfileDetail.html");
		}else if (status==4){
			Record file=Db.use("dc").findById("xiangmu_tech_file", id);
			int is_change=file.getInt("is_change");
			if (is_change==0) {
				setAttr("changeName", "否");
			}else {
				setAttr("changeName", "是");
			}
			long chang_fujian_id=file.getInt("chang_fujian_id");
			if (chang_fujian_id==0) {
				setAttr("changeFujian", "");
			}else {
				Record fujian=Db.use("dc").findById("xiangmu_tech_file_fujian", file.getInt("chang_fujian_id"));
				setAttr("changeFujian", fujian.get("fujian_name"));
			}
			setAttr("file", file);
			String accessToken_user=DingController.getAccessToken();
			DingTalkClient client_user = new DefaultDingTalkClient("https://oapi.dingtalk.com/user/get");
			//创建人
			String create_userId=file.getStr("create_user");
			OapiUserGetRequest create_user=new OapiUserGetRequest();
			create_user.setUserid(create_userId);
			create_user.setHttpMethod("GET");
			OapiUserGetResponse  response_create=null;
			try {
				response_create=client_user.execute(create_user,accessToken_user);
			} catch (ApiException e) {
				e.printStackTrace();
			}
			setAttr("create_user", response_create.getName());
			//审批人
			String sp_userId=file.getStr("sp_user_id");
			OapiUserGetRequest request_user = new OapiUserGetRequest();
			request_user.setUserid(sp_userId);
			request_user.setHttpMethod("GET");
			OapiUserGetResponse  response_user = null;
			try {
				response_user = client_user.execute(request_user, accessToken_user);
			}catch(ApiException e) {
				e.printStackTrace();
			}
			//打印人
			String print_user_id=file.getStr("print_user_id");
			OapiUserGetRequest request_print = new OapiUserGetRequest();
			request_print.setUserid(print_user_id);
			request_print.setHttpMethod("GET");
			OapiUserGetResponse  response_print = null;
			try {
				response_print = client_user.execute(request_print, accessToken_user);
			}catch(ApiException e) {
				e.printStackTrace();
			}
			setAttr("sp_name", response_user.getName());
			setAttr("print_user", response_print.getName());
			Record fujian=Db.use("dc").findById("xiangmu_tech_file_fujian", file.getInt("fujian_id"));
			setAttr("fujianName", fujian.get("fujian_name"));
			render("/page/xiangmu/xiangmuPublish/zsfileDetail.html");
		}else if (status==5){
			Record file=Db.use("dc").findById("zhiliang_tech_file", id);
			int is_change=file.getInt("is_change");
			if (is_change==0) {
				setAttr("changeName", "否");
			}else {
				setAttr("changeName", "是");
			}
			long chang_fujian_id=file.getInt("chang_fujian_id");
			if (chang_fujian_id==0) {
				setAttr("changeFujian", "");
			}else {
				Record fujian=Db.use("dc").findById("zhiliang_tech_file_fujian", file.getInt("chang_fujian_id"));
				setAttr("changeFujian", fujian.get("fujian_name"));
			}
			setAttr("file", file);
			String accessToken_user=DingController.getAccessToken();
			DingTalkClient client_user = new DefaultDingTalkClient("https://oapi.dingtalk.com/user/get");
			//创建人
			String create_userId=file.getStr("create_user");
			OapiUserGetRequest create_user=new OapiUserGetRequest();
			create_user.setUserid(create_userId);
			create_user.setHttpMethod("GET");
			OapiUserGetResponse  response_create=null;
			try {
				response_create=client_user.execute(create_user,accessToken_user);
			} catch (ApiException e) {
				e.printStackTrace();
			}
			setAttr("create_user", response_create.getName());
			//审批人
			String sp_userId=file.getStr("sp_user_id");
			OapiUserGetRequest request_user = new OapiUserGetRequest();
			request_user.setUserid(sp_userId);
			request_user.setHttpMethod("GET");
			OapiUserGetResponse  response_user = null;
			try {
				response_user = client_user.execute(request_user, accessToken_user);
			}catch(ApiException e) {
				e.printStackTrace();
			}
			//打印人
			String print_user_id=file.getStr("print_user_id");
			OapiUserGetRequest request_print = new OapiUserGetRequest();
			request_print.setUserid(print_user_id);
			request_print.setHttpMethod("GET");
			OapiUserGetResponse  response_print = null;
			try {
				response_print = client_user.execute(request_print, accessToken_user);
			}catch(ApiException e) {
				e.printStackTrace();
			}
			setAttr("sp_name", response_user.getName());
			setAttr("print_user", response_print.getName());
			Record fujian=Db.use("dc").findById("zhiliang_tech_file_fujian", file.getInt("fujian_id"));
			setAttr("fujianName", fujian.get("fujian_name"));
			render("/page/zhiliang/zhiliangPublish/zsfileDetail.html");
		}
	}
	//临时文件详情
	public void lsfileDetail() {
		int id=getParaToInt("id");
		int status=getParaToInt("status");
		if (status==1) {
			Record file=Db.use("dc").findById("caigou_tech_file", id);
			int is_change=file.getInt("is_change");
			if (is_change==0) {
				setAttr("changeName", "否");
			}else {
				setAttr("changeName", "是");
			}
			long chang_fujian_id=file.getInt("chang_fujian_id");
			if (chang_fujian_id==0) {
				setAttr("changeFujian", "");
			}else {
				Record fujian=Db.use("dc").findById("caigou_tech_file_fujian", file.getInt("chang_fujian_id"));
				setAttr("changeFujian", fujian.get("fujian_name"));
			}
			setAttr("file", file);
			String accessToken_user=DingController.getAccessToken();
			DingTalkClient client_user = new DefaultDingTalkClient("https://oapi.dingtalk.com/user/get");
			//创建人
			String create_userId=file.getStr("create_user");
			OapiUserGetRequest create_user=new OapiUserGetRequest();
			create_user.setUserid(create_userId);
			create_user.setHttpMethod("GET");
			OapiUserGetResponse  response_create=null;
			try {
				response_create=client_user.execute(create_user,accessToken_user);
			} catch (ApiException e) {
				e.printStackTrace();
			}
			setAttr("create_user", response_create.getName());
			//审批人
			String sp_userId=file.getStr("sp_user_id");
			OapiUserGetRequest request_user = new OapiUserGetRequest();
			request_user.setUserid(sp_userId);
			request_user.setHttpMethod("GET");
			OapiUserGetResponse  response_user = null;
			try {
				response_user = client_user.execute(request_user, accessToken_user);
			}catch(ApiException e) {
				e.printStackTrace();
			}
			//打印人
			String print_user_id=file.getStr("print_user_id");
			OapiUserGetRequest request_print = new OapiUserGetRequest();
			request_print.setUserid(print_user_id);
			request_print.setHttpMethod("GET");
			OapiUserGetResponse  response_print = null;
			try {
				response_print = client_user.execute(request_print, accessToken_user);
			}catch(ApiException e) {
				e.printStackTrace();
			}
			setAttr("sp_name", response_user.getName());
			setAttr("print_user", response_print.getName());
			Record fujian=Db.use("dc").findById("caigou_tech_file_fujian", file.getInt("fujian_id"));
			setAttr("fujianName", fujian.get("fujian_name"));
			render("/page/caigou/caigouPublish/lsfileDetail.html");
		} else if (status==2){
			Record file=Db.use("dc").findById("kehu_tech_file", id);
			int is_change=file.getInt("is_change");
			if (is_change==0) {
				setAttr("changeName", "否");
			}else {
				setAttr("changeName", "是");
			}
			long chang_fujian_id=file.getInt("chang_fujian_id");
			if (chang_fujian_id==0) {
				setAttr("changeFujian", "");
			}else {
				Record fujian=Db.use("dc").findById("kehu_tech_file_fujian", file.getInt("chang_fujian_id"));
				setAttr("changeFujian", fujian.get("fujian_name"));
			}
			setAttr("file", file);
			String accessToken_user=DingController.getAccessToken();
			DingTalkClient client_user = new DefaultDingTalkClient("https://oapi.dingtalk.com/user/get");
			//创建人
			String create_userId=file.getStr("create_user");
			OapiUserGetRequest create_user=new OapiUserGetRequest();
			create_user.setUserid(create_userId);
			create_user.setHttpMethod("GET");
			OapiUserGetResponse  response_create=null;
			try {
				response_create=client_user.execute(create_user,accessToken_user);
			} catch (ApiException e) {
				e.printStackTrace();
			}
			setAttr("create_user", response_create.getName());
			//审批人
			String sp_userId=file.getStr("sp_user_id");
			OapiUserGetRequest request_user = new OapiUserGetRequest();
			request_user.setUserid(sp_userId);
			request_user.setHttpMethod("GET");
			OapiUserGetResponse  response_user = null;
			try {
				response_user = client_user.execute(request_user, accessToken_user);
			}catch(ApiException e) {
				e.printStackTrace();
			}
			//打印人
			String print_user_id=file.getStr("print_user_id");
			OapiUserGetRequest request_print = new OapiUserGetRequest();
			request_print.setUserid(print_user_id);
			request_print.setHttpMethod("GET");
			OapiUserGetResponse  response_print = null;
			try {
				response_print = client_user.execute(request_print, accessToken_user);
			}catch(ApiException e) {
				e.printStackTrace();
			}
			setAttr("sp_name", response_user.getName());
			setAttr("print_user", response_print.getName());
			Record fujian=Db.use("dc").findById("kehu_tech_file_fujian", file.getInt("fujian_id"));
			setAttr("fujianName", fujian.get("fujian_name"));
			render("/page/kehu/kehuPublish/lsfileDetail.html");
		}else if (status==3){
			Record file=Db.use("dc").findById("tech_files", id);
			int is_change=file.getInt("is_change");
			if (is_change==0) {
				setAttr("changeName", "否");
			}else {
				setAttr("changeName", "是");
			}
			long chang_fujian_id=file.getInt("chang_fujian_id");
			if (chang_fujian_id==0) {
				setAttr("changeFujian", "");
			}else {
				Record fujian=Db.use("dc").findById("tech_file_fujian", file.getInt("chang_fujian_id"));
				setAttr("changeFujian", fujian.get("fujian_name"));
			}
			setAttr("file", file);
			String accessToken_user=DingController.getAccessToken();
			DingTalkClient client_user = new DefaultDingTalkClient("https://oapi.dingtalk.com/user/get");
			//创建人
			String create_userId=file.getStr("create_user");
			OapiUserGetRequest create_user=new OapiUserGetRequest();
			create_user.setUserid(create_userId);
			create_user.setHttpMethod("GET");
			OapiUserGetResponse  response_create=null;
			try {
				response_create=client_user.execute(create_user,accessToken_user);
			} catch (ApiException e) {
				e.printStackTrace();
			}
			setAttr("create_user", response_create.getName());
			//审批人
			String sp_userId=file.getStr("sp_user_id");
			OapiUserGetRequest request_user = new OapiUserGetRequest();
			request_user.setUserid(sp_userId);
			request_user.setHttpMethod("GET");
			OapiUserGetResponse  response_user = null;
			try {
				response_user = client_user.execute(request_user, accessToken_user);
			}catch(ApiException e) {
				e.printStackTrace();
			}
			//打印人
			String print_user_id=file.getStr("print_user_id");
			OapiUserGetRequest request_print = new OapiUserGetRequest();
			request_print.setUserid(print_user_id);
			request_print.setHttpMethod("GET");
			OapiUserGetResponse  response_print = null;
			try {
				response_print = client_user.execute(request_print, accessToken_user);
			}catch(ApiException e) {
				e.printStackTrace();
			}
			setAttr("sp_name", response_user.getName());
			setAttr("print_user", response_print.getName());
			Record fujian=Db.use("dc").findById("tech_file_fujian", file.getInt("fujian_id"));
			setAttr("fujianName", fujian.get("fujian_name"));
			render("/page/shengchan/techPublish/lsfileDetail.html");
		}else if (status==4){
			Record file=Db.use("dc").findById("xiangmu_tech_file", id);
			int is_change=file.getInt("is_change");
			if (is_change==0) {
				setAttr("changeName", "否");
			}else {
				setAttr("changeName", "是");
			}
			long chang_fujian_id=file.getInt("chang_fujian_id");
			if (chang_fujian_id==0) {
				setAttr("changeFujian", "");
			}else {
				Record fujian=Db.use("dc").findById("xiangmu_tech_file_fujian", file.getInt("chang_fujian_id"));
				setAttr("changeFujian", fujian.get("fujian_name"));
			}
			setAttr("file", file);
			String accessToken_user=DingController.getAccessToken();
			DingTalkClient client_user = new DefaultDingTalkClient("https://oapi.dingtalk.com/user/get");
			//创建人
			String create_userId=file.getStr("create_user");
			OapiUserGetRequest create_user=new OapiUserGetRequest();
			create_user.setUserid(create_userId);
			create_user.setHttpMethod("GET");
			OapiUserGetResponse  response_create=null;
			try {
				response_create=client_user.execute(create_user,accessToken_user);
			} catch (ApiException e) {
				e.printStackTrace();
			}
			setAttr("create_user", response_create.getName());
			//审批人
			String sp_userId=file.getStr("sp_user_id");
			OapiUserGetRequest request_user = new OapiUserGetRequest();
			request_user.setUserid(sp_userId);
			request_user.setHttpMethod("GET");
			OapiUserGetResponse  response_user = null;
			try {
				response_user = client_user.execute(request_user, accessToken_user);
			}catch(ApiException e) {
				e.printStackTrace();
			}
			//打印人
			String print_user_id=file.getStr("print_user_id");
			OapiUserGetRequest request_print = new OapiUserGetRequest();
			request_print.setUserid(print_user_id);
			request_print.setHttpMethod("GET");
			OapiUserGetResponse  response_print = null;
			try {
				response_print = client_user.execute(request_print, accessToken_user);
			}catch(ApiException e) {
				e.printStackTrace();
			}
			setAttr("sp_name", response_user.getName());
			setAttr("print_user", response_print.getName());
			Record fujian=Db.use("dc").findById("xiangmu_tech_file_fujian", file.getInt("fujian_id"));
			setAttr("fujianName", fujian.get("fujian_name"));
			render("/page/xiangmu/xiangmuPublish/lsfileDetail.html");
		}else if (status==5){
			Record file=Db.use("dc").findById("zhiliang_tech_file", id);
			int is_change=file.getInt("is_change");
			if (is_change==0) {
				setAttr("changeName", "否");
			}else {
				setAttr("changeName", "是");
			}
			long chang_fujian_id=file.getInt("chang_fujian_id");
			if (chang_fujian_id==0) {
				setAttr("changeFujian", "");
			}else {
				Record fujian=Db.use("dc").findById("zhiliang_tech_file_fujian", file.getInt("chang_fujian_id"));
				setAttr("changeFujian", fujian.get("fujian_name"));
			}
			setAttr("file", file);
			String accessToken_user=DingController.getAccessToken();
			DingTalkClient client_user = new DefaultDingTalkClient("https://oapi.dingtalk.com/user/get");
			//创建人
			String create_userId=file.getStr("create_user");
			OapiUserGetRequest create_user=new OapiUserGetRequest();
			create_user.setUserid(create_userId);
			create_user.setHttpMethod("GET");
			OapiUserGetResponse  response_create=null;
			try {
				response_create=client_user.execute(create_user,accessToken_user);
			} catch (ApiException e) {
				e.printStackTrace();
			}
			setAttr("create_user", response_create.getName());
			//审批人
			String sp_userId=file.getStr("sp_user_id");
			OapiUserGetRequest request_user = new OapiUserGetRequest();
			request_user.setUserid(sp_userId);
			request_user.setHttpMethod("GET");
			OapiUserGetResponse  response_user = null;
			try {
				response_user = client_user.execute(request_user, accessToken_user);
			}catch(ApiException e) {
				e.printStackTrace();
			}
			//打印人
			String print_user_id=file.getStr("print_user_id");
			OapiUserGetRequest request_print = new OapiUserGetRequest();
			request_print.setUserid(print_user_id);
			request_print.setHttpMethod("GET");
			OapiUserGetResponse  response_print = null;
			try {
				response_print = client_user.execute(request_print, accessToken_user);
			}catch(ApiException e) {
				e.printStackTrace();
			}
			setAttr("sp_name", response_user.getName());
			setAttr("print_user", response_print.getName());
			Record fujian=Db.use("dc").findById("zhiliang_tech_file_fujian", file.getInt("fujian_id"));
			setAttr("fujianName", fujian.get("fujian_name"));
			render("/page/zhiliang/zhiliangPublish/lsfileDetail.html");
		}
	}
	//审批通过
	public void spPass() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
		Date d= new Date();
		String date = sdf.format(d);
		int id=getParaToInt("id");
		int status=getParaToInt("status");
		if (status==1) {
			Record file=Db.use("dc").findById("caigou_tech_file", id);
			int release_type=file.getInt("release_type");
			//获取文件知会人
			String sql_zhihui="select * from caigou_zhihui_users where caigou_tech_files_id="+id;
			List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
			String zhihuiUsers="";
			for (int i = 0; i < zhihuiList.size(); i++) {
				zhihuiUsers+=zhihuiList.get(i).getStr("zhihui_userId")+",";
			}
			DingUtil.sendText(zhihuiUsers, "知会消息：采购文件“"+file.getStr("file_name")+"”已审批通过，请知悉！" +date);
			DingUtil.sendText(wkUserList1(id), "打印通知：采购文件“"+file.getStr("file_name")+"”已审批通过，请及时打印！");
			
			if (release_type==0 || release_type==2) {
				file.set("sp_status", 1);//临时文件或首次发布,
				Db.use("dc").update("caigou_tech_file", file);
			} else {
				//替换升版文件审批通过之后
				file.set("sp_status", 1);
				Db.use("dc").update("caigou_tech_file", file);
				String nb_fileCode=file.getStr("nb_fileCode");
				String sql_file="select * from caigou_tech_file where nb_fileCode='"+nb_fileCode+"' and sp_status=4";//使用中的旧版文件
				//旧版状态修改为5-已升版
				Record oldFile=Db.use("dc").findFirst(sql_file);
				oldFile.set("sp_status", 5);
				Db.use("dc").update("caigou_tech_file", oldFile);				
			}
			DingUtil.sendText(file.getStr("create_user"), "文件审批通知：采购文件“"+file.getStr("file_name")+"”已审批通过，请知悉！"+date);
		} else if (status==2){
			Record file=Db.use("dc").findById("kehu_tech_file", id);
			int release_type=file.getInt("release_type");
			//获取文件知会人
			String sql_zhihui="select * from kehu_zhihui_users where kehu_tech_files_id="+id;
			List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
			String zhihuiUsers="";
			for (int i = 0; i < zhihuiList.size(); i++) {
				zhihuiUsers+=zhihuiList.get(i).getStr("zhihui_userId")+",";
			}
			DingUtil.sendText(zhihuiUsers, "知会消息：客户文件“"+file.getStr("file_name")+"”已审批通过，请知悉！" +date);
			DingUtil.sendText(wkUserList2(id), "打印通知：客户文件“"+file.getStr("file_name")+"”已审批通过，请及时打印！");
			
			if (release_type==0 || release_type==2) {
				file.set("sp_status", 1);//临时文件或首次发布,
				Db.use("dc").update("kehu_tech_file", file);
			} else {
				//替换升版文件审批通过之后
				file.set("sp_status", 1);
				Db.use("dc").update("kehu_tech_file", file);
				String nb_fileCode=file.getStr("nb_fileCode");
				String sql_file="select * from kehu_tech_file where nb_fileCode='"+nb_fileCode+"' and sp_status=4";//使用中的旧版文件
				//旧版状态修改为5-已升版
				Record oldFile=Db.use("dc").findFirst(sql_file);
				oldFile.set("sp_status", 5);
				Db.use("dc").update("kehu_tech_file", oldFile);
			}
			DingUtil.sendText(file.getStr("create_user"), "文件审批通知：客户文件“"+file.getStr("file_name")+"”已审批通过，请知悉！"+date);
		}else if (status==3){
			Record file=Db.use("dc").findById("tech_files", id);
			int release_type=file.getInt("release_type");
			//获取文件知会人
			String sql_zhihui="select * from zhihui_users where tech_files_id="+id;
			List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
			String zhihuiUsers="";
			for (int i = 0; i < zhihuiList.size(); i++) {
				zhihuiUsers+=zhihuiList.get(i).getStr("zhihui_userId")+",";
			}
			DingUtil.sendText(zhihuiUsers, "知会消息：生产文件“"+file.getStr("file_name")+"”已审批通过，请知悉！" +date);
			DingUtil.sendText(wkUserList3(id), "打印通知：生产文件“"+file.getStr("file_name")+"”已审批通过，请及时打印！");
			
			if (release_type==0 || release_type==2) {
				file.set("sp_status", 1);//临时文件或首次发布,
				Db.use("dc").update("tech_files", file);
			} else {
				//替换升版文件审批通过之后,状态为6-等待旧版本回收
				file.set("sp_status", 6);
				Db.use("dc").update("tech_files", file);
				String nb_fileCode=file.getStr("nb_fileCode");
				String sql_file="select * from tech_files where nb_fileCode='"+nb_fileCode+"' and sp_status=4";//使用中的旧版文件
				//旧版状态修改为5-等待回收
				Record oldFile=Db.use("dc").findFirst(sql_file);
				oldFile.set("sp_status", 5);
				Db.use("dc").update("tech_files", oldFile);
				 //上一版接收人
				String sql_user="select * from tech_files_receive where tech_files_id="+oldFile.getInt("id");
				List<Record> userList=Db.use("dc").find(sql_user);
				String userIds="";
				for (int i = 0; i < userList.size(); i++) {
					userIds+=userList.get(i).getStr("receive_userId")+",";
				}
				DingUtil.sendText(userIds, "生产文件“"+oldFile.getStr("file_name")+"”有新版本发布，当前版本需交至文控处作废，请知悉！"+date);
			}
			DingUtil.sendText(file.getStr("create_user"), "文件审批通知：生产文件“"+file.getStr("file_name")+"”已审批通过，请知悉！"+date);
		}else if (status==4){
			Record file=Db.use("dc").findById("xiangmu_tech_file", id);
			int release_type=file.getInt("release_type");
			//获取文件知会人
			String sql_zhihui="select * from xiangmu_zhihui_users where xiangmu_tech_files_id="+id;
			List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
			String zhihuiUsers="";
			for (int i = 0; i < zhihuiList.size(); i++) {
				zhihuiUsers+=zhihuiList.get(i).getStr("zhihui_userId")+",";
			}
			DingUtil.sendText(zhihuiUsers, "知会消息：项目文件“"+file.getStr("file_name")+"”已审批通过，请知悉！" +date);
			DingUtil.sendText(wkUserList4(id), "打印通知：项目文件“"+file.getStr("file_name")+"”已审批通过，请及时打印！");
			
			if (release_type==0 || release_type==2) {
				file.set("sp_status", 1);//临时文件或首次发布,
				Db.use("dc").update("xiangmu_tech_file", file);
			} else {
				//替换升版文件审批通过之后
				file.set("sp_status", 1);
				Db.use("dc").update("xiangmu_tech_file", file);
				String nb_fileCode=file.getStr("nb_fileCode");
				String sql_file="select * from xiangmu_tech_file where nb_fileCode='"+nb_fileCode+"' and sp_status=4";//使用中的旧版文件
				//旧版状态修改为5-已升版
				Record oldFile=Db.use("dc").findFirst(sql_file);
				oldFile.set("sp_status", 5);
				Db.use("dc").update("xiangmu_tech_file", oldFile);
			}
			DingUtil.sendText(file.getStr("create_user"), "文件审批通知：项目文件“"+file.getStr("file_name")+"”已审批通过，请知悉！"+date);
		}else if (status==5){
			Record file=Db.use("dc").findById("zhiliang_tech_file", id);
			int release_type=file.getInt("release_type");
			//获取文件知会人
			String sql_zhihui="select * from zhiliang_zhihui_users where zhiliang_tech_files_id="+id;
			List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
			String zhihuiUsers="";
			for (int i = 0; i < zhihuiList.size(); i++) {
				zhihuiUsers+=zhihuiList.get(i).getStr("zhihui_userId")+",";
			}
			DingUtil.sendText(zhihuiUsers, "知会消息：质量文件“"+file.getStr("file_name")+"”已审批通过，请知悉！" +date);
			DingUtil.sendText(wkUserList5(id), "打印通知：质量文件“"+file.getStr("file_name")+"”已审批通过，请及时打印！");
			
			if (release_type==0 || release_type==2) {
				file.set("sp_status", 1);//临时文件或首次发布,
				Db.use("dc").update("zhiliang_tech_file", file);
			} else {
				//替换升版文件审批通过之后
				file.set("sp_status", 1);
				Db.use("dc").update("zhiliang_tech_file", file);
				String nb_fileCode=file.getStr("nb_fileCode");
				String sql_file="select * from zhiliang_tech_file where nb_fileCode='"+nb_fileCode+"' and sp_status=4";//使用中的旧版文件
				//旧版状态修改为5-已升版
				Record oldFile=Db.use("dc").findFirst(sql_file);
				oldFile.set("sp_status", 5);
				Db.use("dc").update("zhiliang_tech_file", oldFile);
			}
			DingUtil.sendText(file.getStr("create_user"), "文件审批通知：质量文件“"+file.getStr("file_name")+"”已审批通过，请知悉！"+date);
		}
		Record record=new Record();
		record.set("code", 0);
		record.set("msg", "审批通过！");
		renderJson(record);
	}
	//审批驳回
	public void spBack() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
		Date d= new Date();
		String date = sdf.format(d);
		int id=getParaToInt("id");
		int status=getParaToInt("status");
		if (status==1) {
			Record file=Db.use("dc").findById("caigou_tech_file", id);
			file.set("sp_status", 2);
			Db.use("dc").update("caigou_tech_file", file);
			DingUtil.sendText(file.getStr("create_user"), "文件审批通知：采购文件“"+file.getStr("file_name")+"”被审批驳回，请知悉！"+date);
			//获取文件知会人
			String sql_zhihui="select * from  caigou_zhihui_users where caigou_tech_files_id="+id;
			List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
			String zhihuiUsers="";
			for (int i = 0; i < zhihuiList.size(); i++) {
				zhihuiUsers+=zhihuiList.get(i).getStr("zhihui_userId")+",";
			}
			DingUtil.sendText(zhihuiUsers, "知会消息：采购文件“"+file.getStr("file_name")+"”被审批驳回，请知悉！" +date);
		} else if (status==2){
			Record file=Db.use("dc").findById("kehu_tech_file", id);
			file.set("sp_status", 2);
			Db.use("dc").update("kehu_tech_file", file);
			DingUtil.sendText(file.getStr("create_user"), "文件审批通知：客户文件“"+file.getStr("file_name")+"”被审批驳回，请知悉！"+date);
			//获取文件知会人
			String sql_zhihui="select * from kehu_zhihui_users where kehu_tech_files_id="+id;
			List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
			String zhihuiUsers="";
			for (int i = 0; i < zhihuiList.size(); i++) {
				zhihuiUsers+=zhihuiList.get(i).getStr("zhihui_userId")+",";
			}
			DingUtil.sendText(zhihuiUsers, "知会消息：客户文件“"+file.getStr("file_name")+"”被审批驳回，请知悉！" +date);
		}else if (status==3){
			Record file=Db.use("dc").findById("tech_files", id);
			file.set("sp_status", 2);
			Db.use("dc").update("tech_files", file);
			DingUtil.sendText(file.getStr("create_user"), "文件审批通知：生产文件“"+file.getStr("file_name")+"”被审批驳回，请知悉！"+date);
			//获取文件知会人
			String sql_zhihui="select * from zhihui_users where tech_files_id="+id;
			List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
			String zhihuiUsers="";
			for (int i = 0; i < zhihuiList.size(); i++) {
				zhihuiUsers+=zhihuiList.get(i).getStr("zhihui_userId")+",";
			}
			DingUtil.sendText(zhihuiUsers, "知会消息：生产文件“"+file.getStr("file_name")+"”被审批驳回，请知悉！" +date);
		}else if (status==4){
			Record file=Db.use("dc").findById("xiangmu_tech_file", id);
			file.set("sp_status", 2);
			Db.use("dc").update("xiangmu_tech_file", file);
			DingUtil.sendText(file.getStr("create_user"), "文件审批通知：项目文件“"+file.getStr("file_name")+"”被审批驳回，请知悉！"+date);
			//获取文件知会人
			String sql_zhihui="select * from xiangmu_zhihui_users where xiangmu_tech_files_id="+id;
			List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
			String zhihuiUsers="";
			for (int i = 0; i < zhihuiList.size(); i++) {
				zhihuiUsers+=zhihuiList.get(i).getStr("zhihui_userId")+",";
			}
			DingUtil.sendText(zhihuiUsers, "知会消息：项目文件“"+file.getStr("file_name")+"”被审批驳回，请知悉！" +date);
		}else if (status==5){
			Record file=Db.use("dc").findById("zhiliang_tech_file", id);
			file.set("sp_status", 2);
			Db.use("dc").update("zhiliang_tech_file", file);
			DingUtil.sendText(file.getStr("create_user"), "文件审批通知：质量文件“"+file.getStr("file_name")+"”被审批驳回，请知悉！"+date);
			//获取文件知会人
			String sql_zhihui="select * from zhiliang_zhihui_users where zhiliang_tech_files_id="+id;
			List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
			String zhihuiUsers="";
			for (int i = 0; i < zhihuiList.size(); i++) {
				zhihuiUsers+=zhihuiList.get(i).getStr("zhihui_userId")+",";
			}
			DingUtil.sendText(zhihuiUsers, "知会消息：质量文件“"+file.getStr("file_name")+"”被审批驳回，请知悉！" +date);
		}
		Record record=new Record();
		record.set("code", 0);
		record.set("msg", "审批驳回成功！");
		renderJson(record);
	}
	//附件下载
	public void fujianDownload() {
		Record rd=new Record();
		int file_id=getParaToInt("id");
		int file_status=getParaToInt("status");
		String sql_fj="";
		if (file_status==1) {
			Record file=Db.use("dc").findById("caigou_tech_file", file_id);
			sql_fj="select * from caigou_tech_file_fujian where id="+file.getInt("fujian_id");
		} else if (file_status==2){
			Record file=Db.use("dc").findById("kehu_tech_file", file_id);
			sql_fj="select * from kehu_tech_file_fujian where id="+file.getInt("fujian_id");
		}else if (file_status==3){
			Record file=Db.use("dc").findById("tech_files", file_id);
			sql_fj="select * from tech_file_fujian where id="+file.getInt("fujian_id");
		}else if (file_status==4){
			Record file=Db.use("dc").findById("xiangmu_tech_file", file_id);
			sql_fj="select * from xiangmu_tech_file_fujian where id="+file.getInt("fujian_id");
		}else if (file_status==5){
			Record file=Db.use("dc").findById("zhiliang_tech_file", file_id);
			sql_fj="select * from zhiliang_tech_file_fujian where id="+file.getInt("fujian_id");
		}
		Record fujian=Db.use("dc").findFirst(sql_fj);
		try {
			render(new MyFileRender(fujian.getStr("fujian_name")));
		}catch (Exception e){
			rd.set("code", 1);
			rd.set("msg", "暂无附件下载");
			rd.set("data", "");
			renderJson(rd);
		}
	}
	//变更凭证
	public void pingZhengDownload() {
		Record rd=new Record();
		int file_id=getParaToInt("id");
		int file_status=getParaToInt("status");
		String sql_fj="";
		if (file_status==1) {
			Record file=Db.use("dc").findById("caigou_tech_file", file_id);
			sql_fj="select * from caigou_tech_file_fujian where id="+file.getInt("chang_fujian_id");
		} else if (file_status==2){
			Record file=Db.use("dc").findById("kehu_tech_file", file_id);
			sql_fj="select * from kehu_tech_file_fujian where id="+file.getInt("chang_fujian_id");
		}else if (file_status==3){
			Record file=Db.use("dc").findById("tech_files", file_id);
			sql_fj="select * from tech_file_fujian where id="+file.getInt("chang_fujian_id");
		}else if (file_status==4){
			Record file=Db.use("dc").findById("xiangmu_tech_file", file_id);
			sql_fj="select * from xiangmu_tech_file_fujian where id="+file.getInt("chang_fujian_id");
		}else if (file_status==5){
			Record file=Db.use("dc").findById("zhiliang_tech_file", file_id);
			sql_fj="select * from zhiliang_tech_file_fujian where id="+file.getInt("chang_fujian_id");
		}
		Record fujian=Db.use("dc").findFirst(sql_fj);
		try {
			render(new MyFileRender(fujian.getStr("fujian_name")));
		}catch (Exception e){
			rd.set("code", 1);
			rd.set("msg", "暂无附件下载");
			rd.set("data", "");
			renderJson(rd);
		}
	}
	/**
	 * 公共部分
	 * @param fileId
	 * @return
	 */
	//获取文控列表
	public String wkUserList1(int fileId) {
		Record file=Db.use("dc").findById("caigou_tech_file", fileId);
		String print_user_id=file.getStr("print_user_id");
		return print_user_id;
	}
	public String wkUserList2(int fileId) {
		Record file=Db.use("dc").findById("kehu_tech_file", fileId);
		String print_user_id=file.getStr("print_user_id");
		return print_user_id;
	}
	public String wkUserList3(int fileId) {
		Record file=Db.use("dc").findById("tech_files", fileId);
		String print_user_id=file.getStr("print_user_id");
		return print_user_id;
	}
	public String wkUserList4(int fileId) {
		Record file=Db.use("dc").findById("xiangmu_tech_file", fileId);
		String print_user_id=file.getStr("print_user_id");
		return print_user_id;
	}
	public String wkUserList5(int fileId) {
		Record file=Db.use("dc").findById("zhiliang_tech_file", fileId);
		String print_user_id=file.getStr("print_user_id");
		return print_user_id;
	}
}
