package com.simple.controller.customerInput;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import com.dingtalk.api.response.OapiUserGetResponse;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.upload.UploadFile;
import com.simple.common.model.CustomerFujian;
import com.simple.common.model.CustomerInput;
import com.simple.common.model.CustomerInputParts;
import com.simple.common.model.CustomerInputZhihui;
import com.simple.common.model.CustomerReceiveuser;
import com.simple.common.model.ProBuild;
import com.simple.ding.DingUtil;
import com.simple.file.MyFileRender;
/**
 * 顾客输入customerInput
 * @author FL00024996
 *
 */
public class CustomerInputController extends Controller {
	//指向顾客输入列表页面
	public void toCustomerInputList() {
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		String sqlRole="select * from user_role where user_id='"+user.getUserid()+"' and (role_id=13 or role_id=14 or role_id=1 or role_id=3 or role_id=11 or role_id=10)";
		List<Record> roleList=Db.use("dc").find(sqlRole);
		if (roleList.isEmpty()) {
			set("canCreate", 0);//不能创建
		}else {
			set("canCreate", 1);//可以创建
		}
		String sqlRole1="select * from user_role where user_id='"+user.getUserid()+"' and (role_id=1 or role_id=3)";
		List<Record> roleList1=Db.use("dc").find(sqlRole1);
		if (roleList1.isEmpty()) {
			set("isPrint", 0);//非文控
		}else {
			set("isPrint", 1);//文控
		}
		set("loginUser", user.getUserid());
		render("customerInputList.html");
	}
	//获取顾客输入列表
	public void getCustomerInputList() {
		String customerName=get("customer_name");
		String proCode=get("pro_code");
		String fileType=get("file_type");
		StringBuilder sb=new StringBuilder();
		int page=getInt("page");
		int limit=getInt("limit");
		Record jsp=new Record();
		jsp.set("code", 0);
		sb.append(" from customer_input where 1=1 ");
		if (!"".equals(customerName) && customerName!=null) {
			sb.append(" and customer_name like '%"+customerName+"%'");
		}
		if (!"".equals(proCode) && proCode!=null) {
			sb.append(" and pro_code like'%"+proCode+"%'");
		}
		if (!"".equals(fileType) && fileType!=null) {
			sb.append(" and file_type ='"+fileType+"'");
		}
		int fileStatus=getParaToInt("fileStatus");
		if (fileStatus==0) {
			sb.append(" and file_status=0 ");
		}else if (fileStatus==1) {
			sb.append(" and file_status=1 ");
		}else if (fileStatus==2) {
			sb.append(" and file_status=2 ");
		}
		Page<Record> allPro=Db.use("dc").paginate(page, limit, "select * ",sb.toString()+" order by id desc");
		if (!allPro.getList().isEmpty()) {
			//文件类型:01-技术类；02-质量类；03-物流类；04-其他
			for (int i = 0; i < allPro.getList().size(); i++) {
				String fileType1=allPro.getList().get(i).getStr("file_type");
				if ("01".equals(fileType1)) {
					allPro.getList().get(i).set("fileType", "技术类");
				}else if ("02".equals(fileType1)) {
					allPro.getList().get(i).set("fileType", "质量类");
				}else if ("03".equals(fileType1)) {
					allPro.getList().get(i).set("fileType", "物流类");
				}else if ("04".equals(fileType1)) {
					allPro.getList().get(i).set("fileType", "其他");
				}
			}
			//是否可编辑
			for (int i = 0; i < allPro.getList().size(); i++) {
				int status=allPro.getList().get(i).getInt("file_status");
				if (status==0) {
					//查询所有接收者是否有已接收
					List<CustomerReceiveuser> receiveList=CustomerReceiveuser.dao.find("select * from customer_receiveuser where customer_input_id="+allPro.getList().get(i).getLong("id"));
					boolean isReceive=false;
					for (int j = 0; j < receiveList.size(); j++) {
						int statusReceive=receiveList.get(j).getStatus();
						if (statusReceive==1) {
							isReceive=true;
							break;
						}
					}
					if (!isReceive) {
						allPro.getList().get(i).set("canEdit", 1);//可编辑
					}else {
						allPro.getList().get(i).set("canEdit", 0);
					}
				}else {
					allPro.getList().get(i).set("canEdit", 0);
				}
			}
		}
		jsp.set("count", allPro.getTotalRow());
		jsp.set("data", allPro.getList());
		jsp.set("msg", "查询文件成功");
		renderJson(jsp);
	}
	//指向新建顾客输入页面
	public void toAddCustmerInput() {
		String sqlPro="select * from pro_build where sp_status=1";
		List<Record> proList=Db.use("dc").find(sqlPro);
		set("proList", proList);
		render("addCustomerInput.html");
	}
	//指向编辑顾客输入页面
	public void toEditCustmerInput() {
		long inputId=getParaToLong(0);
		CustomerInput customerInput=CustomerInput.dao.findById(inputId);
		set("customerInput", customerInput);
		//已选择的项目号
		ProBuild selectPro=ProBuild.dao.findFirst("select * from pro_build where FL_pro_no='"+customerInput.getProCode()+"'");
		set("selectPro", selectPro);
		//获取总成零件号
		List<Record> zcPartList=Db.use("dc").find("select * from customer_input_parts where customer_input_id="+inputId);
		String zcPart="";
		for (int i = 0; i < zcPartList.size(); i++) {
			zcPart=zcPart+" "+zcPartList.get(i).getStr("zc_part_code");
		}
		set("zc_list", zcPart);

		//文件类型
		String fileType=customerInput.getFileType();
		if ("01".equals(fileType)) {
			set("file_type", "技术类");
		}else if ("02".equals(fileType)) {
			set("file_type", "质量类");
		}else if ("03".equals(fileType)) {
			set("file_type", "物流类");
		}else if ("04".equals(fileType)) {
			set("file_type", "其他");
		}
		//发布类型
		int publishType=customerInput.getPublishType();
		if (publishType==0) {
			set("publishType", "首版发布");
		}else {
			set("publishType", "升版更新");
		}
		//覆盖输入文件id
		String coverFile=customerInput.getCoverNbfileCode();
		if (!"".equals(coverFile)&&coverFile!=null) {
			CustomerInput coverInput=CustomerInput.dao.findFirst("select * from customer_input where nb_file_code='"+coverFile+"'");
			set("coverInput", coverInput);
		}else {
			Record coverInput=new Record();
			coverInput.set("id", 0);
			coverInput.set("cover_nbfile_code", "");
			set("coverInput", coverInput);
		}
		//接受部门
		String jieshouDep=customerInput.getJieshouDep();
		if ("XM".equals(jieshouDep)) {
			set("jieshouDep", "项目");
		}else if ("SW".equals(jieshouDep)) {
			set("jieshouDep", "商务");
		}else if ("JS".equals(jieshouDep)) {
			set("jieshouDep", "技术");
		}
		//分发对象
		List<Record> jieshouList=Db.use("dc").find("select * from customer_receiveuser where customer_input_id="+inputId);
		String jieshouUsers="";
		for (int i = 0; i < jieshouList.size(); i++) {
			jieshouUsers=jieshouUsers+" "+jieshouList.get(i).getStr("receive_user_name");
		}
		set("jieshouUsers", jieshouUsers);
		//内部文件号
		String nbFileCode=customerInput.getNbFileCode();
		String[] nbFiles=nbFileCode.split("/");
		String nbCode2=nbFiles[1];
		String nbCode3=nbFiles[2];
		String nbCode4=nbFiles[3];
		String nbCode5=nbFiles[4];
		set("nbCode2", nbCode2);
		set("nbCode3", nbCode3);
		set("nbCode4", nbCode4);
		set("nbCode5", nbCode5);
		//文件
		CustomerFujian fileFujian=CustomerFujian.dao.findById(customerInput.getFileFujian());
		set("fileFujian", fileFujian);
		//附件备注
		long remarkFujianId=customerInput.getRemarksFujian();
		if (remarkFujianId!=0) {
			CustomerFujian remarksFujian=CustomerFujian.dao.findById(remarkFujianId);
			set("remarksFujian", remarksFujian);
		}else {
			Record remarksFujian=new Record();
			remarksFujian.set("fujian_name", "");
			remarksFujian.set("id", 0);
			set("remarksFujian", remarksFujian);
		}
		String sqlPro="select * from pro_build where sp_status=1";
		List<Record> proList=Db.use("dc").find(sqlPro);
		set("proList", proList);
		//知会人
		List<CustomerInputZhihui> zhihuiList=CustomerInputZhihui.dao.find("select * from customer_input_zhihui where customer_input_id="+inputId);
		String zhihui1="";//不可查看
		String zhihui2="";
		if (!zhihuiList.isEmpty()) {
			for (int i = 0; i < zhihuiList.size(); i++) {
				int type=zhihuiList.get(i).getType();
				if (type==0) {
					zhihui1=zhihui1+zhihuiList.get(i).getZhihuiUserName()+" ";
				}else {
					zhihui2=zhihui2+zhihuiList.get(i).getZhihuiUserName()+" ";
				}
			}
		}
		set("zhihui1", zhihui1);
		set("zhihui2", zhihui2);
		render("editCustomerInput.html");
	}
	//根据选择的项目代号，获取详细信息
	public void getProDetail() {
		long proId=getParaToLong("proId");
		ProBuild proInfo=ProBuild.dao.findById(proId);
		//总成号列表
		List<Record> zcPartList=Db.use("dc").find("select * from codeaparts where pro_code='"+proInfo.getFlProNo()+"'");
		for (int i = 0; i < zcPartList.size(); i++) {
			zcPartList.get(i).set("status", 1);
		}
		//生成流水码
		String sqlCount="select count(*) from customer_input";
		Record customer=Db.use("dc").findFirst(sqlCount);
		int cusNum=customer.getInt("count(*)")+1;
		String suiji="";
		if (cusNum<10) {
			suiji="0000"+cusNum;
		}else if (cusNum<100) {
			suiji="000"+cusNum;
		}else if (cusNum<1000) {
			suiji="00"+cusNum;
		}else if (cusNum<10000) {
			suiji="0"+cusNum;
		}else{
			suiji=String.valueOf(cusNum);
		}
		//int suiji=(int)((Math.random()*9+1)*10000);
		Record record=new Record();
		record.set("code", 0);
		record.set("msg", "获取成功");
		record.set("data", proInfo);
		record.set("zcpart", zcPartList);
		record.set("suiji", suiji);
		renderJson(record);
	}
	//处理顾客输入
	public void doCustomerInput() throws ParseException {
		Date date=new Date();
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdf1=new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
		String d1=sdf.format(date);
		int proId=getParaToInt("proCode"); 
		ProBuild proBuild=ProBuild.dao.findById(proId);
		String proCode=proBuild.getFlProNo();
		String customerCode=get("customer_code");
		String customerName=get("customer_name");
		String jixing=get("jixing");
		String zcCode=get("zcCode_list");
		String fileType=get("file_type");
		String wlFileName=get("wl_file_name");
		String wlFileCode=get("wl_file_code");
		int publishType=getParaToInt("publish_type");
		String coverFileCode="";
		long coverFileId=0;
		if (publishType==1) {
			coverFileCode=get("cover_file_code");
			coverFileId=getParaToLong("cover_file_id");
		}
		String jieshouDep=get("jieshou_dep");
		String jieshouUserId=get("userId");
		String jiehsouUserName=get("userSel_rec");
		String nbCode1=get("nbCode1");
		String nbCode2=get("nbCode2");
		String nbCode3=get("nbCode3");
		String nbCode4=get("nbCode4");
		String nbCode5=get("nbCode5");
		String nbFileCode=nbCode1+"/"+nbCode2+"/"+nbCode3+"/"+nbCode4+"/"+nbCode5;
		long fileFujianIdd=getParaToLong("file_id");
		String remarks=get("remarks");
		long fujianId=getParaToLong("fujian_id");
		
		Record record=new Record();
		record.set("code", 0);
		//判断文件号是否存在
		String sqlCustomer="select * from customer_input where nb_file_code='"+nbFileCode+"'";
		List<Record> haveCustomer=Db.use("dc").find(sqlCustomer);
		if (haveCustomer.isEmpty()) {
			//顾客输入表中新增数据
			CustomerInput customerInput=getModel(CustomerInput.class);
			customerInput.setProCode(proCode)
			.setCustomerCode(customerCode).setCustomerName(customerName)
			.setJixing(jixing).setSendTime(sdf.parse(d1))
			.setFileType(fileType)
			.setWlFileCode(wlFileCode).setWlFileName(wlFileName)
			.setPublishType(publishType)
			.setNbFileCode(nbFileCode)
			.setCoverFileId(coverFileId).setCoverNbfileCode(coverFileCode)
			.setInputUserId(user.getUserid()).setInputUserName(user.getName())
			.setCreateTime(date)
			.setJieshouDep(jieshouDep)
			.setPublishRemarks(remarks).setRemarksFujian(fujianId)
			.setFileStatus(0).setFileFujian(fileFujianIdd);
			customerInput.save();
			//顾客输入零件表
			long customerId=customerInput.getId();
			String[] zcParts=zcCode.split(",");
			for (int i = 0; i < zcParts.length; i++) {
				CustomerInputParts parts=getModel(CustomerInputParts.class);
				parts.setCustomerInputId(customerId)
				.setProCode(proCode).setZcPartCode(zcParts[i]);
				parts.save();
			}
			//顾客输入接受表
			String[] receiveUserIds=jieshouUserId.split(",");
			String[] receiveUserNames=jiehsouUserName.split(",");
			for (int i = 0; i < receiveUserIds.length; i++) {
				CustomerReceiveuser receiveuser=getModel(CustomerReceiveuser.class);
				receiveuser.setCustomerInputId(customerId)
				.setReceiveUserId(receiveUserIds[i]).setReceiveUserName(receiveUserNames[i])
				.setStatus(0);
				receiveuser.save();
				DingUtil.sendText(receiveUserIds[i], "顾客输入文件通知：您有文件待领取，请登录文件管理系统处理！"+sdf1.format(date));
			}
			//替换版本，将旧版本作废
			if (publishType==1) {
				CustomerInput customerInput2=CustomerInput.dao.findById(coverFileId);
				customerInput2.setFileStatus(2);//2-作废
				customerInput2.update();
			}
			//知会人
			//可查看附件
			String zhiHuiUserId1=get("zhiHuiUserId1");
			if (!"0".equals(zhiHuiUserId1)) {
				String zhihuiUserSel_rec1=get("zhihuiUserSel_rec1");
				String[] zhiHuiIds=zhiHuiUserId1.split(",");
				String[] zhiHuiNames=zhihuiUserSel_rec1.split(",");
				for (int i = 0; i < zhiHuiNames.length; i++) {
					CustomerInputZhihui zhihui=getModel(CustomerInputZhihui.class);
					zhihui.setCustomerInputId(customerId)
					.setZhihuiUserId(zhiHuiIds[i]).setZhihuiUserName(zhiHuiNames[i])
					.setType(1);
					zhihui.save();
					DingUtil.sendText(zhiHuiIds[i], user.getName()+"发布了新的顾客输入文件，文件号为【"+nbFileCode+"】，请知悉！");
				}
			}
			//不可查看附件
			String zhiHuiUserId2=get("zhiHuiUserId2");
			if (!"0".equals(zhiHuiUserId2)) {
				String zhihuiUserSel_rec2=get("zhihuiUserSel_rec2");
				String[] zhiHuiIds=zhiHuiUserId2.split(",");
				String[] zhiHuiNames=zhihuiUserSel_rec2.split(",");
				for (int i = 0; i < zhiHuiNames.length; i++) {
					CustomerInputZhihui zhihui=getModel(CustomerInputZhihui.class);
					zhihui.setCustomerInputId(customerId)
					.setZhihuiUserId(zhiHuiIds[i]).setZhihuiUserName(zhiHuiNames[i])
					.setType(0);
					zhihui.save();
					DingUtil.sendText(zhiHuiIds[i], user.getName()+"发布了新的顾客输入文件，文件号为【"+nbFileCode+"】，请知悉！");
				}
			}
			record.set("msg", "新增成功");
			record.set("status", 0);
		}else {
			record.set("msg", "内部文件号已存在！");
			record.set("status", 1);
		}
		renderJson(record);
	}
	//处理编辑逻辑
	public void doEditCustomerInput() throws ParseException {
		Date date=new Date();
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdf1=new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
		String d1=sdf.format(date);
		long customerId=getParaToLong("id");
		CustomerInput customerInput=CustomerInput.dao.findById(customerId);
		long proId=getParaToLong("proCode");
		ProBuild proBuild=ProBuild.dao.findById(proId);
		String fileType=get("file_type");
		String wlFileName=get("wl_file_name");
		String wlFileCode=get("wl_file_code");
		int publishType=getParaToInt("publish_type");//发布类型
		String coverFileCode="";
		long coverFileId=0;
		if (publishType==1) {
			coverFileCode=get("cover_file_code");
			coverFileId=getParaToLong("cover_file_id");
		}
		String jieshouDep=get("jieshou_dep");
		String nbCode1=get("nbCode1");
		String nbCode2=get("nbCode2");
		String nbCode3=get("nbCode3");
		String nbCode4=get("nbCode4");
		String nbCode5=get("nbCode5");
		String nbFileCode=nbCode1+"/"+nbCode2+"/"+nbCode3+"/"+nbCode4+"/"+nbCode5;
		long fileId=getParaToLong("file_id");
		String remarks=get("remarks");
		long fujianId=getParaToLong("fujian_id");
		
		Record record=new Record();
		record.set("code", 0);
		//判断内部文件号是否存在
		String sqlCustomer="select * from customer_input where nb_file_code='"+nbFileCode+"' and id<>"+customerId;
		List<Record> haveCustomer=Db.use("dc").find(sqlCustomer);
		if (haveCustomer.isEmpty()) {
			//获取原记录是否已覆盖文件
			long oldCoverFile=customerInput.getCoverFileId();
			if (oldCoverFile!=0) {//已覆盖，则将原覆盖文件状态改为1
				CustomerInput oldCoverCustomerInput=CustomerInput.dao.findById(oldCoverFile);
				oldCoverCustomerInput.setFileStatus(1);
				oldCoverCustomerInput.update();
			}
			//修改顾客输入表数据
			customerInput.setProCode(proBuild.getFlProNo()).setCustomerCode(proBuild.getCustomerCode())
			.setCustomerName(proBuild.getCustomerName()).setJixing(proBuild.getModel())
			.setSendTime(sdf.parse(d1)).setFileType(fileType)
			.setWlFileCode(wlFileCode).setWlFileName(wlFileName)
			.setPublishType(publishType).setCoverFileId(coverFileId).setCoverNbfileCode(coverFileCode)
			.setNbFileCode(nbFileCode)
			.setJieshouDep(jieshouDep)
			.setPublishRemarks(remarks).setRemarksFujian(fujianId)
			.setFileFujian(fileId).setFileStatus(0);
			customerInput.update();
			//修改顾客输入零件表
			int zcChange=getParaToInt("zcChange");
			String zcCode_list=get("zcCode_list");
			if (zcChange==1) {
				String[] newZcPart=zcCode_list.split(",");
				List<CustomerInputParts> oldParts=CustomerInputParts.dao.find("select * from customer_input_parts where customer_input_id="+customerId);
				int oldNum=oldParts.size();
				int newNum=newZcPart.length;
				if(oldNum>newNum) {
					for (int i = 0; i < newNum; i++) {
						String zcCode=newZcPart[i];
						oldParts.get(i).setZcPartCode(zcCode);
						oldParts.get(i).update();
					}
					for (int i = newNum; i < oldNum; i++) {
						oldParts.get(i).delete();
					}
				}else {
					for (int i = 0; i < oldNum; i++) {
						String zcCode=newZcPart[i];
						oldParts.get(i).setZcPartCode(zcCode);
						oldParts.get(i).update();
					}
					for (int i = oldNum; i < newNum; i++) {
						String zcCode=newZcPart[i];
						CustomerInputParts addZcParts=getModel(CustomerInputParts.class);
						addZcParts.setZcPartCode(zcCode).setCustomerInputId(customerId);
						addZcParts.save();
					}
				}
			}
			//顾客接受表
			String jieshouUserId=get("userId");
			String jiehsouUserName=get("userSel_rec");
			if (!"0".equals(jieshouUserId)) {
				//删除原来的接收人
				List<CustomerReceiveuser> oldJieshouList=CustomerReceiveuser.dao.find("select * from customer_receiveuser where customer_input_id="+customerId);
				for (int i = 0; i < oldJieshouList.size(); i++) {
					oldJieshouList.get(i).delete();
				}
				String[] receiveUserIds=jieshouUserId.split(",");
				String[] receiveUserNames=jiehsouUserName.split(",");
				for (int i = 0; i < receiveUserIds.length; i++) {
					CustomerReceiveuser receiveuser=getModel(CustomerReceiveuser.class);
					receiveuser.setCustomerInputId(customerId)
					.setReceiveUserId(receiveUserIds[i]).setReceiveUserName(receiveUserNames[i])
					.setStatus(0);
					receiveuser.save();
					DingUtil.sendText(receiveUserIds[i], "顾客输入文件通知：您有文件待领取，请登录文件管理系统处理！"+sdf1.format(date));
				}
			}
			//替换版本，将旧版本作废
			if (publishType==1) {				
				CustomerInput customerInput2=CustomerInput.dao.findById(coverFileId);
				customerInput2.setFileStatus(2);//2-作废
				customerInput2.update();
			}
			//知会人
			//删除原来知会人
			List<CustomerInputZhihui> zhihuiList=CustomerInputZhihui.dao.find("select * from customer_input_zhihui where customer_input_id="+customerId);
			if (!zhihuiList.isEmpty()) {
				for (int i = 0; i < zhihuiList.size(); i++) {
					zhihuiList.get(i).delete();
				}
			}
			//可查看附件
			String zhiHuiUserId1=get("zhiHuiUserId1");
			if (!"0".equals(zhiHuiUserId1)) {
				String zhihuiUserSel_rec1=get("zhihuiUserSel_rec1");
				String[] zhiHuiIds=zhiHuiUserId1.split(",");
				String[] zhiHuiNames=zhihuiUserSel_rec1.split(",");
				for (int i = 0; i < zhiHuiNames.length; i++) {
					CustomerInputZhihui zhihui=getModel(CustomerInputZhihui.class);
					zhihui.setCustomerInputId(customerId)
					.setZhihuiUserId(zhiHuiIds[i]).setZhihuiUserName(zhiHuiNames[i])
					.setType(1);
					zhihui.save();
					DingUtil.sendText(zhiHuiIds[i], customerInput.getInputUserName()+"发布了新的顾客输入文件，文件号为【"+nbFileCode+"】，请知悉！");
				}
			}
			//不可查看附件
			String zhiHuiUserId2=get("zhiHuiUserId2");
			if (!"0".equals(zhiHuiUserId2)) {
				String zhihuiUserSel_rec2=get("zhihuiUserSel_rec2");
				String[] zhiHuiIds=zhiHuiUserId2.split(",");
				String[] zhiHuiNames=zhihuiUserSel_rec2.split(",");
				for (int i = 0; i < zhiHuiNames.length; i++) {
					CustomerInputZhihui zhihui=getModel(CustomerInputZhihui.class);
					zhihui.setCustomerInputId(customerId)
					.setZhihuiUserId(zhiHuiIds[i]).setZhihuiUserName(zhiHuiNames[i])
					.setType(0);
					zhihui.save();
					DingUtil.sendText(zhiHuiIds[i], customerInput.getInputUserName()+"发布了新的顾客输入文件，文件号为【"+nbFileCode+"】，请知悉！");
				}
			}
			record.set("msg", "新增成功");
			record.set("status", 0);
		}else {
			record.set("msg", "内部文件号已存在！");
			record.set("status", 1);
		}
		renderJson(record);
	}
	//指向覆盖文件选择页面
	public void toCoverFile() {
		long coverId=getParaToLong(0);
		set("cover_id", coverId);
		render("coverFileList.html");
	}
	//获取可覆盖文件列表
	public void getCanCoverList() {
		String customerName=get("customer_name");
		String proCode=get("pro_code");
		String fileType=get("file_type");
		StringBuilder sb=new StringBuilder();
		int page=getInt("page");
		int limit=getInt("limit");
		Record jsp=new Record();
		jsp.set("code", 0);
		sb.append(" from customer_input where file_status=1 ");//0-直接编辑
		if (!"".equals(customerName) && customerName!=null) {
			sb.append(" and customer_name like '%"+customerName+"%'");
		}
		if (!"".equals(proCode) && proCode!=null) {
			sb.append(" and pro_code like'%"+proCode+"%'");
		}
		if (!"".equals(fileType) && fileType!=null) {
			sb.append(" and file_type ='"+fileType+"'");
		}
		Page<Record> allPro=Db.use("dc").paginate(page, limit, "select * ",sb.toString()+" order by id desc");
		//文件类型:01-技术类；02-质量类；03-物流类；04-其他
		if (!allPro.getList().isEmpty()) {
			for (int i = 0; i < allPro.getList().size(); i++) {
				String fileType1=allPro.getList().get(i).getStr("file_type");
				if ("01".equals(fileType1)) {
					allPro.getList().get(i).set("fileType", "技术类");
				}else if ("02".equals(fileType1)) {
					allPro.getList().get(i).set("fileType", "质量类");
				}else if ("03".equals(fileType1)) {
					allPro.getList().get(i).set("fileType", "物流类");
				}else if ("04".equals(fileType1)) {
					allPro.getList().get(i).set("fileType", "其他");
				}
			}
		}
		jsp.set("count", allPro.getTotalRow());
		jsp.set("data", allPro.getList());
		jsp.set("msg", "查询文件成功");
		renderJson(jsp);
	}
	//指向编辑页面editCustomerInput.html
	public void editCustomerInput() {
		render("editCustomerInput.html");
	}
	//指向详情页面
	public void toInputDetail() {
		long inputId=getParaToLong(0);
		CustomerInput customerInput=CustomerInput.dao.findById(inputId);
		set("customerInput", customerInput);
		//获取总成零件号
		List<Record> zcPartList=Db.use("dc").find("select * from customer_input_parts where customer_input_id="+inputId);
		String zcPart="";
		for (int i = 0; i < zcPartList.size(); i++) {
			zcPart=zcPart+" "+zcPartList.get(i).getStr("zc_part_code");
		}
		set("zc_list", zcPart);
		//文件类型
		String fileType=customerInput.getFileType();
		if ("01".equals(fileType)) {
			set("file_type", "技术类");
		}else if ("02".equals(fileType)) {
			set("file_type", "质量类");
		}else if ("03".equals(fileType)) {
			set("file_type", "物流类");
		}else if ("04".equals(fileType)) {
			set("file_type", "其他");
		}
		//发布类型
		int publishType=customerInput.getPublishType();
		if (publishType==0) {
			set("publishType", "首版发布");
		}else {
			set("publishType", "升版更新");
		}
		//接受部门
		String jieshouDep=customerInput.getJieshouDep();
		if ("XM".equals(jieshouDep)) {
			set("jieshouDep", "项目");
		}else if ("SW".equals(jieshouDep)) {
			set("jieshouDep", "商务");
		}else if ("JS".equals(jieshouDep)) {
			set("jieshouDep", "技术");
		}
		//文件
		CustomerFujian fileFujian=CustomerFujian.dao.findById(customerInput.getFileFujian());
		set("fileFujian", fileFujian);
		//附件备注
		long remarkFujianId=customerInput.getRemarksFujian();
		if (remarkFujianId!=0) {
			CustomerFujian remarksFujian=CustomerFujian.dao.findById(remarkFujianId);
			set("remarksFujian", remarksFujian);
		}else {
			Record remarksFujian=new Record();
			remarksFujian.set("fujian_name", "");
			set("remarksFujian", remarksFujian);
		}
		//当前用户
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		String sqlRole1="select * from user_role where user_id='"+user.getUserid()+"' and (role_id=1 or role_id=3)";
		List<Record> roleList1=Db.use("dc").find(sqlRole1);
		if (roleList1.isEmpty()) {
			set("isPrint", 0);//非文控
		}else {
			set("isPrint", 1);//文控
		}
		set("loginUser", user.getUserid());
		//知会人
		List<CustomerInputZhihui> zhihuiList=CustomerInputZhihui.dao.find("select * from customer_input_zhihui where customer_input_id="+inputId);
		String zhihui1="";//不可查看
		String zhihui2="";
		if (!zhihuiList.isEmpty()) {
			for (int i = 0; i < zhihuiList.size(); i++) {
				int type=zhihuiList.get(i).getType();
				if (type==0) {
					zhihui1=zhihui1+zhihuiList.get(i).getZhihuiUserName()+" ";
				}else {
					zhihui2=zhihui2+zhihuiList.get(i).getZhihuiUserName()+" ";
				}
			}
		}
		set("zhihui1", zhihui1);
		set("zhihui2", zhihui2);
		render("customerInputDetail.html");
	}
	//获取领取者列表
	public void getReceiveUser() {
		long inputId=getParaToLong(0);
		List<Record> jieshouList=Db.use("dc").find("select * from customer_receiveuser where customer_input_id="+inputId);
		Record record=new Record();
		record.set("code", 0);
		record.set("msg", "获取成功");
		record.set("data", jieshouList);
		renderJson(record);
	}
	//通知领用
	public void TongzhiLingyong() {
		Date date=new Date();
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
		long jieshouId=getParaToLong(0);
		CustomerReceiveuser jieshouUser=CustomerReceiveuser.dao.findById(jieshouId);
		String userId=jieshouUser.getReceiveUserId();
		DingUtil.sendText(userId, "顾客输入文件通知：您有文件待领取，请登录文件管理系统处理！"+sdf.format(date));
		jieshouUser.setStatus(2);
		jieshouUser.update();
		Record record=new Record();
		record.set("code", 0);
		record.set("msg", "通知成功");
		renderJson(record);
	}
	/**
	 * 公共
	 */
	//附件上传
	public void fujianUpload() {
		Record rd=new Record();
		Date date=new Date();
		try {
			UploadFile file = getFile("file", "");
			String fileName=file.getFileName();
			OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
			Record fujian=new Record();
			fujian.set("fujian_name", fileName);
			fujian.set("create_userId", user.getUserid());
			fujian.set("create_userName", user.getName());
			fujian.set("create_time", date);
			Db.use("dc").save("customer_fujian", fujian);

			rd.set("msg","附件上传成功！");
			rd.set("code",0);
			rd.set("data", fujian);
		} catch (Exception e) {
			rd.set("code",1);
			rd.set("msg","附件上传失败！");
			rd.set("data", "");
		}
		renderJson(rd);
	}
	//下载附件
	public void fujianDownload() {
		Record rd=new Record();
		long fujian_id=getParaToLong(0);
		String sql_fj="select * from customer_fujian where id="+fujian_id;
		Record fujian=Db.use("dc").findFirst(sql_fj);
		try {
			//renderFile(fujian.getStr("fujian_name"));
			render(new MyFileRender(fujian.getStr("fujian_name")));
		}catch (Exception e){
			rd.set("code", 1);
			rd.set("msg", "暂无附件下载");
			rd.set("data", "");
			renderJson(rd);
		}
	}
	
}
