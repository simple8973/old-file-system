package com.simple.controller.customerInput;

import java.util.List;

import com.dingtalk.api.response.OapiUserGetResponse;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.simple.common.model.CustomerInput;
import com.simple.common.model.CustomerReceiveuser;
import com.simple.file.MyFileRender;
/**
 * 接收cusJiehsou
 * @author FL00024996
 *
 */
public class CustomerJieshouController extends Controller {
	//指向我接收的列表页面
	public void toJieshouList() {
		render("jieshouList.html");
	}
	public void getJieshouList() {
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		String customerName=get("customer_name");
		String proCode=get("pro_code");
		String fileType=get("file_type");
		StringBuilder sb=new StringBuilder();
		int page=getInt("page");
		int limit=getInt("limit");
		Record jsp=new Record();
		jsp.set("code", 0);
		sb.append(" from customer_input a,customer_receiveuser b where b.receive_user_id='"+user.getUserid()+"' and a.id=b.customer_input_id ");
		if (!"".equals(customerName) && customerName!=null) {
			sb.append(" and a.customer_name like '%"+customerName+"%'");
		}
		if (!"".equals(proCode) && proCode!=null) {
			sb.append(" and a.pro_code like'%"+proCode+"%'");
		}
		if (!"".equals(fileType) && fileType!=null) {
			sb.append(" and a.file_type ='"+fileType+"'");
		}
		int fileStatus=getParaToInt("fileStatus");
		if (fileStatus==0) {
			sb.append(" and a.file_status=0 ");
		}else if (fileStatus==1) {
			sb.append(" and a.file_status=1 ");
		}else if (fileStatus==2) {
			sb.append(" and a.file_status=2 ");
		}
		Page<Record> allPro=Db.use("dc").paginate(page, limit, "select a.*,b.status ",sb.toString()+" order by a.id desc");
		//文件类型:01-技术类；02-质量类；03-物流类；04-其他
		if (!allPro.getList().isEmpty()) {
			for (int i = 0; i < allPro.getList().size(); i++) {
				String fileType1=allPro.getList().get(i).getStr("file_type");
				if ("01".equals(fileType1)) {
					allPro.getList().get(i).set("fileType", "技术类");
				}else if ("02".equals(fileType1)) {
					allPro.getList().get(i).set("fileType", "质量类");
				}else if ("03".equals(fileType1)) {
					allPro.getList().get(i).set("fileType", "物流类");
				}else if ("04".equals(fileType1)) {
					allPro.getList().get(i).set("fileType", "其他");
				}
			}
		}
		jsp.set("count", allPro.getTotalRow());
		jsp.set("data", allPro.getList());
		jsp.set("msg", "查询文件成功");
		renderJson(jsp);
	}
	//接收文件
	public void jieshouCusFile() {
		Record rd=new Record();
		long inputId=getParaToLong(0);
		CustomerInput customerInput=CustomerInput.dao.findById(inputId);
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		String sql_fj="select * from customer_fujian where id="+customerInput.getFileFujian();
		Record fujian=Db.use("dc").findFirst(sql_fj);
		try {
			//领用表改变状态
			CustomerReceiveuser receiveuser=CustomerReceiveuser.dao.findFirst("select * from customer_receiveuser where customer_input_id="+inputId+" and receive_user_id='"+user.getUserid()+"'");
			int status=receiveuser.getStatus();
			if (status!=1) {
				receiveuser.setStatus(1);
				receiveuser.update();
			}
			//顾客输入文件状态
			List<CustomerReceiveuser> receiveuserList=CustomerReceiveuser.dao.find("select * from customer_receiveuser where customer_input_id="+inputId);
			boolean isUse=false;
			for (int i = 0; i < receiveuserList.size(); i++) {
				int jieshouStatus=receiveuserList.get(i).getStatus();
				if (jieshouStatus!=1) {//有未领取用户
					isUse=true;
					break;
				}
			}
			if (!isUse) {
				customerInput.setFileStatus(1);
				customerInput.update();
			}
			//renderFile(fujian.getStr("fujian_name"));			
			render(new MyFileRender(fujian.getStr("fujian_name")));
		}catch (Exception e){
			rd.set("code", 1);
			rd.set("msg", "暂无附件下载");
			rd.set("data", "");
			renderJson(rd);
		}
	}

}
