package com.simple.controller.customerInput;

import com.dingtalk.api.response.OapiUserGetResponse;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
/**
 * 顾客输入知会cusZhihui
 * @author FL00024996
 *
 */
public class CustomerZhihuiController extends Controller {
	//获取知会列表
	public void toZhiHuiList() {
		render("zhihuiList.html");
	}
	public void getZhiHuiList() {
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		String customerName=get("customer_name");
		String proCode=get("pro_code");
		String fileType=get("file_type");
		StringBuilder sb=new StringBuilder();
		int page=getInt("page");
		int limit=getInt("limit");
		Record jsp=new Record();
		jsp.set("code", 0);
		sb.append(" from customer_input a,customer_input_zhihui b where b.zhihui_user_id='"+user.getUserid()+"' and a.id=b.customer_input_id  ");
		if (!"".equals(customerName) && customerName!=null) {
			sb.append(" and a.customer_name like '%"+customerName+"%'");
		}
		if (!"".equals(proCode) && proCode!=null) {
			sb.append(" and a.pro_code like'%"+proCode+"%'");
		}
		if (!"".equals(fileType) && fileType!=null) {
			sb.append(" and a.file_type ='"+fileType+"'");
		}
		int fileStatus=getParaToInt("fileStatus");
		if (fileStatus==0) {
			sb.append(" and a.file_status=0 ");
		}else if (fileStatus==1) {
			sb.append(" and a.file_status=1 ");
		}else if (fileStatus==2) {
			sb.append(" and a.file_status=2 ");
		}
		Page<Record> allPro=Db.use("dc").paginate(page, limit, "select a.*,b.type as zhType  ",sb.toString()+" order by a.id desc");
		if (!allPro.getList().isEmpty()) {
			//文件类型:01-技术类；02-质量类；03-物流类；04-其他
			for (int i = 0; i < allPro.getList().size(); i++) {
				String fileType1=allPro.getList().get(i).getStr("file_type");
				if ("01".equals(fileType1)) {
					allPro.getList().get(i).set("fileType", "技术类");
				}else if ("02".equals(fileType1)) {
					allPro.getList().get(i).set("fileType", "质量类");
				}else if ("03".equals(fileType1)) {
					allPro.getList().get(i).set("fileType", "物流类");
				}else if ("04".equals(fileType1)) {
					allPro.getList().get(i).set("fileType", "其他");
				}
			}
		}
		jsp.set("count", allPro.getTotalRow());
		jsp.set("data", allPro.getList());
		jsp.set("msg", "查询文件成功");
		renderJson(jsp);
	}
}
