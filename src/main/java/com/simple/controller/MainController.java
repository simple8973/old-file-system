package com.simple.controller;

import java.util.List;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;

import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.OapiUserGetRequest;
import com.dingtalk.api.response.OapiUserGetResponse;
import com.jfinal.core.NotAction;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.redis.Redis;
import com.simple.common.controller.BaseController;
import com.simple.ding.DingController;
import com.simple.util.AccessTokenUtil;
import com.taobao.api.ApiException;

public class MainController extends BaseController{
	//钉钉pc登陆
	public void dingLogin(){
		render("dingLogin.html");
	}
	//扫码登陆
	public void loginInit() {
		if(getPara("code")!=null) {
			setAttr("code",getPara("code"));
			setAttr("icon",getPara("icon"));
		}else {
			setAttr("code",0);
			setAttr("icon",1);
		}
		render("login.html");
	}
	public void login() {
		UsernamePasswordToken token=new UsernamePasswordToken(getPara("username"),getPara("password"));
		Subject subject=SecurityUtils.getSubject();
		Record rsp=new Record();
		rsp.set("code", Integer.valueOf(0));
		try {
			subject.login(token);
			rsp.set("result", 1);
			rsp.set("msg", "登陆成功");
			rsp.set("icon", 1);
			
		}catch(IncorrectCredentialsException ice) {
			rsp.set("result", 0);
			rsp.set("msg", "密码错误");
			rsp.set("icon", 5);
			renderJson(rsp);
			return;
		}catch(UnknownAccountException uae) {
			rsp.set("result", 0);
			rsp.set("msg", "用户不存在");
			rsp.set("icon", 5);
			renderJson(rsp);
			return;
		}catch(ExcessiveAttemptsException eae) {
			rsp.set("result", 0);
			rsp.set("msg", "登录次数过多");
			rsp.set("icon", 5);
			renderJson(rsp);
			return;
		}
		Record user=Db.use("dc").findFirst("select * from users where username='"+getPara("username")+"'");
		subject.getSession().setAttribute("user", user);
		try {
			Redis.use("test").incr("loginTimes");
		}catch(Exception localException){
			renderJson(rsp);
		}
	}
	
	//退出登陆
	public void logout() {
		Subject currentUser=SecurityUtils.getSubject();
		currentUser.logout();
		setAttr("code",0);
		setAttr("icon",1);
		render("login.html");
	}
	//系统登陆成功首页
	public void index() {
		setAttr("menu",getMenu());
		set("request",getRequest());
		render("main/index.html");
	}
	public void IndexMain() {
		render("main/main.html");
	}
	
	@NotAction
	public List<Record> menuAuth(List<Record> menu){
		
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		//String sql="select menu_id from menu_permission where role_id=1";
		//String sql="select a.menu_id as menu_id from menu_permission a,file_menu b where (a.menu_id=b.id and b.type=2 AND a.type=1) or (a.menu_id=b.id and b.type=1 and  a.role_id in (select role_id from user_role where user_id='"+user.getUserid()+"') and a.type=1 ) ";
		String sql="select id as menu_id from file_menu b where (b.type=2) "
				+ "or (b.id in (select menu_id from menu_permission where role_id in (select role_id from user_role where user_id='"+user.getUserid()+"')) )";
		//String sql="select a.id as menu_id from file_menu a,menu_permission b where a.type=2 or (a.id=b.menu_id and a.type=1 and b.role_id in (select role_id from user_role where user_id='"+user.getUserid()+"'))";
		//获取用户对应角色权限
		List<Record> menuPermissions=Db.use("dc").find(sql);
		for (int i=0;i<menu.size();i++) {
			boolean flag=true;
			for(int j=0;j<menuPermissions.size();j++) {
				if(((Record)menu.get(i)).getInt("id")==((Record)menuPermissions.get(j)).getInt("menu_id")) {
					flag=false;
				}
				//判断permissions表中获取到的菜单id是否与menus表中的id一致
			}
			if(flag) {
				menu.remove(i);
				//menu.remove(i)一出列表menu中的对象i
				i--;
			}
		}
		//System.out.println(user.getUnionid());
		return menu;
	}
	public List<Record> getMenu(){
		List<Record> top_menu=Db.use("dc").find("select * from file_menu where menu_level=1 and is_hide=0");
		top_menu=menuAuth(top_menu);
		for (int i = 0; i < top_menu.size(); i++) {
			List<Record> second_menu = Db.use("dc").find("select * from file_menu where menu_level=2 and is_hide=0 and menu_Pid="+ ((Record) top_menu.get(i)).getInt("id") + " order by seq_num");
			second_menu = menuAuth(second_menu);
			for (int j = 0; j < second_menu.size(); j++) {
				List<Record> third_menu = Db.use("dc").find("select * from file_menu where menu_level=3 and is_hide=0 and menu_Pid="+ ((Record) second_menu.get(j)).getInt("id") + " order by seq_num");
				third_menu=menuAuth(third_menu);
				for (int k = 0; k < third_menu.size(); k++) {
					List<Record> fourth_menu = Db.use("dc").find("select * from file_menu where menu_level=4 and is_hide=0 and menu_Pid="+ ((Record) third_menu.get(k)).getInt("id") + " order by seq_num");
					fourth_menu=menuAuth(fourth_menu);
					if (fourth_menu.size() > 0) {
						((Record) third_menu.get(k)).set("children", fourth_menu);
					}
				}
				if (third_menu.size() > 0) {
					((Record) second_menu.get(j)).set("children", third_menu);
				}
			}
			if (second_menu.size() > 0) {
				((Record) top_menu.get(i)).set("children", second_menu);
			}
		}
		return top_menu;
	}
	
	@NotAction
	public static void main(String[] args) {
		Redis.use("test").lpush("ray", new Object[] { "1" });
	}

}
