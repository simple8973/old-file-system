package com.simple.controller.FileSendReceive;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.OapiProcessinstanceGetRequest;
import com.dingtalk.api.request.OapiUserGetRequest;
import com.dingtalk.api.response.OapiProcessinstanceGetResponse;
import com.dingtalk.api.response.OapiUserGetResponse;
import com.jfinal.core.Controller;
import com.jfinal.json.FastJson;
import com.jfinal.kit.HttpKit;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.simple.util.AccessTokenUtil;
import com.simple.ding.DingController;
import com.simple.ding.DingUtil;
import com.simple.ding.Env;
import com.taobao.api.ApiException;


public class ShenPiInterFace extends Controller{
	/**
	 * 打印审批回调
	 */
	public void spPrintBack()throws ApiException{
		
		String jsonString=HttpKit.readData(getRequest());
		Kv kv=FastJson.getJson().parse(jsonString, Kv.class);
		
		Record record=new Record();
		record.set("code", 0);
		
		if ("bpms_instance_change".equals(kv.getStr("EventType"))&& Env.DJ_BACK_PROCESSCODE.equals(kv.getStr("processCode"))) {
			
			String accessToken = AccessTokenUtil.getToken();
			DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/topapi/processinstance/get");
			OapiProcessinstanceGetRequest request = new OapiProcessinstanceGetRequest();
			request.setProcessInstanceId(kv.getStr("processInstanceId"));
			OapiProcessinstanceGetResponse response = client.execute(request,accessToken);
			
			String tech_fileId =  response.getProcessInstance().getFormComponentValues().get(1).getValue();
			String fbFile_type=response.getProcessInstance().getFormComponentValues().get(5).getValue();
			int tech_file_id=Integer.parseInt(tech_fileId);
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
			Date d= new Date();
			String date = sdf.format(d);
			if ("finish".equals(kv.getStr("type"))) {
				if (fbFile_type.equals("生产文件")) {
					//判断是否已经在pc端完成审批
					Record filePc=Db.use("dc").findById("tech_files", tech_file_id);
					int fileStatus=filePc.getInt("sp_status");
					if (fileStatus!=0) {
						//pc端已经完成审批
						record.set("msg", "文件发布审批完成！");
					} else {
						//pc端还未审批
						if("agree".equals(kv.getStr("result"))){
							Record up_file=Db.use("dc").findById("tech_files", tech_file_id);
							int release_type=up_file.getInt("release_type");
							//获取文件知会人
							String sql_zhihui="select * from zhihui_users where tech_files_id="+tech_file_id;
							List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
							String zhihuiUsers="";
							for (int i = 0; i < zhihuiList.size(); i++) {
								zhihuiUsers+=zhihuiList.get(i).getStr("zhihui_userId")+",";
							}
							//通知知会人文件已审批通过
							DingUtil.sendText(zhihuiUsers, "知会消息：生产文件“"+up_file.getStr("file_name")+"”已审批通过，请知悉！" +date);
							if (release_type==0 || release_type==2) {
								//临时文件或首次发布,
								up_file.set("sp_status", 1);//审批通过直接修改状态为1-审批通过
								Db.use("dc").update("tech_files", up_file);
							} else {//替换升版文件审批通过之后
								//新版状态修改为6-旧版未回收
								up_file.set("sp_status", 6);
								Db.use("dc").update("tech_files", up_file);
								String nb_fileCode=up_file.getStr("nb_fileCode");
								String sql_file="select * from tech_files where nb_fileCode='"+nb_fileCode+"' and sp_status=4";//使用中的旧版文件
								//旧版状态修改为5-待回收
								Record oldFile=Db.use("dc").findFirst(sql_file);
								oldFile.set("sp_status", 5);
								Db.use("dc").update("tech_files", oldFile);
								int old_fileId=oldFile.getInt("id");
								 //上一版接收人
								String sql_user="select * from tech_files_receive where tech_files_id="+old_fileId;
								List<Record> userList=Db.use("dc").find(sql_user);
								String userIds="";
								for (int i = 0; i < userList.size(); i++) {
									userIds+=userList.get(i).getStr("receive_userId")+",";
								}
								DingUtil.sendText(userIds, "文件“"+oldFile.getStr("file_name")+"”有新版本发布，当前版本需交至文控处作废，请知悉！"+date);
							}
						}else if("refuse".equals(kv.getStr("result"))) {
							//技术文件状态改变
							Record up_file=Db.use("dc").findById("tech_files", tech_file_id);
							up_file.set("sp_status", 2);//审批驳回
							Db.use("dc").update("tech_files", up_file);
							//审批驳回知会
							String sql_zhihui="select * from zhihui_users where tech_files_id="+tech_file_id;
							List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
							String zhihuiUsers="";
							for (int i = 0; i < zhihuiList.size(); i++) {
								zhihuiUsers+=zhihuiList.get(i).getStr("zhihui_userId")+",";
							}
							DingUtil.sendText(zhihuiUsers, "知会消息：生产文件“"+up_file.getStr("file_name")+"”被审批驳回，请知悉！" +date);
						}
						record.set("msg", "文件发布审批完成！");
					}
				}else if (fbFile_type.equals("客户文件")) {
					//判断是否已经在pc端完成审批
					Record filePc=Db.use("dc").findById("kehu_tech_file", tech_file_id);
					int fileStatus=filePc.getInt("sp_status");
					if (fileStatus!=0) {
						//pc端已经完成审批
						record.set("msg", "文件发布审批完成！");
					} else {
						//pc端还未审批
						if("agree".equals(kv.getStr("result"))){
							Record up_file=Db.use("dc").findById("kehu_tech_file", tech_file_id);
							int release_type=up_file.getInt("release_type");
							//获取文件知会人
							String sql_zhihui="select * from kehu_zhihui_users where kehu_tech_files_id="+tech_file_id;
							List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
							String zhihuiUsers="";
							for (int i = 0; i < zhihuiList.size(); i++) {
								zhihuiUsers+=zhihuiList.get(i).getStr("zhihui_userId")+",";
							}
							DingUtil.sendText(zhihuiUsers, "知会消息：客户文件“"+up_file.getStr("file_name")+"”已审批通过，请知悉！" +date);
							if (release_type==0 || release_type==2) {
								//临时文件或首次发布,
								up_file.set("sp_status", 1);//审批通过直接修改状态为1-审批通过
								Db.use("dc").update("kehu_tech_file", up_file);
							} else {//替换升版文件审批通过之后
								//新版状态修改为1-审批通过
								up_file.set("sp_status", 1);
								Db.use("dc").update("kehu_tech_file", up_file);
								String nb_fileCode=up_file.getStr("nb_fileCode");
								String sql_file="select * from kehu_tech_file where nb_fileCode='"+nb_fileCode+"' and sp_status=4";//使用中的旧版文件
								//旧版状态修改为5-已升版
								Record oldFile=Db.use("dc").findFirst(sql_file);
								oldFile.set("sp_status", 5);
								Db.use("dc").update("kehu_tech_file", oldFile);
							}
						}else if("refuse".equals(kv.getStr("result"))) {
							//技术文件状态改变
							Record up_file=Db.use("dc").findById("kehu_tech_file", tech_file_id);
							up_file.set("sp_status", 2);//审批驳回
							Db.use("dc").update("kehu_tech_file", up_file);
							//获取文件知会人
							String sql_zhihui="select * from kehu_zhihui_users where kehu_tech_files_id="+tech_file_id;
							List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
							String zhihuiUsers="";
							for (int i = 0; i < zhihuiList.size(); i++) {
								zhihuiUsers+=zhihuiList.get(i).getStr("zhihui_userId")+",";
							}
							DingUtil.sendText(zhihuiUsers, "知会消息：客户文件“"+up_file.getStr("file_name")+"”被审批驳回，请知悉！" +date);
						}
						record.set("msg", "文件发布审批完成！");
					}
				}else if (fbFile_type.equals("质量文件")) {
					//判断是否已经在pc端完成审批
					Record filePc=Db.use("dc").findById("zhiliang_tech_file", tech_file_id);
					int fileStatus=filePc.getInt("sp_status");
					if (fileStatus!=0) {
						//pc端已经完成审批
						record.set("msg", "文件发布审批完成！");
					} else {
						//pc端还未审批
						if("agree".equals(kv.getStr("result"))){
							Record up_file=Db.use("dc").findById("zhiliang_tech_file", tech_file_id);
							int release_type=up_file.getInt("release_type");
							//获取文件知会人
							String sql_zhihui="select * from zhiliang_zhihui_users where zhiliang_tech_files_id="+tech_file_id;
							List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
							String zhihuiUsers="";
							for (int i = 0; i < zhihuiList.size(); i++) {
								zhihuiUsers+=zhihuiList.get(i).getStr("zhihui_userId")+",";
							}
							DingUtil.sendText(zhihuiUsers, "知会消息：质量文件“"+up_file.getStr("file_name")+"”已审批通过，请知悉！" +date);
							if (release_type==0 || release_type==2) {
								//临时文件或首次发布,
								up_file.set("sp_status", 1);//审批通过直接修改状态为1-审批通过
								Db.use("dc").update("zhiliang_tech_file", up_file);
							} else {//替换升版文件审批通过之后
								//新版状态修改为1-审批通过
								up_file.set("sp_status", 1);
								Db.use("dc").update("zhiliang_tech_file", up_file);
								String nb_fileCode=up_file.getStr("nb_fileCode");
								String sql_file="select * from zhiliang_tech_file where nb_fileCode='"+nb_fileCode+"' and sp_status=4";//使用中的旧版文件
								//旧版状态修改为5-已升版
								Record oldFile=Db.use("dc").findFirst(sql_file);
								oldFile.set("sp_status", 5);
								Db.use("dc").update("zhiliang_tech_file", oldFile);
							}
						}else if("refuse".equals(kv.getStr("result"))) {
							//技术文件状态改变
							Record up_file=Db.use("dc").findById("zhiliang_tech_file", tech_file_id);
							up_file.set("sp_status", 2);//审批驳回
							Db.use("dc").update("zhiliang_tech_file", up_file);
							//获取文件知会人
							String sql_zhihui="select * from zhiliang_zhihui_users where zhiliang_tech_files_id="+tech_file_id;
							List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
							String zhihuiUsers="";
							for (int i = 0; i < zhihuiList.size(); i++) {
								zhihuiUsers+=zhihuiList.get(i).getStr("zhihui_userId")+",";
							}
							DingUtil.sendText(zhihuiUsers, "知会消息：质量文件“"+up_file.getStr("file_name")+"”被审批驳回，请知悉！" +date);
						}
						record.set("msg", "文件发布审批完成！");
					}
				}else if (fbFile_type.equals("项目文件")) {
					//判断是否已经在pc端完成审批
					Record filePc=Db.use("dc").findById("xiangmu_tech_file", tech_file_id);
					int fileStatus=filePc.getInt("sp_status");
					if (fileStatus!=0) {
						//pc端已经完成审批
						record.set("msg", "文件发布审批完成！");
					} else {
						//pc端还未审批
						if("agree".equals(kv.getStr("result"))){
							Record up_file=Db.use("dc").findById("xiangmu_tech_file", tech_file_id);
							int release_type=up_file.getInt("release_type");
							//获取文件知会人
							String sql_zhihui="select * from xiangmu_zhihui_users where xiangmu_tech_files_id="+tech_file_id;
							List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
							String zhihuiUsers="";
							for (int i = 0; i < zhihuiList.size(); i++) {
								zhihuiUsers+=zhihuiList.get(i).getStr("zhihui_userId")+",";
							}
							DingUtil.sendText(zhihuiUsers, "知会消息：项目文件“"+up_file.getStr("file_name")+"”已审批通过，请知悉！" +date);
							if (release_type==0 || release_type==2) {
								//临时文件或首次发布,
								up_file.set("sp_status", 1);//审批通过直接修改状态为1-审批通过
								Db.use("dc").update("xiangmu_tech_file", up_file);
							} else {//替换升版文件审批通过之后
								//新版状态修改为1-审批通过
								up_file.set("sp_status", 1);
								Db.use("dc").update("xiangmu_tech_file", up_file);
								String nb_fileCode=up_file.getStr("nb_fileCode");
								String sql_file="select * from xiangmu_tech_file where nb_fileCode='"+nb_fileCode+"' and sp_status=4";//使用中的旧版文件
								//旧版状态修改为5-已升版
								Record oldFile=Db.use("dc").findFirst(sql_file);
								oldFile.set("sp_status", 5);
								Db.use("dc").update("xiangmu_tech_file", oldFile);
							}
						}else if("refuse".equals(kv.getStr("result"))) {
							//技术文件状态改变
							Record up_file=Db.use("dc").findById("xiangmu_tech_file", tech_file_id);
							up_file.set("sp_status", 2);//审批驳回
							Db.use("dc").update("xiangmu_tech_file", up_file);
							//获取文件知会人
							String sql_zhihui="select * from xiangmu_zhihui_users where xiangmu_tech_files_id="+tech_file_id;
							List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
							String zhihuiUsers="";
							for (int i = 0; i < zhihuiList.size(); i++) {
								zhihuiUsers+=zhihuiList.get(i).getStr("zhihui_userId")+",";
							}
							DingUtil.sendText(zhihuiUsers, "知会消息：项目文件“"+up_file.getStr("file_name")+"”被审批驳回，请知悉！" +date);
						}
						record.set("msg", "文件发布审批完成！");
					}
				}else if (fbFile_type.equals("采购文件")) {
					//判断是否已经在pc端完成审批
					Record filePc=Db.use("dc").findById("caigou_tech_file", tech_file_id);
					int fileStatus=filePc.getInt("sp_status");
					if (fileStatus!=0) {
						//pc端已经完成审批
						record.set("msg", "文件发布审批完成！");
					} else {
						//pc端还未审批
						if("agree".equals(kv.getStr("result"))){
							Record up_file=Db.use("dc").findById("caigou_tech_file", tech_file_id);
							int release_type=up_file.getInt("release_type");
							//获取文件知会人
							String sql_zhihui="select * from caigou_zhihui_users where caigou_tech_files_id="+tech_file_id;
							List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
							String zhihuiUsers="";
							for (int i = 0; i < zhihuiList.size(); i++) {
								zhihuiUsers+=zhihuiList.get(i).getStr("zhihui_userId")+",";
							}
							DingUtil.sendText(zhihuiUsers, "知会消息：文件“"+up_file.getStr("file_name")+"”已审批通过，请知悉！" +date);
							if (release_type==0 || release_type==2) {
								//临时文件或首次发布,
								up_file.set("sp_status", 1);//审批通过直接修改状态为1-审批通过
								Db.use("dc").update("caigou_tech_file", up_file);
							} else {//替换升版文件审批通过之后
								//新版状态修改为1-审批通过
								up_file.set("sp_status", 1);
								Db.use("dc").update("caigou_tech_file", up_file);
								String nb_fileCode=up_file.getStr("nb_fileCode");
								String sql_file="select * from caigou_tech_file where nb_fileCode='"+nb_fileCode+"' and sp_status=4";//使用中的旧版文件
								//旧版状态修改为5-已升版
								Record oldFile=Db.use("dc").findFirst(sql_file);
								oldFile.set("sp_status", 5);
								Db.use("dc").update("caigou_tech_file", oldFile);
							}
						}else if("refuse".equals(kv.getStr("result"))) {
							//技术文件状态改变
							Record up_file=Db.use("dc").findById("caigou_tech_file", tech_file_id);
							up_file.set("sp_status", 2);//审批驳回
							Db.use("dc").update("caigou_tech_file", up_file);
							//获取文件知会人
							String sql_zhihui="select * from  caigou_zhihui_users where caigou_tech_files_id="+tech_file_id;
							List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
							String zhihuiUsers="";
							for (int i = 0; i < zhihuiList.size(); i++) {
								zhihuiUsers+=zhihuiList.get(i).getStr("zhihui_userId")+",";
							}
							DingUtil.sendText(zhihuiUsers, "知会消息：采购文件“"+up_file.getStr("file_name")+"”被审批驳回，请知悉！" +date);
						}
						record.set("msg", "文件发布审批完成！");
					}
				}
				
			}
		}
		renderJson(record);
	}
	/*
	 * 建号审批回调
	 */
	public void spProBuildBack()throws ApiException{
		String jsonString=HttpKit.readData(getRequest());
		Kv kv=FastJson.getJson().parse(jsonString, Kv.class);
		Record record=new Record();
		record.set("code", 0);
		
		if ("bpms_instance_change".equals(kv.getStr("EventType"))&& Env.proCreate_PROCESSCODE.equals(kv.getStr("processCode"))) {
			String accessToken = AccessTokenUtil.getToken();
			DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/topapi/processinstance/get");
			OapiProcessinstanceGetRequest request = new OapiProcessinstanceGetRequest();
			request.setProcessInstanceId(kv.getStr("processInstanceId"));
			OapiProcessinstanceGetResponse response = client.execute(request,accessToken);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
			Date d= new Date();
			String date = sdf.format(d);
			
			String proId =  response.getProcessInstance().getFormComponentValues().get(1).getValue();
			int pro_id=Integer.parseInt(proId);
			
			if ("finish".equals(kv.getStr("type"))) {
				Record proInfo=Db.use("dc").findById("pro_build", pro_id);
				int sp_status=proInfo.getInt("sp_status");
				if (sp_status!=0) {
					//pc端已经完成审批
					record.set("msg", "审批完成！");
				} else {
					//pc端还未审批
					if("agree".equals(kv.getStr("result"))){
						Record up_proInfo=Db.use("dc").findById("pro_build", pro_id);
						//获取文件知会人
						String sql_zhihui="select * from pro_zhihui_users where pro_build_id="+pro_id;
						List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
						String zhihuiUsers="";
						for (int i = 0; i < zhihuiList.size(); i++) {
							zhihuiUsers+=zhihuiList.get(i).getStr("zhihui_userId")+",";
						}
						//通知知会人文件已审批通过
						DingUtil.sendText(zhihuiUsers, "知会消息：FL项目号“"+up_proInfo.getStr("FL_pro_no")+"”建号已审批通过，请知悉！"+date);
						//通知财务审批通过，编辑成本中心-内部顾客号
						DingUtil.sendText("091765545335792299", "知会消息：FL项目号“"+up_proInfo.getStr("FL_pro_no")+"”建号已审批通过，请知悉！"+date);
						//审批通过修改状态
						up_proInfo.set("sp_status", 1);
						Db.use("dc").update("pro_build", up_proInfo);
					}else if("refuse".equals(kv.getStr("result"))) {
						Record up_proInfo=Db.use("dc").findById("pro_build", pro_id);
						//获取文件知会人
						String sql_zhihui="select * from pro_zhihui_users where pro_build_id="+pro_id;
						List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
						String zhihuiUsers="";
						for (int i = 0; i < zhihuiList.size(); i++) {
							zhihuiUsers+=zhihuiList.get(i).getStr("zhihui_userId")+",";
						}
						//通知知会人文件已审批通过
						DingUtil.sendText(zhihuiUsers, "知会消息：FL项目号“"+up_proInfo.getStr("FL_pro_no")+"”建号被审批驳回，请知悉！"+date);
						//审批通过修改状态
						up_proInfo.set("sp_status", 2);
						Db.use("dc").update("pro_build", up_proInfo);
					}
					record.set("msg", "文件发布审批完成！");
				}
			}
				
		}
		renderJson(record);
	}
}
