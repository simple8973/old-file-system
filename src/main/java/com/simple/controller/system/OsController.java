package com.simple.controller.system;

import java.util.ArrayList;
import java.util.List;

import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.OapiDepartmentListRequest;
import com.dingtalk.api.request.OapiUserGetRequest;
import com.dingtalk.api.request.OapiUserSimplelistRequest;
import com.dingtalk.api.response.OapiDepartmentListResponse;
import com.dingtalk.api.response.OapiUserGetResponse;
import com.dingtalk.api.response.OapiUserSimplelistResponse;
import com.dingtalk.api.response.OapiDepartmentListResponse.Department;
import com.jfinal.core.Controller;
import com.jfinal.core.NotAction;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.simple.common.model.Os;
import com.simple.util.AccessTokenUtil;
import com.taobao.api.ApiException;

/**
 * 组织架构
 * @author FL00024996
 *
 */
public class OsController extends Controller{
	//跳转组织架构
	public void index() {
		render("os.html");
	}
	
	
	//获取所有部门
	public void getORG() throws ApiException{
		DingTalkClient client=new DefaultDingTalkClient("https://oapi.dingtalk.com/department/list");
		OapiDepartmentListRequest request = new OapiDepartmentListRequest();
		request.setId("1");
		request.setFetchChild(true);
		request.setHttpMethod("GET");
		OapiDepartmentListResponse response = client.execute(request, AccessTokenUtil.getToken());
		List<Os> oss=new ArrayList<>();
		List<Department> departments=response.getDepartment();
		oss=recursionOs(Long.valueOf(1),departments);
		Record rsp=new Record();
		rsp.set("code", 0);
		rsp.set("data", oss);
		renderJson(rsp);
	}
	//递归整理组织架构json格式
	@NotAction
	public List<Os> recursionOs(Long parentId,List<Department> departments){
		List<Os> oss=new ArrayList<>();
		for (int i=0;i<departments.size();i++) {
			if(departments.get(i).getParentid().equals(parentId)) {
				Os os=new Os();
				os.setId(departments.get(i).getId());
				os.setLabel(departments.get(i).getName());
				os.setChildren(recursionOs(departments.get(i).getId(),departments));
				oss.add(os);
			}
		}
		return oss;
	}
	
	//获取部门id获取用户列表
	public void getUserListByDepartId() throws ApiException{
		DingTalkClient client=new DefaultDingTalkClient("https://oapi.dingtalk.com/user/simplelist");
		OapiUserSimplelistRequest request = new OapiUserSimplelistRequest();
		request.setDepartmentId(getParaToLong("departId"));
		request.setHttpMethod("GET");
		OapiUserSimplelistResponse response = client.execute(request, AccessTokenUtil.getToken());
		Record layTableRes=new Record();
		layTableRes.set("code", 0);
		layTableRes.set("msg", "");
		layTableRes.set("count", Integer.valueOf(response.getUserlist().size()));
		layTableRes.set("data", response.getUserlist());
		renderJson(layTableRes);
	}
	
	//查看员工详细信息
	public void userinfo() throws ApiException{
		DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/user/get");
		OapiUserGetRequest request = new OapiUserGetRequest();
		request.setUserid(getPara(0));
		request.setHttpMethod("GET");
		OapiUserGetResponse response = client.execute(request, AccessTokenUtil.getToken());
		setAttr("user",response);
		//已设置的角色
		Record roles=Db.use("dc").findFirst("SELECT GROUP_CONCAT(role_id) role_ids FROM user_role WHERE user_id = '" + response.getUserid()+"'");
		setAttr("roleIds", roles.get("role_ids"));
		render("userinfo.html");
	}
}
