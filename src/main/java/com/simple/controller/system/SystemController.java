package com.simple.controller.system;

import java.util.List;

import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.OapiUserGetRequest;
import com.dingtalk.api.response.OapiUserGetResponse;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.core.NotAction;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.tx.Tx;
import com.simple.ding.DingController;
import com.taobao.api.ApiException;

public class SystemController extends Controller{
	//指向角色页面
	public void toRole() {
		render("role.html");
	}
	//获取角色列表
	public void getRole() {
		String sql = "select * from roles";
		List<Record> roles = Db.use("dc").find(sql);
		Record rd = new Record();
		rd.set("code", 0);
		rd.set("msg", "获取成功");
		rd.set("data", roles);
		renderJson(rd);
	}
	//删除角色
	public void deleteUserRole() {
		int id = getParaToInt("role_id");
		Record req = new Record();
		try {
			Db.use("dc").deleteById("roles", id);
			Db.use("dc").delete("DELETE FROM menu_permission WHERE role_id = ?", id);
			Db.use("dc").delete("DELETE FROM user_role WHERE role_id = ?", id);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		req.set("code", 0);
		req.set("msg", "角色删除成功！");
		renderJson(req); 
	}
	//编辑角色
	public void roleform() {
		int roleId = getParaToInt(0).intValue();
		setAttr("role", new Record().set("id", Integer.valueOf(0)));
		if (roleId != 0) {
			Record role = Db.use("dc").findById("roles", Integer.valueOf(roleId));
			setAttr("role", role);
		}
		render("roleform.html");
	}
	//获取菜单树
	public void getMenuTree() {
		Record rsp=new Record();
		rsp.set("code", 0);
		rsp.set("msg", "获取成功");
		Record trees=new Record();
		trees.set("trees", getMenu(getParaToInt(0).intValue()));
		rsp.set("data", trees);
		renderJson(rsp);
	}
	//新增、编辑角色
	@Before({ Tx.class })
	public void roleUpdate() {
		String menuIds = get("role_menu_auth");
		int roleId = getParaToInt("role_id");
		Record role = new Record();
		if (roleId != 0) {
			role = Db.use("dc").findById("roles", roleId);
		}
		role.set("role_name", getPara("role_name"));
		role.set("role_nick_name", getPara("role_nick_name"));
		role.set("role_desc", getPara("role_desc"));
		role.set("role_type", getPara("role_type"));
		if (roleId != 0) {
			Db.use("dc").update("roles", role);
		} else {
			Db.use("dc").save("roles", role);
			roleId = role.getInt("id");
		}
		clearMenuAuth(role.getInt("id"));
		String[] menuIdArry = menuIds.split(",");
		
		Record rsp = new Record();
		if(menuIdArry[0].length()==0){
			Record permission = new Record();
			permission.set("type", 1);
			permission.set("menu_id", 0);
			permission.set("role_id", roleId);
			permission.set("role_name", getPara("role_name"));
			Db.use("dc").save("menu_permission", permission);
			
		}else {
			for (int i = 0; i < menuIdArry.length; i++) {
				Record permission = new Record();
				permission.set("type", 1);
				permission.set("menu_id", menuIdArry[i]);
				permission.set("role_id", roleId);
				permission.set("role_name", getPara("role_name"));
				Db.use("dc").save("menu_permission", permission);
			}
		}
		rsp.set("code", Integer.valueOf(0));
		rsp.set("msg", "角色及权限新增或修改成功");
		renderJson(rsp);
	}
	//删除角色现有的菜单权限
	@NotAction
	public void clearMenuAuth(int roleId) {
		String sql = "delete from menu_permission where role_id=" + roleId;
		Db.use("dc").delete(sql);
	}
	@NotAction
	public List<Record> getMenu(int roleId){
		List<Record> top_menu=Db.use("dc").find("select * from file_menu where menu_level=1 and is_hide=0");
		top_menu=menuAuth(top_menu,roleId);
		for (int i = 0; i < top_menu.size(); i++) {
			List<Record> second_menu = Db.use("dc").find("select * from file_menu where menu_level=2 and is_hide=0 and menu_Pid="+ ((Record) top_menu.get(i)).getInt("id") + " order by seq_num");
			second_menu = menuAuth(second_menu, roleId);
			for (int j = 0; j < second_menu.size(); j++) {
				List<Record> third_menu = Db.use("dc").find("select * from file_menu where menu_level=3 and is_hide=0 and menu_Pid="+ ((Record) second_menu.get(j)).getInt("id") + " order by seq_num");
				third_menu=menuAuth(third_menu, roleId);
				for (int k = 0; k < third_menu.size(); k++) {
					List<Record> fourth_menu = Db.use("dc").find("select * from file_menu where menu_level=4 and is_hide=0 and menu_Pid="+ ((Record) third_menu.get(k)).getInt("id") + " order by seq_num");
					fourth_menu=menuAuth(fourth_menu, roleId);
					if (fourth_menu.size() > 0) {
						((Record) third_menu.get(k)).set("list", fourth_menu);
					}
				}
				if (third_menu.size() > 0) {
					((Record) second_menu.get(j)).set("list", third_menu);
				}
			}
			if (second_menu.size() > 0) {
				((Record) top_menu.get(i)).set("list", second_menu);
			}
		}
		return top_menu;
	}
	@NotAction
	public List<Record> menuAuth(List<Record> menu,int roleId){
		String sql="";
		if(roleId!=0) {
			sql = "SELECT menu_id FROM menu_permission WHERE role_id = "+ roleId + " AND TYPE = 1";
		}else {
			sql = "select menu_id from menu_permission WHERE type=1";
		}
		List<Record> menuPermissions = Db.use("dc").find(sql);
		for (int i = 0; i < menu.size(); i++) {
			boolean flag = false;
			for (int j = 0; j < menuPermissions.size(); j++) {
				if (((Record) menu.get(i)).getInt("id") == ((Record) menuPermissions.get(j)).getInt("menu_id")) {
					flag = true;
				}
				// 判断permissions表中获取到的菜单id是否与menus表中的id一致
			}
			if (flag) {
				((Record) menu.get(i)).set("checked", Boolean.valueOf(true));
			} else {
				((Record) menu.get(i)).set("checked", Boolean.valueOf(false));
			}
			((Record) menu.get(i)).set("name", ((Record) menu.get(i)).get("menu_name"));
			((Record) menu.get(i)).set("value", ((Record) menu.get(i)).get("id"));
		}
		return menu;
	}
	//获取角色
	public void getSelectRole() {
		List<Record> roles = Db.use("dc").find("select id,role_nick_name name from roles");
		renderJson(roles);
	}
	//设置用户角色
	public void userUpdate() {
		String userId=getPara("userid");
		String accessToken=DingController.getAccessToken();
		DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/user/get");
		OapiUserGetRequest request = new OapiUserGetRequest();
		request.setUserid(userId);
		request.setHttpMethod("GET");
		OapiUserGetResponse  response = null;
		try {
			response = client.execute(request, accessToken);
		}catch(ApiException e) {
			e.printStackTrace();
		}
		Db.use("dc").delete("delete from user_role where user_id='"+userId+"'");
		String[] roleIdArray=getPara("rolesId").split(",");
		for(int i=0;i<roleIdArray.length;i++) {
			Record user_role=new Record();
			user_role.set("role_id", roleIdArray[i]);
			user_role.set("user_id", userId);
			user_role.set("user_name", response.getName());
			Db.use("dc").save("user_role", user_role);
		}
		Record rsp=new Record();
		rsp.set("code",0);
		rsp.set("msg", "员工权限修改成功！");
		renderJson(rsp);
	}
	
}
