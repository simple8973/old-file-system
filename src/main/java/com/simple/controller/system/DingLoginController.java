package com.simple.controller.system;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;

import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.OapiSnsGetuserinfoBycodeRequest;
import com.dingtalk.api.request.OapiUserGetRequest;
import com.dingtalk.api.request.OapiUserGetUseridByUnionidRequest;
import com.dingtalk.api.request.OapiUserGetuserinfoRequest;
import com.dingtalk.api.response.OapiSnsGetuserinfoBycodeResponse;
import com.dingtalk.api.response.OapiUserGetResponse;
import com.dingtalk.api.response.OapiUserGetUseridByUnionidResponse;
import com.dingtalk.api.response.OapiUserGetuserinfoResponse;
import com.jfinal.core.Controller;
import com.simple.controller.RedirectRender;
import com.simple.ding.DingController;
import com.simple.util.AccessTokenUtil;
import com.taobao.api.ApiException;

//钉钉PC登录处理类
public class DingLoginController extends Controller{
	
	//web端扫码登陆
	public void index() throws ApiException{
		DefaultDingTalkClient client=new DefaultDingTalkClient("https://oapi.dingtalk.com/sns/getuserinfo_bycode");
		OapiSnsGetuserinfoBycodeRequest req=new  OapiSnsGetuserinfoBycodeRequest();
		req.setTmpAuthCode(getPara("code"));
		OapiSnsGetuserinfoBycodeResponse response=client.execute(req,"dingoak196g273figqbeci","VWa-vuadwK7Fts48gQAjf-wHhediXI_vaA4LXudNpdWH5SYHfvO1N-NgdGAifT6j");
		String dingtoken=AccessTokenUtil.getToken();
		//获取UserId
		DingTalkClient client2=new DefaultDingTalkClient("https://oapi.dingtalk.com/user/getUseridByUnionid");
		OapiUserGetUseridByUnionidRequest request2=new OapiUserGetUseridByUnionidRequest();
		request2.setUnionid(response.getUserInfo().getUnionid());
		request2.setHttpMethod("GET");
		OapiUserGetUseridByUnionidResponse userInfo=client2.execute(request2,dingtoken);
		
		if(0==userInfo.getErrcode()) {
			//获取用户详情
			DingTalkClient client3=new DefaultDingTalkClient("https://oapi.dingtalk.com/user/get");
			OapiUserGetRequest request=new OapiUserGetRequest();
			request.setUserid(userInfo.getUserid());
			request.setHttpMethod("GET");
			OapiUserGetResponse response3=client3.execute(request,dingtoken);
			UsernamePasswordToken token=new UsernamePasswordToken(userInfo.getUserid(),userInfo.getUserid());
			Subject subject=SecurityUtils.getSubject();
			subject.login(token);
			subject.getSession().setAttribute("user", response3);
			//redirect("/index");
			render(new RedirectRender("/index"));
		}else {
			//redirect("/loginInit?code=1&icon=5");
			render(new RedirectRender("/loginInit?code=1&icon=5"));
		}
	}
	
	//钉钉pc端免登
	public void dingLogin() throws ApiException{
		String accessToken=DingController.getAccessToken();
		DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/user/getuserinfo");
		OapiUserGetuserinfoRequest request = new OapiUserGetuserinfoRequest();
		request.setCode(get(0));
		request.setHttpMethod("GET");
		OapiUserGetuserinfoResponse response = null;
		try {
			response = client.execute(request, accessToken);
		}catch(ApiException e) {
			e.printStackTrace();
		}
		String userId=response.getUserid();
		DingTalkClient client3 = new DefaultDingTalkClient("https://oapi.dingtalk.com/user/get");
		OapiUserGetRequest request1 = new OapiUserGetRequest();
		request1.setUserid(userId);
		request1.setHttpMethod("GET");
		OapiUserGetResponse userinfo = null;
		try {
			userinfo = client3.execute(request1, accessToken);
		} catch (ApiException e) {
			e.printStackTrace();
		}
		UsernamePasswordToken token = new UsernamePasswordToken(userId, userId);
		Subject subject = SecurityUtils.getSubject();
		subject.login(token);
		subject.getSession().setAttribute("user", userinfo);
		//redirect("/index");
		render(new RedirectRender("/index"));
	}
	
	//开发测试用
	public void test() throws ApiException{ 
		String dingtoken = AccessTokenUtil.getToken();
		//获取用户详情
		DingTalkClient client3 = new DefaultDingTalkClient("https://oapi.dingtalk.com/user/get");
		OapiUserGetRequest request = new OapiUserGetRequest();
		request.setUserid("075943491936435180");//FL00014866项目经理//wenkong04123429531145674
		request.setHttpMethod("GET");
		OapiUserGetResponse response3 = client3.execute(request, dingtoken);
		
		UsernamePasswordToken token = new UsernamePasswordToken("075943491936435180", "075943491936435180");//091765545335792299贾翠珍
		Subject subject = SecurityUtils.getSubject();
		subject.login(token);
		subject.getSession().setAttribute("user", response3);
		//redirect("/index");
		render(new RedirectRender("/index"));
	}
	

}
