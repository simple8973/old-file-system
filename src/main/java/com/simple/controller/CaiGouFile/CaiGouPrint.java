package com.simple.controller.CaiGouFile;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;
import javax.imageio.stream.ImageOutputStream;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;
import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.OapiDepartmentListParentDeptsRequest;
import com.dingtalk.api.request.OapiUserGetRequest;
import com.dingtalk.api.response.OapiDepartmentListParentDeptsResponse;
import com.dingtalk.api.response.OapiUserGetResponse;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.simple.common.model.CaigouTechFile;
import com.simple.common.model.CaigouTechFileFujian;
import com.simple.common.model.CaigouZhihuiUsers;
import com.simple.common.model.OutCheckDep;
import com.simple.common.model.OutCheckFile;
import com.simple.common.model.OutCheckFujian;
import com.simple.ding.DingController;
import com.simple.ding.DingUtil;
import com.simple.file.MyFileRender;
import com.simple.util.AccessTokenUtil;
import com.taobao.api.ApiException;

public class CaiGouPrint extends Controller{
	//指向采购文件打印页面
	public void toCaiGouPrint() {
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		String sql_userRole="select * from user_role  where user_id='"+user.getUserid()+"' and (role_id=1 or role_id=3)";
		List<Record> userRoleList=Db.use("dc").find(sql_userRole);
		if (userRoleList.size()>0) {
			set("isPrint", 1);//是打印
		}else {
			set("isPrint", 0);
		}
		render("CaiGouPrintList.html");
	}
	//获取审批通过的打印文件列表
	public void getPrintList() {
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		StringBuilder sb=new StringBuilder();
		int page=getInt("page");
		int limit=getInt("limit");
		int fileStatus=getParaToInt("fileStatus");
		String sql_userRole="select * from user_role  where user_id='"+user.getUserid()+"' and (role_id=1 or role_id=3)";
		List<Record> userRoleList=Db.use("dc").find(sql_userRole);
		if (userRoleList.size()>0) {
			sb.append(" from caigou_tech_file where ((sp_status=1 or sp_status>=3) and sp_status<20) ");
			int printUser=getParaToInt("printUser");
			if (printUser==1) {
				sb.append(" and print_user_id='314250561633681831' ");
			}
			if (printUser==2) {
				sb.append(" and print_user_id='04123429531145674' ");
			}
		}else {
			sb.append(" from caigou_tech_file where ((sp_status=1 or sp_status>=3) and sp_status<20) and print_user_id='"+user.getUserid()+"'");
		}
		
		if (!"".equals(get("file_name")) && get("file_name")!=null) {
			sb.append(" and file_name like '%"+get("file_name")+"%'");
		}
		if (!"".equals(get("zc_pro_code")) && get("zc_pro_code")!=null) {
			sb.append(" and nb_zc_Code like '%"+get("zc_pro_code")+"%'");
		}
		if (!"".equals(get("nb_file_code")) && get("nb_file_code")!=null) {
			sb.append(" and nb_fileCode like '%"+get("nb_file_code")+"%'");
		}
		if (fileStatus==1) {
			sb.append(" and sp_status=1 ");
		}else if (fileStatus==3) {
			sb.append(" and sp_status=3 ");
		}else if (fileStatus==4) {
			sb.append(" and sp_status=4 ");
		}else if (fileStatus==8) {
			sb.append(" and sp_status=8 ");
		}
		Page<Record> printList=Db.use("dc").paginate(page, limit, "select *,use_cycle-DATEDIFF(NOW(),create_time) AS sy ",sb.toString()+" order by id desc");
//		for (int i = 0; i < printList.getList().size(); i++) {
//			String dayin=UserInfo(printList.getList().get(i).getStr("print_user_id"));
//			printList.getList().get(i).set("dayin", dayin);
//		}
		Record record=new Record();
		record.set("code", 0);
		record.set("msg", "获取打印文件列表成功！");
		record.set("count", printList.getTotalRow());
		record.set("data", printList.getList());
		renderJson(record);
	}

	// 打印签章文件逻辑处理
	public void doPrint() {
		Record req = new Record();
		Db.tx(() -> {
			try {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
				Date d = new Date();
				String date = sdf.format(d);
				int caigouId = getParaToInt("id");
				CaigouTechFile caigouTechFile = CaigouTechFile.dao.findById(caigouId);
				String seal = get("sealList");
				// 获取原发布人所在事业部
				Record depInfo = getUserFirstDep(caigouTechFile.getCreateUser());
				// 文件内容转移
				if ("".equals(depInfo.get("dep_id"))) {
					req.set("code", 1);
					req.set("msg", depInfo.get("dep_id")+"事业部不存在，请联系管理员添加！");
					return false;
				}
				if (!"无需盖章".equals(seal)) {// 签章文件必须上传文件,状态变为3-待领取
					int fileId = getParaToInt("file_id");// file_id
					caigouTechFile.setFujianId(fileId).setSpStatus(3).setSeal(seal);
					caigouTechFile.update();
				} else {// 无需盖章，则只修改状态
					caigouTechFile.setSpStatus(3).setSeal(seal);
					caigouTechFile.update();
				}
				/**
				 * 将文件转移至外检文件---start
				 */
				OutCheckFile checkFile = new OutCheckFile();
				checkFile.setNbZcCode(caigouTechFile.getNbZcCode()).setNbFileCode(caigouTechFile.getNbFilecode())
						.setFileName(caigouTechFile.getFileName()).setEdition(caigouTechFile.getEdition())
						.setUserId(caigouTechFile.getCreateUser()).setUserName(caigouTechFile.getCreateUserName())
						.setCreateTime(caigouTechFile.getCreateTime()).setDepId(depInfo.getLong("dep_id"))
						.setDepName(depInfo.getStr("dep_name"))
						.setCaigouFileId(caigouId);
				checkFile.save();
				// 获取上传的pdf 将 pdf转为 png
				CaigouTechFileFujian caigouFujian = CaigouTechFileFujian.dao
						.findById(caigouTechFile.getFujianId().intValue());
				String oldfileName = caigouFujian.getFujianName();
				String pngName1 = oldfileName.substring(0, oldfileName.lastIndexOf(".")) + ".png";
				String pngName = pngName1.replace("#", "");
				String pdfPath = "D:/FilesFuJian/upload/" + oldfileName;
				String targetPath = "D:/FilesFuJian/upload/" + pngName;
				// 转为png
				FileInputStream instream = new FileInputStream(pdfPath);
				InputStream byteInputStream = null;
				PDDocument doc = PDDocument.load(instream);
				PDFRenderer renderer = new PDFRenderer(doc);
				int pageCount = doc.getNumberOfPages();
				if (pageCount > 0) {
					BufferedImage image = renderer.renderImage(0, 2.0f);
					image.flush();
					ByteArrayOutputStream bs = new ByteArrayOutputStream();
					ImageOutputStream imOut;
					imOut = ImageIO.createImageOutputStream(bs);
					ImageIO.write(image, "png", imOut);
					byteInputStream = new ByteArrayInputStream(bs.toByteArray());
					byteInputStream.close();
				}
				File uploadFile = new File(targetPath);
				FileOutputStream fops;
				fops = new FileOutputStream(uploadFile);
				fops.write(readInputStream(byteInputStream));
				fops.flush();
				fops.close();
				// 保存
				OutCheckFujian outFujian = new OutCheckFujian();
				outFujian.setFileId(checkFile.getId()).setOldFujianName(oldfileName).setNewFujianName(pngName)
						.setCreateTime(new Date()).setType(0).setCreateUserId(caigouTechFile.getCreateUser())
						.setCreateUserName(caigouTechFile.getCreateUserName());
				outFujian.save();
				/**
				 * 将文件转移至外检文件---end
				 */
				// 通知接收人领取文件
				String sql_users = "select * from caigou_tech_files_receive where caigou_tech_files_id=" + caigouId;
				List<Record> receive_user = Db.use("dc").find(sql_users);
				String usersId = "";
				for (int i = 0; i < receive_user.size(); i++) {
					usersId += receive_user.get(i).getStr("receive_userId") + ",";
				}
				DingUtil.sendText(usersId, "文件领用通知：采购文件“" + caigouTechFile.getFileName() + "”已下发，请登陆文件管理系统领取！" + date);
				// 下发之后通知知会人
				String sql_zhihui = "select * from caigou_zhihui_users where caigou_tech_files_id=" + caigouId;
				List<Record> zhihuiList = Db.use("dc").find(sql_zhihui);
				for (int i = 0; i < zhihuiList.size(); i++) {
					DingUtil.sendText(zhihuiList.get(i).getStr("zhihui_userId"),"知会通知：采购文件“ " + caigouTechFile.getFileName() + " ” 已下发等待领取，请知悉！" + date);
				}
				req.set("code", 0);
				req.set("msg", "文件已下发，等待领取！");
				return true;
			} catch (Exception e) {
				e.printStackTrace();
				req.set("code", 1);
				req.set("msg", "操作失败：" + e.getMessage());
				return false;
			}
		});
		renderJson(req);
	}
	public static byte[] readInputStream(InputStream inStream) throws Exception {
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len = 0;
        while ((len = inStream.read(buffer)) != -1) {
            outStream.write(buffer, 0, len);
        }
        inStream.close();
        return outStream.toByteArray();
    }
	//确认下发
//	public void SureXiaFa() {
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
//		Date d= new Date();
//		String date = sdf.format(d);
//		int file_id=getParaToInt(0);
//		Record file=Db.use("dc").findById("caigou_tech_file", file_id);
//		file.set("sp_status", 3);//已下发待领取
//		Db.use("dc").update("caigou_tech_file", file);
//		//通知接收人领取文件
//		String sql_users="select * from caigou_tech_files_receive where caigou_tech_files_id="+file_id;
//		List<Record> receive_user=Db.use("dc").find(sql_users);
//		String usersId="";
//		for (int i = 0; i < receive_user.size(); i++) {
//			usersId+=receive_user.get(i).getStr("receive_userId")+",";
//		}
//		DingUtil.sendText(usersId, "文件领用通知：采购文件“"+file.get("file_name")+"”已放入公共盘中，请及时领取！"+date);
//		//下发之后通知知会人
//		String sql_zhihui="select * from caigou_zhihui_users where caigou_tech_files_id="+file_id;
//		List<Record> zhihuiList=Db.use("dc").find(sql_zhihui);
//		String zhiHui="";
//		for (int i = 0; i < zhihuiList.size(); i++) {
//			zhiHui+=zhihuiList.get(i).getStr("zhihui_userId")+",";
//		}
//		DingUtil.sendText(zhiHui, "知会通知：采购文件“ "+file.getStr("file_name")+" ” 已下发至公共盘等待领取，请知悉！"+date);
//		Record rd=new Record();
//		rd.set("code", 0);
//		rd.set("msg", "文件已下发！");
//		rd.set("data","" );
//		renderJson(rd);
//	}
	//通知接收人领取文件
	public void lingqu() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
		Date d= new Date();
		String date = sdf.format(d);
		int id=getParaToInt(0);
		Record user=Db.use("dc").findById("caigou_tech_files_receive", id);
		String userid=user.getStr("receive_userId");
		DingUtil.sendText(userid,"您有文件待领取，请及时领取！"+date);
		Record rd=new Record();
		rd.set("code", 0);
		rd.set("msg", "通知成功");
		rd.set("data", "");
		renderJson(rd);
	}
	//正式文件详情
	public void zsfileDetail() {
		int id=getParaToInt(0);
		Record file=Db.use("dc").findById("caigou_tech_file", id);
		int is_change=file.getInt("is_change");
		if (is_change==0) {
			setAttr("changeName", "否");
		}else {
			setAttr("changeName", "是");
		}
		setAttr("file", file);
		String accessToken_user=DingController.getAccessToken();
		DingTalkClient client_user = new DefaultDingTalkClient("https://oapi.dingtalk.com/user/get");
		//创建人
		String create_userId=file.getStr("create_user");
		OapiUserGetRequest create_user=new OapiUserGetRequest();
		create_user.setUserid(create_userId);
		create_user.setHttpMethod("GET");
		OapiUserGetResponse  response_create=null;
		try {
			response_create=client_user.execute(create_user,accessToken_user);
		} catch (ApiException e) {
			e.printStackTrace();
		}
		setAttr("create_user", response_create.getName());
		//审批人
		String sp_userId=file.getStr("sp_user_id");
		OapiUserGetRequest request_user = new OapiUserGetRequest();
		request_user.setUserid(sp_userId);
		request_user.setHttpMethod("GET");
		OapiUserGetResponse  response_user = null;
		try {
			response_user = client_user.execute(request_user, accessToken_user);
		}catch(ApiException e) {
			e.printStackTrace();
		}
		//打印人
		String print_user_id=file.getStr("print_user_id");
		OapiUserGetRequest request_print = new OapiUserGetRequest();
		request_print.setUserid(print_user_id);
		request_print.setHttpMethod("GET");
		OapiUserGetResponse  response_print = null;
		try {
			response_print = client_user.execute(request_print, accessToken_user);
		}catch(ApiException e) {
			e.printStackTrace();
		}
		setAttr("sp_name", response_user.getName());
		setAttr("print_user", response_print.getName());
		setAttr("fujianName", fujian(file.getInt("fujian_id")));
		render("zsfileDetail.html");
	}
	//临时文件详情
	public void lsfileDetail() {
		int id=getParaToInt(0);
		Record file=Db.use("dc").findById("caigou_tech_file", id);
		int is_change=file.getInt("is_change");
		if (is_change==0) {
			setAttr("changeName", "否");
		}else {
			setAttr("changeName", "是");
		}
		setAttr("file", file);
		String accessToken_user=DingController.getAccessToken();
		DingTalkClient client_user = new DefaultDingTalkClient("https://oapi.dingtalk.com/user/get");
		//创建人
		String create_userId=file.getStr("create_user");
		OapiUserGetRequest create_user=new OapiUserGetRequest();
		create_user.setUserid(create_userId);
		create_user.setHttpMethod("GET");
		OapiUserGetResponse  response_create=null;
		try {
			response_create=client_user.execute(create_user,accessToken_user);
		} catch (ApiException e) {
			e.printStackTrace();
		}
		setAttr("create_user", response_create.getName());
		//审批人
		String sp_userId=file.getStr("sp_user_id");
		OapiUserGetRequest request_user = new OapiUserGetRequest();
		request_user.setUserid(sp_userId);
		request_user.setHttpMethod("GET");
		OapiUserGetResponse  response_user = null;
		try {
			response_user = client_user.execute(request_user, accessToken_user);
		}catch(ApiException e) {
			e.printStackTrace();
		}
		//打印人
		String print_user_id=file.getStr("print_user_id");
		OapiUserGetRequest request_print = new OapiUserGetRequest();
		request_print.setUserid(print_user_id);
		request_print.setHttpMethod("GET");
		OapiUserGetResponse  response_print = null;
		try {
			response_print = client_user.execute(request_print, accessToken_user);
		}catch(ApiException e) {
			e.printStackTrace();
		}
		setAttr("sp_name", response_user.getName());
		setAttr("print_user", response_print.getName());
		setAttr("fujianName", fujian(file.getInt("fujian_id")));
		render("lsfileDetail.html");
	}
	//附件下载
	public void fujianDownload() {
		Record rd=new Record();
		int file_id=getParaToInt(0);
		Record file=Db.use("dc").findById("caigou_tech_file", file_id);
		String sql_fj="select * from caigou_tech_file_fujian where id="+file.getInt("fujian_id");
		Record fujian=Db.use("dc").findFirst(sql_fj);
		try {
			render(new MyFileRender(fujian.getStr("fujian_name")));
		}catch (Exception e){
			rd.set("code", 1);
			rd.set("msg", "暂无附件下载");
			rd.set("data", "");
			renderJson(rd);
		}
	}
	public void pingZhengDownload() {
		Record rd=new Record();
		int file_id=getParaToInt(0);
		Record file=Db.use("dc").findById("caigou_tech_file", file_id);
		String sql_fj="select * from caigou_tech_file_fujian where id="+file.getInt("chang_fujian_id");
		Record fujian=Db.use("dc").findFirst(sql_fj);
		try {
			render(new MyFileRender(fujian.getStr("fujian_name")));
		}catch (Exception e){
			rd.set("code", 1);
			rd.set("msg", "暂无附件下载");
			rd.set("data", "");
			renderJson(rd);
		}
	}
	//编辑正式文件
	public void toEditZsTech() {
		int id=getParaToInt(0);
		Record file=Db.use("dc").findById("caigou_tech_file", id);
		setAttr("file", file);
		setAttr("zcProCodeList", getZcProCode());
		setAttr("fujianName", fujian(file.getInt("fujian_id")));
		setAttr("dayin", wkUserList());
		render("EditZsFile.html");
	}
	//编辑临时文件
	public void toEditLsTech() {
		int id=getParaToInt(0);
		Record file=Db.use("dc").findById("caigou_tech_file", id);
		setAttr("file", file);
		setAttr("zcProCodeList", getZcProCode());
		setAttr("fujianName", fujian(file.getInt("fujian_id")));
		setAttr("dayin", wkUserList());
		render("EditLsFile.html");
	}
	public void doEditZsTechFile() {
		Date date=new Date();
		Record rd=new Record();
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		int id=getParaToInt("id");
		String file_type=get("file_type");
		String zc_pro_code=get("zc_pro_code");
		String nb_file_code=get("nb_file_code");
		String file_name=get("file_name");
		String edition=get("edition");
		String purpose=get("purpose");
		String print_explain=get("print_explain");
		int give_num=getParaToInt("give_num");
		int fujianId=getParaToInt("fujian_id");
		String print_user_id=get("dayin");
		rd.set("code", 0);
		rd.set("msg", "编辑文件成功！");
		rd.set("data","" );
		//修改zhiliang_tech_file表数据
		Record editZsFile=Db.use("dc").findById("caigou_tech_file", id);
		editZsFile.set("file_type", file_type);
		editZsFile.set("nb_zc_Code", zc_pro_code);
		editZsFile.set("nb_fileCode", nb_file_code);
		editZsFile.set("file_name", file_name);
		editZsFile.set("edition", edition);
		editZsFile.set("purpose", purpose);
		editZsFile.set("print_explain", print_explain);
		editZsFile.set("give_num", give_num);
		editZsFile.set("fujian_id",fujianId); //附件id
		editZsFile.set("print_user_id", print_user_id);
		Db.use("dc").update("caigou_tech_file", editZsFile);
		//获取接收员
		String userIds=get("userId");
		if(!"0".equals(userIds)) {
			String userNames=get("userSel_rec");
			String[] userId=userIds.split("\\,");
			String[] userName=userNames.split("\\,");
			for (int i = 0; i < userId.length; i++) {
				Record receiveUser=new Record();
				receiveUser.set("caigou_tech_files_id", id);
				receiveUser.set("receive_userId", userId[i]);
				receiveUser.set("receive_userName", userName[i]);
				Db.use("dc").save("caigou_tech_files_receive", receiveUser);
			}
		}
//		//获取知会人员
//		String zhiHuiUserId=get("zhiHuiUserId");
//		if (!"0".equals(zhiHuiUserId)) {
//			String zhihuiUserSel_rec=get("zhihuiUserSel_rec");
//			String[] zhiHuiUserIds=zhiHuiUserId.split("\\,");
//			String[] zhihuiUserSel_recs=zhihuiUserSel_rec.split("\\,");
//			for (int i = 0; i < zhiHuiUserIds.length; i++) {
//				Record zhiHuiUser=new Record();
//				zhiHuiUser.set("caigou_tech_files_id", id);
//				zhiHuiUser.set("zhihui_userId", zhiHuiUserIds[i]);
//				zhiHuiUser.set("zhihui_userName", zhihuiUserSel_recs[i]);
//				Db.use("dc").save("caigou_zhihui_users", zhiHuiUser);
//			}
//		}
		//可查看附件
		String zhiHuiUserId1=get("zhiHuiUserId1");
		if (!"0".equals(zhiHuiUserId1)) {
			String zhihuiUserSel_rec1=get("zhihuiUserSel_rec1");
			String[] zhiHuiIds=zhiHuiUserId1.split(",");
			String[] zhiHuiNames=zhihuiUserSel_rec1.split(",");
			for (int i = 0; i < zhiHuiNames.length; i++) {
				CaigouZhihuiUsers caigouZhihuiUser=getModel(CaigouZhihuiUsers.class);
				caigouZhihuiUser.setCaigouTechFilesId(id)
				.setZhihuiUserid(zhiHuiIds[i]).setZhihuiUsername(zhiHuiNames[i])
				.setType(1);
				caigouZhihuiUser.save();
			}
		}
		//不可查看附件
		String zhiHuiUserId2=get("zhiHuiUserId2");
		if (!"0".equals(zhiHuiUserId2)) {
			String zhihuiUserSel_rec2=get("zhihuiUserSel_rec2");
			String[] zhiHuiIds=zhiHuiUserId2.split(",");
			String[] zhiHuiNames=zhihuiUserSel_rec2.split(",");
			for (int i = 0; i < zhiHuiNames.length; i++) {
				CaigouZhihuiUsers caigouZhihuiUser=getModel(CaigouZhihuiUsers.class);
				caigouZhihuiUser.setCaigouTechFilesId(id)
				.setZhihuiUserid(zhiHuiIds[i]).setZhihuiUsername(zhiHuiNames[i])
				.setType(0);
				caigouZhihuiUser.save();
			}
		}
		//编辑记录表中新增记录
		Record editRecord=new Record();
		editRecord.set("edit_userId", user.getUserid());
		editRecord.set("edit_userName", user.getName());
		editRecord.set("caigou_tech_file_id", id);
		editRecord.set("caigou_tech_file_code", nb_file_code);
		editRecord.set("create_time", date);
		Db.use("dc").save("caigou_edit_tech_file", editRecord);
		renderJson(rd);
	}
	public void doEditLsTechFile() {
		Date date=new Date();
		OapiUserGetResponse user=(OapiUserGetResponse) getSessionAttr("user");
		Record rd=new Record();
		//获取页面数据
		int id=getParaToInt("id");
		String file_type=get("file_type");
		String zc_pro_code=get("zc_pro_code");
		String nb_file_code=get("nb_file_code");
		String file_name=get("file_name");
		String edition=get("edition");
		String purpose=get("purpose");
		int int_use_cycle=getParaToInt("int_use_cycle");
		String print_explain=get("print_explain");
		String unit_use_cycle=get("unit_use_cycle");
		int give_num=getParaToInt("give_num");
		int fujianId=getParaToInt("fujian_id");
		String print_user_id=get("dayin");
		rd.set("code", 0);
		rd.set("msg", "临时文件发布成功！");
		rd.set("data","" );
		//修改tech_files表数据
		Record editLsFile=Db.use("dc").findById("caigou_tech_file", id);
		editLsFile.set("file_type", file_type);
		editLsFile.set("nb_zc_Code", zc_pro_code);
		editLsFile.set("nb_fileCode", nb_file_code);
		editLsFile.set("file_name", file_name);
		editLsFile.set("edition", edition);
		editLsFile.set("purpose", purpose);
		editLsFile.set("print_explain", print_explain);
		editLsFile.set("print_user_id", print_user_id);
		editLsFile.set("give_num", give_num);
		editLsFile.set("fujian_id",fujianId); //附件id
		int use_cycle=0;
		if(unit_use_cycle.equals("天")) {
			use_cycle=int_use_cycle;
		}else if (unit_use_cycle.equals("周")) {
			use_cycle=int_use_cycle*7;
		}else if (unit_use_cycle.equals("月")) {
			use_cycle=int_use_cycle*30;
		}else if (unit_use_cycle.equals("年")) {
			use_cycle=int_use_cycle*365;
		}
		editLsFile.set("use_cycle", use_cycle);
		Db.use("dc").update("caigou_tech_file", editLsFile);
		//获取接收员
		String userIds=get("userId");
		if(!"0".equals(userIds)) {
			String userNames=get("userSel_rec");
			String[] userId=userIds.split("\\,");
			String[] userName=userNames.split("\\,");
			for (int i = 0; i < userId.length; i++) {
				Record receiveUser=new Record();
				receiveUser.set("caigou_tech_files_id", id);
				receiveUser.set("receive_userId", userId[i]);
				receiveUser.set("receive_userName", userName[i]);
				Db.use("dc").save("caigou_tech_files_receive", receiveUser);
			}
		}
//		//获取知会人员
//		String zhiHuiUserId=get("zhiHuiUserId");
//		if (!"0".equals(zhiHuiUserId)) {
//			String zhihuiUserSel_rec=get("zhihuiUserSel_rec");
//			String[] zhiHuiUserIds=zhiHuiUserId.split("\\,");
//			String[] zhihuiUserSel_recs=zhihuiUserSel_rec.split("\\,");
//			for (int i = 0; i < zhiHuiUserIds.length; i++) {
//				Record zhiHuiUser=new Record();
//				zhiHuiUser.set("caigou_tech_files_id", id);
//				zhiHuiUser.set("zhihui_userId", zhiHuiUserIds[i]);
//				zhiHuiUser.set("zhihui_userName", zhihuiUserSel_recs[i]);
//				Db.use("dc").save("caigou_zhihui_users", zhiHuiUser);
//			}
//		}
		//可查看附件
		String zhiHuiUserId1=get("zhiHuiUserId1");
		if (!"0".equals(zhiHuiUserId1)) {
			String zhihuiUserSel_rec1=get("zhihuiUserSel_rec1");
			String[] zhiHuiIds=zhiHuiUserId1.split(",");
			String[] zhiHuiNames=zhihuiUserSel_rec1.split(",");
			for (int i = 0; i < zhiHuiNames.length; i++) {
				CaigouZhihuiUsers caigouZhihuiUser=getModel(CaigouZhihuiUsers.class);
				caigouZhihuiUser.setCaigouTechFilesId(id)
				.setZhihuiUserid(zhiHuiIds[i]).setZhihuiUsername(zhiHuiNames[i])
				.setType(1);
				caigouZhihuiUser.save();
			}
		}
		//不可查看附件
		String zhiHuiUserId2=get("zhiHuiUserId2");
		if (!"0".equals(zhiHuiUserId2)) {
			String zhihuiUserSel_rec2=get("zhihuiUserSel_rec2");
			String[] zhiHuiIds=zhiHuiUserId2.split(",");
			String[] zhiHuiNames=zhihuiUserSel_rec2.split(",");
			for (int i = 0; i < zhiHuiNames.length; i++) {
				CaigouZhihuiUsers caigouZhihuiUser=getModel(CaigouZhihuiUsers.class);
				caigouZhihuiUser.setCaigouTechFilesId(id)
				.setZhihuiUserid(zhiHuiIds[i]).setZhihuiUsername(zhiHuiNames[i])
				.setType(0);
				caigouZhihuiUser.save();
			}
		}
		//编辑记录表中新增记录
		Record editRecord=new Record();
		editRecord.set("edit_userId", user.getUserid());
		editRecord.set("edit_userName", user.getName());
		editRecord.set("caigou_tech_file_id", id);
		editRecord.set("caigou_tech_file_code", nb_file_code);
		editRecord.set("create_time", date);
		Db.use("dc").save("caigou_edit_tech_file", editRecord);
		renderJson(rd);
	}
	//指向打印驳回页面
	public void toPrintBohui() {
		int id=getParaToInt(0);
		Record file=Db.use("dc").findById("caigou_tech_file", id);
		set("file", file);
		set("id", id);
		render("bohuiReason.html");
	}
	//处理驳回
	public void doPrintBohui() {
		int id=getParaToInt("id");
		String bohuiReason=get("bohuiReason");
		CaigouTechFile caigouTechFile=CaigouTechFile.dao.findById(id);
		caigouTechFile.setSpStatus(2);//审批驳回
		caigouTechFile.update();
		DingUtil.sendText(caigouTechFile.getCreateUser(), "采购文件【"+caigouTechFile.getFileName()+"】被驳回，驳回理由："+bohuiReason+"。请知悉！");
		Record record=new Record();
		record.set("code", 0);
		record.set("msg", "驳回成功");
		record.set("data", "");
		renderJson(record);
	}
	/**
	 * 公用模块
	 * @return
	 */
	//获取项目总成号
	public List<Record> getZcProCode(){
		String sql_zc="select * from codeaparts";
		List<Record> zcProList=Db.use("dc").find(sql_zc);
		return zcProList;
	}
	//获取附件
	public String fujian(int fujianId) {
		Record fujian=Db.use("dc").findById("caigou_tech_file_fujian", fujianId);
		String fujianName=fujian.getStr("fujian_name");
		return fujianName;
	}
	//获取文控角色用户列表
	public List<Record> wkUserList() {
		String sql_wkUser="select * from user_role where role_id=3";
		List<Record> wk_users=Db.use("dc").find(sql_wkUser);
		return wk_users;
	}
	//指向下发文件页面
	public void PrintFile(){
		int file_id=getParaToInt(0);
		setAttr("id", file_id);
		render("printFile.html");
	}
	//获取用户详情
	public String UserInfo(String userId) {
		DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/user/get");
		OapiUserGetRequest request = new OapiUserGetRequest();
		request.setUserid(userId);
		request.setHttpMethod("GET");
		OapiUserGetResponse response = null;
		try {
			response=client.execute(request,AccessTokenUtil.getToken());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response.getName();		
	}
	/**通过userid 获取 用户所在一级部门
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年8月16日 下午2:50:24
	 */
	public Record getUserFirstDep(String userId) {
		//获取用户部门列表
		String accessToken_user=DingController.getAccessToken();
		DingTalkClient client1 = new DefaultDingTalkClient("https://oapi.dingtalk.com/department/list_parent_depts");
		OapiDepartmentListParentDeptsRequest req = new OapiDepartmentListParentDeptsRequest();
		req.setUserId(userId);
		req.setHttpMethod("GET");
		OapiDepartmentListParentDeptsResponse rsp = null;
		try {
			rsp = client1.execute(req, accessToken_user);
		} catch (ApiException e) {
			e.printStackTrace();
		}
		String depList=rsp.getDepartment();
		Pattern p = Pattern.compile("\\[\\[(.+?)\\]\\]");
		Matcher matcher = p.matcher(depList);
		Record record=new Record();
		record.set("dep_name", "").set("dep_id", "");
		if (matcher.find()) {
			String[] depInfoList=matcher.group(1).split(", ");
			OutCheckDep depInfo=null;
			if (depInfoList.length==1) {
				depInfo=OutCheckDep.dao.findFirst("select * from out_check_dep where dep_id="+Long.valueOf(depInfoList[depInfoList.length-1]));
			}else if (depInfoList.length==2) {
				depInfo=OutCheckDep.dao.findFirst("select * from out_check_dep where dep_id="+Long.valueOf(depInfoList[depInfoList.length-2]));
			}else {
				depInfo=OutCheckDep.dao.findFirst("select * from out_check_dep where dep_id="+Long.valueOf(depInfoList[depInfoList.length-3]));
			}
			if (depInfo!=null) {
				record.set("dep_name", depInfo.getDepName()).set("dep_id", depInfo.getDepId());
			}
		}
		return record;
	}
}
