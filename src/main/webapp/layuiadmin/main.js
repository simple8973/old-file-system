/*JS*/

/*VUE*/
document.write('<script src="/layuiadmin/vue/vue.js"></script>');
/*layui*/
document.write('<script src="/layuiadmin/layui/layui.js"></script>');
/*axio*/
document.write('<script src="/layuiadmin/axios/axios.js"></script>');
/*element-ui*/
document.write('<script src="/layuiadmin/element-ui/lib/index.js"></script>');
/*vxe-table*/
document.write('<script src="/layuiadmin/xe-utils/dist/xe-utils.js"></script>');
document.write('<script src="/layuiadmin/vxe-table/lib/index.js"></script>');
/*CSS*/

/*layui*/
document.write('<link rel="stylesheet" href="/layuiadmin/layui/css/layui.css" media="all">');
/*element-ui*/
document.write('<link rel="stylesheet" href="/layuiadmin/element-ui/lib/theme-chalk/index.css">');
/*vxe-table*/
document.write('<link rel="stylesheet" href="/layuiadmin/vxe-table/lib/index.css">');
