layui.use(['form','layer'],function(){
    var form = layui.form
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery;

    form.on("submit(addOs)",function(data){
        //弹出loading
        var index = top.layer.msg('数据提交中，请稍候',{icon: 16,time:false,shade:0.8});
        // 实际使用时的提交信息
        $.post("/sys/addOs",{
        	os_id : $("#os_id").val(),  //机构ID
        	//os_level : $("#os_level").val(),  //机构层级
        	parent_os : $("#parent_os").val(),  //父级机构
        	title_en : $("#title_en").val(),  //机构标识
        	name : $("#name").val(),    //机构名称
        	seq_num : $("#seq_num").val(),    //排序
        },function(res){
        
        })
        setTimeout(function(){
            //top.layer.close(index);
            layer.msg("机构添加/修改成功！");
            //layer.closeAll("iframe");
            //刷新父页面
            location.reload();
        },2000);
        return false;
    })
    
    form.on("submit(delOs)",function(data){
        //弹出loading
        var index = top.layer.msg('机构删除中，请稍候',{icon: 16,time:false,shade:0.8});
        // 实际使用时的提交信息
        $.post("/sys/delOs",{
        	os_id : $("#os_id").val(),  //机构ID
        },function(res){
        
        })
        setTimeout(function(){
            //top.layer.close(index);
            layer.msg("机构删除成功！");
            //layer.closeAll("iframe");
            //刷新父页面
            location.reload();
        },2000);
        return false;
    })
    
    form.on("submit(addKpisj)",function(data){
        //弹出loading
        var index = top.layer.msg('数据提交中，请稍候',{icon: 16,time:false,shade:0.8});
        // 实际使用时的提交信息
        $.post("/kpi/kpisjAdd",{
             target : $(".target").val(),  //指标
             month : $(".month").val(),  //类别
             monthValue : $(".monthValue").val(),  //年份
             kpiNote : $(".kpiNote").val(),    //1月
        },function(res){
        
        })
        setTimeout(function(){
            top.layer.close(index);
            top.layer.msg("录入实绩成功！");
            layer.closeAll("iframe");
            //刷新父页面
            parent.location.reload();
        },2000);
        return false;
    })

    //格式化时间
    function filterTime(val){
        if(val < 10){
            return "0" + val;
        }else{
            return val;
        }
    }
    //定时发布
    var time = new Date();
    var submitTime = time.getFullYear()+'-'+filterTime(time.getMonth()+1)+'-'+filterTime(time.getDate())+' '+filterTime(time.getHours())+':'+filterTime(time.getMinutes())+':'+filterTime(time.getSeconds());

})