layui.use(['form','layer','table','laytpl'],function(){
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        laytpl = layui.laytpl,
        table = layui.table;
    
    form.on("submit(addMenu)",function(data){
        //弹出loading
        var index = top.layer.msg('数据提交中，请稍候',{icon: 16,time:false,shade:0.8});
        // 实际使用时的提交信息
        $.post("/sys/addMenu",{
        	 id : $("#id").val(),  //菜单ID
             menu_level : $("#menu_level").val(),  //菜单等级
             parent_menu : $("#parent_menu").val(),  //父级菜单
             title_en : $("#title_en").val(),  //菜单标识
             title : $("#title").val(),  //菜单标题
             seq_num : $("#seq_num").val(),  //菜单排序
             icon : $("#icon").val(),  //菜单图标
             href : $("#href").val(),  //菜单链接
             spread : $("#spread").val(),  //是否展开
             is_hide : $("#is_hide").val(),  //是否隐藏
        },function(res){
        	setTimeout(function(){
                layer.close(index);
                if(res==1){
                	layer.msg("菜单添加/修改成功！");
                }else{
                	layer.msg("菜单添加/修改失败，请联系管理员！");
                }
                //刷新父页面
                location.reload();
            },2000);
        })
        return false;
    })

    //搜索【此功能需要后台配合，所以暂时没有动态效果演示】
    $(".search_btn").on("click",function(){
        if($(".searchVal").val() != ''){
            table.reload("newsListTable",{
                page: {
                    curr: 1 //重新从第 1 页开始
                },
                where: {
                    key: $(".searchVal").val()  //搜索的关键字
                }
            })
        }else{
            layer.msg("请输入搜索的内容");
        }
    });

    //添加菜单
    function addMenu(edit){
        var index = layui.layer.open({
            title : "添加菜单",
            type : 2,
            content : "menuAddInit",
            success : function(layero, index){
                var body = layui.layer.getChildFrame('body', index);
                if(edit){
                    body.find(".top_menu").val(edit.top_menu);  //一级菜单
                    body.find(".is_mid_menu input[value="+edit.is_mid_menu+"]").prop("checked","checked");  //是否二级菜单
                    body.find(".parent_menu").val(edit.parent_menu);  //菜单名称  //父级菜单
                    body.find(".title").val(edit.title);  //菜单名称
                    body.find(".seq_num").val(edit.seq_num);    //菜单序号
                    body.find(".icon").val(edit.icon);    //菜单图标
                    body.find(".href").val(edit.href);    //菜单链接
                    body.find(".spread input[value="+edit.is_hide+"]").prop("checked","checked");  //是否展开
                    body.find(".is_hide input[value="+edit.is_hide+"]").prop("checked","checked");  //是否隐藏
                    form.render();
                }
                setTimeout(function(){
                    layui.layer.tips('点击此处返回菜单列表', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                },500)
            }
        })
        layui.layer.full(index);
        window.sessionStorage.setItem("index",index);
        //改变窗口大小时，重置弹窗的宽高，防止超出可视区域（如F12调出debug的操作）
        $(window).on("resize",function(){
            layui.layer.full(window.sessionStorage.getItem("index"));
        })
    }
    $(".addNews_btn").click(function(){
    	addMenu();
    })

    //批量删除
    $(".delAll_btn").click(function(){
        var checkStatus = table.checkStatus('userListTable'),
            data = checkStatus.data,
            newsId = [];
        if(data.length > 0) {
            for (var i in data) {
                newsId.push(data[i].newsId);
            }
            layer.confirm('确定删除选中的用户？', {icon: 3, title: '提示信息'}, function (index) {
                // $.get("删除文章接口",{
                //     newsId : newsId  //将需要删除的newsId作为参数传入
                // },function(data){
                tableIns.reload();
                layer.close(index);
                // })
            })
        }else{
            layer.msg("请选择需要删除的用户");
        }
    })

    //列表操作
    table.on('tool(menuList)', function(obj){
        var layEvent = obj.event,
            data = obj.data;
        if(layEvent === 'edit'){ //编辑
        	//layer.msg('ID：'+ data.id + ' 的查看操作');
            addMenu(data);
        }else if(layEvent === 'usable'){ //启用禁用
            var _this = $(this),
                usableText = "是否确定禁用此用户？",
                btnText = "已禁用";
            if(_this.text()=="已禁用"){
                usableText = "是否确定启用此用户？",
                btnText = "已启用";
            }
            layer.confirm(usableText,{
                icon: 3,
                title:'系统提示',
                cancel : function(index){
                    layer.close(index);
                }
            },function(index){
                _this.text(btnText);
                layer.close(index);
            },function(index){
                layer.close(index);
            });
        }else if(layEvent === 'del'){ //删除
            layer.confirm('确定删除此用户？',{icon:3, title:'提示信息'},function(index){
                // $.get("删除文章接口",{
                //     newsId : data.newsId  //将需要删除的newsId作为参数传入
                // },function(data){
                    tableIns.reload();
                    layer.close(index);
                // })
            });
        }
    });

})
