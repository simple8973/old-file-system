layui.use(['form','layer'],function(){
    var form = layui.form
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery;

    form.on("submit(changeMenuAuth)",function(data){
        //弹出loading
        var index = top.layer.msg('数据提交中，请稍候',{icon: 16,time:false,shade:0.8});
        var quanx = "";
        $("input:checkbox[name='quanx']:checked").each(function() { // 遍历name=standard的多选框
        	quanx += $(this).val()+',';
    	});
        // 实际使用时的提交信息
        $.post("/sys/changeMenuAuth",{
            quanx : quanx,  //菜单IDS
           roleId : $(".roleId").val() //角色ID
        },function(res){
        
        })
        setTimeout(function(){
            top.layer.close(index);
            top.layer.msg("菜单权限修改成功！");
            layer.closeAll("iframe");
            //刷新父页面
            parent.location.reload();
        },2000);
        return false;
    })
    
    //格式化时间
    function filterTime(val){
        if(val < 10){
            return "0" + val;
        }else{
            return val;
        }
    }
    //定时发布
    var time = new Date();
    var submitTime = time.getFullYear()+'-'+filterTime(time.getMonth()+1)+'-'+filterTime(time.getDate())+' '+filterTime(time.getHours())+':'+filterTime(time.getMinutes())+':'+filterTime(time.getSeconds());

})